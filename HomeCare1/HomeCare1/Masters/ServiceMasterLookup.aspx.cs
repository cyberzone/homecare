﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.Masters
{
    public partial class ServiceMasterLookup : System.Web.UI.Page
    {

        dboperations dbo;
        DataSet DS;
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindServiceGrid()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID + ' ' +isnull(HSM_NAME,'')   like '%" + txtSearch.Text.Trim() + "%' ";


            if (drpCategory.SelectedIndex != 0)
            {

                Criteria += " AND  HSM_CAT_ID='" + drpCategory.SelectedValue + "'";
            }

            dbo = new dboperations();
            DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            gvServices.Visible = false;
            lblTotal.Text = Convert.ToString(DS.Tables[0].Rows.Count);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvServices.Visible = true;
                gvServices.DataSource = DS;
                gvServices.DataBind();
            }
            else
            {
                gvServices.DataBind();
            }

        }

        void BindCategory()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HSCM_STATUS='A' "; // AND  HSCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            dbo = new dboperations();
            DS = new DataSet();
            DS = dbo.ServiceCategoryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCategory.DataSource = DS;
                drpCategory.DataTextField = "HSCM_NAME";
                drpCategory.DataValueField = "HSCM_CAT_ID";
                drpCategory.DataBind();
            }
            drpCategory.Items.Insert(0, "--- All ---");
            drpCategory.Items[0].Value = "0";

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                try
                {
                    BindCategory();
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    ViewState["Value"] = Convert.ToString(Request.QueryString["Value"]);


                    txtSearch.Text = Convert.ToString(ViewState["Value"]);

                    BindServiceGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void gvServices_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvServices.PageIndex = e.NewPageIndex;
                BindServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.gvServices_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.drpCategory_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindServiceGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ServiceMaster.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}