﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.Masters
{
    public partial class PasswordChange : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("~/HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CommonBAL objCom = new CommonBAL();
                dboperations dbo = new dboperations();
                DataSet DS = new DataSet();

                string EncryPassword = "";

                string Criteria = " 1=1 ";
                string OldPwd = "";
                if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                {
                    Criteria += " AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "'";
                    DS = dbo.UserMasterGet(Criteria);
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        OldPwd = Convert.ToString(DS.Tables[0].Rows[0]["HUM_USER_PASS"]);
                    }

                }


                if (Convert.ToString(Session["PWD_PREV_NOTREPEAT"]) == "1")
                {
                    Criteria = " 1=1 ";
                    Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "'";

                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")//&& OldPwd.Length > 20
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtNewPwd.Text.Trim()));
                        Criteria += " AND (HUM_USER_PASS='" + EncryPassword + "' OR HUM_PREV_PASS1='" + EncryPassword + "' OR HUM_PREV_PASS2='" + EncryPassword + "')";
                    }
                    else
                    {
                        Criteria += " AND (HUM_USER_PASS='" + txtNewPwd.Text.Trim() + "' OR HUM_PREV_PASS1='" + txtNewPwd.Text.Trim() + "' OR HUM_PREV_PASS2='" + txtNewPwd.Text.Trim() + "')";
                    }


                    DS = new DataSet();
                    DS = dbo.UserMasterGet(Criteria);
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        lblStatus.Text = "Password should not match with previous two passwords";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }

                }

                Criteria = " 1=1 ";

                if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")///&& OldPwd.Length > 20
                {
                    EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtOldPwd.Text.Trim()));
                    Criteria += "  AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";

                }
                else
                {
                    Criteria += " AND  HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "' AND   HUM_USER_PASS= '" + txtOldPwd.Text.Trim() + "'";

                }


                DataSet ds = new DataSet();

                ds = dbo.UserMasterGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string FieldNameWithValues = "";

                    EncryPassword = "";
                    if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
                    {
                        EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtNewPwd.Text.Trim()));

                        FieldNameWithValues = " HUM_PREV_PASS1 = HUM_PREV_PASS2 ,HUM_PREV_PASS2 = HUM_USER_PASS ";
                        FieldNameWithValues += ",HUM_USER_PASS ='" + EncryPassword + "',HUM_CREATED_DATE=getdate()";
                    }
                    else
                    {
                        FieldNameWithValues = " HUM_PREV_PASS1 = HUM_PREV_PASS2 ,HUM_PREV_PASS2 = HUM_USER_PASS ";
                        FieldNameWithValues += " ,HUM_USER_PASS ='" + txtNewPwd.Text.Trim() + "',HUM_CREATED_DATE=getdate()";
                    }

                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_USER_MASTER", Criteria); lblStatus.Text = "Password Changed Successfully";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblStatus.Text = "Your Old Password Is Incorrect";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PasswordChange.btnSave_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }
    }
}