﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using HomeCare1.BAL;

/// <summary>
/// Summary description for StaffMasterDAL
/// </summary>
/// 
namespace HomeCare1.DAL
{
    public class StaffMasterDAL
    {
        dboperations objDB = new dboperations();

        public string AddStaffMaster(StaffMasterBAL objStaff)
        {
            string strReturnId;
            strReturnId = objDB.ExecuteNonQueryReturn("HMS_SP_StaffMasterAdd",
               new SqlParameter("@HSFM_BRANCH_ID", objStaff.BRANCH_ID)
            , new SqlParameter("@HSFM_STAFF_ID", objStaff.STAFF_ID)
            , new SqlParameter("@HSFM_FNAME", objStaff.FNAME)
            , new SqlParameter("@HSFM_MNAME", objStaff.MNAME)
            , new SqlParameter("@HSFM_LNAME", objStaff.LNAME)
            , new SqlParameter("@HSFM_CATEGORY", objStaff.CATEGORY)
            , new SqlParameter("@HSFM_DEPT_ID", objStaff.DEPT_ID)
            , new SqlParameter("@HSFM_SF_STATUS", objStaff.SF_STATUS)
            , new SqlParameter("@HSFM_POBOX", objStaff.POBOX)
            , new SqlParameter("@HSFM_ADDR", objStaff.ADDR)
            , new SqlParameter("@HSFM_CITY", objStaff.CITY)
            , new SqlParameter("@HSFM_COUNTRY", objStaff.COUNTRY)
            , new SqlParameter("@HSFM_STATE", objStaff.STATE)
            , new SqlParameter("@HSFM_EDU_QLFY", objStaff.EDU_QLFY)
            , new SqlParameter("@HSFM_DESIG", objStaff.DESIG)
            , new SqlParameter("@HSFM_DOB", objStaff.DOB)
            , new SqlParameter("@HSFM_AGE", objStaff.AGE)
            , new SqlParameter("@HSFM_RELIGION", objStaff.RELIGION)
            , new SqlParameter("@HSFM_SEX", objStaff.SEX)
            , new SqlParameter("@HSFM_MARITAL", objStaff.MARITAL)
            , new SqlParameter("@HSFM_NO_CHILD", objStaff.NO_CHILD)
            , new SqlParameter("@HSFM_BLOODGROUP", objStaff.BLOODGROUP)
            , new SqlParameter("@HSFM_NLTY", objStaff.NLTY)
            , new SqlParameter("@HSFM_PHONE1", objStaff.PHONE1)
            , new SqlParameter("@HSFM_PHONE2", objStaff.PHONE2)
            , new SqlParameter("@HSFM_FAX", objStaff.FAX)
            , new SqlParameter("@HSFM_MOBILE", objStaff.MOBILE)
            , new SqlParameter("@HSFM_EMAIL", objStaff.EMAIL)
            , new SqlParameter("@HSFM_PASSPORT", objStaff.PASSPORT)
            , new SqlParameter("@HSFM_PASS_ISSUE", objStaff.PASS_ISSUE)
            , new SqlParameter("@HSFM_PASS_EXP", objStaff.PASS_EXP)
            , new SqlParameter("@HSFM_SS_ID", objStaff.SS_ID)
            , new SqlParameter("@HSFM_SS_ISSUE", objStaff.SS_ISSUE)
            , new SqlParameter("@HSFM_SS_EXP", objStaff.SS_EXP)
            , new SqlParameter("@HSFM_DRV_LIC", objStaff.DRV_LIC)
            , new SqlParameter("@HSFM_DRV_LIC_ISSUE", objStaff.DRV_LIC_ISSUE)
            , new SqlParameter("@HSFM_DRV_LIC_EXP", objStaff.DRV_LIC_EXP)
            , new SqlParameter("@HSFM_MOH_NO", objStaff.MOH_NO)
            , new SqlParameter("@HSFM_MOH_ISSUE", objStaff.MOH_ISSUE)
            , new SqlParameter("@HSFM_MOH_EXP", objStaff.MOH_EXP)
            , new SqlParameter("@HSFM_OTHER_NO", objStaff.OTHER_NO)
            , new SqlParameter("@HSFM_OTHER_ISSUE", objStaff.OTHER_ISSUE)
            , new SqlParameter("@HSFM_OTHER_EXP", objStaff.OTHER_EXP)
            , new SqlParameter("@HSFM_BASIC", objStaff.BASIC)
            , new SqlParameter("@HSFM_HRA", objStaff.HRA)
            , new SqlParameter("@HSFM_DA", objStaff.DA)
            , new SqlParameter("@HSFM_OTHR_ALLOW", objStaff.OTHR_ALLOW)
            , new SqlParameter("@HSFM_ADV_SALARY", objStaff.ADV_SALARY)
            , new SqlParameter("@HSFM_LOAN", objStaff.LOAN)
            , new SqlParameter("@HSFM_VACATIONPAY", objStaff.VACATIONPAY)
            , new SqlParameter("@HSFM_HOMETRAVEL", objStaff.HOMETRAVEL)
            , new SqlParameter("@HSFM_WORKINGHRS", objStaff.WORKINGHRS)
            , new SqlParameter("@HSFM_FRI_COMPENSATION", objStaff.FRI_COMPENSATION)
            , new SqlParameter("@HSFM_ROSTER_ID", objStaff.ROSTER_ID)
            , new SqlParameter("@HSFM_DOJ", objStaff.DOJ)
            , new SqlParameter("@HSFM_DOEXIT", objStaff.DOEXIT)
            , new SqlParameter("@HSFM_NOOFPAYMENT", objStaff.NOOFPAYMENT)
            , new SqlParameter("@HSFM_LOANDEDUCT", objStaff.LOANDEDUCT)
            , new SqlParameter("@HSFM_FIXEDOVERTIME", objStaff.FIXEDOVERTIME)
            , new SqlParameter("@HSFM_OVERTIMEPHR", objStaff.OVERTIMEPHR)
            , new SqlParameter("@HSFM_ABSENTPHR", objStaff.ABSENTPHR)
            , new SqlParameter("@HSFM_COMM_ID", objStaff.COMM_ID)
            , new SqlParameter("@HSFM_PRE_DRTOKEN", objStaff.PRE_DRTOKEN)
            , new SqlParameter("@HSFM_TOKEN_START", objStaff.TOKEN_START)
            , new SqlParameter("@HSFM_TOKEN_CURRENT", objStaff.TOKEN_CURRENT)
            , new SqlParameter("@HSFM_TOKEN_END", objStaff.TOKEN_END)
            , new SqlParameter("@HSFM_APNT_TIME_INT", objStaff.APNT_TIME_INT)
            , new SqlParameter("@HSFM_NO_APNT_DATEFROM1", objStaff.NO_APNT_DATEFROM1)
            , new SqlParameter("@HSFM_NO_APNT_FROM1TIME", objStaff.NO_APNT_FROM1TIME)
            , new SqlParameter("@HSFM_NO_APNT_DATETO1", objStaff.NO_APNT_DATETO1)
            , new SqlParameter("@HSFM_NO_APNT_TO1TIME", objStaff.NO_APNT_TO1TIME)
            , new SqlParameter("@HSFM_NO_APNT_DATEFROM2", objStaff.NO_APNT_DATEFROM2)
            , new SqlParameter("@HSFM_NO_APNT_FROM2TIME", objStaff.NO_APNT_FROM2TIME)
            , new SqlParameter("@HSFM_NO_APNT_DATETO2", objStaff.NO_APNT_DATETO2)
            , new SqlParameter("@HSFM_NO_APNT_TO2TIME", objStaff.NO_APNT_TO2TIME)
            , new SqlParameter("@HSFM_NO_APNT_DATEFROM3", objStaff.NO_APNT_DATEFROM3)
            , new SqlParameter("@HSFM_NO_APNT_FROM3TIME", objStaff.NO_APNT_FROM3TIME)
            , new SqlParameter("@HSFM_NO_APNT_DATETO3", objStaff.NO_APNT_DATETO3)
            , new SqlParameter("@HSFM_NO_APNT_TO3TIME", objStaff.NO_APNT_TO3TIME)
            , new SqlParameter("@HSFM_OTHERS1", objStaff.OTHERS1)
            , new SqlParameter("@HSFM_OTHERS2", objStaff.OTHERS2)
            , new SqlParameter("@HSFM_OTHERS3", objStaff.OTHERS3)
            , new SqlParameter("@HSFM_OTHERS4", objStaff.OTHERS4)
            , new SqlParameter("@HSFM_OTHERS5", objStaff.OTHERS5)
            , new SqlParameter("@HSFM_SRV_CATEGORY", objStaff.SRV_CATEGORY)
            , new SqlParameter("@HSFM_SPECIALITY", objStaff.SPECIALITY)
            , new SqlParameter("@HSFM_TOKEN_RESERVE", objStaff.TOKEN_RESERVE)
            , new SqlParameter("@HSFM_EMR_DATE", objStaff.EMR_DATE)
            , new SqlParameter("@HSFM_EMR_CAT", objStaff.EMR_CAT)
            , new SqlParameter("@HSFM_EMR_DEP", objStaff.EMR_DEP)
            , new SqlParameter("@HSFM_ROOM", objStaff.ROOM)
            , new SqlParameter("@HSP_PHOTO", objStaff.HSP_PHOTO)
            , new SqlParameter("@HSP_Signature", objStaff.HSP_Signature)
            , new SqlParameter("@EMR_DR_DEP", objStaff.EMR_DR_DEP)
            , new SqlParameter("@IsManual", objStaff.IsManual)
            , new SqlParameter("@HIN_SCRN_ID", "STAFF")
            , new SqlParameter("@UserId", objStaff.UserId), "@ReturnStaffId");

            return strReturnId;
        }

        public DataSet GetStaffMaster(string Criteria)
        {
            DataSet DS = new DataSet();
            DS = objDB.ExecuteReader("HMS_SP_StaffMasterGet", new SqlParameter("@Criteria", Criteria));

            return DS;
        }


        public void DeleteStaffMaster(StaffMasterBAL objStaff)
        {
            objDB.ExecuteNonQuery("HMS_SP_StaffMasterDelete",
                new SqlParameter("@HSFM_BRANCH_ID", objStaff.BRANCH_ID)
              , new SqlParameter("@HSFM_STAFF_ID", objStaff.STAFF_ID)
              , new SqlParameter("@UserId", objStaff.UserId));
        }
    }

}