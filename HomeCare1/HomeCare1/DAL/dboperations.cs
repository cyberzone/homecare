﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.DAL;
using HomeCare1.BAL;

namespace HomeCare1.DAL
{
    public class dboperations
    {

        connectiondb c = new connectiondb();

        public DataSet BranchMasterGet()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_BranchMasterGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet UserMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_UserMasterGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet RollTransGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_RollTransGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet AuthenticateUser(String Branch, String User_ID, String Password)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_AuthenticateUser";
            cmd.Parameters.Add(new SqlParameter("@HUM_BRANCH_ID", SqlDbType.VarChar)).Value = Branch;
            cmd.Parameters.Add(new SqlParameter("@HUM_USER_ID", SqlDbType.VarChar)).Value = User_ID;
            cmd.Parameters.Add(new SqlParameter("@HUM_USER_PASS", SqlDbType.VarChar)).Value = Password;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet city()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_CityMaster";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public void CityMasterAdd(string BranchId, string CityName, String UserName)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_CityMasterAdd ";
            cmd.Parameters.Add(new SqlParameter("@HCIM_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(BranchId);
            cmd.Parameters.Add(new SqlParameter("@HCIM_ID", SqlDbType.VarChar)).Value = Convert.ToString(CityName);
            cmd.Parameters.Add(new SqlParameter("@HCIM_NAME", SqlDbType.VarChar)).Value = Convert.ToString(CityName);
            cmd.Parameters.Add(new SqlParameter("@HCIM_CREATED_USER", SqlDbType.VarChar)).Value = Convert.ToString(UserName);

            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public DataSet country()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_CountryMaster";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet area()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_AreaMaster";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);

            c.conclose();
            return ds;


        }


        public DataSet Nationality()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_NationalityMaster";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet DepMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_DepMasterGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet IDCaptionMasterGet()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_IDCaptionMasterGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;


        }


        public string InitialNoGet(String BranchId, String ScreenId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_InitialNoGet";
            cmd.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.VarChar)).Value = BranchId;
            cmd.Parameters.Add(new SqlParameter("@ScreenId", SqlDbType.VarChar)).Value = ScreenId;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            string fileNo = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {

                fileNo = ds.Tables[0].Rows[0]["FileNo"].ToString();
            }
            c.conclose();
            return fileNo;

        }


        public DataSet retun_patient_details(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientMasterGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet retun_doctor_detailss(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_StaffMasterGet ";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet retun_inccmp_details(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_CompanyGet ";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet SystemOptionGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_SystemOptionGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet PatientVisitGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientVisitGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet PatientVisitDelGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientVisitDelGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet Patient_Master_VisitGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_Patient_Master_VisitGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet DepartmentGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_DepartmentGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }



        public DataSet retun_inccmp_cat(string inscompid)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Select_Company_cat ";
            cmd.Parameters.Add(new SqlParameter("@HCBD_COMP_ID", SqlDbType.VarChar)).Value = inscompid;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;
        }
        public DataSet return_icd_code()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Select_ICD_Code ";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public string return_invoice_no()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "Select_Invoice_No ";
            string invoice_no = cmd.ExecuteScalar().ToString();

            c.conclose();
            return invoice_no;
        }

        public DataSet return_token_no(string staffid)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TokenDisplayrGet ";
            cmd.Parameters.Add(new SqlParameter("@HSFM_STAFF_ID", SqlDbType.VarChar)).Value = staffid;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);

            c.conclose();
            return ds;
        }

        public DataSet select_from_secondary(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientSecCompanyGet ";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet select_from_patient_balance_history(string patient_id)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientBalHistory";
            cmd.Parameters.Add(new SqlParameter("@HPB_PT_ID", SqlDbType.VarChar)).Value = patient_id;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }


        public DataSet GetPatientBalHisTotal(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientBalHisTotal";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }




        public DataSet DoctorMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_DoctorMasterGet ";
            cmd.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet PatientCompanyGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientCompanyGet ";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet CompBenefitsGet(String Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_CompBenefitsGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }


        public void CompBenefitsDelete(String Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM  HMS_COMP_BENEFITS  WHERE " + Criteria;

            cmd.ExecuteNonQuery();

        }
        public DataSet CompBenefitsDtlsGet(String Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_CompBenefitsDtlsGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }



        public DataSet ScanCardMasterGet(String Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ScanCardMasterGet ";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet ScanCardImageGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ScanCardImageGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet InsCatMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_InsCatMasterGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }


        private string vPatienId, vBranchId, vCardName, vCompId, vCompName, vCardNo, vPolicyCardNo, vExpDate, visDeafult, vStatus, vImagePathFront, vImagePathBack, vPolicyStart;

        public string PatienId
        {
            get { return vPatienId; }
            set { vPatienId = value; }
        }

        public string BranchId
        {
            get { return vBranchId; }
            set { vBranchId = value; }
        }


        public string CardName
        {
            get { return vCardName; }
            set { vCardName = value; }
        }

        public string CompId
        {
            get { return vCompId; }
            set { vCompId = value; }
        }

        public string CompName
        {
            get { return vCompName; }
            set { vCompName = value; }
        }

        public string CardNo
        {
            get { return vCardNo; }
            set { vCardNo = value; }
        }

        public string PolicyCardNo
        {
            get { return vPolicyCardNo; }
            set { vPolicyCardNo = value; }
        }

        public string ExpDate
        {
            get { return vExpDate; }
            set { vExpDate = value; }
        }

        public string isDeafult
        {
            get { return visDeafult; }
            set { visDeafult = value; }
        }

        public string Status
        {
            get { return vStatus; }
            set { vStatus = value; }
        }

        public string ImagePathFront
        {
            get { return vImagePathFront; }
            set { vImagePathFront = value; }
        }

        public string ImagePathBack
        {
            get { return vImagePathBack; }
            set { vImagePathBack = value; }
        }

        public string PolicyStart
        {
            get { return vPolicyStart; }
            set { vPolicyStart = value; }
        }

        public string HSC_PLAN_TYPE { get; set; }



        public void ScanCardImageAdd(dboperations objOp)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ScanCardImageAdd ";
            cmd.Parameters.Add(new SqlParameter("@HSC_PT_ID", SqlDbType.VarChar)).Value = Convert.ToString(PatienId);
            cmd.Parameters.Add(new SqlParameter("@HSC_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(BranchId);
            cmd.Parameters.Add(new SqlParameter("@HSC_CARD_NAME", SqlDbType.VarChar)).Value = Convert.ToString(CardName);
            cmd.Parameters.Add(new SqlParameter("@HSC_INS_COMP_ID", SqlDbType.VarChar)).Value = Convert.ToString(CompId);
            cmd.Parameters.Add(new SqlParameter("@HSC_INS_COMP_NAME", SqlDbType.VarChar)).Value = Convert.ToString(CompName);
            cmd.Parameters.Add(new SqlParameter("@HSC_CARD_NO", SqlDbType.VarChar)).Value = Convert.ToString(CardNo);
            cmd.Parameters.Add(new SqlParameter("@HSC_POLICY_NO", SqlDbType.VarChar)).Value = Convert.ToString(PolicyCardNo);
            cmd.Parameters.Add(new SqlParameter("@HSC_EXPIRY_DATE", SqlDbType.VarChar)).Value = Convert.ToString(ExpDate);
            cmd.Parameters.Add(new SqlParameter("@HSC_POLICY_START", SqlDbType.VarChar)).Value = Convert.ToString(PolicyStart);
            cmd.Parameters.Add(new SqlParameter("@HSC_ISDEFAULT", SqlDbType.Bit)).Value = Convert.ToBoolean(isDeafult);
            cmd.Parameters.Add(new SqlParameter("@HSC_STATUS", SqlDbType.Bit)).Value = Convert.ToBoolean(Status);
            cmd.Parameters.Add(new SqlParameter("@HSC_IMAGE_PATH_FRONT", SqlDbType.VarChar)).Value = Convert.ToString(ImagePathFront);
            cmd.Parameters.Add(new SqlParameter("@HSC_IMAGE_PATH_BACK", SqlDbType.VarChar)).Value = Convert.ToString(ImagePathBack);
            cmd.Parameters.Add(new SqlParameter("@HSC_PLAN_TYPE", SqlDbType.VarChar)).Value = HSC_PLAN_TYPE;

            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public DataSet ScanCardImageDelete(string PatienId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ScanCardImageDelete";
            cmd.Parameters.Add(new SqlParameter("@HSC_PT_ID", SqlDbType.VarChar)).Value = PatienId;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet PatientSecCompanyDelete(string PatienId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientSecCompanyDelete";
            cmd.Parameters.Add(new SqlParameter("@HPM_PT_ID", SqlDbType.VarChar)).Value = PatienId;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public Boolean CheckEFilling(string boScanSave, string PatientID)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            if (boScanSave == "1")
            {
                cmd.CommandText = "Select * from EMR_SCAN_MASTER where ESM_PT_ID='" + PatientID + "'";
            }
            else
            {
                cmd.CommandText = "Select * from EMR_SCAN_FILE where ESF_PT_ID='" + PatientID + "'";
            }

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            Boolean isEFilling = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                isEFilling = true;
            }

            return isEFilling;

        }



        public DataSet TempImageGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_TempImageGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }

        public void TempImageAdd(String PatienId, String BranchId, Byte[] CardImage)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TempImageAdd ";
            cmd.Parameters.Add(new SqlParameter("@HTI_PT_ID", SqlDbType.VarChar)).Value = Convert.ToString(PatienId);
            cmd.Parameters.Add(new SqlParameter("@HTI_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(BranchId);
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE1", SqlDbType.Image)).Value = DBNull.Value;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE2", SqlDbType.Image)).Value = DBNull.Value;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE3", SqlDbType.Image)).Value = DBNull.Value;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE4", SqlDbType.Image)).Value = DBNull.Value;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE_TYPE", SqlDbType.VarChar)).Value = DBNull.Value;


            cmd.ExecuteNonQuery();
            c.conclose();
        }


        public void TempImageAdd1(String PatienId, String BranchId, Byte[] CardImage1, Byte[] CardImage2, Byte[] CardImage3, Byte[] CardImage4, String ImageType)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TempImageAdd ";
            cmd.Parameters.Add(new SqlParameter("@HTI_PT_ID", SqlDbType.VarChar)).Value = Convert.ToString(PatienId);
            cmd.Parameters.Add(new SqlParameter("@HTI_BRANCH_ID", SqlDbType.VarChar)).Value = Convert.ToString(BranchId);
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE1", SqlDbType.Image)).Value = CardImage1;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE2", SqlDbType.Image)).Value = CardImage2;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE3", SqlDbType.Image)).Value = CardImage3;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE4", SqlDbType.Image)).Value = CardImage4;
            cmd.Parameters.Add(new SqlParameter("@HTI_IMAGE_TYPE", SqlDbType.VarChar)).Value = ImageType;



            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public void TempImageDelete(String PatienId)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TempImageDelte";
            cmd.Parameters.Add(new SqlParameter("@HTI_PT_ID", SqlDbType.VarChar)).Value = Convert.ToString(PatienId);
            cmd.ExecuteNonQuery();
            c.conclose();
        }


        public DataSet PatientPhotoGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientPhotoGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet ScanCardImageModify(string PatientId, string BranchId, string ImageFront, string ImageBack)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ScanCardImageModify";
            cmd.Parameters.Add(new SqlParameter("@HSC_PT_ID", SqlDbType.VarChar)).Value = PatientId;
            cmd.Parameters.Add(new SqlParameter("@HSC_BRANCH_ID", SqlDbType.VarChar)).Value = BranchId;
            cmd.Parameters.Add(new SqlParameter("@HSC_IMAGE_PATH_FRONT", SqlDbType.VarChar)).Value = ImageFront;
            cmd.Parameters.Add(new SqlParameter("@HSC_IMAGE_PATH_BACK", SqlDbType.VarChar)).Value = ImageBack;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }


        public DataSet GetMaxPatientVisit(string PatientId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select Max(cast(HPV_SEQNO as int)) as HPVSEQNO From HMS_PATIENT_VISIT where HPV_PT_ID='" + PatientId + "'";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet GetEClaimView(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EclaimViewGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet GetEClaimSubView(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EclaimSubViewGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }



        public DataSet GetEClaimViewGroupBy(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EclaimViewGropByGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet GetEClaimSubViewGroupBy(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EclaimSubViewGropByGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }


        public DataSet ResubmissionViewGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ResubmissionViewGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet GetResubmissionViewGroupBy(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ResubmissionViewGropByGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }



        public DataSet ScreenCustomizationGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ScreenCustomizationGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }


        public DataSet PatientInfoGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientInfoGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }



        public void PatientPhotoDelete(String BranchId, String PatienId)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientPhotoDelete";
            cmd.Parameters.Add(new SqlParameter("@HPP_BRANCH_ID", SqlDbType.VarChar)).Value = BranchId;
            cmd.Parameters.Add(new SqlParameter("@HPP_PT_ID", SqlDbType.VarChar)).Value = PatienId;

            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public void PatientPhotoAdd(String PatienId, String BranchId, Byte[] CardImage, Byte[] CardImage1)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PatientPhotoAdd";
            cmd.Parameters.Add(new SqlParameter("@HPP_BRANCH_ID", SqlDbType.VarChar)).Value = BranchId;
            cmd.Parameters.Add(new SqlParameter("@HPP_PT_ID", SqlDbType.VarChar)).Value = PatienId;
            cmd.Parameters.Add(new SqlParameter("@HPP_INS_CARD", SqlDbType.Image)).Value = CardImage;
            cmd.Parameters.Add(new SqlParameter("@HPP_INS_CARD1", SqlDbType.Image)).Value = CardImage1;

            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public void ScanCardImageGenerate(String BranchId, String PatienId)
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ScanCardImageGenerate";
            cmd.Parameters.Add(new SqlParameter("@HPM_BRANCH_ID", SqlDbType.VarChar)).Value = BranchId;
            cmd.Parameters.Add(new SqlParameter("@HPM_PT_ID", SqlDbType.VarChar)).Value = PatienId;


            cmd.ExecuteNonQuery();
            c.conclose();
        }



        public void ScanCardImageUpdateOldCard(String BranchId, String PatienId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE HMS_SCANCARD_IMAGES SET HSC_ISDEFAULT=0  WHERE  HSC_CARD_NAME = 'Insurance' AND HSC_ISDEFAULT='True' AND HSC_BRANCH_ID='" + BranchId + "' AND  HSC_PT_ID='" + PatienId + "'";
            cmd.ExecuteNonQuery();
            c.conclose();


        }



        public DataSet GeneralMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_GeneralMasterGet ";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;
        }


        public void PatientVisitUpdate(string SexNo, string Status)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE HMS_PATIENT_VISIT SET HPV_STATUS='" + Status + "' WHERE HPV_SEQNO=" + SexNo;

            cmd.ExecuteNonQuery();


        }


        public DataSet StaffPhotoGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_StaffPhotoGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet PriorityGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PriorityGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet KnowFromGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_KnowFromGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet TempRemittanceGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TempRemittanceGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet TempRemittanceGroupByGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TempRemittanceGroupByGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }


        public DataSet TempRemittanceDelete(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TempRemittanceDelete";
            cmd.Parameters.Add(new SqlParameter("@XMLFILENAME", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }



        public DataSet CompanytBalHistory(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_CompanytBalHistory";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public string CompanyBalanceGet(string CompanyId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = "Select sum(HCB_BALANCE_AMT ) AS TotalBalanceAmount  from HMS_COMPANY_BALANCE_HISTORY where HCB_COMP_ID='" + CompanyId + "'";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            string strBalance = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0].IsNull("TotalBalanceAmount") == false && ds.Tables[0].Rows[0]["TotalBalanceAmount"].ToString() != "")
                {
                    strBalance = ds.Tables[0].Rows[0]["TotalBalanceAmount"].ToString();
                }
            }

            return strBalance;

        }

        public DataSet RemittancexmlDataAdd(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandText = "HMS_SP_RemittancexmlDataAdd";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }


        public DataSet RemittanceXMLDataGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandText = "HMS_SP_RemittanceXMLDataGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }




        public DataSet GetLastCunsultDtls(string PatientId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select TOP 1 convert(varchar,HIM_DATE,103)HIM_DATE,HIT_AMOUNT  from HMS_INVOICE_MASTER HIM inner join HMS_INVOICE_TRANSACTION HIT " +
                              "  on HIM.HIM_INVOICE_ID =HIT.HIT_INVOICE_ID  where HIT_SERV_TYPE ='C' AND HIM_PT_ID='" + PatientId + "'  ORDER BY HIM_DATE DESC";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }
        public void TransferInvoicetoTmpEclaim(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandText = "HMS_SP_TransferInvoicetoTmpEclaim";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public void ManageEMRData(string BranchId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandText = "HMS_SP_ManageEMRData";
            cmd.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.VarChar)).Value = BranchId;
            cmd.ExecuteNonQuery();
            c.conclose();

        }

        public DataSet InvoiceMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandText = "HMS_SP_InvoiceMasterGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet InvoiceTransGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandText = "HMS_SP_InvoiceTransGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }


        public DataSet ResubmissionGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ResubmissionGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet ResubmissionTransGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_ResubmissionTransGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public void AudittrailAdd(string BranchId, string ScreenId, string Type, string Remarks, string Createduser)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_AudittrailAdd";
            cmd.Parameters.Add(new SqlParameter("@HAT_BRANCH_ID", SqlDbType.VarChar)).Value = BranchId;
            cmd.Parameters.Add(new SqlParameter("@HAT_SCRN_ID", SqlDbType.VarChar)).Value = ScreenId;
            cmd.Parameters.Add(new SqlParameter("@HAT_TYPE", SqlDbType.VarChar)).Value = Type;
            cmd.Parameters.Add(new SqlParameter("@HAT_REMARK", SqlDbType.VarChar)).Value = Remarks;
            cmd.Parameters.Add(new SqlParameter("@HAT_CREATED_USER", SqlDbType.VarChar)).Value = Createduser;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            c.conclose();


        }


        public bool CheckPaymentReference(string PaymentReference)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet DSRem = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select *  from HMS_REMITTANCE_XML_DATA WHERE PaymentReference='" + PaymentReference + "'";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(DSRem);
            c.conclose();
            if (DSRem.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;

        }


        public DataSet TempEmiratesIdGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_TempEmiratesIdGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }


        public DataSet InvoiceClaimDtlsGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_InvoiceClaimDtlsGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }

        public DataSet ResubmissionClaimDtlsGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_ResubmissionClaimDtlsGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }

        public DataSet DrICDMasterGet(string Criteria, string strTop)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            if (strTop != "")
            {
                cmd.Parameters.Add(new SqlParameter("@Top", SqlDbType.VarChar)).Value = strTop;
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Top", SqlDbType.VarChar)).Value = DBNull.Value;
            }

            cmd.CommandText = "HMS_SP_DrICDMasterGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }

        public DataSet ServiceMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_ServiceMasterGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }

        public DataSet HMS_SP_ServiceMasterTopGet(string Criteria, string strTop)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;

            if (strTop != "")
            {
                cmd.Parameters.Add(new SqlParameter("@Top", SqlDbType.Int)).Value = strTop;
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Top", SqlDbType.Int)).Value = DBNull.Value;
            }

            cmd.CommandText = "HMS_SP_ServiceMasterTopGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }




        public DataSet ReceiptTranGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_ReceiptTranGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }



        public DataSet RosterMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_RosterMasterGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }


        public DataSet AppointmentOutlookGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_AppointmentOutlookGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;

        }

        public DataSet AppointmentStatusGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM HMS_APPOINTMENTSTATUS where " + Criteria + "  ORDER BY HAS_ORDERNUMBER ASC";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet AppointmentServicesGet()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM HMS_APPOINTMENTSERVICES ORDER BY HAS_ORDERNUMBER ASC";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public void AppointmentOutlookUpdate(string AppId, string Status)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE HMS_APPOINTMENT_OUTLOOKSTYLE SET HAM_STATUS='" + Status + "' WHERE HAM_APPOINTMENTID=" + AppId;

            cmd.ExecuteNonQuery();


        }

        public void AppointmentOutlookDelete(string AppId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM  HMS_APPOINTMENT_OUTLOOKSTYLE  WHERE HAM_APPOINTMENTID=" + AppId;

            cmd.ExecuteNonQuery();


        }

        //DATA GET FROM HMS_SERVICE_CATEGORY_MASTER TABLE.
        public DataSet ServiceCategoryGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_ServiceCategoryGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;


        }

        //DATA GET FROM HMS_AGRMT_SERVICE TABLE.
        public DataSet AgrementServiceGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_AgrementServiceGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;
        }

        //DATA GET FROM HMS_AGRMT_CATEGORY TABLE.
        public DataSet AgrementCategoryGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.CommandText = "HMS_SP_AgrementCategoryGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;
        }

        public void AgrementCategoryDelete(string CompanyId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " DELETE FROM HMS_AGRMT_CATEGORY Where HAC_COMP_ID='" + CompanyId + "'";
            cmd.ExecuteNonQuery();
            c.conclose();
        }


        public void UpdatePerDayLimit(Decimal PerdayLimit, string CategoryType, string CompanyId)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " UPDATE HMS_COMP_BENEFITS SET  HCB_PERDAYLIMIT = " + PerdayLimit + " From HMS_COMP_BENEFITS WHERE     HCB_CAT_TYPE = '" + CategoryType + "' AND HCB_COMP_ID = '" + CompanyId + "'";
            cmd.ExecuteNonQuery();
            c.conclose();
        }


        public void UpdateCoveringType(string CompanyId)
        {
            string strQuery = "";
            strQuery = "  UPDATE    HMS_AGRMT_SERVICE  SET    HAS_COVERING_TYPE = 'U' FROM   HMS_SERVICE_MASTER INNER JOIN HMS_AGRMT_SERVICE ON HMS_SERVICE_MASTER.HSM_SERV_ID = HMS_AGRMT_SERVICE.HAS_SERV_ID " +
                        " AND HMS_SERVICE_MASTER.HSM_BRANCH_ID = HMS_AGRMT_SERVICE.HAS_BRANCH_ID   WHERE     ISNULL(HMS_SERVICE_MASTER.HSM_PHASECODE, '') <> '' AND HMS_AGRMT_SERVICE.HAS_COMP_ID = '" + CompanyId + "'";
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public void UpdateCompAgrmType(string CompanyId, string BranchId, string AgreementType)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " UPDATE HMS_COMPANY_MASTER SET HCM_AGREEMENT_TYPE='" + AgreementType + "' WHERE HCM_COMP_ID='" + CompanyId + "' AND hcm_branch_id='" + BranchId + "'";
            cmd.ExecuteNonQuery();
            c.conclose();
        }

        public DataSet HaadServiceGet(string Criteria, string CategoryType, string strTop)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            if (CategoryType != "")
            {
                cmd.Parameters.Add(new SqlParameter("@CategoryType", SqlDbType.VarChar)).Value = CategoryType;
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@CategoryType", SqlDbType.VarChar)).Value = DBNull.Value;
            }
            if (strTop != "")
            {
                cmd.Parameters.Add(new SqlParameter("@Top", SqlDbType.VarChar)).Value = strTop;
            }
            else
            {
                cmd.Parameters.Add(new SqlParameter("@Top", SqlDbType.VarChar)).Value = DBNull.Value;
            }
            cmd.CommandText = "HMS_SP_HaadServiceGet";
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();

            return ds;
        }

        public DataSet TreatmentTypeGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select  * FROM   HMS_INS_TRTMT_TYPE WHERE  " + Criteria + " ORDER BY HITT_ID ASC";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet ServiceGroupGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from HMS_SERVICE_GROUP_MASTER WHERE " + Criteria + " ORDER BY HSGM_ID ASC";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public Int32 MaxServiceIdGet(string Criteria)
        {
            //THIS FUNCTION USED FOR GET THE NEXT SERVICE ID IT WILL TAKE ONLY NUMBERS
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT ISNULL(MAX(CAST(HSM_SERV_ID AS DECIMAL(9,3))),0) AS 'HSM_SERV_ID' FROM HMS_SERVICE_MASTER WHERE ISNUMERIC(HSM_SERV_ID)=1 AND " + Criteria;

            Int32 HSM_SERV_ID = Convert.ToInt32(cmd.ExecuteScalar());
            c.conclose();
            return HSM_SERV_ID;
        }

        public DataSet ServiceTypeGet()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select  * FROM   HMS_VIEW_ServiceTypeGet ";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }



        public DataSet CodeTypeGet()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select  * FROM   HMS_VIEW_CodeTypeGet ";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet InsCatMastereGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select  * FROM   HMS_INS_CAT_MASTER WHERE  " + Criteria + " ORDER BY HICM_INS_CAT_ID ASC";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public DataSet CompanyStatementMasterGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select  * FROM   HMS_COMP_STATMENT_MASTER WHERE  " + Criteria + " ORDER BY HCSM_DESCRIPTION ASC";

            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;
        }

        public void fnUpdatetoDb(String strTable, String strFldandval, string strUpdateCondition)
        {
            string strsql = "UPDATE " + strTable + " SET " + strFldandval + " WHERE " + strUpdateCondition;
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strsql;
            cmd.ExecuteNonQuery();
            c.conclose();


        }

        public string ExecuteNonQuery(params object[] obj)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandText = Convert.ToString(obj[0]);
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 1; i < obj.Length; i++)
            {
                cmd.Parameters.Add(obj[i]);
            }

            int k = cmd.Parameters.Count;
            for (int i = 0; i < k; i++)
            {
                if (cmd.Parameters[i].Value == "")
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }

            //SqlParameter returnValue = new SqlParameter("@ReturnId", SqlDbType.VarChar);
            //returnValue.Direction = ParameterDirection.Output;
            //cmd.Parameters.Add(returnValue);
            cmd.ExecuteNonQuery();
            c.conclose();

            return ""; //Convert.ToString(returnValue.Value);


        }

        public DataSet ExecuteReader(params object[] obj)
        {
            DataSet DS = new DataSet();
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandText = Convert.ToString(obj[0]);
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 1; i < obj.Length; i++)
            {
                cmd.Parameters.Add(obj[i]);
            }
            cmd.ExecuteNonQuery();
            c.conclose();

            SqlDataAdapter ADPT = new SqlDataAdapter(cmd);
            ADPT.Fill(DS);

            return DS;


        }

        public DataSet ExecuteQuery(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet DS = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SQLQuery;
            cmd.ExecuteNonQuery();
            c.conclose();

            adpt.SelectCommand = cmd;
            adpt.Fill(DS);

            return DS;


        }

        public string ExecuteNonQueryReturn(params object[] obj)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandText = Convert.ToString(obj[0]);
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 1; i < obj.Length - 1; i++)
            {
                cmd.Parameters.Add(obj[i]);
            }


            int k = cmd.Parameters.Count;
            for (int i = 0; i < k; i++)
            {
                if (cmd.Parameters[i].Value == "")
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            //cmd.Parameters.Add(new SqlParameter("@EMR_DR_DEP", SqlDbType.Image)).Value = DBNull.Value;


            SqlParameter returnValue = new SqlParameter(Convert.ToString(obj[obj.Length - 1]), SqlDbType.VarChar, 15);
            returnValue.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnValue);

            cmd.ExecuteNonQuery();

            string strReturnId;
            strReturnId = Convert.ToString(returnValue.Value);

            c.conclose();

            return strReturnId;// Convert.ToString(returnValue.Value);


        }

    }
}