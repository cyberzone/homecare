﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.BAL;

/// <summary>
/// Summary description for PTEncounterDAL
/// </summary>
/// 
namespace HomeCare1.DAL
{
    public class PTEncounterDAL
    {

        dboperations objDB = new dboperations();

        public string AddPTEncounter(PTEncounterBAL obj)
        {
            string strReturnId;
            strReturnId = objDB.ExecuteNonQueryReturn("HMS_SP_HC_PT_EncounterAdd",
             new SqlParameter("@HPE_ID", obj.HPE_ID)
            , new SqlParameter("@HPE_HHV_ID", obj.HPE_HHV_ID)
            , new SqlParameter("@HPE_AREA", obj.HPE_AREA)
            , new SqlParameter("@HPE_ARRIVAL_TIME", obj.HPE_ARRIVAL_TIME)
            , new SqlParameter("@HPE_VISIT_DATE", obj.HPE_VISIT_DATE)
            , new SqlParameter("@HPE_DIAGNOSIS", obj.HPE_DIAGNOSIS)
            , new SqlParameter("@HPE_CONDITION", obj.HPE_CONDITION)
            , new SqlParameter("@HPE_CONDITION_OTH", obj.HPE_CONDITION_OTH)
            , new SqlParameter("@HPE_INFOBTAINDFROM", obj.HPE_INFOBTAINDFROM)
            , new SqlParameter("@HPE_INFOBTAINDFROM_OTH", obj.HPE_INFOBTAINDFROM_OTH)
            , new SqlParameter("@HPE_EXPLI_GIVEN", obj.HPE_EXPLI_GIVEN)
            , new SqlParameter("@HPE_EXPLI_GIVEN_OTH", obj.HPE_EXPLI_GIVEN_OTH)
            , new SqlParameter("@HPE_ASSESSMENT_DONE_CODE", obj.HPE_ASSESSMENT_DONE_CODE)
            , new SqlParameter("@HPE_ASSESSMENT_DONE_NAME", obj.HPE_ASSESSMENT_DONE_NAME)
            , new SqlParameter("@HPE_PRESENT_COMPLAINT", obj.HPE_PRESENT_COMPLAINT)
            , new SqlParameter("@HPE_PAST_MEDICAL_HISTORY", obj.HPE_PAST_MEDICAL_HISTORY)
            , new SqlParameter("@HPE_PAST_SURGICAL_HISTORY", obj.HPE_PAST_SURGICAL_HISTORY)
            , new SqlParameter("@HPE_PSY_SOC_ECONOMIC_HISTORY", obj.HPE_PSY_SOC_ECONOMIC_HISTORY)
            , new SqlParameter("@HPE_FAMILY_HISTORY", obj.HPE_FAMILY_HISTORY)
            , new SqlParameter("@HPE_PHYSICAL_ASSESSMENT", obj.HPE_PHYSICAL_ASSESSMENT)
            , new SqlParameter("@HPE_REVIEW_OF_SYSTEMS", obj.HPE_REVIEW_OF_SYSTEMS)
            , new SqlParameter("@HPE_INVESTIGATIONS", obj.HPE_INVESTIGATIONS)
            , new SqlParameter("@HPE_PRINCIPAL_DIAGNOSIS", obj.HPE_PRINCIPAL_DIAGNOSIS)
            , new SqlParameter("@HPE_SECONDARY_DIAGNOSIS", obj.HPE_SECONDARY_DIAGNOSIS)
            , new SqlParameter("@HPE_TREATMENT_PLAN", obj.HPE_TREATMENT_PLAN)
            , new SqlParameter("@HPE_PROCEDURE", obj.HPE_PROCEDURE)
            , new SqlParameter("@HPE_TREATMENT", obj.HPE_TREATMENT)
            , new SqlParameter("@HPE_FOLLOWUPNOTES", obj.HPE_FOLLOWUPNOTES)
            , new SqlParameter("@UserId", obj.UserId)
            , "@ReturnEncounterId"

            );

            return strReturnId;
        }

        public DataSet GetPTEncounter(string Criteria)
        {
            DataSet DS = new DataSet();
            DS = objDB.ExecuteReader("HMS_SP_HC_PT_EncounterGet", new SqlParameter("@Criteria", Criteria));

            return DS;
        }


        public void DeletePTEncounter(PTEncounterBAL obj)
        {
            objDB.ExecuteNonQuery("HMS_SP_HC_PT_EncounterDelete",
              new SqlParameter("@HPE_ID", obj.HPE_ID)
              , new SqlParameter("@UserId", obj.UserId));
        }
    }
}