﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CommonDAL
/// </summary>
/// 
namespace HomeCare1.DAL
{
    public class CommonDAL
    {

        dboperations objDB = new dboperations();

        public DataSet GetStaffCategory(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_VIEW_StaffCategoryGet WHERE " + Criteria + " ORDER BY Name";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet GetStaffDesignation(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_STAFF_DESIG_MASTER WHERE " + Criteria;

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet GetReligion(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_VIEW_ReligionGet WHERE " + Criteria;

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet GetBloodGroup(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_VIEW_BloodGroupGet WHERE " + Criteria;

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet GetStateMaster(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select HMSS_STATE_NAME,HMSS_STATE_ID From HMS_STATEMASTER WHERE " + Criteria + " ORDER BY  HMSS_STATE_NAME";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;


        }

        public DataSet GetSpeciality(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT  HDS_DESCRIPTION,HDS_CODE  FROM HMS_DOMS_SPECIALITY WHERE " + Criteria + " ORDER BY  HDS_DESCRIPTION";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet GetCommission(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select * From HMS_COMMISSION WHERE " + Criteria + " ORDER BY  HCO_ID";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet GetEMRDoctorDep(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select * From HMS_EMR_DOCTOR_DEP WHERE " + Criteria;

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }


        public DataSet CalculateCompAmount(string AmountType, string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL = "";

            switch (AmountType)
            {
                case "OPENBAL":
                    strSQL = "SELECT ISNULL(SUM(HCB_AMT),0) AS CompAmount FROM HMS_COMPANY_BALANCE_HISTORY WHERE" + Criteria + " AND  HCB_TRANS_ID='OPENBAL'";
                    break;
                case "INVOICE":
                    strSQL = "SELECT ISNULL(SUM(HCB_AMT),0)  AS CompAmount  FROM HMS_COMPANY_BALANCE_HISTORY WHERE " + Criteria + " AND  HCB_TRANS_TYPE='INVOICE'";
                    break;
                case "PAIDAMT":
                    strSQL = "SELECT ISNULL(SUM(HCB_PAID_AMT),0)  AS CompAmount  FROM HMS_COMPANY_BALANCE_HISTORY WHERE " + Criteria + " AND   HCB_TRANS_TYPE='INVOICE'";
                    break;
                case "RETURNPAIDAMT":
                    strSQL = "SELECT ISNULL(SUM(HCB_PAID_AMT),0)  AS CompAmount  FROM HMS_COMPANY_BALANCE_HISTORY WHERE " + Criteria + " AND   HCB_TRANS_TYPE='INV_RTN'";
                    break;
                case "RETURN":
                    strSQL = "SELECT ISNULL(SUM(HCB_AMT),0)  AS CompAmount  FROM HMS_COMPANY_BALANCE_HISTORY WHERE " + Criteria + " AND  HCB_TRANS_TYPE='INV_RTN'";
                    break;
                case "REJECTED":
                    strSQL = "SELECT ISNULL(SUM(HCB_REJECT_AMT),0)  AS CompAmount  FROM HMS_COMPANY_BALANCE_HISTORY WHERE " + Criteria + " AND   HCB_TRANS_TYPE='INVOICE'";
                    break;
                case "ADVANCE":
                    strSQL = "SELECT ISNULL(SUM(HCB_PAID_AMT),0)  AS CompAmount  FROM HMS_COMPANY_BALANCE_HISTORY WHERE " + Criteria + " AND   HCB_TRANS_TYPE='ADVANCE'";
                    break;
                default:
                    break;
            }

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }


        public DataSet GetHaadServicessList(string Servicetype, string SearchFilder)
        {
            DataSet DS = new DataSet();
            DS = objDB.ExecuteReader("HMS_SP_HaadServicessListGet", new SqlParameter("@servicetype", Servicetype)
                , new SqlParameter("@searchfilter", SearchFilder));

            return DS;
        }

        public DataSet GetHomeCareReports()
        {
            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();

            DS = DBO.ExecuteReader("SELECT * FROM HMS_VIEW_HC_Reports ORDER BY DisplayOrder");
            return (DS);


        }

        public DataSet GetDeductibleType(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_VIEW_DeductibleTypeGet WHERE " + Criteria + " ORDER BY DisplayOrder";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }


        public DataSet GetCoInsuranceType(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_VIEW_CoInsuranceTypeGet WHERE " + Criteria + " ORDER BY DisplayOrder";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }


        public DataSet GetPatientTitle(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_VIEW_PatientTitleGet WHERE " + Criteria + " ORDER BY DisplayOrder";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet GetConsentForm(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT *   FROM HMS_VIEW_ConsentFormGet WHERE " + Criteria + " ORDER BY DisplayOrder";

            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public DataSet fnGetFieldValue(string FieldNames, string TableName, string Criteria, string OrderBy)
        {
            DataSet DS = new DataSet();
            string strSQL;

            if (OrderBy != "")
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria + " ORDER BY " + OrderBy;
            }
            else
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria;
            }


            DS = objDB.ExecuteQuery(strSQL);

            return DS;
        }

        public void fnUpdateTableData(string FieldNameWithValues, string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " UPDATE   " + TableName + " SET " + FieldNameWithValues + " WHERE " + Criteria;


            objDB.ExecuteQuery(strSQL);


        }


        public void fnDeleteTableData(string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " DELETE  FROM " + TableName + " WHERE " + Criteria;


            objDB.ExecuteQuery(strSQL);


        }

        public string fnGetDate(String formatstring)
        {
            DateTime dtDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT getdate() ";

            DS = objDB.ExecuteQuery(strSQL);

            dtDate = Convert.ToDateTime(DS.Tables[0].Rows[0][0]);


            return Convert.ToString(dtDate.ToString(formatstring));
        }



        public Boolean fnCheckduplicate(string TableName, string Criteria)
        {
            DataSet DS = new DataSet();

            string strSQL;

            strSQL = " SELECT *  FROM " + TableName + " WHERE " + Criteria;
            DS = objDB.ExecuteQuery(strSQL);

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }


        public void AudittrailAdd(string BranchId, string ScreenId, string Type, string Remarks, string UserID)
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HAT_BRANCH_ID", BranchId);
            Param.Add("HAT_SCRN_ID", ScreenId);
            Param.Add("HAT_TYPE", Type);
            Param.Add("HAT_REMARK", Remarks);
            Param.Add("HAT_CREATED_USER", UserID);

            objDB.ExecuteNonQuery("HMS_SP_AudittrailAdd", Param);

        }

    }

}