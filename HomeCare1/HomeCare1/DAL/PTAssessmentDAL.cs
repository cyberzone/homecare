﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.BAL;

/// <summary>
/// Summary description for PTAssessmentDAL
/// </summary>
/// 
namespace HomeCare1.DAL
{
    public class PTAssessmentDAL
    {

        dboperations objDB = new dboperations();

        public void AddPTAssessment(PTAssessmentBAL obj)
        {
            string strReturnId;
            strReturnId = objDB.ExecuteNonQuery("HMS_SP_HC_PT_AssessmentAdd"
                        , new SqlParameter("@HPA_ID", obj.HPA_ID)
                        , new SqlParameter("@HPA_HHV_ID", obj.HPA_HHV_ID)
                        , new SqlParameter("@HPA_BRANCH_ID", obj.HPA_BRANCH_ID)
                        , new SqlParameter("@HPA_PT_ID", obj.HPA_PT_ID)
                        , new SqlParameter("@HPA_HEALTH_PRESPASTHIST", obj.HPA_HEALTH_PRESPASTHIST)
                        , new SqlParameter("@HPA_HEALTH_PRESPAS_PROB", obj.HPA_HEALTH_PRESPAS_PROB)
                        , new SqlParameter("@HPA_HEALTH_PRESPAS_PREVPROB", obj.HPA_HEALTH_PRESPAS_PREVPROB)
                        , new SqlParameter("@HPA_HEALTH_ALLERGIES_MEDIC", obj.HPA_HEALTH_ALLERGIES_MEDIC)
                        , new SqlParameter("@HPA_HEALTH_ALLERGIES_FOOD", obj.HPA_HEALTH_ALLERGIES_FOOD)
                        , new SqlParameter("@HPA_HEALTH_ALLERGIES_OTHER", obj.HPA_HEALTH_ALLERGIES_OTHER)
                        , new SqlParameter("@HPA_HEALTH_MEDICHIST_MEDICINE", obj.HPA_HEALTH_MEDICHIST_MEDICINE)
                        , new SqlParameter("@HPA_HEALTH_MEDICHIST_SLEEP", obj.HPA_HEALTH_MEDICHIST_SLEEP)
                        , new SqlParameter("@HPA_HEALTH_MEDICHIST_PSCHOYASS", obj.HPA_HEALTH_MEDICHIST_PSCHOYASS)
                        , new SqlParameter("@HPA_HEALTH_PAIN_EXPRESSES", obj.HPA_HEALTH_PAIN_EXPRESSES)
                        , new SqlParameter("@HPA_PHY_GAST_BOWELSOUNDS", obj.HPA_PHY_GAST_BOWELSOUNDS)
                        , new SqlParameter("@HPA_PHY_GAST_ELIMINATION", obj.HPA_PHY_GAST_ELIMINATION)
                        , new SqlParameter("@HPA_PHY_GAST_GENERAL", obj.HPA_PHY_GAST_GENERAL)
                        , new SqlParameter("@HPA_PHY_REPRO_MALE", obj.HPA_PHY_REPRO_MALE)
                        , new SqlParameter("@HPA_PHY_REPRO_FEMALE", obj.HPA_PHY_REPRO_FEMALE)
                        , new SqlParameter("@HPA_PHY_REPRO_BREASTS", obj.HPA_PHY_REPRO_BREASTS)
                        , new SqlParameter("@HPA_PHY_GENIT_GENERAL", obj.HPA_PHY_GENIT_GENERAL)
                        , new SqlParameter("@HPA_PHY_SKIN_COLOR", obj.HPA_PHY_SKIN_COLOR)
                        , new SqlParameter("@HPA_PHY_SKIN_TEMPERATURE", obj.HPA_PHY_SKIN_TEMPERATURE)
                        , new SqlParameter("@HPA_PHY_SKIN_LESIONS", obj.HPA_PHY_SKIN_LESIONS)
                        , new SqlParameter("@HPA_PHY_NEURO_GENERAL", obj.HPA_PHY_NEURO_GENERAL)
                        , new SqlParameter("@HPA_PHY_NEURO_CONSCIO", obj.HPA_PHY_NEURO_CONSCIO)
                        , new SqlParameter("@HPA_PHY_NEURO_ORIENTED", obj.HPA_PHY_NEURO_ORIENTED)
                        , new SqlParameter("@HPA_PHY_NEURO_TIMERESP", obj.HPA_PHY_NEURO_TIMERESP)
                        , new SqlParameter("@HPA_PHY_CARDIO_GENERAL", obj.HPA_PHY_CARDIO_GENERAL)
                        , new SqlParameter("@HPA_PHY_CARDIO_PULSE", obj.HPA_PHY_CARDIO_PULSE)
                        , new SqlParameter("@HPA_PHY_CARDIO_PEDALPULSE", obj.HPA_PHY_CARDIO_PEDALPULSE)
                        , new SqlParameter("@HPA_PHY_CARDIO_EDEMA", obj.HPA_PHY_CARDIO_EDEMA)
                        , new SqlParameter("@HPA_PHY_CARDIO_NAILBEDS", obj.HPA_PHY_CARDIO_NAILBEDS)
                        , new SqlParameter("@HPA_PHY_CARDIO_CAPILLARY", obj.HPA_PHY_CARDIO_CAPILLARY)
                        , new SqlParameter("@HPA_PHY_ENT_EYES", obj.HPA_PHY_ENT_EYES)
                        , new SqlParameter("@HPA_PHY_ENT_EARS", obj.HPA_PHY_ENT_EARS)
                        , new SqlParameter("@HPA_PHY_ENT_NOSE", obj.HPA_PHY_ENT_NOSE)
                        , new SqlParameter("@HPA_PHY_ENT_THROAT", obj.HPA_PHY_ENT_THROAT)
                        , new SqlParameter("@HPA_PHY_RESP_CHEST", obj.HPA_PHY_RESP_CHEST)
                        , new SqlParameter("@HPA_PHY_RESP_BREATHPATT", obj.HPA_PHY_RESP_BREATHPATT)
                        , new SqlParameter("@HPA_PHY_RESP_BREATHSOUND", obj.HPA_PHY_RESP_BREATHSOUND)
                        , new SqlParameter("@HPA_RESP_BREATHCOUGH", obj.HPA_RESP_BREATHCOUGH)
                        , new SqlParameter("@HPA_PHY_NUTRI_DIET", obj.HPA_PHY_NUTRI_DIET)
                        , new SqlParameter("@HPA_PHY_NUTRI_NUTRISUPPORT", obj.HPA_PHY_NUTRI_NUTRISUPPORT)
                        , new SqlParameter("@HPA_PHY_NUTRI_APPETITE", obj.HPA_PHY_NUTRI_APPETITE)
                        , new SqlParameter("@HPA_PHY_NUTRI_FEEDINGDIF", obj.HPA_PHY_NUTRI_FEEDINGDIF)
                        , new SqlParameter("@HPA_PHY_NUTRI_WEIGHTSTATUS", obj.HPA_PHY_NUTRI_WEIGHTSTATUS)
                        , new SqlParameter("@HPA_PHY_NUTRI_DIAG", obj.HPA_PHY_NUTRI_DIAG)
                        , new SqlParameter("@HPA_RISK_SKINRISK_SENSORY", obj.HPA_RISK_SKINRISK_SENSORY)
                        , new SqlParameter("@HPA_RISK_SKINRISK_MOISTURE", obj.HPA_RISK_SKINRISK_MOISTURE)
                        , new SqlParameter("@HPA_RISK_SKINRISK_ACTIVITY", obj.HPA_RISK_SKINRISK_ACTIVITY)
                        , new SqlParameter("@HPA_RISK_SKINRISK_MOBILITY", obj.HPA_RISK_SKINRISK_MOBILITY)
                        , new SqlParameter("@HPA_RISK_SKINRISK_NUTRITION", obj.HPA_RISK_SKINRISK_NUTRITION)
                        , new SqlParameter("@HPA_RISK_SKINRISK_FRICTION", obj.HPA_RISK_SKINRISK_FRICTION)
                        , new SqlParameter("@HPA_RISK_SOCIO_LIVING", obj.HPA_RISK_SOCIO_LIVING)
                        , new SqlParameter("@HPA_RISK_SAFETY_GENERAL", obj.HPA_RISK_SAFETY_GENERAL)
                        , new SqlParameter("@HPA_RISK_FUN_SELFCARING", obj.HPA_RISK_FUN_SELFCARING)
                        , new SqlParameter("@HPA_RISK_FUN_MUSCULOS", obj.HPA_RISK_FUN_MUSCULOS)
                        , new SqlParameter("@HPA_RISK_FUN_EQUIPMENT", obj.HPA_RISK_FUN_EQUIPMENT)
                        , new SqlParameter("@HPA_RISK_EDU_GENERAL", obj.HPA_RISK_EDU_GENERAL)
                        , new SqlParameter("@UserId", obj.UserId)

            );


        }

        public DataSet GetPTAssessment(string Criteria)
        {
            DataSet DS = new DataSet();
            DS = objDB.ExecuteReader("HMS_SP_HC_PT_AssessmentGet", new SqlParameter("@Criteria", Criteria));

            return DS;
        }


        public void DeletePTAssessment(PTAssessmentBAL obj)
        {
            objDB.ExecuteNonQuery("HMS_SP_HC_PT_AssessmentDelete",
              new SqlParameter("@HPA_ID", obj.HPA_ID)
              , new SqlParameter("@UserId", obj.UserId));
        }
    }

}