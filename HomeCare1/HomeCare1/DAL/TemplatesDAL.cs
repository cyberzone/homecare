﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.DAL
{
    public class TemplatesDAL
    {
        dboperations objDB = new dboperations();


        public string AddEMRTemplates(TemplatesBAL obj)
        {
            string strReturnId;
            strReturnId = objDB.ExecuteNonQuery("HMS_SP_EMRTemplatesAdd",
              new SqlParameter("@ET_BRANCH_ID", obj.ET_BRANCH_ID)
            , new SqlParameter("@ET_TYPE", obj.ET_TYPE)
            , new SqlParameter("@ET_NAME", obj.ET_NAME)
            , new SqlParameter("@ET_TEMPLATE", obj.ET_TEMPLATE)
            , new SqlParameter("@ET_APPLY", obj.ET_APPLY)
            , new SqlParameter("@ET_DR_CODE", obj.ET_DR_CODE)
            , new SqlParameter("@ET_DEP_ID", obj.ET_DEP_ID));


            return strReturnId;
        }
        public DataSet GetEMRTemplates(string Criteria)
        {
            DataSet DS = new DataSet();
            DS = objDB.ExecuteReader("HMS_SP_EMRTemplatesGet", new SqlParameter("@Criteria", Criteria));

            return DS;
        }

        public string DeleteEMRTemplates(TemplatesBAL obj)
        {
            string strReturnId;
            strReturnId = objDB.ExecuteNonQuery("HMS_SP_EMRTemplatesDelete",
              new SqlParameter("@ET_BRANCH_ID", obj.ET_BRANCH_ID)
            , new SqlParameter("@ET_TYPE", obj.ET_TYPE)
            , new SqlParameter("@ET_NAME", obj.ET_NAME));


            return strReturnId;
        }

    }

}