﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.BAL;

/// <summary>
/// Summary description for PhysicalTherapyEvaluationDAL
/// </summary>
/// 
namespace HomeCare1.DAL
{
    public class PhysicalTherapyEvaluationDAL
    {
        public void AddPhysicalTherapyEvaluation(PhysicalTherapyEvaluationBAL OBJPSEval)
        {

            dboperations objDBOp = new dboperations();
            objDBOp.ExecuteNonQuery("HMS_SP_HC_PhysicalTherapyAdd"
                                  , new SqlParameter("@HHPH_BRANCH_ID", OBJPSEval.BRANCH_ID)
                                  , new SqlParameter("@HHPH_HHV_ID", OBJPSEval.HHV_ID)
                                  , new SqlParameter("@HHPH_CHIEF_COMPLAINT", OBJPSEval.CHIEF_COMPLAINT)
                                  , new SqlParameter("@HHPH_INJURY_DATE", OBJPSEval.INJURY_DATE)
                                  , new SqlParameter("@HHPH_SURGERY_DATE", OBJPSEval.SURGERY_DATE)
                                  , new SqlParameter("@HHPH_INJURY_REMARKS", OBJPSEval.INJURY_REMARKS)
                                  , new SqlParameter("@HHPH_THERAPY_RECEIVED", OBJPSEval.THERAPY_RECEIVED)
                                  , new SqlParameter("@HHPH_THERAPY_DATE", OBJPSEval.THERAPY_DATE)
                                  , new SqlParameter("@HHPH_THERAPY_VISIT_COUNT", OBJPSEval.THERAPY_VISIT_COUNT)
                                  , new SqlParameter("@HHPH_CONDITION", OBJPSEval.CONDITION)
                                  , new SqlParameter("@HHPH_SYMPTOMS", OBJPSEval.SYMPTOMS)
                                  , new SqlParameter("@HHPH_PAIN_BEST", OBJPSEval.PAIN_BEST)
                                  , new SqlParameter("@HHPH_PAINT_WORST", OBJPSEval.PAINT_WORST)
                                  , new SqlParameter("@HHPH_CONDITION_BETTER", OBJPSEval.CONDITION_BETTER)
                                  , new SqlParameter("@HHPH_CONDITION_WORSE", OBJPSEval.CONDITION_WORSE)
                                  , new SqlParameter("@HHPH_PREV_MED_INTERVENTION", OBJPSEval.PREV_MED_INTERVENTION)
                                  , new SqlParameter("@HHPH_PREV_MED_INTERVENTION_OTHER", OBJPSEval.PREV_MED_INTERVENTION_OTHER)
                                  , new SqlParameter("@HHPH_GOALS_TOBE_ACHIEVED", OBJPSEval.GOALS_TOBE_ACHIEVED)
                                  , new SqlParameter("@HHPH_DIFFICULTY_SWALLOWING", OBJPSEval.DIFFICULTY_SWALLOWING)
                                  , new SqlParameter("@HHPH_MOTION_SICKNESS", OBJPSEval.MOTION_SICKNESS)
                                  , new SqlParameter("@HHPH_STROKE", OBJPSEval.STROKE)
                                  , new SqlParameter("@HHPH_ARTHRITIS", OBJPSEval.ARTHRITIS)
                                  , new SqlParameter("@HHPH_FEVER_CHILLS_SWEATS", OBJPSEval.FEVER_CHILLS_SWEATS)
                                  , new SqlParameter("@HHPH_OSTEOPOROSIS", OBJPSEval.OSTEOPOROSIS)
                                  , new SqlParameter("@HHPH_HIGH_BLOOD_PRESSURE", OBJPSEval.HIGH_BLOOD_PRESSURE)
                                  , new SqlParameter("@HHPH_UNEXPLAINED_WEIGHT_LOSS", OBJPSEval.UNEXPLAINED_WEIGHT_LOSS)
                                  , new SqlParameter("@HHPH_ANEMIA", OBJPSEval.ANEMIA)
                                  , new SqlParameter("@HHPH_HEART_TROUBLE", OBJPSEval.HEART_TROUBLE)
                                  , new SqlParameter("@HHPH_BLOOD_CLOTS", OBJPSEval.BLOOD_CLOTS)
                                  , new SqlParameter("@HHPH_BLEEDING_PROBLEMS", OBJPSEval.BLEEDING_PROBLEMS)
                                  , new SqlParameter("@HHPH_PACEMAKER", OBJPSEval.PACEMAKER)
                                  , new SqlParameter("@HHPH_SHORTNESS_OF_BREATH", OBJPSEval.SHORTNESS_OF_BREATH)
                                  , new SqlParameter("@HHPH_HIV_HEPATITIS", OBJPSEval.HIV_HEPATITIS)
                                  , new SqlParameter("@HHPH_EPILEPSY_SEIZURES", OBJPSEval.EPILEPSY_SEIZURES)
                                  , new SqlParameter("@HHPH_HISTORY_SMOKING", OBJPSEval.HISTORY_SMOKING)
                                  , new SqlParameter("@HHPH_HISTORY_ALCOHOL_ABUSE", OBJPSEval.HISTORY_ALCOHOL_ABUSE)
                                  , new SqlParameter("@HHPH_HISTORY_DRUG_ABUSE", OBJPSEval.HISTORY_DRUG_ABUSE)
                                  , new SqlParameter("@HHPH_DIABETES", OBJPSEval.DIABETES)
                                  , new SqlParameter("@HHPH_DEPRESSION_ANXIETY", OBJPSEval.DEPRESSION_ANXIETY)
                                  , new SqlParameter("@HHPH_MYOFASCIAL_PAIN", OBJPSEval.MYOFASCIAL_PAIN)
                                  , new SqlParameter("@HHPH_FIBROMYALGIA", OBJPSEval.FIBROMYALGIA)
                                  , new SqlParameter("@HHPH_PREGNANCY", OBJPSEval.PREGNANCY)
                                  , new SqlParameter("@HHPH_CANCER", OBJPSEval.CANCER)
                                  , new SqlParameter("@HHPH_PREV_SURGERIES", OBJPSEval.PREV_SURGERIES)
                                  , new SqlParameter("@HHPH_OTHER", OBJPSEval.OTHER)
                                  , new SqlParameter("@HHPH_MEDICATIONS", OBJPSEval.MEDICATIONS)
                                  , new SqlParameter("@HHPH_ALLERGIES", OBJPSEval.ALLERGIES)
                                  , new SqlParameter("@UserId", OBJPSEval.UserId));
        }

        public void DeletePhysicalTherapyEvaluation(PhysicalTherapyEvaluationBAL OBJPSEval)
        {
            DataSet DS = new DataSet();

            dboperations objDBOp = new dboperations();
            objDBOp.ExecuteNonQuery("HMS_SP_HC_PhysicalTherapyDelete"
                                  , new SqlParameter("@HHPH_HHV_ID", OBJPSEval.HHV_ID));


        }
    }

}