﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for dbOperation
/// </summary>
namespace HomeCare1.DAL
{
    public class dbOperation
    {
        public dbOperation()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString);

        public void conopen()
        {
            con.Open();

        }

        public void conclose()
        {


            con.Close();



        }

        public DataSet ExecuteReader(string Procedure, IDictionary<string, string> Param)
        {
            DataSet DS = new DataSet();
            conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }
            cmd.ExecuteNonQuery();
            conclose();

            SqlDataAdapter ADPT = new SqlDataAdapter(cmd);
            ADPT.Fill(DS);

            return DS;


        }

        public DataSet ExecuteReader(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet DS = new DataSet();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SQLQuery;
            cmd.ExecuteNonQuery();
            conclose();

            adpt.SelectCommand = cmd;
            adpt.Fill(DS);

            return DS;


        }

        public string ExecuteNonQuery(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet DS = new DataSet();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SQLQuery;
            cmd.ExecuteNonQuery();
            conclose();


            return "";


        }


        public string ExecuteNonQuery(string Procedure, IDictionary<string, string> Param)
        {
            conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }

            cmd.ExecuteNonQuery();
            conclose();

            return ""; //Convert.ToString(returnValue.Value);


        }

        public string ExecuteNonQueryReturn(string Procedure, IDictionary<string, string> Param)
        {
            conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }

            SqlParameter returnValue = new SqlParameter("@ReturnValue", SqlDbType.VarChar, 50);
            returnValue.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnValue);
            cmd.ExecuteNonQuery();
            conclose();
            string strReturnId;
            strReturnId = Convert.ToString(returnValue.Value);
            return strReturnId;


        }


    }
}