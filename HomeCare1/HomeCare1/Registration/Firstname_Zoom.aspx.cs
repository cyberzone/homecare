﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.Registration
{
    public partial class Firstname_Zoom : System.Web.UI.Page
    {
        connectiondb c = new connectiondb();
        dboperations dbo = new dboperations();
        DataSet ds = new DataSet();

        public string PTRegChangePolicyNoCaption = "";
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindNationality()
        {
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcNationality.DataSource = ds;
                drpSrcNationality.DataValueField = "hnm_id";
                drpSrcNationality.DataTextField = "hnm_name";
                drpSrcNationality.DataBind();
                drpSrcNationality.Items.Insert(0, "--- All --");
                drpSrcNationality.Items[0].Value = "0";

            }

            ds.Clear();
            ds.Dispose();

        }

        void BindCompany()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcCompany.DataSource = ds;
                drpSrcCompany.DataValueField = "HCM_COMP_ID";
                drpSrcCompany.DataTextField = "HCM_NAME";
                drpSrcCompany.DataBind();
                drpSrcCompany.Items.Insert(0, "--- All ---");
                drpSrcCompany.Items[0].Value = "0";

            }

            ds.Clear();
            ds.Dispose();

        }


        void BindSubCompany()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' ";


            Criteria += " AND  HCM_COMP_ID !=  HCM_BILL_CODE and HCM_BILL_CODE = '" + drpSrcCompany.SelectedValue + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            drpNetworkName.Items.Clear();
            drpPlanType.Items.Clear();


            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNetworkName.DataSource = ds;
                drpNetworkName.DataValueField = "HCM_COMP_ID";
                drpNetworkName.DataTextField = "HCM_NAME";
                drpNetworkName.DataBind();

            }

            drpNetworkName.Items.Insert(0, "--- All ---");
            drpNetworkName.Items[0].Value = "0";
            drpNetworkName.SelectedIndex = 0;
        }

        void BindPlanType()
        {
            drpPlanType.Items.Clear();

            if (drpSrcCompany.Items.Count > 0)
            {
                if (drpSrcCompany.SelectedItem.Text != "")
                {
                    DataSet DS = new DataSet();
                    string Criteria = " 1=1 ";
                    if (drpNetworkName.SelectedIndex != 0)
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                    }
                    else
                    {
                        Criteria += " AND HCB_COMP_ID = '" + drpSrcCompany.SelectedValue + "'";
                    }
                    DS = dbo.CompBenefitsGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        drpPlanType.DataSource = DS;
                        drpPlanType.DataTextField = "HCB_CAT_TYPE";
                        drpPlanType.DataValueField = "HCB_CAT_TYPE";
                        drpPlanType.DataBind();
                    }

                    drpPlanType.Items.Insert(0, "--- All ---");
                    drpPlanType.Items[0].Value = "0";
                }
            }

        }

        void BindSrcDoctor()
        {

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcDoctor.DataSource = ds;
                drpSrcDoctor.DataValueField = "HSFM_STAFF_ID";
                drpSrcDoctor.DataTextField = "FullName";
                drpSrcDoctor.DataBind();
            }
            drpSrcDoctor.Items.Insert(0, "--- All ---");
            drpSrcDoctor.Items[0].Value = "0";



        }

        void BindGrid()
        {
            string Criteria = " 1=1 ";


            if (txtSrcName.Text.Trim() != "")
            {
                if (Convert.ToString(Session["PTNameMultiple"]) != "" && Convert.ToString(Session["PTNameMultiple"]) == "N")
                {
                    Criteria += " AND HPM_PT_FNAME like '" + txtSrcName.Text.Trim() + "%' ";
                }
                else
                {

                    if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                    {
                        Criteria += " AND HPM_PT_FNAME like '" + txtSrcName.Text.Trim() + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                    {
                        Criteria += " AND HPM_PT_FNAME + ' ' + ISNULL(HPM_PT_MNAME,'')   like '%" + txtSrcName.Text.Trim() + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                    {
                        Criteria += " AND HPM_PT_FNAME + ' ' + ISNULL(HPM_PT_MNAME,'')  = '" + txtSrcName.Text.Trim() + "' ";
                    }
                }

            }

            if (txtSrcLName.Text.Trim() != "")
            {
                if (Convert.ToString(Session["PTNameMultiple"]) != "" && Convert.ToString(Session["PTNameMultiple"]) == "N")
                {

                    Criteria += " AND  SUBSTRING(HPM_PT_FNAME,CHARINDEX(' ',HPM_PT_FNAME),len(HPM_PT_FNAME))    like '%" + txtSrcLName.Text.Trim() + "%' ";
                }
                else
                {
                    if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                    {
                        Criteria += " AND HPM_PT_LNAME like '" + txtSrcLName.Text + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                    {
                        Criteria += " AND ISNULL(HPM_PT_LNAME,'')   like '%" + txtSrcLName.Text.Trim() + "%' ";
                    }
                    else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                    {
                        Criteria += " AND  ISNULL(HPM_PT_LNAME,'')   = '" + txtSrcLName.Text.Trim() + "' ";
                    }
                }

            }


            if (txtSrcFileNo.Text.Trim() != "")
            {


                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PT_ID like '" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PT_ID  like '%" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PT_ID   = '" + txtSrcFileNo.Text.Trim() + "' ";
                }



            }
            if (txtSrcEmiratesId.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_IQAMA_NO like '" + txtSrcEmiratesId.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_IQAMA_NO  like '%" + txtSrcEmiratesId.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_IQAMA_NO   = '" + txtSrcEmiratesId.Text.Trim() + "' ";
                }

            }


            if (txtSrcMobile1.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_MOBILE like '" + txtSrcMobile1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_MOBILE  like '%" + txtSrcMobile1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_MOBILE   = '" + txtSrcMobile1.Text.Trim() + "' ";
                }

            }

            if (txtSrcMobile2.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PHONE2 like '" + txtSrcMobile2.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PHONE2  like '%" + txtSrcMobile2.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PHONE2   = '" + txtSrcMobile2.Text.Trim() + "' ";
                }

            }

            if (txtSrcPhone1.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PHONE1 like '" + txtSrcPhone1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PHONE1  like '%" + txtSrcPhone1.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PHONE1   = '" + txtSrcPhone1.Text.Trim() + "' ";
                }

            }

            if (txtSrcPhone3.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PHONE3 like '" + txtSrcPhone3.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PHONE3  like '%" + txtSrcPhone3.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PHONE3   = '" + txtSrcPhone3.Text.Trim() + "' ";
                }

            }



            if (txtSrcPOBox.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_POBOX like '" + txtSrcPOBox.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_POBOX  like '%" + txtSrcPOBox.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_POBOX   = '" + txtSrcPOBox.Text.Trim() + "' ";
                }

            }
            if (drpSrcNationality.SelectedIndex != 0)
            {
                Criteria += " AND HPM_NATIONALITY like '" + drpSrcNationality.SelectedValue + "'";
            }

            if (drpSrcCompany.SelectedIndex != 0)
            {
                Criteria += " AND HPM_BILL_CODE = '" + drpSrcCompany.SelectedValue + "'";
            }

            if (drpNetworkName.Items.Count > 0)
            {
                if (drpNetworkName.SelectedIndex != 0)
                {
                    Criteria += " AND HPM_INS_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
                }
            }

            if (drpPlanType.Items.Count > 0)
            {
                if (drpPlanType.SelectedIndex != 0)
                {
                    Criteria += " AND HPM_POLICY_TYPE = '" + drpPlanType.SelectedValue + "'";
                }

            }

            if (drpPatientType.Items.Count > 0)
            {
                if (drpPatientType.SelectedIndex != 0)
                {

                    if (drpPatientType.SelectedIndex == 1)
                    {
                        Criteria += " AND ( HPM_PT_TYPE ='CA' OR  HPM_PT_TYPE ='CASH') ";
                    }
                    else if (drpPatientType.SelectedIndex == 2)
                    {
                        Criteria += " AND ( HPM_PT_TYPE ='CR' OR  HPM_PT_TYPE ='CREDIT') ";
                    }
                }

            }



            if (txtSrcPolicyNo.Text.Trim() != "")
            {

                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_POLICY_NO like '" + txtSrcPolicyNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_POLICY_NO  like '%" + txtSrcPolicyNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_POLICY_NO   = '" + txtSrcPolicyNo.Text.Trim() + "' ";
                }

            }


            string strStartDate = txtFromDate.Text.Trim();
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text.Trim() != "" && txtFromTime.Text.Trim() == "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPM_MODIFIED_DATE,101),101) >= '" + strForStartDate + "'";
            }
            else if (txtFromDate.Text != "" && txtFromTime.Text.Trim() != "")
            {
                Criteria += " AND HPM_MODIFIED_DATE >= '" + strForStartDate + " " + txtFromTime.Text.Trim() + "'";

            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "" && txtToTime.Text == "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPM_MODIFIED_DATE,101),101) <= '" + strForToDate + "'";
            }
            else if (txtToDate.Text != "" && txtToTime.Text != "")
            {
                Criteria += " AND HPM_MODIFIED_DATE <= '" + strForToDate + " " + txtToTime.Text + "'";

            }

            if (drpSrcDoctor.SelectedIndex != 0)
            {
                Criteria += " AND HPM_DR_ID='" + drpSrcDoctor.SelectedValue + "'";
            }


            if (txtSrcInvId.Text != "")
            {

                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PT_ID IN ( select HIM_PT_ID  from HMS_INVOICE_MASTER WHERE HIM_INVOICE_ID  like '" + txtSrcInvId.Text + "%' ) ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PT_ID IN ( select HIM_PT_ID  from HMS_INVOICE_MASTER  WHERE HIM_INVOICE_ID  like '%" + txtSrcInvId.Text + "%' ) ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PT_ID IN ( select HIM_PT_ID  from HMS_INVOICE_MASTER  WHERE HIM_INVOICE_ID   = '" + txtSrcInvId.Text + "' ) ";
                }

            }

            // }
            ds = dbo.retun_patient_details(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            lblTotal.Text = "0";

            gvGridView.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

                lblTotal.Text = ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }

            c.conclose();
            ds.Clear();
            ds.Dispose();
        }

        void DefaultValueLoad()
        {
            PTRegChangePolicyNoCaption = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PTRegChangePolicyNoCaption"]).ToLower().Trim();

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (hidLocalDate.Value != "")
                {
                    Session["LocalDate"] = hidLocalDate.Value;
                }

                if (!IsPostBack)
                {

                    hidPageName.Value = Request.QueryString["PageName"].ToString();
                    ViewState["CtrlName"] = Request.QueryString["CtrlName"].ToString();
                    ViewState["Value"] = Request.QueryString["Value"].ToString();
                    DefaultValueLoad();

                    if (PTRegChangePolicyNoCaption == "true")
                    {
                        lblPolicyNo.Text = "Card No.";
                    }
                    else
                    {
                        lblPolicyNo.Text = "Member ID";
                    }



                    BindNationality();
                    BindCompany();
                    BindSrcDoctor();
                    if (ViewState["CtrlName"] != "")
                    {
                        if (ViewState["CtrlName"].ToString() == "FileNo")
                        {
                            txtSrcFileNo.Text = ViewState["Value"].ToString();
                        }
                        else if (ViewState["CtrlName"].ToString() == "Name")
                        {
                            txtSrcName.Text = ViewState["Value"].ToString();

                        }
                        else if (ViewState["CtrlName"].ToString() == "LName")
                        {
                            txtSrcLName.Text = ViewState["Value"].ToString();

                        }



                        else if (ViewState["CtrlName"].ToString() == "EmiratesID")
                        {
                            txtSrcEmiratesId.Text = ViewState["Value"].ToString();

                        }
                        else if (ViewState["CtrlName"].ToString() == "POBox")
                        {
                            txtSrcPOBox.Text = ViewState["Value"].ToString();

                        }
                        else if (ViewState["CtrlName"].ToString() == "Phone1")
                        {
                            txtSrcPhone1.Text = ViewState["Value"].ToString();
                        }
                        else if (ViewState["CtrlName"].ToString() == "Mobile1")
                        {
                            txtSrcMobile1.Text = ViewState["Value"].ToString();
                        }
                        else if (ViewState["CtrlName"].ToString() == "Mobile2")
                        {
                            txtSrcMobile2.Text = ViewState["Value"].ToString();
                        }

                        else if (ViewState["CtrlName"].ToString() == "POBox")
                        {
                            txtSrcPOBox.Text = ViewState["Value"].ToString();
                        }
                        else if (ViewState["CtrlName"].ToString() == "PolicyNo")
                        {
                            txtSrcPolicyNo.Text = ViewState["Value"].ToString();
                        }
                        else if (ViewState["CtrlName"].ToString() == "EmiratesID")
                        {
                            txtSrcEmiratesId.Text = ViewState["Value"].ToString();
                        }



                        BindGrid();
                    }
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Firstname_Zoom.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            ViewState["CtrlName"] = "";
            BindGrid();


        }


        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvGridView.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Firstname_Zoom.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ServSelectIndex"] = gvScanCard.RowIndex;




                Label lblPatientId, lblPatientName, lblMobile, lblEmailId;

                lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
                lblPatientName = (Label)gvScanCard.Cells[0].FindControl("lblPatientName");
                lblMobile = (Label)gvScanCard.Cells[0].FindControl("lblMobile");
                lblEmailId = (Label)gvScanCard.Cells[0].FindControl("lblEmailId");

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "passvalue('" + lblPatientId.Text + "','" + lblPatientName.Text + "','" + lblMobile.Text + "'  ,'" + lblEmailId.Text + "'  );", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Firstname_Zoom.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void drpSrcCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                BindSubCompany();
                BindPlanType();



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Firstname_Zoom.drpSrcCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpNetworkName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindPlanType();
        }
    }
}