﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Patient_Registration.aspx.cs" Inherits="HomeCare1.Registration.Patient_Registration" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

     <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else {
                return false;
            }
        }

        function BindDoctorDtls(StaffId, StaffName, Dept, TokenNo) {

            document.getElementById("<%=txtDoctorID.ClientID%>").value = StaffId;
            document.getElementById("<%=txtDoctorName.ClientID%>").value = StaffName;
            document.getElementById("<%=txtDepName.ClientID%>").value = Dept;
            document.getElementById("<%=txtTokenNo.ClientID%>").value = TokenNo;


        }
        function BindCompanyDtls(CompanyId, CompanyName) {

            document.getElementById("<%=txtCompany.ClientID%>").value = CompanyId;
            document.getElementById("<%=txtCompanyName.ClientID%>").value = CompanyName;

        }


        function disableinc() {

            var e = document.getElementById("<%=drpPatientType.ClientID%>");
            var ty = e.options[e.selectedIndex].value;
            if (ty == "CA") {
                document.getElementById("<%=txtCompany.ClientID%>").disabled = true;
                document.getElementById("<%=txtCompanyName.ClientID%>").disabled = true;
                document.getElementById("<%=txtPolicyNo.ClientID%>").disabled = true;
                document.getElementById("<%=txtPolicyId.ClientID%>").disabled = true;
                document.getElementById("<%=txtIDNo.ClientID%>").disabled = true;
                document.getElementById("<%=txtExpiryDate.ClientID%>").disabled = true;
                document.getElementById("<%=drpPlanType.ClientID%>").disabled = true;
                document.getElementById("<%=txtCompany.ClientID%>").value = '';
                document.getElementById("<%=txtCompanyName.ClientID%>").value = '';
                document.getElementById("<%=txtPolicyNo.ClientID%>").value = '';
                document.getElementById("<%=txtPolicyId.ClientID%>").value = '';
                document.getElementById("<%=txtIDNo.ClientID%>").value = '';
                document.getElementById("<%=txtExpiryDate.ClientID%>").value = '';
                document.getElementById("<%=drpPlanType.ClientID%>").value = 'Select';



            }
            else {

                document.getElementById("<%=txtCompany.ClientID%>").disabled = false;
                document.getElementById("<%=txtCompanyName.ClientID%>").disabled = false;
                document.getElementById("<%=txtPolicyNo.ClientID%>").disabled = false;
                document.getElementById("<%=txtPolicyId.ClientID%>").disabled = false;
                document.getElementById("<%=txtIDNo.ClientID%>").disabled = false;
                document.getElementById("<%=txtExpiryDate.ClientID%>").disabled = false;
                document.getElementById("<%=drpPlanType.ClientID%>").disabled = false;


            }
        }

        function GetDays(year, month) {
            return new Date(year, month, 0).getDate();

        }

        function AgeCalculation() {
            if (document.getElementById("<%=txtDOB.ClientID%>").value != "__/__/____") {
                var dob = document.getElementById("<%=txtDOB.ClientID%>").value;
                document.getElementById("<%=txtAge.ClientID%>").value = 0;
                var arrDOB = dob.split('/');
                var currentyear = document.getElementById("<%=Year.ClientID%>").value

                var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
                var arrTodayDate1 = TodayDate.split('/');


                if (arrDOB.length == 3 && dob.length == 10) {

                    if (currentyear != arrDOB[2]) {
                        var age = currentyear - arrDOB[2];
                        var Month;

                        if (parseFloat(arrTodayDate1[1]) >= parseFloat(arrDOB[1])) {
                            Month = arrTodayDate1[1] - arrDOB[1];
                        }

                        else {
                            age = age - 1;
                            var diff = 12 - arrDOB[1];
                            Month = parseFloat(arrTodayDate1[1]) + parseFloat(diff);
                        }

                        document.getElementById("<%=txtAge.ClientID%>").value = age;
                        document.getElementById("<%=txtMonth.ClientID%>").value = Month;

                    }
                    else {

                        document.getElementById("<%=txtAge.ClientID%>").value = 0;
                        var Month;

                        if (parseFloat(arrTodayDate1[1]) >= parseFloat(arrDOB[1])) {
                            Month = arrTodayDate1[1] - arrDOB[1];

                        }

                        else {
                            Month = (parseFloat(arrTodayDate1[1]) + 11) - arrDOB[1];
                        }

                        document.getElementById("<%=txtMonth.ClientID%>").value = Month;

                    }
                }
                else {

                    document.getElementById("<%=txtAge.ClientID%>").value = 0;

                }


            }
            else {
                document.getElementById("<%=txtAge.ClientID%>").value = 0;

            }
        }

        function DBOcalculation() {

            if (document.getElementById("<%=txtDOB.ClientID%>").value == "") {
                var age = document.getElementById("<%=txtAge.ClientID%>").value;

                var currentyear = document.getElementById("<%=Year.ClientID%>").value
                var DBOYear = currentyear - age;
                if (age != DBOYear) {
                    document.getElementById("<%=txtDOB.ClientID%>").value = '01/01/' + DBOYear
                    document.getElementById("<%=txtMonth.ClientID%>").value = '';
                }


            }

        }

        function PastDateCheck() {

            var ExpiryDat = document.getElementById("<%=txtExpiryDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]


                //alert(ReFromTodayDate)
                //  alert(ReFromExpDate)

                // if (ReFromTodayDate > ReFromExpDate) {
                //  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Expiry Date Should be Grater than Today Date";
                //  return false;

                // }

            }
            return true;


        }

        function PastDateCheck1() {

            var ExpiryDat = document.getElementById("<%=txtCardExpDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;
            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]


                // if (ReFromTodayDate > ReFromExpDate) {

                //  document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Expiry Date Should be Grater than Today Date";
                //  return false;
                // }

            }
            return true;

        }

        function PolicyExpCheck() {

            var ExpiryDat = document.getElementById("<%=txtExpiryDate.ClientID%>").value;
            var TodayDate = document.getElementById("<%=hidTodayDate.ClientID%>").value;

            var currentyear = document.getElementById("<%=Year.ClientID%>").value

            if (ExpiryDat != "") {

                var date_array = TodayDate.split("/");
                var ReFromTodayDate = date_array[2] + date_array[1] + date_array[0]
                var CurrMonth = date_array[1];

                var date_array1 = ExpiryDat.split("/");
                var ReFromExpDate = date_array1[2] + date_array1[1] + date_array1[0]
                var ExpYear = date_array1[2];
                var ExpMonth = date_array1[1];

                if (currentyear > ExpYear) {
                    alert('Insurance Card Expired');
                    //return false;

                }
                else if (currentyear == ExpYear && CurrMonth > ExpMonth) {
                    alert('Insurance Card Expired');
                }
                else if (currentyear == ExpYear && CurrMonth == ExpMonth) {
                    var diffDays = date_array1[0] - date_array[0]
                    if (diffDays <= 7) {
                        alert('Insurance Card will Expire in ' + diffDays + ' Days');
                    }
                }

            }
            return true;

        }



        $(document).ready(function () {
            $('#btnClear').click(function () {
                $(':input[type=text]').val('');
            });
        });
        function AsyncFileUpload1_uploadStarted() {
            //document.getElementById("imageFront").style.display = "none";
        }

        function AsyncFileUpload1_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgFront.ClientID %>");
            document.getElementById("<%=hidImgFront.ClientID%>").value = 'true';
            var img = new Image();
            img.onload = function () {
                //imageDisplay.style.cssText = "height:160px;width:285px";
                imageDisplay.src = img.src;
                //imageDisplay.style.display = "block";
            };

            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();

        }

        function AsyncFileUpload2_uploadComplete(sender, args) {
            var imageDisplay = document.getElementById("<%= imgBack.ClientID %>");
            var img = new Image();
            img.onload = function () {
                //imageDisplay.style.cssText = "height:160px;width:285px";
                imageDisplay.src = img.src;
                //imageDisplay.style.display = "block";
            };
            img.src = "..\\Uploads\\Temp\\" + args.get_fileName();
        }


        function Val() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var hh = today.getHours();
            var min = today.getMinutes();
            var sec = today.getSeconds();

            var yyyy = today.getFullYear();
            if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;



            // alert(today);

            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;

            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the File No";
                return false;
            }

            if (document.getElementById('<%=txtFName.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the First Name";
                return false;
            }

            if (document.getElementById('<%=drpSex.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Sex";
                return false;
            }


            if (document.getElementById('<%=txtDOB.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtDOB.ClientID%>').value) == false) {
                    document.getElementById('<%=txtDOB.ClientID%>').focus()
                    return false;
                }
            }

            var AgeMandatory = '<%=Convert.ToString(ViewState["AgeMandatory"]) %>'
            if (AgeMandatory == "1" && document.getElementById('<%=txtAge.ClientID%>').value == "") {

                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Age";
                return false;

            }


            var EmiratesIdMandatory = '<%=Convert.ToString(ViewState["EmiratesIdMandatory"]) %>'
            if (EmiratesIdMandatory == "1" && document.getElementById('<%=txtEmiratesID.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the EmiratesID / Passport";
                return false;

            }

            var NationalityMandatory = '<%=Convert.ToString(ViewState["NationalityMandatory"]) %>'
            if (NationalityMandatory == "1" && document.getElementById('<%=drpNationality.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Nationality";
                return false;

            }

            if (document.getElementById('<%=txtMobile1.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Mobile No";
                return false;
            }

            if (document.getElementById('<%=hidPateName.ClientID%>').value != "HomeCare") {
                if (document.getElementById('<%=txtDoctorID.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Doctor Id";
                    return false;
                }
            }

            var strEmail = document.getElementById('<%=txtEmail.ClientID%>').value
            if (/\S+/.test(strEmail)) {
                if (echeck(document.getElementById('<%=txtEmail.ClientID%>').value) == false) {
                    return false;
                }
            }

            var PriorityDisplay = '<%=Convert.ToString(Session["PriorityDisplay"]) %>'
            if (PriorityDisplay == "Y" && document.getElementById('<%=drpPriority.ClientID%>').value == "0") {

                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Priority";
                return false;

            }



            if (document.getElementById('<%=drpPatientType.ClientID%>').value == "CR") {


                if (document.getElementById('<%=drpDefaultCompany.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company Name";
                    return false;
                }
                if (document.getElementById('<%=drpDefaultCompany.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Company Name";
                    return false;
                }

                if (document.getElementById('<%=txtPolicyNo.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Member ID";
                    return false;
                }


                if (document.getElementById('<%=txtPolicyStart.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtPolicyStart.ClientID%>').value) == false) {
                        document.getElementById('<%=txtPolicyStart.ClientID%>').focus()
                        return false;
                    }
                }

                if (document.getElementById('<%=txtExpiryDate.ClientID%>').value == "") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Expiry Date";
                    return false;
                }

                if (document.getElementById('<%=txtExpiryDate.ClientID%>').value != "") {
                    if (isDate(document.getElementById('<%=txtExpiryDate.ClientID%>').value) == false) {
                        document.getElementById('<%=txtExpiryDate.ClientID%>').focus()
                        return false;
                    }
                }




                if (document.getElementById('<%=drpPlanType.ClientID%>').value == "0") {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Plan Type";
                    return false;
                }


                var PackageManditory = '<%=Convert.ToString(Session["PackageManditory"]) %>'
                if (PackageManditory == "Y" && document.getElementById('<%=txtNetworkClass.ClientID%>').value == "") {

                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Network Class";
                    return false;
                }

                var DeductibleMandatory = '<%=Convert.ToString(ViewState["DeductibleMandatory"]) %>'
                if (DeductibleMandatory == "1" && document.getElementById('<%=txtDeductible.ClientID%>').value == "" && document.getElementById('<%=drpDedType.ClientID%>').value != "") {

                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Deductible";
                    return false;

                }


            }

            var LatestVisitHours = document.getElementById('<%=hidLatestVisitHours.ClientID%>').value;
            var MultipleVisitADay = document.getElementById('<%=hidMultipleVisitADay.ClientID%>').value;

            var LastVisitDrId = document.getElementById('<%=hidLastVisitDrId.ClientID%>').value;
            var DoctorID = document.getElementById('<%=txtDoctorID.ClientID%>').value;
            document.getElementById('<%=hidWaitingList.ClientID%>').value = true;
            if (document.getElementById('<%=chkUpdateWaitList.ClientID%>').checked == true) {
                if (LastVisitDrId == DoctorID) {
                    if (LatestVisitHours != "" && MultipleVisitADay != "") {

                        if (parseFloat(LatestVisitHours) < 24) {
                            if (parseFloat(LatestVisitHours) < parseFloat(MultipleVisitADay)) {
                                var isUpdateWaitList = window.confirm('This patient is already exist in Patient Waiting List, Do you want update waiting List ? ');
                                document.getElementById('<%=hidWaitingList.ClientID%>').value = isUpdateWaitList;

                            }
                        }
                    }
                }
            }

            var TokenPrint = '<%=Convert.ToString(Session["TokenPrint"]) %>'

            if (TokenPrint == "Y") {
                var isTokenPrint = window.confirm('Do you want to print Token ? ');
                document.getElementById('<%=hidTokenPrint.ClientID%>').value = isTokenPrint;
            }
            else {
                document.getElementById('<%=hidTokenPrint.ClientID%>').value = 'false';

            }


            var LabelPrint = '<%= Convert.ToString( ViewState["LABEL_PRINT"])  %>'
            if (LabelPrint == "1") {
                var isLabelPrint = window.confirm('Do you want to print Label ? ');
                document.getElementById('<%=hidLabelPrint.ClientID%>').value = isLabelPrint;
            }
            else {
                document.getElementById('<%=hidLabelPrint.ClientID%>').value = 'false';

            }




            return PolicyExpCheck();
        }



        function ValScanCardAdd() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=drpCardName.ClientID%>').value == "0") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Card Name";
                document.getElementById('<%=drpCardName.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtPolicyCardNo.ClientID%>').value == "") {
                var PTRegChangePolicyNoCaption = '<%=PTRegChangePolicyNoCaption.ToLower()%>'
                if (PTRegChangePolicyNoCaption == 'true') {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Card No.";
                }
                else {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the policy Number";
                }
                document.getElementById('<%=txtPolicyCardNo.ClientID%>').focus();
                return false;
            }



            if (document.getElementById('<%=txtEffectiveDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtEffectiveDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtEffectiveDate.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtCardExpDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Card Expiry Date";
                document.getElementById('<%=txtCardExpDate.ClientID%>').focus();
                return false;
            }

            if (document.getElementById('<%=txtCardExpDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtCardExpDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtCardExpDate.ClientID%>').focus()
                    return false;
                }
            }

            //return PastDateCheck1()
            // return PolicyExpCheck();


        }
        function Val1() {

            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

        }



        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function RefDrIdSelected() {
            if (document.getElementById('<%=txtRefDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function RefDRNameSelected() {
            if (document.getElementById('<%=txtRefDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }




        function CompIdSelected() {
            if (document.getElementById('<%=txtCompany.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompany.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');


                if (Data1.length > 1) {

                    document.getElementById('<%=txtCompany.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Data1[1];


                }
            }

            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">


        function BindSelectedPlanType(PlanType) {

            document.getElementById("<%=txtPlanType.ClientID%>").value = PlanType;

        }


        function BenefitsExecPopup(strValue) {

            var CompID = document.getElementById('<%=txtCompany.ClientID%>').value;
            var win = window.open("../Masters/BenefitsExeclusions.aspx?PageName=PatientReg&CompanyID=" + CompID + "&PlanType=" + strValue, "newwin", "top=200,left=100,height=750,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }


        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=PatientReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function PatientPopup(PageName, strValue, evt) {

            // if (PageName == 'FileNo') {
            //  document.getElementById('<%=txtFileNo.ClientID%>').value = strValue.toUpperCase()
            //  }

            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;

            if (chCode == 126) {
                // var win = window.open("Firstname_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                // win.focus();
                document.getElementById('<%=hidPopup.ClientID%>').value = 'true';
                return true;

            }
            if (chCode == 33) {
                // var win = window.open("PatientInfo_Zoom.aspx?PagePath=PATIENTREG&PageName=" + PageName + "&Value=" + strValue, "newwin", "top=200,left=270,height=520,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                // win.focus();
                document.getElementById('<%=hidInfoPopup.ClientID%>').value = 'true';
                return false;

            }

            if (PageName == 'Phone' || PageName == 'Mobile') {
                return OnlyNumeric(evt);
            }

            return true;
        }
        function InsurancePopup(PageName, strValue, evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode == 126) {
                var win = window.open("../Masters/Insurance_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
                return false;
            }
            return true;
        }
        function DoctorPopup(PageName, strValue, evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode == 126) {
                var win = window.open("../Masters/Doctor_Name_Zoom.aspx?PageName=" + PageName + "&Value=" + strValue, "newwin1", "top=200,left=270,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
                return false;
            }
            return true;
        }

        function OpenScan() {
            window.open("../EmiratesData.aspx", "newwin", "height=475,width=900,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }


        function WebReportPopup(strValue) {

            var win = window.open("../WebReport/DAMANAuthorizationRequest.aspx?PatientId=" + strValue, "newwin1", "top=80,left=200,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();
        }

        function ScandCardPopup(strValue) {
            var win = window.open("../WebReport/ScandCardPrint.aspx", "newwin1", "top=80,left=100,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();
        }



        function GeneralConsentPopup(strValue, GCID) {
            var win = window.open("../WebReport/GeneralConsentReport.aspx?PatientId=" + strValue + "&GCID=" + GCID, "newwin1", "top=80,left=200,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();
        }


        function HistoryPopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("PatientVisitHistory.aspx?Value=" + PtId, "newwin1", "top=200,left=100,height=500,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function InvoicePopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("PatientInvoicePopup.aspx?Value=" + PtId, "newwin1", "top=80,left=80,height=850,width=1100,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }

        function ReportPopup(RptName, Mode, PrintCount) {

            var win = window.open("../CReports/ReportsView.aspx?ReportName=" + RptName + "&Mode=" + Mode + "&PrintCount=" + PrintCount, "newwin1", "left=200,top=80,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();


        }

        function ReportPrintPopup(RptName, Mode, PrintCount) {

            var win = window.open("../CReports/PrintCrystalReports.aspx?ReportName=" + RptName + "&Mode=" + Mode + "&PrintCount=" + PrintCount, "newwin1", "left=200,top=80,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();


        }

        function GCHistoryPopup() {
            var PtId = document.getElementById('<%=txtFileNo.ClientID%>').value
            var win = window.open("GeneralConsentHistory.aspx?PatientId=" + PtId, "newwin1", "top=200,left=300,height=550,width=900,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();
        }



        function DoUpper(Cont) {
            var strValue;
            strValue = Cont.value;
            Cont.value = strValue.toUpperCase();
        }


        function ShowOnLoad(Report, DBString, Formula, PrintCount) {

            if (Report == "HmsLabel.rpt") {
                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + Formula + '\'$PRINT$$$' + PrintCount);
            }
            else {

                exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${hms_patient_visit.HPV_SEQNO}=\'' + Formula + '\'$PRINT$$$1');

            }


        }

        function ShowClaimPrint(Report, DBString, FileNo, BranchId) {

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\' $SCREEN$$$1');



        }

        function ShowClaimPrintPDF(Report, FileNo, BranchId) {

            //exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\' $SCREEN$$$1');

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }



        function ShowBalance(Report, DBString, FileNo) {

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' $SCREEN$$$1');

        }


        function ShowMRRPrintPDF(FileNo) {

            var Report = "HmsMRRPrint.rpt";

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }


        function ShowGeneConTreatment(Report, DBString, FileNo, BranchId) {

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\' $SCREEN$$$1');



        }

        function ShowIDPrint(Report, DBString, FileNo) {

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' $SCREEN$$$1');

        }

        function ShowGCReport(DBString, GCId) {
            // var Report = "GeneralTreatmentConsent.rpt";
            var Report = "GeneralTreatmentConsent1.rpt";

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${GeneralConsentView.HGC_ID}=' + GCId + ' $SCREEN$$$1');

        }

        function ShowGCReportPDF(FileNo) {
            var Report = "GeneralTreatmentConsent1.rpt";

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowGridGCReportPDF(GCId) {
            var Report = "GeneralTreatmentConsent1.rpt";

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HGC_ID}=' + GCId + '', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }
        function ShowDamanGCReport(DBString, GCId) {

            //  var Report = "DamanConscent.rpt";
            var Report = "MedConsent1.rpt";

            exec('D:\\HMS\\printRpt.exe', '-nomerge 1' + DBString + '$D:\\HMS\\Reports\\' + Report + '${GeneralConsentView.HGC_ID}=' + GCId + ' $SCREEN$$$1');

        }

        function ShowGCReport1(GCId) {
            window.open('../CReports/ReportsView.aspx?ReportName=DamanConscent.rpt&SelectionFormula={GeneralConsentView.HGC_ID}=' + GCId, '_new', 'menubar=no,left=200,top=80,height=850,width=1075,scrollbars=1')

        }

        function ShowDamanGCReportPDF(FileNo) {
            var Report = "MedConsent1.rpt";

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowConsentFormReportPDF(FileNo) {
            //var Report = "MedConsent1.rpt";

            var Report = document.getElementById('<%=drpConsentForm.ClientID%>').value + ".rpt"

            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }


        function ShowSignature(DBString, FileName, BranchId) {
            var SignaturePad = document.getElementById('<%=hidSignaturePad.ClientID%>').value
            if (SignaturePad == 'GENIUS') {
                exec('D:\\HMS\\GeniusSignature\\HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
            }
            else if (SignaturePad == 'WACOM') {
                exec('D:\\HMS\\WacomSignature\\DemoButtons.exe', DBString + '$' + FileName + '$' + BranchId);
            }
            else {
                exec('D:\\HMS\\GeniusSignature\\HMSSignature.exe', DBString + '$' + FileName + '$' + BranchId);
            }

        }


        function GetEmiratesIdData(DBString, FileName, BranchId) {

            exec('D:\\HMS\\EmiratesId\\EmiratesID.exe', DBString + '$' + FileName + '$' + BranchId);

        }




        function exec(cmdline, params) {

            var shell = new ActiveXObject("WScript.Shell");
            if (params) {
                params = ' ' + params;
            }
            else {
                params = '';
            }
            shell.Run('"' + cmdline + '"' + params);
            //}
        }

    </script>




    <style type="text/css">
        .style1
        {
            width: 50%;
        }

        .style3
        {
            width: 100%;
            height: 600px;
        }

        .style4
        {
            width: 157px;
        }

        #Text1
        {
            width: 135px;
        }

        .style5
        {
            width: 450px;
        }

        .style6
        {
            width: 157px;
            height: 23px;
        }

        .style8
        {
            height: 23px;
            margin-left: 560px;
        }

        #Text2
        {
            width: 135px;
        }

        #Text3
        {
            width: 135px;
        }

        #Text4
        {
            width: 135px;
        }

        #Text5
        {
            width: 135px;
        }

        #Text6
        {
            width: 135px;
        }

        #Text7
        {
            width: 135px;
        }

        #Text8
        {
            width: 435px;
        }

        #Text9
        {
            width: 135px;
        }

        #Text10
        {
            width: 135px;
        }

        #Text11
        {
            width: 135px;
        }

        #Text12
        {
            width: 135px;
        }

        #Text13
        {
            width: 135px;
        }

        #Text14
        {
            width: 135px;
        }

        #Text15
        {
            width: 135px;
        }

        #Text16
        {
            width: 135px;
        }

        .style14
        {
        }

        #Text17
        {
            width: 135px;
        }

        #Text18
        {
            width: 135px;
        }

        #Text19
        {
            width: 135px;
        }

        #Text20
        {
            width: 135px;
        }

        #Text21
        {
            width: 135px;
        }

        .style16
        {
            height: 23px;
            width: 140px;
        }

        .style17
        {
        }

        #Text22
        {
            width: 135px;
        }

        #Text23
        {
            width: 300px;
        }

        #Text24
        {
            width: 135px;
        }

        #Text25
        {
            width: 135px;
        }

        #Text26
        {
            width: 135px;
        }

        #Text27
        {
            width: 135px;
        }

        .style18
        {
            width: 157px;
            height: 19px;
        }

        .style20
        {
            height: 19px;
            width: 119px;
        }

        .style21
        {
            width: 184px;
            height: 19px;
        }

        .style22
        {
            height: 19px;
        }

        #Text28
        {
            width: 135px;
        }

        #Text29
        {
            width: 135px;
        }

        #Text30
        {
            width: 135px;
        }

        #Text31
        {
            width: 135px;
        }

        #Text32
        {
            width: 135px;
        }

        #Text33
        {
            width: 245px;
        }

        #Text34
        {
            width: 300px;
        }

        .style24
        {
            width: 100%;
            height: 200px;
        }

        .style25
        {
            width: 135px;
        }

        .style26
        {
            width: 169px;
        }

        .style27
        {
            width: 144px;
        }

        #TextArea1
        {
            width: 320px;
            height: 160px;
        }

        .style28
        {
            width: 60%;
            height: 221px;
        }

        .style29
        {
            width: 328px;
        }

        .style30
        {
            width: 323px;
        }

        #TextArea2
        {
            width: 660px;
        }

        .style32
        {
            width: 99%;
            height: 30px;
        }

        .style33
        {
            width: 119px;
            margin-left: 40px;
        }

        .style34
        {
            width: 184px;
        }

        .style35
        {
            width: 186px;
            margin-left: 320px;
        }

        .style36
        {
            width: 186px;
            height: 19px;
        }

        .style37
        {
            width: 140px;
        }

        .style38
        {
            width: 156px;
        }

        .style39
        {
            width: 69px;
        }

        .style40
        {
            width: 87px;
        }

        .style41
        {
            width: 295px;
        }

        .style42
        {
            width: 100%;
        }

        .auto-style5
        {
            width: 330px;
        }

        .auto-style10
        {
            width: 37px;
        }

        .auto-style12
        {
            width: 70px;
        }

        .auto-style14
        {
            width: 59px;
        }

        .auto-style15
        {
            width: 41px;
        }

        .auto-style23
        {
            width: 76px;
        }

        .auto-style26
        {
            width: 295px;
            margin-left: 40px;
            height: 40px;
        }

        .auto-style27
        {
            width: 263px;
        }

        .auto-style28
        {
            width: 263px;
            margin-left: 320px;
        }

        .auto-style32
        {
            width: 577px;
        }

        .auto-style33
        {
            width: 577px;
            height: 40px;
        }

        .auto-style34
        {
            width: 263px;
            height: 40px;
        }

        .auto-style35
        {
            width: 295px;
            height: 40px;
        }

        .auto-style36
        {
            height: 40px;
        }

        .auto-style37
        {
            width: 577px;
            height: 31px;
        }

        .auto-style38
        {
            width: 263px;
            height: 31px;
        }

        .auto-style39
        {
            width: 295px;
            height: 31px;
        }

        .auto-style40
        {
            height: 31px;
        }

        .auto-style41
        {
            width: 140px;
            height: 31px;
        }

        .auto-style42
        {
            width: 577px;
            height: 34px;
        }

        .auto-style43
        {
            width: 263px;
            height: 34px;
        }

        .auto-style44
        {
            width: 295px;
            height: 34px;
        }

        .auto-style45
        {
            height: 34px;
        }

        .auto-style46
        {
            width: 577px;
            height: 39px;
        }

        .auto-style47
        {
            width: 263px;
            height: 39px;
        }

        .auto-style48
        {
            width: 295px;
            height: 39px;
        }

        .auto-style49
        {
            height: 39px;
        }

        .auto-style50
        {
            width: 330px;
            height: 40px;
        }
    </style>


    <script language="javascript" type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        jQuery(document).ready(function () {
            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
            // alert(today);
        });
    </script>

</asp:Content>

<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

  
    <table >
        <tr>
            <td class="PageHeader">Patient Registration
               
            </td>
        </tr>
    </table>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEFilling" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </td>
            </tr>
        </table>
     <div  style="padding:5px; width: 100%;   overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
        <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">


            <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" Patient Details-1 " Width="100%">
                <contenttemplate>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                           <input type="hidden" id="hidPermission" runat="server" value="9" />
                            <asp:HiddenField ID="hidTokenPrint" runat="server" />
                            <asp:HiddenField ID="hidWaitingList" runat="server" />
                            <asp:HiddenField ID="hidLabelPrint" runat="server" />
                            <asp:HiddenField ID="Year" runat="server" />
                            <asp:HiddenField ID="hidTodayDate" runat="server" />
                            <asp:HiddenField ID="hidTodayTime" runat="server" />
                            <asp:HiddenField ID="hidTodayDBDate" runat="server" />
                            <asp:HiddenField ID="hidLatestVisit" runat="server" />
                            <asp:HiddenField ID="hidLatestVisitHours" runat="server" />
                              <asp:HiddenField ID="hidLastVisitDrId" runat="server" />
                            <asp:HiddenField ID="hidMultipleVisitADay" runat="server" />
                            <asp:HiddenField ID="hidPopup" runat="server" />
                             <asp:HiddenField ID="hidInfoPopup" runat="server" />
                            <asp:HiddenField ID="hidImgFront" runat="server" />
                            <asp:HiddenField ID="hidHospitalName" runat="server" />
                            <asp:HiddenField ID="hidPTVisitSQNo" runat="server" />
                            <asp:HiddenField ID="hidActualFileNo" runat="server" />
                            <asp:HiddenField ID="hidLocalDate" runat="server" />
                            <asp:HiddenField ID="hidSignaturePad" runat="server" value="WACOM" />

                            <asp:HiddenField ID="hidPateName" runat="server" value="" />

                            <table border="0" cellpadding="5" cellspacing="5" style="width: 100%">
                                <tr>
                                    <td colspan="6">
                                        <asp:Label ID="Label1" runat="server" CssClass="lblCaption1" Text="General Information" Font-Bold="True"></asp:Label>
                                       
                                          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;<asp:Label ID="lblHomeCarePT" runat="server" CssClass="lblCaption1" style="color:red;font-weight:bold;" Text="Home Care Patient" visible="false"  ></asp:Label>
                                       
                                    </td>

                                </tr>
                                <tr>
                                    <td style="border: solid 1px #4fbdf0;" colspan="6"></td>

                                </tr>
                                <tr>
                                    <td style="height: 5px;" colspan="6">
                                    </td>

                                </tr>
                                <tr>
                                    <div id="divDept" runat="server" visible="false">
                                    <td  class="lblCaption1" style="height:40px;" >
                                        Department
                                    </td>
                                    <td colspan="5">
                                        <asp:DropDownList ID="drpDepartment" runat="server" CssClass="label" Width="150px" ></asp:DropDownList>
                                    </td>
                                        </div>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="btnNew" runat="server" style="padding-left:5px;padding-right:5px;" Text="New" Visible="false" CssClass="button orange small" Width="50px" OnClick="btnNew_Click" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnNew" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td valign="middle">
                                        <asp:Button ID="bntFamilyApt" runat="server" style="padding-left:5px;padding-right:5px;width:110px;"  Text="Check Family Appt." CssClass="button orange small" OnClick="bntFamilyApt_Click" />
                                    </td>
                                    <td  width="120px" valign="middle" >
                                        <asp:ImageButton ID="btnScan" runat="server" ImageUrl="~/Images/View.jpg" ToolTip="Emirate Id Scan" Height="20PX" OnClick="btnScan_Click" />
                                        <asp:Button ID="btnEmIdShow" runat="server" style="padding-left:5px;padding-right:5px;width:100px;"   Text="Show Emirates ID" ToolTip="Show Emirates Id Details" Visible="true"  CssClass="button orange small" OnClick="btnEmIdShow_Click" />


                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkStatus" runat="server" CssClass="lblCaption1" Text="Active" Checked="true" />

                                    </td>
                                    <td width="120px" >
                                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                            <ContentTemplate>
                                                <asp:CheckBox ID="chkCompCashPT" runat="server" CssClass="lblCaption1"
                                                    Text="Comp. Cash PT" width="120px"  OnCheckedChanged="CheckBox7_CheckedChanged" Checked="false" AutoPostBack="true" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td valign="middle" class="auto-style5" rowspan="4">
                                        <asp:Image ID="imgPhoto" runat="server" Height="120px" Width="100px" Visible="false" />
                                        <asp:Image ID="imgPhoto1" runat="server" Height="120px" Width="100px" Visible="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style32">
                                        <asp:Label ID="lblVisitType" runat="server" CssClass="lblCaption1" Text="New" ForeColor="Maroon"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" CssClass="lblCaption1" Text="File No"></asp:Label>


                                    </td>
                                    <td class="auto-style27">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtFileNo" runat="server" MaxLength="10" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" CssClass="label" Width="70px" AutoPostBack="true"  onkeypress="return PatientPopup('FileNo',this.value,event)" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>
                                                <asp:CheckBox ID="chkManual" runat="server" CssClass="lblCaption1" OnCheckedChanged="chkManual_CheckedChanged"
                                                    Text="Manual No" AutoPostBack="True" Checked="false" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="txtFileNo" />
                                            </Triggers>
                                        </asp:UpdatePanel>


                                    </td>
                                    <td >

                                        <asp:Label ID="Label3" runat="server" CssClass="lblCaption1" Text="Patient Type"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="drpPatientType" runat="server" CssClass="label" Width="85px" BorderWidth="1px" BorderColor="red" AutoPostBack="true" OnSelectedIndexChanged="drpPatientType_SelectedIndexChanged">
                                            <asp:ListItem Value="CA">Cash</asp:ListItem>
                                            <asp:ListItem Value="CR">Credit</asp:ListItem>
                                        </asp:DropDownList>

                                    </td>



                                    <td>
                                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                            <ContentTemplate>
                                                <asp:CheckBox ID="chkUpdateWaitList" runat="server" CssClass="lblCaption1" Checked="true"
                                                    Text="Update Waiting List" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel41" runat="server">
                                            <ContentTemplate>
                                        <asp:CheckBox ID="chkFamilyMember" runat="server" width="120px"  CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkFamilyMember_CheckedChanged"
                                            Text="Family Member"  />
                                                </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td></td>


                                </tr>
                                <tr>
                                    <td class="auto-style33">
                                        <asp:Label ID="Label4" runat="server" CssClass="lblCaption1" Text="Name"></asp:Label>


                                    </td>
                                    <td class="auto-style34" colspan="4">
                                        <asp:DropDownList ID="drpTitle" runat="server" CssClass="label" Width="70px" BorderWidth="1px" BorderColor="#e3f7ef" visible="false"  >
                                        </asp:DropDownList>
                                                                         
                                          <asp:TextBox ID="txtFName" MaxLength="50" runat="server" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="200px"  onkeypress="return PatientPopup('Name',this.value,event)"  ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>

                                   
                                        <asp:TextBox ID="txtMName" MaxLength="50" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="200px"  onkeypress="return PatientPopup('Name',this.value,event)"   ondblclick="return PatientPopup1('Name',this.value);"></asp:TextBox>

                                    

                                       <asp:TextBox ID="txtLName" MaxLength="50" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="200px"  onkeypress="return PatientPopup('Name',this.value,event)"   ondblclick="return PatientPopup1('LName',this.value);"></asp:TextBox>


                                    </td>
                                    <td class="auto-style36"></td>


                                </tr>

                                <tr>
                                    <td class="auto-style37">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblCaption1" Text="DOB"></asp:Label>
                                    </td>
                                    <td class="auto-style38">
                                        <asp:TextBox ID="txtDOB" runat="server" Width="75px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onblur="AgeCalculation()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtDOB" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtDOB" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                        <asp:Label ID="Label120" runat="server" CssClass="label" Text="Age"></asp:Label>
                                        <asp:TextBox ID="txtAge" runat="server" Width="20px" MaxLength="3" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="DBOcalculation()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:TextBox ID="txtMonth" runat="server" Width="20px" MaxLength="2" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>

                                    </td>
                                    <td class="lblCaption1">Sex &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="drpSex" runat="server" CssClass="label" Width="130px" BorderWidth="1px" BorderColor="red">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                        <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                        <asp:ListItem Text="Not Specified" Value="Not Specified"></asp:ListItem>
                                    </asp:DropDownList>


                                    </td>

                                    <td  class="lblCaption1">
                                       P O Box&nbsp;&nbsp;&nbsp;</asp:Label><asp:TextBox ID="txtPoBox" runat="server" MaxLength="50" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove"
                                            onkeypress="return PatientPopup('POBox',this.value,event)" Width="100px"   ondblclick="return PatientPopup1('POBox',this.value);"></asp:TextBox>

                                    </td>


                                    <td >
                                    </td>
                                    <td class="auto-style41"></td>
                                </tr>
                                <tr>
                                    <td class="auto-style42">
                                        <asp:Label ID="Label8" runat="server" CssClass="lblCaption1" Text="Address"></asp:Label>
                                    </td>
                                    <td class="auto-style43">
                                        <asp:TextBox ID="txtAddress" MaxLength="150" CssClass="label" BorderColor="#cccccc" BorderWidth="1px" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                    <td >
                                        <asp:DropDownList ID="drpIDCaption" runat="server" CssClass="lblCaption1" BorderWidth="1px" BorderColor="#cccccc" Width="168px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style45">
                                        <asp:TextBox ID="txtEmiratesID" runat="server" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" MaxLength="20" Width="150px" onkeypress="return PatientPopup('EmiratesID',this.value,event)"  ondblclick="return PatientPopup1('EmiratesID',this.value);" ></asp:TextBox>
                                    </td>
                                    <td >
                                        <asp:Label ID="Label9" runat="server" CssClass="lblCaption1" Text="City"></asp:Label>

                                    </td>
                                    <td class="auto-style45">
                                        <asp:DropDownList ID="drpCity" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="155px">
                                        </asp:DropDownList></td>

                                </tr>
                                <tr>
                                    <td class="auto-style42">
                                        <asp:Label ID="Label12" runat="server" CssClass="lblCaption1" Text="Area"></asp:Label>
                                    </td>
                                    <td class="auto-style43">
                                        <asp:DropDownList ID="drpArea" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                    <td >
                                        <asp:Label ID="Label10" runat="server" CssClass="lblCaption1" Text="Country"></asp:Label>
                                    </td>
                                    <td class="auto-style45">
                                        <asp:DropDownList ID="drpCountry" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="155px">
                                        </asp:DropDownList>
                                    </td>
                                    <td >
                                        <asp:Label ID="Label13" runat="server" CssClass="lblCaption1" Text="Nationality"></asp:Label>
                                    </td>
                                    <td class="auto-style45">
                                        <asp:DropDownList ID="drpNationality" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="155px">
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style33">
                                        <asp:Label ID="Label11" runat="server" CssClass="lblCaption1" Text="Blood"></asp:Label>
                                    </td>
                                    <td class="auto-style34">
                                        <asp:DropDownList ID="drpBlood" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="200px">
                                            <asp:ListItem Selected="True" Text="Select" Value="" />
                                            <asp:ListItem>A +ve</asp:ListItem>
                                            <asp:ListItem>A -ve</asp:ListItem>
                                            <asp:ListItem>B +ve</asp:ListItem>
                                            <asp:ListItem>B -ve</asp:ListItem>
                                            <asp:ListItem>O +ve</asp:ListItem>
                                            <asp:ListItem>O -ve</asp:ListItem>
                                            <asp:ListItem>AB +ve</asp:ListItem>
                                            <asp:ListItem>AB -ve</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td >
                                        <asp:Label ID="Label14" runat="server" CssClass="lblCaption1" Text="Marital Status"></asp:Label>
                                    </td>
                                    <td class="auto-style36">
                                        <asp:DropDownList ID="drpMStatus" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="155px">
                                            <asp:ListItem Selected="True" Text="Select" Value="" />
                                            <asp:ListItem>Single</asp:ListItem>
                                            <asp:ListItem>Married</asp:ListItem>
                                            <asp:ListItem>Widowed</asp:ListItem>
                                            <asp:ListItem>Divorced</asp:ListItem>
                                            <asp:ListItem>Not Specified</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label48" runat="server" CssClass="lblCaption1" Text="Fax"></asp:Label>
                                    </td>
                                    <td class="auto-style36">
                                        <asp:TextBox ID="txtFax" runat="server" CssClass="label" BorderWidth="1" BorderColor="#cccccc" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style46">
                                        <asp:Label ID="Label15" runat="server" CssClass="lblCaption1" Text="Home PH. No."></asp:Label>
                                    </td>
                                    <td class="auto-style47">
                                        <asp:TextBox ID="txtPhone1" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="200px" MaxLength="50" onkeypress="return PatientPopup('Phone',this.value,event)"  ondblclick="return PatientPopup1('Phone1',this.value);" ></asp:TextBox>
                                    </td>
                                    <td >
                                        <asp:Label ID="lblMobile1" runat="server" CssClass="lblCaption1" Text="Mobile No1"></asp:Label>
                                    </td>
                                    <td class="auto-style49">
                                        <asp:TextBox ID="txtMobile1" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="red" BorderWidth="1" CausesValidation="True" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Mobile',this.value,event)" AutoPostBack="true" OnTextChanged="txtMobile1_TextChanged" ondblclick="return PatientPopup1('Mobile1',this.value);"></asp:TextBox>
                                    </td>
                                    <td >
                                        <asp:Label ID="lblMobile2" runat="server" CssClass="lblCaption1" Text="Mobile No2"></asp:Label>
                                    </td>
                                    <td class="auto-style49">
                                        <asp:TextBox ID="txtMobile2" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="150px" MaxLength="50" onkeypress="return PatientPopup('Mobile',this.value,event)"   ondblclick="return PatientPopup1('Mobile2',this.value);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style46">
                                        <asp:Label ID="Label16" runat="server" CssClass="lblCaption1" Text="Office PH. No."></asp:Label>
                                    </td>
                                    <td class="auto-style47">
                                        <asp:TextBox ID="txtPhone3" runat="server" CssClass="label"  BorderStyle="Groove" Width="200px" MaxLength="50"  ></asp:TextBox>
                                    </td>
                                    <td >
                                        <asp:Label ID="Label44" runat="server" CssClass="lblCaption1" Text="Email"></asp:Label>
                                    </td>
                                    <td colspan="3" >
                                        <asp:TextBox ID="txtEmail" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                 
                                </tr>
                                 <tr>
                                   
                                     <td style="height: 30px;">
                                        <asp:Label ID="Label71" runat="server" CssClass="lblCaption1" Text="Occupation"></asp:Label>
                                    </td>
                                    <td >
                                        <asp:TextBox ID="txtOccupation" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="200px" ></asp:TextBox>

                                    </td>
                                   <div id="divKnowFrom" runat="server">
                                        <td style="height: 30px;">
                                            <asp:Label ID="Label87" runat="server" CssClass="lblCaption1"
                                                Text="Know From"></asp:Label>
                                        </td>

                                        <td>
                                            <asp:DropDownList ID="drpKnowFrom" runat="server" CssClass="label"  BorderWidth="1" BorderColor="#cccccc" Width="150px"></asp:DropDownList>
                                        </td>
                                    </div>

                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label19" runat="server" CssClass="lblCaption1"
                                            Text="Doctor  Information" Font-Bold="True"></asp:Label>
                                    </td>

                                    <td colspan="2"></td>

                                    <td colspan="2"></td>

                                </tr>
                                <tr>
                                    <td style="border: solid 1px #4fbdf0;" colspan="6"></td>

                                </tr>
                                <tr>
                                    <td style="height: 5px;" colspan="6"></td>

                                </tr>
                                <tr>
                                    <td class="auto-style32">
                                        <asp:Label ID="Label20" runat="server" CssClass="lblCaption1" Text="Doctor ID"></asp:Label>
                                    </td>
                                    <td class="auto-style28" colspan="2">
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                           <div ID="divDr" style="visibility:hidden;"></div>           
                           <asp:TextBox ID="txtDoctorID" runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#CCCCCC" Width="70px" AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()"  ></asp:TextBox>
                           <asp:TextBox ID="txtDoctorName" runat="server" CssClass="label" BorderWidth="1px"  BorderColor="#CCCCCC"    Width="275px"  AutoPostBack="true" OnTextChanged="txtDoctorName_TextChanged" MaxLength="50" onblur="return DRNameSelected()"  ></asp:TextBox>
                           <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"
                                ></asp:AutoCompleteExtender>
                           <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName" 
                                 ></asp:AutoCompleteExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>

                                    <td>
                                        <asp:TextBox ID="txtDepName" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="150px" Enabled="false" MaxLength="50"></asp:TextBox>
                                    </td>
                                    <div id="divToken" runat="server">
                                        <td>
                                            <asp:Label ID="Label21" runat="server" CssClass="lblCaption1" Text="Token No:"></asp:Label>
   
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtTokenNo" runat="server" CssClass="label" BorderWidth="1" BorderColor="#cccccc" Width="150px" MaxLength="5" Enabled="false"></asp:TextBox><asp:CheckBox ID="chkManualToken" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnCheckedChanged="chkManualToken_CheckedChanged"
                                                Text="Manual" Width="160px" />

                                        </td>
                                    </div>

                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label23" runat="server" CssClass="lblCaption1" Text="Ref. Doctor"></asp:Label>
                                    </td>
                                    <td colspan="5">

                                          <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                            <ContentTemplate>
                                       <asp:TextBox ID="txtRefDoctorID" runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#CCCCCC" Width="70px"   onblur="return RefDRIdSelected()"  ></asp:TextBox>
                                       <asp:TextBox ID="txtRefDoctorName" runat="server" CssClass="label" BorderWidth="1px"  BorderColor="#CCCCCC"    Width="275px"    MaxLength="50" onblur="return RefDRNameSelected()"  ></asp:TextBox>
                                       <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="Server" TargetControlID="txtRefDoctorID" MinimumPrefixLength="1" ServiceMethod="RefGetDoctorId"
                                            ></asp:AutoCompleteExtender>
                                       <asp:AutoCompleteExtender ID="AutoCompleteExtender9" runat="Server" TargetControlID="txtRefDoctorName" MinimumPrefixLength="1" ServiceMethod="RefGetDoctorName" 
                                             ></asp:AutoCompleteExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <div id="divPriority" runat="server">
                                    <tr>
                                        <td class="auto-style32">
                                            <asp:Label ID="Label86" runat="server" CssClass="lblCaption1" Text="Priority"></asp:Label>
                                        </td>
                                        <td colspan="5">
                                            <asp:DropDownList ID="drpPriority" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                </div>
                                <div id="divOtherInfo" runat="server" visible="false">
                                    <tr>
                                        <td colspan="6">
                                            <asp:Label ID="Label22" runat="server" CssClass="lblCaption1"
                                                Text="Other Information" Font-Bold="True"></asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="border: solid 1px #4fbdf0;" colspan="6"></td>

                                    </tr>
                                    <tr>
                                        <td style="height: 5px;" colspan="6"></td>

                                    </tr>
                                    <tr>
                                        <td class="auto-style33">
                                           
                                        </td>
                                        <td class="auto-style34">
                                           
                                        </td>
                                        <td class="auto-style35">
                                            <asp:Label ID="Label27" runat="server" CssClass="lblCaption1" Text="Kin Name"></asp:Label>
                                        </td>
                                        <td class="auto-style36">
                                            <asp:TextBox ID="txtKinName" runat="server" CssClass="label" BorderWidth="1" BorderColor="#cccccc" Width="150px" ></asp:TextBox>
                                        </td>
                                        <td >
                                            <asp:Label ID="Label24" runat="server" CssClass="lblCaption1" Text="Kin's Relation"></asp:Label>
                                        </td>
                                        <td class="auto-style36">
                                            <asp:TextBox ID="txtKinRelation" runat="server" CssClass="label" BorderWidth="1" BorderColor="#cccccc" Width="150px" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style32">
                                            <asp:Label ID="Label28" runat="server" CssClass="lblCaption1" Text="Father Mob.No."></asp:Label>
                                        </td>
                                        <td class="auto-style27">
                                            <asp:TextBox ID="txtKinPhone" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="200px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </td>
                                        <td class="style41">
                                            <asp:Label ID="Label25" runat="server" CssClass="lblCaption1" Text="Mother Mob.No"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtKinMobile" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label29" runat="server" CssClass="lblCaption1" Text=" No Of Children"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNoOfChildren" CssClass="label" BorderWidth="1" BorderColor="#cccccc" MaxLength="2" runat="server" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                       <td  style="height:30px;">
                                        <asp:Label ID="lblMobile3" runat="server" CssClass="lblCaption1" Text="Other  Mob.No."></asp:Label>
                                    </td>
                                    <td colspan="5">
                                        <asp:TextBox ID="txtKinMobile1" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                    </tr>
                                </div>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label30" runat="server" CssClass="lblCaption1"
                                            Text="Insurance Details" Font-Bold="True"></asp:Label>
                                    </td>

                                    <td colspan="4"></td>

                                </tr>
                                <tr>
                                    <td style="border: solid 1px #4fbdf0;" colspan="6"></td>

                                </tr>
                                <tr>
                                    <td style="height: 5px;" colspan="6"></td>

                                </tr>
                                <tr>
                                    <td style="height: 10px;" class="lblCaption1">Company
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpDefaultCompany" CssClass="label" runat="server" BorderWidth="1px" BorderColor="red" class="label" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="drpDefaultCompany_SelectedIndexChanged"></asp:DropDownList>

                                    </td>
                                    <td class="lblCaption1" runat="server" id="tdCaptionNetwork">Network
                                    </td>
                                    <td runat="server" id="tdNetwork">
                                        <asp:DropDownList ID="drpNetworkName" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="155px" AutoPostBack="true" OnSelectedIndexChanged="drpNetworkName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" CssClass="lblCaption1" Text="Plan Type"></asp:Label>
                                        <asp:ImageButton ID="btnHCBShow" runat="server" ImageUrl="~/Images/zOOM.PNG" ToolTip="Category Lookup" Height="20PX" Width="25PX"   OnClick="btnHCBShow_Click" />
                                         
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpPlanType" CssClass="label" BorderWidth="1px" BorderColor="red" runat="server" Width="155px"  Enabled="true" >
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr id="trPayerID" runat="server">
                                   
                                     <td style="height: 30px;">
                                        <asp:Label ID="lblPTCompany" runat="server" CssClass="lblCaption1"
                                            Text="Payer "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                                   <asp:DropDownList ID="drpPayer" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="200px" >
                                                 </asp:DropDownList>
                                                <asp:TextBox ID="txtPTCompany" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="200px"  visible="false"></asp:TextBox>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtPTCompany" MinimumPrefixLength="1" ServiceMethod="GetPatientCompany"></asp:AutoCompleteExtender>

                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="style41">
                                        <asp:Label ID="Label36" runat="server" CssClass="lblCaption1" Text="ID"></asp:Label>
                                    </td>
                                    <td class="auto-style50">
                                        <asp:TextBox ID="txtPolicyId" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                  
                                        <td>

                                            <asp:Label ID="lblNetworkClass" runat="server" CssClass="lblCaption1" Text="Network"></asp:Label>

                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtNetworkClass" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender7" runat="Server" TargetControlID="txtNetworkClass" MinimumPrefixLength="1" ServiceMethod="GetGeneralClass"></asp:AutoCompleteExtender>
                                                </ContentTemplate>

                                            </asp:UpdatePanel>

                                        </td>
                                   
                                </tr>
                                <tr>
                                     <td style="height: 30px;">
                                        <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1" Text="Policy No. &nbsp"></asp:Label>

                                    </td>
                                    <td class="auto-style27">
                                        <asp:TextBox ID="txtPolicyNo" CssClass="label" runat="server" Enabled="false" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="red" Width="200px" MaxLength="30" onkeypress="return PatientPopup('PolicyNo',this.value,event)"  ondblclick="return PatientPopup1('PolicyNo',this.value);"></asp:TextBox>
                                    </td>
                                   
                                    <div id="divDeductCoIns" runat="server">
                                        <td class="style41" style="height:40px;">

                                            <asp:Label ID="Label47" runat="server" CssClass="lblCaption1" Text="Deductible & Co-pay"></asp:Label>
                                        </td>
                                        <td class="auto-style27">

                                            <asp:DropDownList ID="drpDedType" runat="server" CssClass="label" BorderWidth="1" BorderColor="#cccccc">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtDeductible" runat="server" Width="50px" height="23px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


                                        </td>
                                        <td>
                                            <asp:Label ID="Label81" runat="server" CssClass="lblCaption1" Text="Co-Insurance"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpCoInsType" runat="server" CssClass="label" BorderWidth="1" BorderColor="#cccccc">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtCoIns" runat="server" Width="50px" height="23px" CssClass="label" BorderWidth="1" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </td>
                                    </div>
                                    
                                </tr>
                                <tr>
                                   
                                      <td style="height: 30px;">
                                        <asp:Label ID="Label100" runat="server" CssClass="lblCaption1" Text="Effective Date"></asp:Label>

                                    </td>
                                    <td class="auto-style27">
                                        <asp:TextBox ID="txtPolicyStart" runat="server" Enabled="false" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px" onblur="PastDateCheck()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender7" runat="server"
                                            Enabled="True" TargetControlID="txtPolicyStart" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                    </td>
                                    <td class="style41">
                                        <asp:Label ID="Label33" runat="server" CssClass="lblCaption1" Text=" Policy No"></asp:Label>

                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtIDNo" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <tr>
                                     <td style="height: 30px;">
                                        <asp:Label ID="Label35" runat="server" CssClass="lblCaption1" Text="Exp Date"></asp:Label>

                                    </td>
                                    <td class="auto-style27">
                                        <asp:TextBox ID="txtExpiryDate" runat="server" Enabled="false" CssClass="label" BorderWidth="1px" BorderColor="red" Width="200px" onblur="PastDateCheck()" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtExpiryDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                    </td>


                                </tr>
                               
                                <tr>
                                    <td>

                                    </td>
                                    <td >
                                          <asp:DropDownList ID="drpConsentForm"  BorderColor="#e3f7ef" BorderStyle="Groove" CssClass="label" runat="server" Width="100px" Visible="true">
                                                            </asp:DropDownList>
                                        <asp:Button ID="btnConsentFormPrint" runat="server" style="padding-left:5px;padding-right:5px;width:100px;"  Visible="true" CssClass="button orange small"   Text="Consent Form" OnClick="btnConsentFormPrint_Click" />

                                        <asp:Button ID="btnDamanGC" runat="server" style="padding-left:5px;padding-right:5px;width:100px;" Visible="false"   CssClass="button orange small"   Text="Daman Consent" OnClick="btnDamanGC_Click" />
                                        <asp:Button ID="btnGeneConTreatment" runat="server" style="padding-left:5px;padding-right:5px;width:100px;"  Visible="false" CssClass="button orange small"   Text="Gen. Consent" OnClick="btnGeneConTreatment_Click" />
                                    </td>
                                   
                                    <td  >
                                    </td>
                                    <td>
                                        <asp:Button ID="btnIDPrint" Visible="true" runat="server" style="padding-left:5px;padding-right:5px;width:70px;"  CssClass="button orange small" Width="100px" Text="ID Print" OnClick="btnIDPrint_Click" />

                                        <asp:Button ID="btnMRRPrint" runat="server" style="padding-left:5px;padding-right:5px;width:70px;"  CssClass="button orange small"   Text="MRR Print" OnClick="btnMRRPrint_Click" />

                                    </td>
                                    <td>


                                    </td>
                                    <td>
                                        <asp:Button ID="btnHistory" runat="server" style="padding-left:5px;padding-right:5px;width:70px;"  CssClass="button orange small"   Height="25px" Text="Prev.Visit" OnClientClick="HistoryPopup();" />
                                        <asp:Button ID="btnBalance" Visible="true" runat="server" style="padding-left:5px;padding-right:5px;width:70px;"  CssClass="button orange small" Width="100px" Text="Inv. Dtls"  OnClientClick="InvoicePopup();" />


                                   </td>
                                </tr>
                               
                                <tr>
                                    <td colspan="1">
                                        <asp:Label ID="Label6" runat="server" CssClass="lblCaption1"
                                            Text="Visit Details" Font-Bold="True"></asp:Label>
                                    </td>

                                    <div id="divCunsult" runat="server" visible="false">
                                    <td colspan="5" class="lblCaption1" style="color:#0094ff">
                                       Last Consultation Date:&nbsp;
                                         <asp:Label ID="lblLastCunsultDate" runat="server" CssClass="lblCaption1" Font-Bold="true" ></asp:Label>
                                       &nbsp;&nbsp;&nbsp; Amount:&nbsp;
                                         <asp:Label ID="lblLastCunsultAmount" runat="server" CssClass="lblCaption1" Font-Bold="true" ></asp:Label>

                                    </td>
                                    </div>
                                </tr>
                                <tr>
                                    <td style="border: solid 1px #4fbdf0;" colspan="6"></td>

                                </tr>
                            </table>
                            <asp:LinkButton ID="lnkRemMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkRemMessage"
                                PopupControlID="pnlRemMessage" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
                                PopupDragHandleControlID="pnlRemMessage">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlRemMessage" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                            <%=strReminderMessage %>
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>


                            <asp:LinkButton ID="lnkMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="lnkMessage"
                                PopupControlID="pnlPrevPrice" BackgroundCssClass="modalBackground" CancelControlID="btnClose"
                                PopupDragHandleControlID="pnlPrevPrice">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlPrevPrice" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                            <%=strMessage %>
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>
                              <asp:LinkButton ID="lnkHCBMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="ModalPopupExtender7" runat="server" TargetControlID="lnkHCBMessage"
                                PopupControlID="pnlHCB" BackgroundCssClass="modalBackground" CancelControlID="btnHCBClose"
                                PopupDragHandleControlID="pnlHCB">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlHCB" runat="server" Height="330px" Width="680px" CssClass="modalPopup"  >
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnHCBClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 230px;">
                                            

                                            <div style="padding-top: 0px; width: 650px; height: 275px; overflow: auto;border-color:#e3f7ef;border-style:groove;" >

                                                <asp:GridView ID="gvHCB" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"  
                                                    EnableModelValidation="True" Width="99%"    PageSize="50">
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <RowStyle CssClass="GridRow" />
                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                    <Columns>


                                                        <asp:TemplateField HeaderText="Category" >
                                                            <ItemTemplate>
                                                                 
                                                                    <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCBD_CATEGORY") %>'></asp:Label>
                                                                 
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Category Type" >
                                                            <ItemTemplate>
                                                                 
                                                                    <asp:Label ID="lblCat" runat="server" Text='<%# Bind("HCB_CATEGORY_TYPEDesc") %>'></asp:Label>
                                                                 
                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Limit">
                                                            <ItemTemplate>
                                                                    <asp:Label ID="lblLimit" runat="server" Text='<%# Bind("HCBD_LIMIT") %>' Style="text-align: right; padding-right: 5px;"  Width="100%"></asp:Label>
                                                             
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Type" >
                                                            <ItemTemplate>
                                                                    <asp:Label ID="lblInsType" runat="server" Text='<%# Bind("HCBD_CO_INS_TYPE") %>' Style="text-align: right; padding-right: 5px;"  Width="100%"></asp:Label>
                                                                 
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Co-Ins." >
                                                            <ItemTemplate>
                                                                    <asp:Label ID="lblInsType" runat="server" Text='<%# Bind("HCBD_CO_INS_AMT") %>' Style="text-align: right; padding-right: 5px;"  Width="100%"></asp:Label>
                                                                 
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ded Type" >
                                                            <ItemTemplate>
 
                                                                    <asp:Label ID="lblDedType" runat="server" Text='<%# Bind("HCBD_DED_TYPE") %>' Style="text-align: right; padding-right: 5px;"  Width="100%"></asp:Label>
                                                                
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Deductible" >
                                                            <ItemTemplate>

                                                               
                                                                    <asp:Label ID="lblDedAmt" runat="server" Text='<%# Bind("HCBD_DED_AMT") %>' Style="text-align: right; padding-right: 5px;"  Width="100%"></asp:Label>
                                                                
                                                            </ItemTemplate>

                                                        </asp:TemplateField>
                                                      
                                                    </Columns>

                                                </asp:GridView>


                                            </div>

                                            
                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>

                        


                            <table width="95%" border="0" cellpadding="5" cellspacing="5" style="background-color: #efefef;">
                                <tr>
                                    <td style="width: 60px; height: 30px;">
                                        <asp:Label ID="Label40" runat="server" CssClass="lblCaption1" Text="Branch &nbsp;&nbsp; &nbsp;:"></asp:Label>
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:Label ID="lblVisitedBranch" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">
                                        <asp:Label ID="Label45" runat="server" CssClass="lblCaption1" Text="Doctor &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPrevDrId" runat="server" CssClass="lblCaption1" Width="100px" Font-Bold="true"></asp:Label>

                                    </td>
                                    <td colspan="2">
                                        <asp:Label ID="lblPrevDrName" runat="server" CssClass="lblCaption1" Width="300px" Font-Bold="true"></asp:Label>
                                    </td>

                                    <td style="width: 100px;">
                                        <asp:Label ID="Label82" runat="server" CssClass="lblCaption1" Text="Created User:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCreadedUser" runat="server" CssClass="lblCaption1" Width="150px" Font-Bold="true"></asp:Label>

                                    </td>

                                </tr>

                                <tr>
                                    <td style="width: 100px;">
                                        <asp:Label ID="Label41" runat="server" CssClass="lblCaption1" Text="First Visit :"></asp:Label>
                                    </td>
                                    <td style="width: 200px;">

                                        <asp:Label ID="lblFirstVisit" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td style="width: 100px;">
                                        <asp:Label ID="Label46" runat="server" CssClass="lblCaption1" Text="Previous Visit :"></asp:Label>
                                    </td>
                                    <td style="width: 100px;">

                                        <asp:Label ID="lblPrevVisit" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">
                                        <asp:Label ID="Label42" runat="server" CssClass="lblCaption1" Text="Latest Visit&nbsp;:"></asp:Label>
                                    </td>
                                    <td style="width: 100px;">

                                        <asp:Label ID="lblLastVisit" runat="server" CssClass="lblCaption1" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td style="width: 100px;">
                                        <asp:Label ID="Label38" runat="server" CssClass="lblCaption1" Text="Modified User:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblModifiedUser" runat="server" CssClass="lblCaption1" Width="150px" Font-Bold="true"></asp:Label>

                                    </td>

                                </tr>

                            </table>

                        
</ContentTemplate>
</asp:UpdatePanel>


</contenttemplate>
</asp:TabPanel>
            <asp:TabPanel runat="server" ID="TabPanelCard" HeaderText=" Patient Details-2 ">
                <contenttemplate>
                    <div>
                        <table cellpadding="5" cellspacing="5" style="width: 850px; height: 100px;" border="0">
                            <tr>
                                <td class="lblCaption1" style="height:30px">Card Type
                                </td>
                                <td class="lblCaption1">Card Name
                                </td>

                                <td class="lblCaption1" runat="server" id="tdCaptionTPA">TPA</td>
                                <td class="lblCaption1" >Company
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="height:30px">
                                    <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="drpCardType" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="drpCardType_SelectedIndexChanged">
                                                <asp:ListItem Text="Insurance" Value="Insurance"></asp:ListItem>
                                                <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <asp:DropDownList ID="drpCardName" CssClass="label" BorderWidth="1px" BorderColor="red" runat="server" Width="100px"></asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td runat="server" id="tdTPA">
                                    <asp:DropDownList ID="drpParentCompany" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="drpParentCompany_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                               
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtCompany" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="70px" MaxLength="10"  onblur="return CompIdSelected()" onkeypress="return PatientPopup('CompanyId',this.value,event)" AutoPostBack="true" OnTextChanged="txtCompany_TextChanged"></asp:TextBox>
                                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="label" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="250px" MaxLength="50"  onblur="return CompNameSelected()" onkeypress="return PatientPopup('CompanyName',this.value,event)" AutoPostBack="true" OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>
                                              <div ID="divComp" style="visibility:hidden;"></div>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany" 
                                                ></asp:autocompleteextender>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"
                                                 ></asp:autocompleteextender>

                                       </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                                <td>

                                </td>
                                   


                            </tr>
                            <tr>
                                 <td class="lblCaption1" style="height:30px"> 
                                      <asp:Label ID="lblPolicyNo1" runat="server" CssClass="lblCaption1" Text="Policy No."></asp:Label>
                                </td> 
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPolicyCardNo" CssClass="label" runat="server" BorderWidth="1px" MaxLength="30" BorderColor="Red" Width="100px"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>  
                                 <td >
                                    <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox ID="chkIsDefault" runat="server" Text="Default Card" CssClass="lblCaption1" Checked="True" />
                                           &nbsp;  <asp:CheckBox ID="chkCardActive" runat="server" Text="Active" CssClass="lblCaption1" Checked="True" />

                                  
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
   
                                <td  colspan="2" class="lblCaption1">
                                   <asp:Label ID="lblPlanType" runat="server" CssClass="lblCaption1" Text="Plan Type" visible="false"></asp:Label>
                                       <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtPlanType" runat="server" CssClass="label" visible="false" BackColor="#e3f7ef" BorderColor="#e3f7ef" BorderStyle="Groove" Width="200px" MaxLength="50"  ondblclick="return BenefitsExecPopup(this.value);"   ></asp:TextBox>
                                     <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtPlanType" MinimumPrefixLength="1" ServiceMethod="GetPlanType">
                        </asp:AutoCompleteExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height:30px">Effective Date</td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender6" runat="server"
                                                Enabled="True" TargetControlID="txtEffectiveDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Enabled="True" TargetControlID="txtEffectiveDate" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                               
                             
                                <td class="lblCaption1" id="tdCardNo" runat="server">
                                    <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                        <ContentTemplate>
                                           Policy No
                                         &nbsp;  &nbsp; &nbsp;<asp:TextBox ID="txtCardNo" CssClass="label" BorderColor="#cccccc" runat="server" BorderWidth="1px" MaxLength="30" Width="100px"></asp:TextBox>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                 <td class="lblCaption1">Expiry Date</td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtCardExpDate" runat="server" CssClass="label" BorderWidth="1px" BorderColor="Red" Width="100px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                                Enabled="True" TargetControlID="txtCardExpDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="True" TargetControlID="txtCardExpDate" Mask="99/99/9999" MaskType="Date" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""></asp:MaskedEditExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                             
                               
                               
                                <td></td>
                                <td colspan="2" ></td>


                            </tr>
                          

                        </table>
                        <br /> <br />
                        <table cellpadding="5" cellspacing="5"   style="width: 850px;" border="0">
                            <tr>

                                <td>
                                    <asp:Label ID="Label70" runat="server" CssClass="lblCaption1"
                                        Text="Card Front Image"></asp:Label>

                                </td>
                                <td style="width: 50px"></td>
                                <td>
                                    <asp:Label ID="Label49" runat="server" CssClass="lblCaption1"
                                        Text="Card Back Image"></asp:Label>
                                </td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>

                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgFront" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>

                                    </asp:UpdatePanel>

                                </td>
                                <td style="width: 50px"></td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgBack" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td ></td>
                                <td valign="top">
                                               <asp:Button ID="btnClaimPrint" runat="server" style="padding-left:5px;padding-right:5px;width:100px;" CssClass="button orange small"  Text="Claim Form Print" OnClick="btnClaimPrint_Click" />
                                    <br /><br /><asp:Button ID="btnWebRepot" runat="server" style="padding-left:5px;padding-right:5px;width:100px;" CssClass="button orange small" Text="Claim Form Web" OnClick="btnWebRepot_Click" />
                                    <br /><br /><asp:Button ID="btnCardPrint" runat="server" style="padding-left:5px;padding-right:5px;width:100px;" CssClass="button orange small"  Text="Card Print" OnClick="btnCardPrint_Click" />
                                    <br /><asp:Button ID="btnDentalWebRepot" runat="server" style="padding-left:5px;padding-right:5px;width:100px;" CssClass="button orange small"   Text="Web Report"  OnClick="btnDentalWebRepot_Click" visible="false" />
                                    <br /><input type="checkbox" runat="server" id="chkCompletionRpt" name="chkCompletionRpt" />
                                           <label for="chkCompletionRpt" class="lblCaption1" >Completion Report</label>


                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:AsyncFileUpload ID="AsyncFileUpload1" runat="server"
                                        OnUploadedComplete="AsyncFileUpload1_UploadedComplete"
                                        OnClientUploadComplete="AsyncFileUpload1_uploadComplete"
                                        Width="225px" FailedValidation="False" />
                                </td>
                                <td style="width: 50px"></td>
                                <td>
                                    <asp:AsyncFileUpload ID="AsyncFileUpload2" runat="server"
                                        OnUploadedComplete="AsyncFileUpload2_UploadedComplete"
                                        OnClientUploadComplete="AsyncFileUpload2_uploadComplete"
                                        Width="225px" FailedValidation="False" />
                                </td>
                               
                            </tr>

                        </table>
                        <table cellpadding="5" cellspacing="5" width="70%">
                            <tr>
                                <td >

                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnScanCardAdd" runat="server" Width="80px" CssClass="button orange small" OnClientClick="return ValScanCardAdd();"
                                                OnClick="btnScanCardAdd_Click" Text="Add" />
                                            <asp:Button ID="btnScanCardUpdate" runat="server" Width="80px" CssClass="button orange small" OnClientClick="return ValScanCardAdd();" Visible="false"
                                                OnClick="btnScanCardUpdate_Click" Text="Update" />
                                    <asp:Button ID="bntClear" runat="server" Width="80px" CssClass="button orange small" OnClick="bntClear_Click" Text="Clear" />

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnScanCardAdd" />
                                            <asp:PostBackTrigger ControlID="btnScanCardUpdate" />

                                        </Triggers>
                                    </asp:UpdatePanel>

                            </tr>
                        </table>
                    </div>
                    <br />
                    <div>
                        <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvScanCard" runat="server" AllowSorting="True" CssClass="Grid" OnRowDataBound="gvScanCard_RowDataBound"
                                    AutoGenerateColumns="False" Width="100%" EnableModelValidation="True">
                                    <HeaderStyle CssClass="GridHeader" Font-Bold="True" />
                                    <RowStyle CssClass="GridRow" />
                                    <AlternatingRowStyle CssClass="GridAlterRow" />

                                    <Columns>
                                        <asp:TemplateField HeaderText=" Card Name">
                                            <ItemTemplate>

                                                <asp:LinkButton ID="lnkCardName" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblIsNew" runat="server" Text='<%# Bind("HSC_ISNEW") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCardType" runat="server" Text='<%# Bind("HSC_CARD_TYPE") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCardName" runat="server" Text='<%# Bind("HSC_CARD_NAME") %>'></asp:Label>
                                                </asp:LinkButton>


                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText=" Company Id">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCompId" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblCompId" runat="server" Text='<%# Bind("HSC_INS_COMP_ID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Company Name">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCompName" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblCompName" runat="server" Text='<%# Bind("HSC_INS_COMP_NAME") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Policy No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPolicyCardNo" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblPolicyCardNo" runat="server" Text='<%# Bind("HSC_POLICY_NO") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Plan Type">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPlanType" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblPlanType" runat="server" Text='<%# Bind("HSC_PLAN_TYPE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText=" Card Number">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCardNo" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblCardNo" runat="server" Text='<%# Bind("HSC_CARD_NO") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                       

                                        <asp:TemplateField HeaderText="Expiry Date">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCExpDate" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblExpDate" runat="server" Text='<%# Bind("HSC_EXPIRY_DATE") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Effective Date">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEffectiveDate" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblEffectiveDate" runat="server" Text='<%# Bind("HSC_POLICY_START") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Default">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkIsDefault" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblIsDefault" runat="server" Text='<%# Bind("HSC_ISDEFAULT") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblIsDefaultDesc" runat="server" Text='<%# Bind("HSC_ISDEFAULTDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkStatus" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("HSC_STATUS") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="Label52" runat="server" Text='<%# Bind("HSC_STATUSDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Card Front Image">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkFrontPath" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblFrontPath" runat="server" Text='<%# Bind("HSC_IMAGE_PATH_FRONT") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Card Back Image">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBackPath" runat="server" OnClick="EditScanCard_Click">
                                                    <asp:Label ID="lblBackPath" runat="server" Text='<%# Bind("HSC_IMAGE_PATH_BACK") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>

                                                <asp:ImageButton ID="DeleteScanCard" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="DeleteScanCard_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                        
                    </div>
                   
                      
                
</contenttemplate>

</asp:TabPanel>

            <asp:TabPanel runat="server" ID="Panel1" HeaderText="  Patient Details3 " Visible="false">
                <contenttemplate>
                    <div>
                        <table style="width: 98%; height: 200px;" cellpadding="5" cellspacing="5" border="0">

                            <tr>
                                <td style="width: 150px">
                                    <span>
                                        <asp:Label ID="Label34" runat="server" CssClass="lblCaption1"
                                            Text="General Information" Font-Bold="True"></asp:Label>
                                    </span></td>
                                <td style="width: 300px">
                                    <td style="width: 150px">
                                        <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border: solid 1px #4fbdf0;" colspan="4"></td>

                            </tr>
                            <tr>
                                <td style="width: 150px">

                                    <asp:Label ID="Label60" runat="server" CssClass="lblCaption1"
                                        Text="Opening balance"></asp:Label>
                                </td>
                                <td style="width: 300px">
                                    <asp:TextBox ID="txtOpBalance" runat="server" Width="150px" Style="text-align: right;"></asp:TextBox>
                                </td>
                                <td style="width: 150px">

                                    <asp:Label ID="Label61" runat="server" CssClass="lblCaption1"
                                        Text="Op. balance date"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOpBalanceDate" runat="server" Width="150px"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                        Enabled="True" TargetControlID="txtOpBalanceDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 150px">

                                    <asp:Label ID="Label62" runat="server" CssClass="lblCaption1"
                                        Text="Credit Limit"></asp:Label>
                                </td>
                                <td style="width: 300px">
                                    <asp:TextBox ID="txtCrLimit" runat="server" Width="150px" Style="text-align: right;"></asp:TextBox>
                                </td>
                                <td style="width: 150px">
                                    <span>
                                        <asp:Label ID="Label63" runat="server" CssClass="lblCaption1"
                                            Text="Deposit Amount"></asp:Label>
                                    </span></td>
                                <td>
                                    <asp:TextBox ID="txtDepositAmt" runat="server" Width="150px" Enabled="false" Style="text-align: right;"></asp:TextBox>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 150px">

                                    <asp:Label ID="Label64" runat="server" CssClass="lblCaption1"
                                        Text="Invoice Amount"></asp:Label>
                                </td>
                                <td style="width: 300px">
                                    <asp:TextBox ID="txtInvoiceAmt" runat="server" Width="150px" Enabled="false" Style="text-align: right;"></asp:TextBox>
                                </td>
                                <td style="width: 150px">

                                    <asp:Label ID="Label65" runat="server" CssClass="lblCaption1"
                                        Text="Paid Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPadiAmt" runat="server" Width="150px" Enabled="false" Style="text-align: right;"></asp:TextBox>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 150px">

                                    <asp:Label ID="Label66" runat="server" CssClass="lblCaption1"
                                        Text="Rejected Amount"></asp:Label>
                                </td>
                                <td style="width: 300px">
                                    <asp:TextBox ID="txtRejectedAmt" runat="server" Width="150px" Enabled="false" Style="text-align: right;"></asp:TextBox>
                                </td>
                                <td style="width: 150px">

                                    <asp:Label ID="Label67" runat="server" CssClass="lblCaption1"
                                        Text="Balance Due"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBalanceDue" runat="server" Width="150px" Enabled="false" Style="text-align: right;"></asp:TextBox>
                                </td>

                            </tr>

                        </table>

                        <table style="width: 98%; height: 200px;" cellpadding="5" cellspacing="5">


                            <tr>
                                <td style="width: 450px;">
                                    <span>
                                        <asp:Label ID="Label72" runat="server" CssClass="lblCaption1"
                                            Text="Allergies" Font-Bold="True"></asp:Label>
                                    </span>

                                </td>
                                <td>
                                    <span>
                                        <asp:Label ID="Habbit" runat="server" CssClass="lblCaption1"
                                            Text="Habbit" Font-Bold="True"></asp:Label>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 450px;">
                                    <asp:TextBox ID="txtAllergies" runat="server" Height="40px" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                </td>
                                <td>
                                    <span>
                                        <asp:TextBox ID="txtHabbit" runat="server" Height="40px" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 450px;">
                                    <span>
                                        <asp:Label ID="Label73" runat="server" CssClass="lblCaption1"
                                            Text="Past Medical History" Font-Bold="True"></asp:Label>
                                    </span>

                                </td>
                                <td>
                                    <span>
                                        <asp:Label ID="Label74" runat="server" CssClass="lblCaption1"
                                            Text="Drug History" Font-Bold="True"></asp:Label>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 450px;">
                                    <asp:TextBox ID="txtPastMedHistory" runat="server" Height="40px" TextMode="MultiLine"
                                        Width="400px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDrugHistory" runat="server" Height="40px" TextMode="MultiLine"
                                        Width="400px"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 450px;">
                                    <span>
                                        <asp:Label ID="Label75" runat="server" CssClass="lblCaption1"
                                            Text="Family History" Font-Bold="True"></asp:Label>
                                    </span>
                                </td>
                                <td>
                                    <span>
                                        <asp:Label ID="Label76" runat="server" CssClass="lblCaption1"
                                            Text="Others" Font-Bold="True"></asp:Label>
                                    </span>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 450px;">
                                    <asp:TextBox ID="txtFamilyHistory" runat="server" Height="40px" TextMode="MultiLine"
                                        Width="400px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOthers" runat="server" Height="40px" TextMode="MultiLine"
                                        Width="400px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 450px;">
                                    <span>
                                        <asp:Label ID="Label77" runat="server" CssClass="lblCaption1"
                                            Text="Patient Satus" Font-Bold="True"></asp:Label>
                                    </span>
                                </td>
                                <td>
                                    <span>
                                        <asp:Label ID="Label26" runat="server" CssClass="lblCaption1"
                                            Text="Hold Message" Font-Bold="True"></asp:Label>
                                    </span>
                                </td>
                            </tr>
                            <tr>

                                <td style="width: 450px;">
                                    <span>
                                        <asp:DropDownList ID="drpPatientStatus" runat="server"
                                            Width="155px">
                                            <asp:ListItem Selected="True" Text="Select" Value="" />
                                            <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                            <asp:ListItem Value="Open">Open</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHoldMessage" runat="server" Height="40px" TextMode="MultiLine"
                                        Width="400px"></asp:TextBox>
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2">
                                    <span>
                                        <asp:Label ID="Label43" runat="server" CssClass="lblCaption1"
                                            Text="Initial Findings" Font-Bold="True"></asp:Label>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txtInitialFindings" runat="server" Height="40px" Width="820px" EnableViewState="false"
                                        TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>
                                        <asp:CheckBox ID="chkCmpletionRpt" runat="server" CssClass="lblCaption1"
                                            Text="Completion Report" />
                                    </span>
                                </td>
                            </tr>

                        </table>


                       

                    </div>

                
</contenttemplate>

</asp:TabPanel>
            <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Patient Details - 3 " Width="90%" Visible="true">
                <contenttemplate>
                    <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                        <ContentTemplate>
                            <div>
                                <table cellpadding="5" cellspacing="5" style="width: 100%;" border="0">
                                    <tr>
                                        <td class="lblCaption1" style="height: 30px;">Substitute Name
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubstitute" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="label" Width="240px" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Age
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsAge" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="label" Width="150px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Nationality
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpSubsNationality" BorderWidth="1" BorderColor="#cccccc" runat="server" CssClass="label" Width="153px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 30px;"> Policy No
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsIdNo" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="label" Width="240px" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Type
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsType" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="label" Width="150px" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td class="lblCaption1">Capacity
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSubsCapacity" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="label" Width="150px" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="height: 30px;">Translator Name
                                        </td>
                                        <td colspan="5">
                                            <asp:TextBox ID="txtTranslator" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="label" Width="240px" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="5" cellspacing="5" style="width: 98%;" border="0">

                                    <tr>

                                        <td style="height: 5px;">
                                            <asp:Label ID="Label83" runat="server" CssClass="lblCaption1" Font-Bold="true"
                                                Text="Patient"></asp:Label>


                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:Label ID="Label84" runat="server" CssClass="lblCaption1" Font-Bold="true"
                                                Text="Substitute "></asp:Label>

                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:Label ID="Label85" runat="server" BorderWidth="1" BorderColor="#cccccc" CssClass="lblCaption1" Font-Bold="true" MaxLength="50"
                                                Text="Translator "></asp:Label>

                                        </td>


                                    </tr>

                                    <tr>

                                        <td style="width: 30px;">
                                            <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                                                <ContentTemplate>
                                                    <asp:Image ID="imgSig1" runat="server" Height="120px" BorderWidth="1" BorderColor="#cccccc" Width="285px" />
                                                </ContentTemplate>

                                            </asp:UpdatePanel>


                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                                                <ContentTemplate>
                                                    <asp:Image ID="imgSig2" runat="server" Height="120px" BorderWidth="1" BorderColor="#cccccc" Width="285px" />
                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                                                <ContentTemplate>
                                                    <asp:Image ID="imgSig3" runat="server" Height="120px" BorderWidth="1" BorderColor="#cccccc" Width="285px" />
                                                </ContentTemplate>

                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnSignature1" runat="server" style="padding-left:5px;padding-right:5px;width:70px;" CssClass="button orange small" Text="Signature" OnClick="btnSignature1_Click" />
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;   &nbsp; &nbsp; &nbsp;&nbsp;   &nbsp; &nbsp; 
                                                     <asp:Button ID="btnSigShow1" runat="server" style="padding-left:5px;padding-right:5px;width:70px;" Text="Show"  CssClass="button orange small" OnClick="btnSigShow1_Click" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSigShow1" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnSignature2" runat="server" style="padding-left:5px;padding-right:5px;width:70px;" CssClass="button orange small"   Text="Signature" OnClick="btnSignature2_Click" />
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;   &nbsp; &nbsp; &nbsp;&nbsp;   &nbsp; &nbsp; 
                                                <asp:Button ID="btnSigShow2" runat="server" style="padding-left:5px;padding-right:5px;width:70px;" Text="Show"  CssClass="button orange small" OnClick="btnSigShow2_Click" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSigShow2" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 50px"></td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel38" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnSignature3" runat="server"  style="padding-left:5px;padding-right:5px;width:70px;" CssClass="button orange small"  Text="Signature" OnClick="btnSignature3_Click" />
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;   &nbsp; &nbsp; &nbsp;&nbsp;   &nbsp; &nbsp; 
                                                <asp:Button ID="btnSigShow3" runat="server" style="padding-left:5px;padding-right:5px;width:70px;" Text="Show"  CssClass="button orange small" OnClick="btnSigShow3_Click" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSigShow3" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table cellpadding="5" cellspacing="5" style="width: 100%;" border="0">
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvGCGridView" runat="server" AllowPaging="True" Width="100%"
                                                        AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGCGridView_Sorting" OnRowDataBound="gvGCGridView_RowDataBound"
                                                        EnableModelValidation="True" OnPageIndexChanging="gvGCGridView_PageIndexChanging" PageSize="200">
                                                        <HeaderStyle CssClass="GridHeader" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <AlternatingRowStyle CssClass="GridAlterRow" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkReport" runat="server" OnClick="GCReport_Click">
                                                                        <asp:Label ID="lblHistory" CssClass="GridRow" Width="50px" Style="text-align: center; padding-right: 2px;" runat="server" Text='REPORT'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="No" SortExpression="HPV_SEQNO" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" Style="text-align: center; padding-right: 2px;" runat="server" Text='<%# Bind("HGC_ID") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="File No" SortExpression="HPV_PT_Id">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSeqNo" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HGC_ID") %>'></asp:Label>

                                                                    <asp:Label ID="lblPatientId" CssClass="GridRow"  runat="server" Text='<%# Bind("HGC_PT_ID") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name" SortExpression="HGC_SUBS_NAME">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="Label2" CssClass="GridRow"  runat="server" Text='<%# Bind("HGC_SUBS_NAME") %>'></asp:Label>

                                                                </ItemTemplate>

                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Age" SortExpression="HGC_SUBS_AGE">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="Label4" CssClass="GridRow" Width="50px" runat="server" Text='<%# Bind("HGC_SUBS_AGE") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Nationality" SortExpression="HGC_SUBS_NATIONALITY">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="Label7" CssClass="GridRow"  runat="server" Text='<%# Bind("HGC_SUBS_NATIONALITY") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Policy No" SortExpression="HGC_SUBS_IDNO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label3" CssClass="GridRow"  runat="server" Text='<%# Bind("HGC_SUBS_IDNO") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type" SortExpression="HGC_SUBS_TYPE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label8" CssClass="GridRow"  runat="server" Text='<%# Bind("HGC_SUBS_TYPE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Capacity" SortExpression="HGC_SUBS_CAPACITY">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label9" CssClass="GridRow"  runat="server" Text='<%# Bind("HGC_SUBS_CAPACITY") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Transalator" SortExpression="HGC_TRAN_NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label10" CssClass="GridRow" runat="server" Text='<%# Bind("HGC_TRAN_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>

                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <br />
                            </div>

                        </ContentTemplate>

                    </asp:UpdatePanel>
                
</contenttemplate>
            

















</asp:TabPanel>

        </asp:TabContainer>

         </div>
        <div style="padding-top: 5px; padding-bottom: 5px; width: 100%; height: 33px;">
           
            <table  cellpadding="5" cellspacing="5" width="50%">
                <tr>
                    <td >
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <contenttemplate>
                                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" Text="Save" OnClientClick="return Val();"
                                    OnClick="btnSave_Click"
                                    Enabled="true" />
                            </contenttemplate>
                            <triggers>
                                <asp:PostBackTrigger ControlID="btnSave" />

                            </triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td >
                        <asp:Button ID="btnClear" runat="server" class="button orange small" Width="100px" Text="Cancel" OnClick="btnClear_Click" OnClientClick="Val1();" />
                    </td>
                    <td >
                        <asp:Button ID="btnTokenPrint" runat="server" CssClass="button orange small" Width="100px" Text="Token Print" OnClick="btnTokenPrint_Click" />
                    </td>
                    <td >&nbsp;
                                 <asp:TextBox ID="txtLablePrintCount" CssClass="label" BorderWidth="1" BorderColor="#cccccc" runat="server" Width="20px" MaxLength="2" Text="3" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CheckBox ID="chkLabelPrint" runat="server" />

                    </td>
                    <td>
                        <asp:Button ID="btnLabelPrint" runat="server" CssClass="button orange small" Width="100px" Text="Label Print" OnClick="btnLabelPrint_Click" />
                    </td>

                </tr>
            </table>

        </div>

   

    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

    <style type="text/css" media="print">
        #nottoprint
        {
            display: none;
        }

        #printToPrinter
        {
            display: block;
        }

            #printToPrinter ul li
            {
                width: 290px;
                color: #000;
                padding: 48px 5px;
            }
    </style>


    <style type="text/css" media="print">
        #nottoprint1
        {
            display: none;
            vertical-align: top;
        }

        #printToPrinter1
        {
            display: block;
        }
    </style>


    <style type="text/css" media="screen">
        #printToPrinter
        {
            display: none;
        }

        #printToPrinter1
        {
            display: none;
        }
    </style>
</asp:content>