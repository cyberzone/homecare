﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.Scans
{
    public partial class DisplayAttachments : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        void BindScreenCustomization()
        {
            dboperations objDP = new dboperations();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HC_SCANNING' ";

            DS = new DataSet();
            DS = objDP.ScreenCustomizationGet(Criteria);
            ViewState["HC_SCAN_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "HC_SCAN_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["HC_SCAN_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }


        }

        void BindScanFileFromPhysical()
        {
            string ScanFileName = Convert.ToString(ViewState["FileName"]);
            string ScanPath = @"\" + Convert.ToString(ViewState["PT_ID"]) + @"\";
            if (Convert.ToString(ViewState["Parent1"]) != "" && Convert.ToString(ViewState["Parent2"]) != "")
            {
                ScanPath += Convert.ToString(ViewState["Parent1"]) + @"\" + Convert.ToString(ViewState["Parent2"]) + @"\";
            }
            else if (Convert.ToString(ViewState["Parent1"]) != "" && Convert.ToString(ViewState["Parent2"]) == "")
            {
                ScanPath += Convert.ToString(ViewState["Parent1"]) + @"\";
            }

            string FileExtension = ScanFileName.Substring(ScanFileName.LastIndexOf('.'), ScanFileName.Length - ScanFileName.LastIndexOf('.'));


            string ScanSourcePath = @Convert.ToString(ViewState["HC_SCAN_PATH"]) + ScanPath + ScanFileName;


            if (System.IO.File.Exists(ScanSourcePath) == true)
            {
                if (FileExtension.ToLower() == ".pdf")
                {
                    Response.ContentType = "Application/pdf";
                }
                else if (FileExtension.ToLower() == ".docx" || FileExtension.ToLower() == ".doc")
                {
                    Response.ContentType = "application/msword";// "application/ms-word";
                }
                else if (FileExtension.ToLower() == ".xlsx" || FileExtension.ToLower() == ".xls")
                {
                    Response.AddHeader("content-disposition", "attachment; filename=" + ScanFileName);
                    Response.AddHeader("Content-Type", "application/Excel");
                    Response.ContentType = "application/vnd.xls";
                }
                else if (FileExtension.ToLower() == ".jpg")
                {
                    Response.ContentType = "image/JPEG";
                }
                else if (FileExtension.ToLower() == ".png")
                {
                    Response.ContentType = "image/png";
                }
                else if (FileExtension.ToLower() == ".bmp")
                {
                    Response.ContentType = "image/bmp";
                }
                else if (FileExtension.ToLower() == ".gif")
                {
                    Response.ContentType = "image/gif";
                }



                Response.WriteFile(ScanSourcePath);
                Response.End();


            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["PT_ID"] = Request.QueryString["PT_ID"];
            ViewState["FileName"] = Request.QueryString["FileName"];
            ViewState["Parent1"] = Request.QueryString["Parent1"];
            ViewState["Parent2"] = Request.QueryString["Parent2"];


            BindScreenCustomization();
            /*
            if (GlobalValues.FileDescription.ToUpper() == "ALAMAL" || GlobalValues.FileDescription.ToUpper() == "MAMPILLY")
            {
                BindScanFileFromPhysical();
            }
            else
            {
                BindScanFile();
            }
            */


            BindScanFileFromPhysical();

        }
    }
}