﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.Scans
{
    public partial class ScanList : System.Web.UI.Page
    {
        DataSet DS;
        CommonBAL objCom = new CommonBAL();


        #region Methods

        void BindScreenCustomization()
        {
            dboperations objDP = new dboperations();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='HC_SCANNING' ";

            DS = new DataSet();
            DS = objDP.ScreenCustomizationGet(Criteria);
            ViewState["HC_SCAN_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "HC_SCAN_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["HC_SCAN_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }


        }

        void papulateMenuTree()
        {
            string SCanPath, FileNoFolder = "";
            SCanPath = Convert.ToString(ViewState["HC_SCAN_PATH"]);
            FileNoFolder = Convert.ToString(Session["HHV_PT_ID"]);

            DS = new DataSet();

            string[] folders = System.IO.Directory.GetDirectories(@SCanPath + "\\" + FileNoFolder, "*", System.IO.SearchOption.AllDirectories);



            for (int DirectoryCount = 0; DirectoryCount < folders.Length; DirectoryCount++)
            {


                MenuItem L1 = new MenuItem(folders[DirectoryCount], folders[DirectoryCount], "", folders[DirectoryCount], "_self");
                // Menu1.Items.Add(L1);

            }

        }



        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["HomeCareId"] == null) { Response.Redirect("../HomeCare/WaitingList.aspx"); }

            if (!IsPostBack)
            {


                BindScreenCustomization();
                string SCanPath, FileNoFolder = "";
                SCanPath = Convert.ToString(ViewState["HC_SCAN_PATH"]);
                FileNoFolder = Convert.ToString(Session["HHV_PT_ID"]);

                System.IO.DirectoryInfo RootDir = new System.IO.DirectoryInfo(SCanPath + "\\" + FileNoFolder);

                if (System.IO.Directory.Exists(SCanPath + "\\" + FileNoFolder) == true)
                {
                    // output the directory into a node
                    TreeNode RootNode = OutputDirectory(RootDir, null);
                    // add the output to the tree

                    MyTree.Nodes.Add(RootNode);
                }

                if (MyTree.Nodes.Count > 0)
                {
                    MyTree.Nodes[0].Expand();
                }
                //MyTree.Nodes[0].ChildNodes[0].Expand();

                //MyTree.ExpandAll();
                // papulateMenuTree();
            }

        }

        TreeNode OutputDirectory(System.IO.DirectoryInfo directory, TreeNode parentNode)
        {




            // validate param
            if (directory == null) return null;
            // create a node for this directory
            TreeNode DirNode = new TreeNode(directory.Name);
            // get subdirectories of the current directory
            System.IO.DirectoryInfo[] SubDirectories = directory.GetDirectories();
            // OutputDirectory(SubDirectories[0], "Directories");
            // output each subdirectory


            for (int DirectoryCount = 0; DirectoryCount < SubDirectories.Length; DirectoryCount++)
            {
                OutputDirectory(SubDirectories[DirectoryCount], DirNode);
            }
            // output the current directories file
            //DataSet DS = new DataSet();
            //string Criteria = " 1=1 ";
            //Criteria += " AND  ESF_PT_ID='" + Convert.ToString(Session["HHV_PT_ID"]) + "' and ESF_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //Criteria += " AND ESF_CAT_ID='" + directory.Name + "'";
            //Criteria += " AND ESF_FULLPATH='" + @"\" + Convert.ToString(Session["HHV_PT_ID"]) + @"\" + directory.Parent.Name + @"\" + directory.Name + @"\'";

            //DS = objCom.ScanFilesGet(Criteria);

            //if (GlobalValues.FileDescription.ToUpper() == "ALAMAL" || GlobalValues.FileDescription.ToUpper() == "MAMPILLY")

            System.IO.FileInfo[] Files = directory.GetFiles();
            for (int FileCount = 0; FileCount < Files.Length; FileCount++)
            {
                DirNode.ChildNodes.Add(new TreeNode(Files[FileCount].Name));
            }

            /*
            System.IO.FileInfo[] Files = directory.GetFiles();
            for (int FileCount = 0; FileCount < Files.Length; FileCount++)
            {
                DirNode.ChildNodes.Add(new TreeNode(Files[FileCount].Name));
            }        // if the parent node is null, return this node

            */
            // otherwise add this node to the parent and return the parent
            if (parentNode == null)
            {
                return DirNode;
            }
            else
            {
                parentNode.ChildNodes.Add(DirNode);
                return parentNode;
            }
        }

        protected void MyTree_SelectedNodeChanged(object sender, EventArgs e)
        {

            string Parent1 = "", Parent2 = "";
            ImageEditorPDC.Src = "";
            //MyTree.SelectedNode.Parent.Text 
            //MyTree.SelectedNode.Parent.Parent.Text 

            if (MyTree.SelectedNode.Depth == 3 && MyTree.SelectedNode.ChildNodes.Count == 0)//NOTE: FILE IS BEING IN THE CATEGORY FOLDER
            {
                //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + Convert.ToString(Session["HHV_PT_ID"]) + "','" + MyTree.SelectedNode.Text + "');", true);

                //  imgAttach.ImageUrl = "DisplayAttachments.aspx?PT_ID=" + Convert.ToString(Session["HHV_PT_ID"]) + "&FileName=" + MyTree.SelectedNode.Text;

                string strFileName = MyTree.SelectedNode.Text;
                //if (GlobalValues.FileDescription.ToUpper() == "NEWHEART")
                //{
                //    if (strFileName.LastIndexOf("_") > 0)
                //    {
                //        strFileName = strFileName.Substring(0, strFileName.LastIndexOf("_"));
                //    }
                //}

                ViewState["FileName"] = strFileName;

                Parent1 = MyTree.SelectedNode.Parent.Parent.Text;//"03-08-2015"
                Parent2 = MyTree.SelectedNode.Parent.Text;//"general"



                ImageEditorPDC.Src = "DisplayAttachments.aspx?PT_ID=" + Convert.ToString(Session["HHV_PT_ID"]) + "&FileName=" + strFileName + "&Parent1=" + Parent1 + "&Parent2=" + Parent2;

                // ImageEditorPDC.Attributes.Add("onclick", "return ShowAttachment('"+ Convert.ToString(Session["HHV_PT_ID"]) +"','"+ strFileName +"');");


            }
            else if (MyTree.SelectedNode.Depth == 2 && MyTree.SelectedNode.ChildNodes.Count == 1)//NOTE: FILE IS BEING IN THE CATEGORY FOLDER WHEN CLICK THE CATEGORY IT WILL DISPLAY THE FILE
            {
                string strFileName = MyTree.SelectedNode.ChildNodes[0].Text;
                //if (GlobalValues.FileDescription.ToUpper() == "NEWHEART")
                //{
                //    if (strFileName.LastIndexOf("_") > 0)
                //    {
                //        strFileName = strFileName.Substring(0, strFileName.LastIndexOf("_"));
                //    }

                //}

                ViewState["FileName"] = strFileName;
                Parent1 = MyTree.SelectedNode.Parent.Text;//"03-08-2015"
                Parent2 = MyTree.SelectedNode.Text;//"general""


                ImageEditorPDC.Src = "DisplayAttachments.aspx?PT_ID=" + Convert.ToString(Session["HHV_PT_ID"]) + "&FileName=" + strFileName + "&Parent1=" + Parent1 + "&Parent2=" + Parent2;
                // ImageEditorPDC.Attributes.Add("onclick", "return ShowAttachment('" + Convert.ToString(Session["HHV_PT_ID"]) + "','" + strFileName + "');");

            }
            else if (MyTree.SelectedNode.Depth == 2 && MyTree.SelectedNode.ChildNodes.Count == 0)//NOTE: FILE IS BEING IN THE DATE FOLDER
            {
                string strFileName = MyTree.SelectedNode.Text;
                //if (GlobalValues.FileDescription.ToUpper() == "NEWHEART")
                //{
                //    if (strFileName.LastIndexOf("_") > 0)
                //    {
                //        strFileName = strFileName.Substring(0, strFileName.LastIndexOf("_"));
                //    }

                //}

                ViewState["FileName"] = strFileName;
                Parent1 = MyTree.SelectedNode.Parent.Text;//"03-08-2015"
                Parent2 = "";//"general""


                ImageEditorPDC.Src = "DisplayAttachments.aspx?PT_ID=" + Convert.ToString(Session["HHV_PT_ID"]) + "&FileName=" + strFileName + "&Parent1=" + Parent1 + "&Parent2=" + Parent2;
                // ImageEditorPDC.Attributes.Add("onclick", "return ShowAttachment('" + Convert.ToString(Session["HHV_PT_ID"]) + "','" + strFileName + "');");

            }
            else if (MyTree.SelectedNode.Depth == 1 && MyTree.SelectedNode.ChildNodes.Count == 0)//NOTE: FILE IS BEING IN THE FILE FOLDER
            {
                string strFileName = MyTree.SelectedNode.Text;
                //if (GlobalValues.FileDescription.ToUpper() == "NEWHEART")
                //{
                //    if (strFileName.LastIndexOf("_") > 0)
                //    {
                //        strFileName = strFileName.Substring(0, strFileName.LastIndexOf("_"));
                //    }

                //}

                ViewState["FileName"] = strFileName;
                Parent1 = "";// MyTree.SelectedNode.Parent.Text;//"03-08-2015"
                Parent2 = "";// MyTree.SelectedNode.Text;//"general""


                ImageEditorPDC.Src = "DisplayAttachments.aspx?PT_ID=" + Convert.ToString(Session["HHV_PT_ID"]) + "&FileName=" + strFileName + "&Parent1=" + Parent1 + "&Parent2=" + Parent2;
                // ImageEditorPDC.Attributes.Add("onclick", "return ShowAttachment('" + Convert.ToString(Session["HHV_PT_ID"]) + "','" + strFileName + "');");

            }

            ViewState["Parent1"] = Parent1;
            ViewState["Parent2"] = Parent2;
            //  btnZoom.Attributes.Add("ondblclick", "ShowAttachment1()");


        }


        protected void btnZoom_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + Convert.ToString(Session["HHV_PT_ID"]) + "','" + Convert.ToString(ViewState["FileName"]) + "','" + Convert.ToString(ViewState["Parent1"]) + "','" + Convert.ToString(ViewState["Parent2"]) + "');", true);
        }
    }
}