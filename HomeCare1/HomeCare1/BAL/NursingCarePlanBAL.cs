﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class NursingCarePlanBAL
    {

        public string HNCP_ID { set; get; }
        public string HNCP_HHV_ID { set; get; }
        public string HNCP_BRANCH_ID { set; get; }
        public string HNCP_PT_ID { set; get; }
        public string HNCP_ASSESSMENT { set; get; }
        public string HNCP_ANALYSIS { set; get; }
        public string HNCP_PLANNING { set; get; }
        public string HNCP_EVALUATION { set; get; }


        public string UserId { set; get; }




        public void AddNursingCarePlan()
        {

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HNCP_ID", HNCP_ID);
            Param.Add("HNCP_HHV_ID", HNCP_HHV_ID);
            Param.Add("HNCP_BRANCH_ID", HNCP_BRANCH_ID);
            Param.Add("HNCP_PT_ID", HNCP_PT_ID);
            Param.Add("HNCP_ASSESSMENT", HNCP_ASSESSMENT);
            Param.Add("HNCP_ANALYSIS", HNCP_ANALYSIS);
            Param.Add("HNCP_PLANNING", HNCP_PLANNING);
            Param.Add("HNCP_EVALUATION", HNCP_EVALUATION);

            Param.Add("UserId", UserId);

            DBO.ExecuteNonQuery("HMS_SP_HC_NursingCarePlanAdd", Param);



        }

        public DataSet GetNursingCarePlan(string Criteria)
        {
            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = DBO.ExecuteReader("HMS_SP_HC_NursingCarePlanGet", Param);
            return (DS);

        }

        public void DeleteNursingCarePlan()
        {
            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HNCP_ID", HNCP_ID);
            Param.Add("UserId", UserId);
            DBO.ExecuteNonQuery("HMS_SP_HC_NursingCarePlanDelete", Param);
        }
    }
}