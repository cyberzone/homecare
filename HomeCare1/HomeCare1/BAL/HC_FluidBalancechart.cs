﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using HomeCare1.DAL;
namespace HomeCare1.BAL
{
    public class HC_FluidBalancechart
    {
        public HC_FluidBalancechart()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        dbOperation objDB = new dbOperation();
        public string BranchID { set; get; }
        public string EMRID { set; get; }
        public string PTID { set; get; }


        public string HFBC_FLUBAL_ID { set; get; }
        public string HFBC_DATE { set; get; }
        public string HFBC_NATURE_OF_FOOD { set; get; }
        public string HFBC_ROUTE_ORAL { set; get; }
        public string HFBC_ROUTE_IV { set; get; }
        public string HFBC_ROUTE_OTHER { set; get; }
        public string HFBC_URINE { set; get; }
        public string HFBC_VOMIT { set; get; }
        public string HFBC_NG_ASPIRATION { set; get; }
        public string HFBC_OTHER { set; get; }

        public string HFBC_TOTAL_INPUT { set; get; }
        public string HFBC_TOTAL_OUTPUT { set; get; }
        public string HFBC_RESULT { set; get; }

        public string UserID { set; get; }


        public DataSet FluidBalanceChartGet(string Criteria)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_HCFluidBalanceChartGet", Param);

            return DS;


        }

        public void FluidBalanceChartAdd()
        {


            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HFBC_BRANCH_ID", BranchID);
            Param.Add("HFBC_ID", EMRID);
            Param.Add("HFBC_PT_ID", PTID);
            Param.Add("HFBC_FLUBAL_ID", HFBC_FLUBAL_ID);

            Param.Add("HFBC_DATE", HFBC_DATE);
            Param.Add("HFBC_NATURE_OF_FOOD", HFBC_NATURE_OF_FOOD);
            Param.Add("HFBC_ROUTE_ORAL", HFBC_ROUTE_ORAL);
            Param.Add("HFBC_ROUTE_IV", HFBC_ROUTE_IV);
            Param.Add("HFBC_ROUTE_OTHER", HFBC_ROUTE_OTHER);

            Param.Add("HFBC_URINE", HFBC_URINE);
            Param.Add("HFBC_VOMIT", HFBC_VOMIT);

            Param.Add("HFBC_NG_ASPIRATION", HFBC_NG_ASPIRATION);
            Param.Add("HFBC_OTHER", HFBC_OTHER);

            Param.Add("HFBC_TOTAL_INPUT", HFBC_TOTAL_INPUT);
            Param.Add("HFBC_TOTAL_OUTPUT", HFBC_TOTAL_OUTPUT);
            Param.Add("HFBC_RESULT", HFBC_RESULT);

            Param.Add("UserID", UserID);

            objDB.ExecuteNonQuery("HMS_SP_HCFluidBalanceChartAdd", Param);

        }
    }
}