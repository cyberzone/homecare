﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Net.Mail;
using HomeCare1.DAL;

using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Summary description for CommonBAL
/// </summary>
/// 
namespace HomeCare1.BAL
{
    public class CommonBAL
    {

        dbOperation objDB = new dbOperation();

        public string Criteria { set; get; }

        public string HCM_ID { set; get; }
        public string HCM_BRANCH_ID { set; get; }
        public string HCM_CODE { set; get; }
        public string HCM_DESC { set; get; }
        public string HCM_TYPE { set; get; }
        public string HCM_STATUS { set; get; }

        public string UserID { set; get; }



        public DataSet GetStaffCategory(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetStaffCategory(Criteria);
            return (DS);

        }

        public DataSet GetStaffDesignation(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetStaffDesignation(Criteria);
            return (DS);

        }

        public DataSet GetReligion(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetReligion(Criteria);
            return (DS);

        }

        public DataSet GetBloodGroup(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetBloodGroup(Criteria);
            return (DS);

        }

        public DataSet GetStateMaster(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_StaffMasterGet", Param);

            return DS;

        }
        public DataSet GetSpeciality(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetSpeciality(Criteria);
            return (DS);

        }

        public DataSet GetCommission(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetCommission(Criteria);
            return (DS);

        }

        public DataSet GetEMRDoctorDep(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetEMRDoctorDep(Criteria);
            return (DS);

        }
        public DataSet CalculateCompAmount(string AmountType, string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.CalculateCompAmount(AmountType, Criteria);
            return (DS);

        }
        public DataSet GetHaadServicessList(string Servicetype, string SearchFilder)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetHaadServicessList(Servicetype, SearchFilder);
            return (DS);

        }


        public DataSet GetHomeCareReports()
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetHomeCareReports();
            return (DS);


        }

        public int MailSend(string strToAddress, string strSubject, string strBody)
        {
            //create the mail message
            MailMessage mail = new MailMessage();
            string strFromEmail = System.Configuration.ConfigurationSettings.AppSettings["Hospital_FromMailID"].ToString();
            string strMail_Server = System.Configuration.ConfigurationSettings.AppSettings["Hospital_MailServer"].ToString();
            string Hospital_MailUesrName = System.Configuration.ConfigurationSettings.AppSettings["Hospital_MailUesrName"].ToString();
            string Hospital_MailPassword = System.Configuration.ConfigurationSettings.AppSettings["Hospital_MailPassword"].ToString();

            Hospital_MailUesrName = System.Configuration.ConfigurationSettings.AppSettings["Hospital_MailUesrName"].ToString();
            //set the addresses
            mail.From = new MailAddress(strFromEmail);
            mail.To.Add(strToAddress);

            //set the content
            mail.Subject = strSubject;
            mail.Body = strBody;
            mail.IsBodyHtml = true;
            //send the message
            SmtpClient smtp = new SmtpClient(strMail_Server);

            if (Hospital_MailUesrName != "")
            {
                //to authenticate we set the username and password properites on the SmtpClient
                smtp.Credentials = new System.Net.NetworkCredential(Hospital_MailUesrName, Hospital_MailPassword);
            }

            smtp.Send(mail);




            return 0;
        }

        public DataSet GetEMRSegmentVisitDtls(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRSegmentVisitDtlsGet", Param);



            return DS;

        }


        public DataSet EMRPTVitalsGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRPTVitalsGet", Param);



            return DS;

        }

        public DataSet LaboratoryResultGet(string BranchID, string FileNo)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("filenumber", FileNo);
            DS = objDB.ExecuteReader("WEMR_spS_GetLaboratoryResult", Param);



            return DS;

        }



        public DataSet RadiologyResultGet(string BranchID, string FileNo)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("filenumber", FileNo);
            DS = objDB.ExecuteReader("WEMR_spS_GetRadiologyResult", Param);



            return DS;

        }

        public DataSet WEMR_spS_GetEMRS(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMRS", Param);



            return DS;

        }

        public DataSet WEMR_spS_GetEMRSHPI(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMRSHPI", Param);



            return DS;

        }

        public DataSet WEMR_spS_GetSegmentVisits(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetSegmentVisits", Param);



            return DS;

        }

        public DataSet WEMR_spS_GetEMRSDatas(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMRSDatas", Param);



            return DS;

        }


        public DataSet GetDeductibleType(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetDeductibleType(Criteria);
            return (DS);

        }

        public DataSet GetCoInsuranceType(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetCoInsuranceType(Criteria);
            return (DS);

        }

        public DataSet GetPatientTitle(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetPatientTitle(Criteria);
            return (DS);

        }

        public DataSet GetConsentForm(string Criteria)
        {
            DataSet DS = new DataSet();

            CommonDAL objComm = new CommonDAL();
            DS = objComm.GetConsentForm(Criteria);
            return (DS);

        }


        public DataSet CommonMastersGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_CommonMastersGet", Param);



            return DS;

        }

        public void CommonMastersAdd()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HCM_ID", HCM_ID);
            Param.Add("HCM_BRANCH_ID", HCM_BRANCH_ID);
            Param.Add("HCM_CODE", HCM_CODE);
            Param.Add("HCM_DESC", HCM_DESC);
            Param.Add("HCM_TYPE", HCM_TYPE);
            Param.Add("HCM_STATUS", HCM_STATUS);
            Param.Add("UserID", UserID);



            objDB.ExecuteNonQuery("HMS_SP_CommonMastersAdd", Param);
        }

        public DataSet WEMR_spS_GetPainScore(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetPainScore", Param);



            return DS;

        }


        public DataSet WEMR_spS_GetEMCPTInvoice(string BranchID, string EMRID)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("branchid", BranchID);
            Param.Add("patientmasterid", EMRID);
            DS = objDB.ExecuteReader("WEMR_spS_GetEMCPTInvoice", Param);



            return DS;

        }



        public DataSet PatientDrTransferGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_PatientDrTransferGet", Param);



            return DS;

        }

        public string fnGetDate(String formatstring)
        {
            DateTime dtDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT getdate() ";

            DS = objDB.ExecuteReader(strSQL);

            dtDate = Convert.ToDateTime(DS.Tables[0].Rows[0][0]);


            return Convert.ToString(dtDate.ToString(formatstring));
        }

        public Int32 fnGetDateDiff(String Date1, string Date2)
        {
            Int32 intDateDiff;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT dbo.GetDateDiff ('" + Date1 + "','" + Date2 + "')";

            DS = objDB.ExecuteReader(strSQL);

            intDateDiff = Convert.ToInt32(DS.Tables[0].Rows[0][0]);


            return intDateDiff;
        }

        public string fnGetDateddMMyyyy()
        {
            string strDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " select convert(varchar,getdate(),103) ";

            DS = objDB.ExecuteReader(strSQL);

            strDate = Convert.ToString(DS.Tables[0].Rows[0][0]);


            return strDate;
        }


        public DataSet EMR_DYNAMIC_CONTENT(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT * FROM   EMR_DYNAMIC_CONTENT WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }

        public DataSet HoursGet()
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            DS = objDB.ExecuteReader("HMS_SP_HoursGet", Param);
            return DS;
        }

        public DataSet MinutesGet(string Interval)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Interval", Interval);
            DS = objDB.ExecuteReader("HMS_SP_MinutesGet", Param);
            return DS;
        }



        public DataSet fnGetFieldValue(string FieldNames, string TableName, string Criteria, string OrderBy)
        {
            DataSet DS = new DataSet();
            string strSQL;

            if (OrderBy != "")
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria + " ORDER BY " + OrderBy;
            }
            else
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria;
            }


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }

        public void fnInsertTableData(string FieldName, string Values, string TableName)
        {

            string strSQL;

            strSQL = " INSERT INTO " + TableName + "(" + FieldName + " ) VALUES( " + Values + ")";
            objDB.ExecuteNonQuery(strSQL);

        }



        public void fnInsertTableDataWithoutValuesKey(string FieldName, string Values, string TableName)
        {

            string strSQL;

            strSQL = " INSERT INTO " + TableName + "(" + FieldName + " ) ( " + Values + ")";
            objDB.ExecuteNonQuery(strSQL);

        }


        public void fnUpdateTableData(string FieldNameWithValues, string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " UPDATE   " + TableName + " SET " + FieldNameWithValues + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }


        public void fnDeleteTableData(string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " DELETE  FROM " + TableName + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }


        public string SimpleEncrypt(string strData)
        {
            CharEncrypt();
            UpperCharEncrypt();
            DigitEncrypt();
            SplCharEncrypt();

            int key = 5;
            int v;
            Int64 c1;
            string z = "";
            string strValue = "";

            for (v = 0; v < strData.Length; v++)
            {
                strValue = strData.Substring(v, 1);

                DataTable DT;
                DataRow[] TempRow = null;
                if (strValue.Any(char.IsLower) == true)
                {
                    DT = (DataTable)CharEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else if (strValue.Any(char.IsUpper) == true)
                {
                    DT = (DataTable)UpperCharEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else if (strValue.Any(char.IsDigit) == true)
                {
                    DT = (DataTable)DigitEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else if (CheckSplChar(strValue) == true)
                {
                    DT = (DataTable)SplCharEncryptData;
                    TempRow = DT.Select("Code='" + strValue + "'");
                    byte[] charByte = Encoding.ASCII.GetBytes(Convert.ToString(TempRow[0]["Name"]));
                    c1 = charByte[0];
                }
                else
                {
                    byte[] charByte = Encoding.ASCII.GetBytes(strValue);
                    c1 = charByte[0];
                }


                char character;


                if (strValue == "{")
                {
                    z += "€";
                }
                else if (strValue == "}")
                {
                    z += ",";
                }
                else
                {
                    character = Convert.ToChar(c1 + key);
                    z += character.ToString();
                }


                // c1 = Asc(Mid(Name, v, 1))
                //c1 = Chr(c1 + Key)
                /// z = z & c1
                /// 
            }
            return z;
        }

        static DataTable CharEncryptData;
        static DataTable UpperCharEncryptData;
        static DataTable DigitEncryptData;
        static DataTable SplCharEncryptData;

        void CharEncrypt()
        {
            // a	b	c	d	e	f	g	h	i	j	k	l	m	n	o	p	q	r	s	t	u	v	w	x	y	z
            // m	p	a	y	n	q	k	o	b	l	j	r	c	t	i	s	u	d	v	w	e	z	x	g	f	h

            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "a";
            objrow["Name"] = "m";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "b";
            objrow["Name"] = "p";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "c";
            objrow["Name"] = "a";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "d";
            objrow["Name"] = "y";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "e";
            objrow["Name"] = "n";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "f";
            objrow["Name"] = "q";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "g";
            objrow["Name"] = "k";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "h";
            objrow["Name"] = "o";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "i";
            objrow["Name"] = "b";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "j";
            objrow["Name"] = "l";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "k";
            objrow["Name"] = "j";
            DT.Rows.Add(objrow);



            objrow = DT.NewRow();
            objrow["Code"] = "l";
            objrow["Name"] = "r";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "m";
            objrow["Name"] = "c";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "n";
            objrow["Name"] = "t";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "o";
            objrow["Name"] = "i";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "p";
            objrow["Name"] = "s";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "q";
            objrow["Name"] = "u";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "r";
            objrow["Name"] = "d";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "s";
            objrow["Name"] = "v";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "t";
            objrow["Name"] = "w";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "u";
            objrow["Name"] = "e";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "v";
            objrow["Name"] = "z";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "w";
            objrow["Name"] = "x";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "x";
            objrow["Name"] = "g";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "y";
            objrow["Name"] = "f";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "z";
            objrow["Name"] = "h";
            DT.Rows.Add(objrow);



            CharEncryptData = (DataTable)DT;

        }

        void UpperCharEncrypt()
        {
            // A	B	C	D	E	F	G	H	I	J	K	L	M	N	O	P	Q	R	S	T	U	V	W	X	Y	Z
            // O	B	L	J	R	C	T	I	S	U	D	V	W	E	Z	X	G	F	H	M	P	A	Y	N	Q	K


            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "A";
            objrow["Name"] = "O";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "B";
            objrow["Name"] = "B";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "C";
            objrow["Name"] = "L";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "D";
            objrow["Name"] = "J";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "E";
            objrow["Name"] = "R";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "F";
            objrow["Name"] = "C";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "G";
            objrow["Name"] = "T";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "H";
            objrow["Name"] = "I";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "I";
            objrow["Name"] = "S";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "J";
            objrow["Name"] = "U";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "K";
            objrow["Name"] = "D";
            DT.Rows.Add(objrow);



            objrow = DT.NewRow();
            objrow["Code"] = "L";
            objrow["Name"] = "V";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "M";
            objrow["Name"] = "W";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "N";
            objrow["Name"] = "E";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "O";
            objrow["Name"] = "Z";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "P";
            objrow["Name"] = "X";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "Q";
            objrow["Name"] = "G";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "R";
            objrow["Name"] = "F";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "S";
            objrow["Name"] = "H";
            DT.Rows.Add(objrow);



            objrow = DT.NewRow();
            objrow["Code"] = "T";
            objrow["Name"] = "M";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "U";
            objrow["Name"] = "P";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "V";
            objrow["Name"] = "A";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "W";
            objrow["Name"] = "Y";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "X";
            objrow["Name"] = "N";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "Y";
            objrow["Name"] = "Q";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "Z";
            objrow["Name"] = "K";
            DT.Rows.Add(objrow);



            UpperCharEncryptData = (DataTable)DT;

        }

        void DigitEncrypt()
        {
            //0	1	2	3	4	5	6	7	8	9
            //6	4	7	5	0	8	2	3	9	1



            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "0";
            objrow["Name"] = "6";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "1";
            objrow["Name"] = "4";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "2";
            objrow["Name"] = "7";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "3";
            objrow["Name"] = "5";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "4";
            objrow["Name"] = "0";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "5";
            objrow["Name"] = "8";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "6";
            objrow["Name"] = "2";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "7";
            objrow["Name"] = "3";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "8";
            objrow["Name"] = "9";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "9";
            objrow["Name"] = "1";
            DT.Rows.Add(objrow);


            DigitEncryptData = (DataTable)DT;

        }

        void SplCharEncrypt()
        {
            //  !	@	#	$	%	^	&	*	(	)	_	-	+	.	,	\
            //  %	^	_	-	!	@	(	)	#	$	&	*	,	\	+	.




            DataTable DT = new DataTable();

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DT.Columns.Add(Code);
            DT.Columns.Add(Name);

            DataRow objrow;

            objrow = DT.NewRow();
            objrow["Code"] = "!";
            objrow["Name"] = "%";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "@";
            objrow["Name"] = "^";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "#";
            objrow["Name"] = "_";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "$";
            objrow["Name"] = "-";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "%";
            objrow["Name"] = "!";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "^";
            objrow["Name"] = "@";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "&";
            objrow["Name"] = "(";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "*";
            objrow["Name"] = ")";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "(";
            objrow["Name"] = "#";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = ")";
            objrow["Name"] = "$";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "_";
            objrow["Name"] = "&";
            DT.Rows.Add(objrow);


            objrow = DT.NewRow();
            objrow["Code"] = "-";
            objrow["Name"] = "*";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = "+";
            objrow["Name"] = ",";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = ".";
            objrow["Name"] = @"\";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = ",";
            objrow["Name"] = "+";
            DT.Rows.Add(objrow);

            objrow = DT.NewRow();
            objrow["Code"] = @"\";
            objrow["Name"] = ".";
            DT.Rows.Add(objrow);




            SplCharEncryptData = (DataTable)DT;

        }

        bool CheckSplChar(string strData)
        {
            string strSplValue = @"!@#$%^&*()_-+.,\";

            if (strSplValue.IndexOf(strData) != -1)
            {
                return true;
            }

            return false;
        }

        public string SimpleDecrypt(string strData)
        {
            CharEncrypt();
            UpperCharEncrypt();
            DigitEncrypt();
            SplCharEncrypt();

            int key = 5;
            int v;
            Int64 c1;
            string z = "";
            string strValue = "";
            for (v = 0; v < strData.Length; v++)
            {
                strValue = strData.Substring(v, 1);
                byte[] charByte = Encoding.ASCII.GetBytes(strData.Substring(v, 1));
                c1 = charByte[0];

                char character;
                if (strValue == "€")
                {
                    strValue = "{";
                }
                else if (strValue == ",")
                {
                    strValue = "}";
                }
                else
                {

                    strValue = Convert.ToChar(c1 - key).ToString();
                }



                DataTable DT;
                DataRow[] TempRow = null;
                if (strValue.Any(char.IsLower) == true)
                {
                    DT = (DataTable)CharEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else if (strValue.Any(char.IsUpper) == true)
                {
                    DT = (DataTable)UpperCharEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else if (strValue.Any(char.IsDigit) == true)
                {
                    DT = (DataTable)DigitEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else if (CheckSplChar(strValue) == true)
                {
                    DT = (DataTable)SplCharEncryptData;
                    TempRow = DT.Select("Name='" + strValue + "'");
                    z += Convert.ToString(TempRow[0]["Code"]);// character.ToString();
                }
                else
                {
                    z += strValue;// character.ToString();

                }



                // c1 = Asc(Mid(Name, v, 1))
                //c1 = Chr(c1 + Key)
                /// z = z & c1
            }
            return z;
        }
    }
}