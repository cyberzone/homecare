﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HomeCare1.DAL;
namespace HomeCare1.BAL
{
    public class GlobelValues
    {
        public static string HomeCare_ID { set; get; }

        public static string DataSource = System.Configuration.ConfigurationSettings.AppSettings["DB_SERVER"].ToString().Trim();
        public static string DBName = System.Configuration.ConfigurationSettings.AppSettings["DB_DATABASE"].ToString().Trim();
        public static string DBUserId = System.Configuration.ConfigurationSettings.AppSettings["DB_USERNAME"].ToString().Trim();
        public static string DBUserPWD = System.Configuration.ConfigurationSettings.AppSettings["DB_PASSWORD"].ToString().Trim();


        public static string HospitalName = System.Configuration.ConfigurationSettings.AppSettings["HospitalName"].ToString().Trim();
        public static string HospitalLicenseNo = System.Configuration.ConfigurationSettings.AppSettings["HospitalLicenseNo"].ToString().Trim();
        public static string HospitalPhoneNo = System.Configuration.ConfigurationSettings.AppSettings["HospitalPhoneNo"].ToString().Trim();

        public static string EMR_Path = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["EMR_Path"].ToString());

        public static string FileDescription = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["FileDescription"].ToString());

    }
}