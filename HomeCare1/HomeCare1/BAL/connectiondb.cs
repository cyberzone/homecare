﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using HomeCare1.DAL;
/// <summary>
/// Summary description for connectiondb
/// </summary>
/// 

namespace HomeCare1.BAL
{
    public class connectiondb
    {

        public SqlConnection con = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString));


        public void conopen()
        {
            con.Open();

        }
        public void conclose()
        {


            con.Close();



        }
    }

}