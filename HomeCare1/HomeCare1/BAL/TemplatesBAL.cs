﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class TemplatesBAL
    {

        public string ET_BRANCH_ID { set; get; }
        public string ET_TYPE { set; get; }
        public string ET_CODE { set; get; }
        public string ET_NAME { set; get; }
        public string ET_TEMPLATE { set; get; }
        public string ET_APPLY { set; get; }
        public string ET_DR_CODE { set; get; }
        public string ET_DEP_ID { set; get; }

        public string AddEMRTemplates(TemplatesBAL TempBAL)
        {
            TemplatesDAL obj = new TemplatesDAL();
            string strReturnId;
            strReturnId = obj.AddEMRTemplates(TempBAL);
            return strReturnId;

        }

        public DataSet GetEMRTemplates(string Criteria)
        {

            DataSet DS = new DataSet();

            TemplatesDAL obj = new TemplatesDAL();
            DS = obj.GetEMRTemplates(Criteria);
            return (DS);

        }


        public void DeleteEMRTemplates(TemplatesBAL TempBAL)
        {
            TemplatesDAL obj = new TemplatesDAL();
            obj.DeleteEMRTemplates(TempBAL);
        }

    }
}