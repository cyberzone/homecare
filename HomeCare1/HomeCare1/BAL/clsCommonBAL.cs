﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;


/// <summary>
/// Summary description for clsCommonBAL
/// </summary>
/// 
namespace HomeCare1.BAL
{
    public class clsCommonBAL
    {

        dbOperation objDB = new dbOperation();


        public clsCommonBAL()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public DataSet CustomerGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_CustomerGet", Param);



            return DS;

        }

        public DataSet DoctorGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_DoctorGet", Param);



            return DS;

        }
        public DataSet ScreenCustomizationGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_ScreenCustomizationGet", Param);



            return DS;

        }


        public string CompanyBalanceGet(string CompanyId)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select sum(BalanceAmount) AS TotalBalanceAmount  from IvCustBalHist where CustomerCode='" + CompanyId + "'";

            DS = objDB.ExecuteReader(strSQL);

            string strBalance = "0";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("TotalBalanceAmount") == false && DS.Tables[0].Rows[0]["TotalBalanceAmount"].ToString() != "")
                {
                    strBalance = DS.Tables[0].Rows[0]["TotalBalanceAmount"].ToString();
                }
            }

            return strBalance;

        }

        public string InitialNoGet()
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT   CONVERT(VARCHAR,ISNULL(IvReceipt,0)+ 1) as ReceiptNo	FROM GeneralConfig";

            DS = objDB.ExecuteReader(strSQL);

            string strReceiptNo = "0";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("ReceiptNo") == false && DS.Tables[0].Rows[0]["ReceiptNo"].ToString() != "")
                {
                    strReceiptNo = DS.Tables[0].Rows[0]["ReceiptNo"].ToString();
                }
            }

            return strReceiptNo;

        }


        public DataSet ScanCategoryGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select ESC_NAME,ESC_CODE from  EMEDICAL.DBO.EMR_SCAN_CATEGORY WHERE " + Criteria;

            DS = objDB.ExecuteReader(strSQL);

            return DS;

        }



    }
}