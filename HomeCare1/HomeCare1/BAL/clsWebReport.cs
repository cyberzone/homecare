﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.DAL;

/// <summary>
/// Summary description for clsWebReport
/// </summary>
/// 
namespace HomeCare1.BAL
{
    public class clsWebReport
    {

        connectiondb c = new connectiondb();


        public DataSet PatientHistoryGet(string Criteria)
        {
            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = DBO.ExecuteReader("HMS_SP_PatientHistoryGet", Param);
            return (DS);


        }

        public DataSet PTProceduresGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PTProceduresGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }


        public DataSet EMRDentalTransGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EMRDentalTransGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }


        public DataSet EMRPTMasterGet(string Criteria)
        {

            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = DBO.ExecuteReader("HMS_SP_EMRPTMasterGet", Param);
            return (DS);


        }

        public DataSet DiagnosisGet(string Criteria)
        {

            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = DBO.ExecuteReader("HMS_SP_DiagnosisGet", Param);
            return (DS);


        }

        public DataSet PTPharmacyGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PTPharmacyGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }


        public DataSet EMRPTRadiologyGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EMRPTRadiologyGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet EMRPTLaboratoryGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EMRPTLaboratoryGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }


        public DataSet PTVitalsGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_PTVitalsGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet EMRPTReferenceGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_EMRPTReferenceGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }



        public DataSet TempImageGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_TempImageGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

        public DataSet TempImageAdd(string PatientId, byte[] TempImage)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO HIM_TempImage values(" + PatientId + "," + TempImage + ")";
            cmd.ExecuteNonQuery();

            c.conclose();
            return ds;


        }


        //public static string ConvertToTxt(string data)
        //{
        //    string result = data;
        //    try
        //    {
        //        using (System.Windows.Forms.RichTextBox rtfTemp = new System.Windows.Forms.RichTextBox())
        //        {
        //            rtfTemp.Rtf = data;
        //            result = rtfTemp.Text;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result = data;
        //    }


        //    return result;
        //}

    }
}