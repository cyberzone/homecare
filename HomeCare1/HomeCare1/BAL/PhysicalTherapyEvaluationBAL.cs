﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class PhysicalTherapyEvaluationBAL
    {

        public string ID { get; set; }
        public string BRANCH_ID { get; set; }
        public string HHV_ID { get; set; }
        public string CHIEF_COMPLAINT { get; set; }
        public string INJURY_DATE { get; set; }
        public string SURGERY_DATE { get; set; }
        public string INJURY_REMARKS { get; set; }
        public string THERAPY_RECEIVED { get; set; }
        public string THERAPY_DATE { get; set; }
        public string THERAPY_VISIT_COUNT { get; set; }
        public string CONDITION { get; set; }
        public string SYMPTOMS { get; set; }
        public string PAIN_BEST { get; set; }
        public string PAINT_WORST { get; set; }
        public string CONDITION_BETTER { get; set; }
        public string CONDITION_WORSE { get; set; }
        public string PREV_MED_INTERVENTION { get; set; }
        public string PREV_MED_INTERVENTION_OTHER { get; set; }
        public string GOALS_TOBE_ACHIEVED { get; set; }
        public string DIFFICULTY_SWALLOWING { get; set; }
        public string MOTION_SICKNESS { get; set; }
        public string STROKE { get; set; }
        public string ARTHRITIS { get; set; }
        public string FEVER_CHILLS_SWEATS { get; set; }
        public string OSTEOPOROSIS { get; set; }
        public string HIGH_BLOOD_PRESSURE { get; set; }
        public string UNEXPLAINED_WEIGHT_LOSS { get; set; }
        public string ANEMIA { get; set; }
        public string HEART_TROUBLE { get; set; }
        public string BLOOD_CLOTS { get; set; }
        public string BLEEDING_PROBLEMS { get; set; }
        public string PACEMAKER { get; set; }
        public string SHORTNESS_OF_BREATH { get; set; }
        public string HIV_HEPATITIS { get; set; }
        public string EPILEPSY_SEIZURES { get; set; }
        public string HISTORY_SMOKING { get; set; }
        public string HISTORY_ALCOHOL_ABUSE { get; set; }
        public string HISTORY_DRUG_ABUSE { get; set; }
        public string DIABETES { get; set; }
        public string DEPRESSION_ANXIETY { get; set; }
        public string MYOFASCIAL_PAIN { get; set; }
        public string FIBROMYALGIA { get; set; }
        public string PREGNANCY { get; set; }
        public string CANCER { get; set; }
        public string PREV_SURGERIES { get; set; }
        public string OTHER { get; set; }
        public string MEDICATIONS { get; set; }
        public string ALLERGIES { get; set; }
        public string UserId { get; set; }



        public void AddPhysicalTherapyEvaluation(PhysicalTherapyEvaluationBAL OBJPSEval)
        {

            PhysicalTherapyEvaluationDAL objPSEvalDAL = new PhysicalTherapyEvaluationDAL();
            objPSEvalDAL.AddPhysicalTherapyEvaluation(OBJPSEval);

        }

        public DataSet GetPhysicalTherapyEvaluation(string Criteria)
        {
            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = DBO.ExecuteReader("HMS_SP_HC_PhysicalTherapyGet", Param);
            return (DS);

        }

        public void DeleteTravelStatus(PhysicalTherapyEvaluationBAL OBJPSEval)
        {
            PhysicalTherapyEvaluationDAL objPSEvalDAL = new PhysicalTherapyEvaluationDAL();
            objPSEvalDAL.DeletePhysicalTherapyEvaluation(OBJPSEval);
        }
    }
}