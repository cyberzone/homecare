﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;

/// <summary>
/// Summary description for clsHCStaffAssignMonitor
/// </summary>
/// 
namespace HomeCare1.BAL
{
    public class clsHCStaffAssignMonitor
    {

        dbOperation objDB = new dbOperation();



        public string HSAM_BRANCH_ID { set; get; }
        public string HSAM_ID { set; get; }
        public string HSAM_PT_ID { set; get; }
        public string HSAM_PT_NAME { set; get; }
        public string HSAM_PLACE { set; get; }
        public string HSAM_MOBILE { set; get; }
        public string HSAM_DATE { set; get; }
        public string HSAM_START_TIME { set; get; }
        public string HSAM_END_TIME { set; get; }
        public string HSAM_STAFF_ID { set; get; }
        public string HSAM_STAFF_NAME { set; get; }
        public string HSAM_STATUS { set; get; }
        public string HSAM_ASSIGNER { set; get; }
        public string HSAM_COMMENT { set; get; }
        public string HSAM_PROCEDURE_CODE { set; get; }
        public string HSAM_PROCEDURE_DESC { set; get; }
        public string UserID { set; get; }


        public DataSet HCStaffAssignMonitorGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_HCStaffAssignMonitorGet", Param);
            return DS;

        }

        public void HCStaffAssignMonitorAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HSAM_BRANCH_ID", HSAM_BRANCH_ID);
            Param.Add("HSAM_ID", HSAM_ID);
            Param.Add("HSAM_PT_ID", HSAM_PT_ID);
            Param.Add("HSAM_PT_NAME", HSAM_PT_NAME);
            Param.Add("HSAM_PLACE", HSAM_PLACE);
            Param.Add("HSAM_MOBILE", HSAM_MOBILE);
            Param.Add("HSAM_DATE", HSAM_DATE);
            Param.Add("HSAM_START_TIME", HSAM_START_TIME);
            Param.Add("HSAM_END_TIME", HSAM_END_TIME);
            Param.Add("HSAM_STAFF_ID", HSAM_STAFF_ID);
            Param.Add("HSAM_STAFF_NAME", HSAM_STAFF_NAME);
            Param.Add("HSAM_STATUS", HSAM_STATUS);
            Param.Add("HSAM_ASSIGNER", HSAM_ASSIGNER);
            Param.Add("HSAM_COMMENT", HSAM_COMMENT);
            Param.Add("HSAM_PROCEDURE_CODE", HSAM_PROCEDURE_CODE);
            Param.Add("HSAM_PROCEDURE_DESC", HSAM_PROCEDURE_DESC);



            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_HCStaffAssignMonitorAdd", Param);

        }

        public void HCStaffAssignMonitorDelete()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("HSAM_BRANCH_ID", HSAM_BRANCH_ID);
            Param.Add("HSAM_ID", HSAM_ID);

            objDB.ExecuteNonQuery("HMS_SP_HCStaffAssignMonitorDelete", Param);


        }

    }
}