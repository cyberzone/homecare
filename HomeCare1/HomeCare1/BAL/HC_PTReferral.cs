﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class HC_PTReferral
    {
        public HC_PTReferral()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        dbOperation objDB = new dbOperation();

        public string branchid { set; get; }
        public string patientmasterid { set; get; }
        public string referencetype { set; get; }
        public string remarks { set; get; }
        public string type { set; get; }
        public string indoctorid { set; get; }
        public string kinname { set; get; }
        public string kinaddress { set; get; }
        public string kinphone { set; get; }
        public string diagnosis { set; get; }
        public string recommended { set; get; }
        public string managementtransfer { set; get; }
        public string beforetransfer { set; get; }
        public string aftertransfer { set; get; }
        public string templatecode { set; get; }
        public string outhospital { set; get; }
        public string outdoctor { set; get; }


        public string PatientCurrentstatus { set; get; }
        public string SpecialEndorsement { set; get; }


        public DataSet HCPTReferenceGet(string Criteria)
        {

            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_HCPTReferenceGet", Param);

            return DS;

        }

        public void HCPTReferenceAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();

            Param.Add("branchid", branchid);
            Param.Add("patientmasterid", patientmasterid);
            Param.Add("referencetype", referencetype);
            Param.Add("remarks", remarks);
            Param.Add("type", type);
            Param.Add("indoctorid", indoctorid);
            Param.Add("outhospital", outhospital);
            Param.Add("outdoctor", outdoctor);
            Param.Add("kinname", kinname);
            Param.Add("kinaddress", kinaddress);
            Param.Add("kinphone", kinphone);
            Param.Add("diagnosis", diagnosis);
            Param.Add("recommended", recommended);
            Param.Add("managementtransfer", managementtransfer);
            Param.Add("beforetransfer", beforetransfer);
            Param.Add("aftertransfer", aftertransfer);
            Param.Add("templatecode", templatecode);
            Param.Add("PatientCurrentstatus", PatientCurrentstatus);
            Param.Add("SpecialEndorsement", SpecialEndorsement);


            objDB.ExecuteNonQuery("HMS_SP_HCPTReferenceAdd", Param);

        }


    }

}