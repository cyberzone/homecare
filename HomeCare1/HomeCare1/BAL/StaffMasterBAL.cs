﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class StaffMasterBAL
    {

        public string BRANCH_ID { set; get; }
        public string STAFF_ID { set; get; }
        public string FNAME { set; get; }
        public string MNAME { set; get; }
        public string LNAME { set; get; }
        public string FNAME_A { set; get; }
        public string MNAME_A { set; get; }
        public string LNAME_A { set; get; }
        public string GUARDIAN_NAME { set; get; }
        public string CATEGORY { set; get; }
        public string DEPT_ID { set; get; }
        public string SF_STATUS { set; get; }
        public string POBOX { set; get; }
        public string ADDR { set; get; }
        public string CITY { set; get; }
        public string COUNTRY { set; get; }
        public string STATE { set; get; }
        public string EDU_QLFY { set; get; }
        public string DESIG { set; get; }
        public string DOB { set; get; }
        public string AGE { set; get; }
        public string RELIGION { set; get; }
        public string SEX { set; get; }
        public string MARITAL { set; get; }
        public string NO_CHILD { set; get; }
        public string BLOODGROUP { set; get; }
        public string NLTY { set; get; }
        public string PHONE1 { set; get; }
        public string PHONE2 { set; get; }
        public string FAX { set; get; }
        public string MOBILE { set; get; }
        public string EMAIL { set; get; }
        public string PASSPORT { set; get; }
        public string PASS_ISSUE { set; get; }
        public string PASS_EXP { set; get; }
        public string SS_ID { set; get; }
        public string SS_ISSUE { set; get; }
        public string SS_EXP { set; get; }
        public string DRV_LIC { set; get; }
        public string DRV_LIC_ISSUE { set; get; }
        public string DRV_LIC_EXP { set; get; }
        public string MOH_NO { set; get; }
        public string MOH_ISSUE { set; get; }
        public string MOH_EXP { set; get; }
        public string OTHER_NO { set; get; }
        public string OTHER_ISSUE { set; get; }
        public string OTHER_EXP { set; get; }
        public string BASIC { set; get; }
        public string HRA { set; get; }
        public string DA { set; get; }
        public string OTHR_ALLOW { set; get; }
        public string ADV_SALARY { set; get; }
        public string LOAN { set; get; }
        public string VACATIONPAY { set; get; }
        public string HOMETRAVEL { set; get; }
        public string WORKINGHRS { set; get; }
        public string FRI_COMPENSATION { set; get; }
        public string ROSTER_ID { set; get; }
        public string DOJ { set; get; }
        public string DOEXIT { set; get; }
        public string NOOFPAYMENT { set; get; }
        public string LOANDEDUCT { set; get; }
        public string FIXEDOVERTIME { set; get; }
        public string OVERTIMEPHR { set; get; }
        public string ABSENTPHR { set; get; }
        public string COMM_ID { set; get; }
        public string PRE_DRTOKEN { set; get; }
        public string TOKEN_START { set; get; }
        public string TOKEN_CURRENT { set; get; }
        public string TOKEN_END { set; get; }
        public string APNT_TIME_INT { set; get; }
        public string NO_APNT_DATEFROM1 { set; get; }
        public string NO_APNT_FROM1TIME { set; get; }
        public string NO_APNT_DATETO1 { set; get; }
        public string NO_APNT_TO1TIME { set; get; }
        public string NO_APNT_DATEFROM2 { set; get; }
        public string NO_APNT_FROM2TIME { set; get; }
        public string NO_APNT_DATETO2 { set; get; }
        public string NO_APNT_TO2TIME { set; get; }
        public string NO_APNT_DATEFROM3 { set; get; }
        public string NO_APNT_FROM3TIME { set; get; }
        public string NO_APNT_DATETO3 { set; get; }
        public string NO_APNT_TO3TIME { set; get; }
        public string OTHERS1 { set; get; }
        public string OTHERS2 { set; get; }
        public string OTHERS3 { set; get; }
        public string OTHERS4 { set; get; }
        public string OTHERS5 { set; get; }
        public string COMMIT_FLAG { set; get; }
        public string SRV_CATEGORY { set; get; }
        public string SPECIALITY { set; get; }
        public string TOKEN_RESERVE { set; get; }
        public string EMR_DATE { set; get; }
        public string EMR_CAT { set; get; }
        public string EMR_DEP { set; get; }
        public string ROOM { set; get; }

        public string EMR_DR_DEP { set; get; }

        public Byte[] HSP_PHOTO { set; get; }
        public Byte[] HSP_Signature { set; get; }
        public Boolean IsManual { set; get; }
        public string UserId { set; get; }

        public string AddStaffMaster(StaffMasterBAL objSMBAL)
        {
            StaffMasterDAL obj = new StaffMasterDAL();
            string strReturnId;
            strReturnId = obj.AddStaffMaster(objSMBAL);
            return strReturnId;

        }

        public DataSet GetStaffMaster(string Criteria)
        {
            DataSet DS = new DataSet();

            StaffMasterDAL obj = new StaffMasterDAL();
            DS = obj.GetStaffMaster(Criteria);
            return (DS);

        }

        public void DeleteStaffMaster(StaffMasterBAL objStaff)
        {
            StaffMasterDAL obj = new StaffMasterDAL();
            obj.DeleteStaffMaster(objStaff);
        }
    }
}