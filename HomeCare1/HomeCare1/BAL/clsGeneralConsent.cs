﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.DAL;

/// <summary>
/// Summary description for clsGeneralConsent
/// </summary>
namespace HomeCare1.BAL
{
    public class clsGeneralConsent
    {
        connectiondb c = new connectiondb();
        private string vPatienId, vBranchId, vPatienSignature, vSubsName, vSubsSignature, vSubsAge, vSubsNationality, vSubsIdNo, vSubsType, vSubsCapacity,
            vTranName, vTranSignature, vDrId, vDrName, vUserId;

        public string BranchId
        {
            get { return vBranchId; }
            set { vBranchId = value; }
        }

        public string PatienId
        {
            get { return vPatienId; }
            set { vPatienId = value; }
        }


        public string PatienSignature
        {
            get { return vPatienSignature; }
            set { vPatienSignature = value; }
        }

        public string SubsName
        {
            get { return vSubsName; }
            set { vSubsName = value; }
        }

        public string SubsSignature
        {
            get { return vSubsSignature; }
            set { vSubsSignature = value; }
        }

        public string SubsAge
        {
            get { return vSubsAge; }
            set { vSubsAge = value; }
        }

        public string SubsNationality
        {
            get { return vSubsNationality; }
            set { vSubsNationality = value; }
        }

        public string SubsIdNo
        {
            get { return vSubsIdNo; }
            set { vSubsIdNo = value; }
        }

        public string SubsType
        {
            get { return vSubsType; }
            set { vSubsType = value; }
        }

        public string SubsCapacity
        {
            get { return vSubsCapacity; }
            set { vSubsCapacity = value; }
        }

        public string TranName
        {
            get { return vTranName; }
            set { vTranName = value; }
        }

        public string TranSignature
        {
            get { return vTranSignature; }
            set { vTranSignature = value; }
        }

        public string DrId
        {
            get { return vDrId; }
            set { vDrId = value; }
        }


        public string DrName
        {
            get { return vDrName; }
            set { vDrName = value; }
        }

        public string UserId
        {
            get { return vUserId; }
            set { vUserId = value; }
        }

        public void GeneralConsentAdd()
        {

            c.conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_GeneralConsentAdd";
            cmd.Parameters.Add(new SqlParameter("@HGC_BRANCH_ID", SqlDbType.VarChar)).Value = BranchId;
            cmd.Parameters.Add(new SqlParameter("@HGC_PT_ID", SqlDbType.VarChar)).Value = PatienId;
            cmd.Parameters.Add(new SqlParameter("@HGC_PT_SIGNATURE", SqlDbType.VarChar)).Value = PatienSignature;
            cmd.Parameters.Add(new SqlParameter("@HGC_SUBS_NAME", SqlDbType.VarChar)).Value = SubsName;
            cmd.Parameters.Add(new SqlParameter("@HGC_SUBS_SIGNATURE", SqlDbType.VarChar)).Value = SubsSignature;
            cmd.Parameters.Add(new SqlParameter("@HGC_SUBS_AGE", SqlDbType.VarChar)).Value = SubsAge;

            cmd.Parameters.Add(new SqlParameter("@HGC_SUBS_NATIONALITY", SqlDbType.VarChar)).Value = SubsNationality;
            cmd.Parameters.Add(new SqlParameter("@HGC_SUBS_IDNO", SqlDbType.VarChar)).Value = SubsIdNo;
            cmd.Parameters.Add(new SqlParameter("@HGC_SUBS_TYPE", SqlDbType.VarChar)).Value = SubsType;
            cmd.Parameters.Add(new SqlParameter("@HGC_SUBS_CAPACITY", SqlDbType.VarChar)).Value = SubsCapacity;

            cmd.Parameters.Add(new SqlParameter("@HGC_TRAN_NAME", SqlDbType.VarChar)).Value = TranName;
            cmd.Parameters.Add(new SqlParameter("@HGC_TRAN_SIGNATURE", SqlDbType.VarChar)).Value = TranSignature;

            cmd.Parameters.Add(new SqlParameter("@HGC_DR_ID", SqlDbType.VarChar)).Value = DrId;
            cmd.Parameters.Add(new SqlParameter("@HGC_DR_NAME", SqlDbType.VarChar)).Value = DrName;

            cmd.Parameters.Add(new SqlParameter("@HGC_CREATED_USER", SqlDbType.VarChar)).Value = UserId;


            cmd.ExecuteNonQuery();
            c.conclose();
        }


        public DataSet GeneralConsentGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_GeneralConsentGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;

            adpt.Fill(ds);
            c.conclose();
            return ds;


        }

    }

}