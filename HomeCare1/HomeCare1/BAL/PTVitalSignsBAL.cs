﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class PTVitalSignsBAL
    {

        dbOperation objDB = new dbOperation();

        public string HPV_ID { set; get; }
        public string HPV_BRANCH_ID { set; get; }
        public string HPV_HHV_ID { set; get; }
        public string HPV_PT_ID { set; get; }
        public string HPV_DATE { set; get; }
        public string HPV_WEIGHT { set; get; }
        public string HPV_HEIGHT { set; get; }
        public string HPV_BMI { set; get; }
        public string HPV_TEMPERATURE_F { set; get; }
        public string HPV_TEMPERATURE_C { set; get; }
        public string HPV_PULSE { set; get; }
        public string HPV_RESPIRATION { set; get; }
        public string HPV_BP_SYSTOLIC { set; get; }
        public string HPV_BP_DIASTOLIC { set; get; }
        public string HPV_SPO2 { set; get; }




        public string UserId { set; get; }

        public string AddPTVitalSigns()
        {
            string strReturnId;
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HPV_ID", HPV_ID);
            Param.Add("HPV_BRANCH_ID", HPV_BRANCH_ID);
            Param.Add("HPV_HHV_ID", HPV_HHV_ID);
            Param.Add("HPV_PT_ID", HPV_PT_ID);
            Param.Add("HPV_DATE", HPV_DATE);
            Param.Add("HPV_WEIGHT", HPV_WEIGHT);
            Param.Add("HPV_HEIGHT", HPV_HEIGHT);
            Param.Add("HPV_BMI", HPV_BMI);
            Param.Add("HPV_TEMPERATURE_F", HPV_TEMPERATURE_F);
            Param.Add("HPV_TEMPERATURE_C", HPV_TEMPERATURE_C);
            Param.Add("HPV_PULSE", HPV_PULSE);
            Param.Add("HPV_RESPIRATION", HPV_RESPIRATION);
            Param.Add("HPV_BP_SYSTOLIC", HPV_BP_SYSTOLIC);
            Param.Add("HPV_BP_DIASTOLIC", HPV_BP_DIASTOLIC);
            Param.Add("HPV_SPO2", HPV_SPO2);

            Param.Add("UserId", UserId);

            strReturnId = objDB.ExecuteNonQuery("HMS_SP_HC_PT_VitalSignAdd", Param);
            return strReturnId;

        }

        public DataSet GetPTVitalSigns(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_HC_PT_VitalSignGet", Param);
            return (DS);


        }

        public void DeletePTVitalSigns()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HPV_ID", HPV_ID);
            Param.Add("UserId", UserId);

            objDB.ExecuteNonQuery("HMS_SP_HC_PT_VitalSignDelete", Param);

        }

    }
}