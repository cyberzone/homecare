﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;
/// <summary>
/// Summary description for EMR_PT_DiagnosisBAL
/// </summary>

namespace HomeCare1.BAL
{
    public class EMR_PT_DiagnosisBAL
    {
        dbOperation objDB = new dbOperation();

        public string EPD_BRANCH_ID { set; get; }
        public string EPD_ID { set; get; }
        public string EPD_DIAG_CODE { set; get; }
        public string EPD_DIAG_NAME { set; get; }
        public string EPD_REMARKS { set; get; }
        public string EPD_TEMPLATE_CODE { set; get; }
        public string EPD_PROBLEM_TYPE { set; get; }


        public DataSet GetEMR_PTDiagnosis(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRPTDiagnosisGet", Param);



            return DS;

        }

        public void AddEMR_PTDiagnosis()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPD_BRANCH_ID", EPD_BRANCH_ID);
            Param.Add("EPD_ID", EPD_ID);
            Param.Add("EPD_DIAG_CODE", EPD_DIAG_CODE);
            Param.Add("EPD_DIAG_NAME", EPD_DIAG_NAME);
            Param.Add("EPD_REMARKS", EPD_REMARKS);
            Param.Add("EPD_TEMPLATE_CODE", EPD_TEMPLATE_CODE);
            Param.Add("EPD_PROBLEM_TYPE", EPD_PROBLEM_TYPE);

            objDB.ExecuteNonQuery("HMS_SP_EMR_PTDiagnosisAdd", Param);

        }

        public void DeleteEMR_PTDiagnosis()
        {
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPD_ID", EPD_ID);
            Param.Add("EPD_DIAG_CODE", EPD_DIAG_CODE);


            objDB.ExecuteNonQuery("HMS_SP_EMR_PTDiagnosisDelete", Param);

        }

    }

}