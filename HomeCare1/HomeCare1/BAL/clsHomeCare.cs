﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HomeCare1.DAL;
/// <summary>
/// Summary description for clsHomeCare
/// </summary>
/// 
namespace HomeCare1.BAL
{
    public class clsHomeCare
    {

        connectiondb c = new connectiondb();

        public DataSet HomeCareVisitGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_HomeCareVisitGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet ScheduleServiceGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_HC_ScheduleServGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public DataSet HomeCareStaffGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_HC_StaffAssignGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }


        public DataSet HomeCareVisitDelete(string HHV_ID)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_HomeCareVisitDelete";
            cmd.Parameters.Add(new SqlParameter("@HHV_ID", SqlDbType.VarChar)).Value = HHV_ID;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }

        public void ScheduleServiceDelete(string HHV_ID, string HHSS_ID)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_HC_ScheduleServDelete";
            cmd.Parameters.Add(new SqlParameter("@HHV_ID", SqlDbType.VarChar)).Value = HHV_ID;
            cmd.Parameters.Add(new SqlParameter("@HHSS_ID", SqlDbType.Int)).Value = HHSS_ID;




            cmd.ExecuteNonQuery();
            c.conclose();

        }

        public void HomeCareStaffDelete(string HHS_ID)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " DELETE FROM HMS_HC_STAFF_ASSIGN Where HHS_ID='" + HHS_ID + "'";
            cmd.ExecuteNonQuery();
            c.conclose();
        }


        public DataSet HomeCareNotesGet(string Criteria)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "HMS_SP_HC_PT_MonitoringGet";
            cmd.Parameters.Add(new SqlParameter("@Criteria", SqlDbType.VarChar)).Value = Criteria;
            cmd.ExecuteNonQuery();
            adpt.SelectCommand = cmd;
            adpt.Fill(ds);
            c.conclose();
            return ds;

        }


        public void HomeCareNotesDelete(string HHN_ID)
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " UPDATE HMS_HC_PT_MONITORING SET HHN_STAtUS ='InActive'  WHERE HHN_ID='" + HHN_ID + "'";
            cmd.ExecuteNonQuery();
            c.conclose();
        }


        public void HomeCareReports()
        {
            c.conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            cmd.Connection = c.con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " SELECT * FROM HMS_VIEW_HC_Reports";
            cmd.ExecuteNonQuery();
            c.conclose();
        }


    }
}