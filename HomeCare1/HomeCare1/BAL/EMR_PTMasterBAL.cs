﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;
namespace HomeCare1.BAL
{
    public class EMR_PTMasterBAL
    {
        dbOperation objDB = new dbOperation();

        public string EPM_ID { set; get; }
        public string EPM_BRANCH_ID { set; get; }
        public string EPM_DATE { set; get; }
        public string EPM_PT_ID { set; get; }
        public string EPM_PT_NAME { set; get; }
        public string EPM_AGE { set; get; }
        public string EPM_AGE_TYPE { set; get; }
        public string EPM_DR_CODE { set; get; }
        public string EPM_DR_NAME { set; get; }
        public string EPM_INS_CODE { set; get; }
        public string EPM_INS_NAME { set; get; }
        public string EPM_START_DATE { set; get; }
        public string EPM_END_DATE { set; get; }
        public string EPM_DEP_ID { set; get; }
        public string EPM_DEP_NAME { set; get; }
        public string EMR_PT_TYPE { set; get; }

        public string UserId { set; get; }


        public DataSet GetEMR_PTMaster(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_EMRPTMasterGet", Param);



            return DS;

        }


        public void AddEMR_PTMaster(out string EMR_ID)
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("EPM_BRANCH_ID", EPM_BRANCH_ID);
            Param.Add("EPM_ID", EPM_ID);
            Param.Add("EPM_DATE", EPM_DATE);
            Param.Add("EPM_PT_ID", EPM_PT_ID);
            Param.Add("EPM_PT_NAME", EPM_PT_NAME);
            Param.Add("EPM_AGE", EPM_AGE);

            Param.Add("EPM_AGE_TYPE", EPM_AGE_TYPE);
            Param.Add("EPM_DR_CODE", EPM_DR_CODE);
            Param.Add("EPM_DR_NAME", EPM_DR_NAME);
            Param.Add("EPM_INS_CODE", EPM_INS_CODE);
            Param.Add("EPM_INS_NAME", EPM_INS_NAME);

            Param.Add("EPM_START_DATE", EPM_START_DATE);
            Param.Add("EPM_END_DATE", EPM_END_DATE);
            Param.Add("EPM_DEP_ID", EPM_DEP_ID);
            Param.Add("EPM_DEP_NAME", EPM_DEP_NAME);
            Param.Add("EMR_PT_TYPE", EMR_PT_TYPE);

            Param.Add("UserId", UserId);
            EMR_ID = objDB.ExecuteNonQueryReturn("HMS_SP_EMR_PT_Add", Param);

        }


    }
}
