﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class PTEncounterBAL
    {

        public string HPE_ID { set; get; }
        public string HPE_HHV_ID { set; get; }
        public string HPE_AREA { set; get; }
        public string HPE_ARRIVAL_TIME { set; get; }
        public string HPE_VISIT_DATE { set; get; }
        public string HPE_DIAGNOSIS { set; get; }
        public string HPE_CONDITION { set; get; }
        public string HPE_CONDITION_OTH { set; get; }
        public string HPE_INFOBTAINDFROM { set; get; }
        public string HPE_INFOBTAINDFROM_OTH { set; get; }
        public string HPE_WEIGHT { set; get; }
        public string HPE_HEIGHT { set; get; }
        public string HPE_BMI { set; get; }
        public string HPE_TEMPERATURE_F { set; get; }
        public string HPE_TEMPERATURE_C { set; get; }
        public string HPE_PULSE { set; get; }
        public string HPE_RESPIRATION { set; get; }
        public string HPE_BP_SYSTOLIC { set; get; }
        public string HPE_BP_DIASTOLIC { set; get; }
        public string HPE_HPO2 { set; get; }
        public string HPE_EXPLI_GIVEN { set; get; }
        public string HPE_EXPLI_GIVEN_OTH { set; get; }
        public string HPE_ASSESSMENT_DONE_CODE { set; get; }
        public string HPE_ASSESSMENT_DONE_NAME { set; get; }


        public string HPE_PRESENT_COMPLAINT { set; get; }
        public string HPE_PAST_MEDICAL_HISTORY { set; get; }
        public string HPE_PAST_SURGICAL_HISTORY { set; get; }
        public string HPE_PSY_SOC_ECONOMIC_HISTORY { set; get; }
        public string HPE_FAMILY_HISTORY { set; get; }
        public string HPE_PHYSICAL_ASSESSMENT { set; get; }
        public string HPE_REVIEW_OF_SYSTEMS { set; get; }
        public string HPE_INVESTIGATIONS { set; get; }
        public string HPE_PRINCIPAL_DIAGNOSIS { set; get; }
        public string HPE_SECONDARY_DIAGNOSIS { set; get; }
        public string HPE_TREATMENT_PLAN { set; get; }
        public string HPE_PROCEDURE { set; get; }
        public string HPE_TREATMENT { set; get; }
        public string HPE_FOLLOWUPNOTES { set; get; }

        public string UserId { set; get; }


        public string AddPTEncounter(PTEncounterBAL objINBAL)
        {
            PTEncounterDAL obj = new PTEncounterDAL();
            string strReturnId;
            strReturnId = obj.AddPTEncounter(objINBAL);
            return strReturnId;

        }

        public DataSet GetPTEncounter(string Criteria)
        {
            DataSet DS = new DataSet();

            PTEncounterDAL obj = new PTEncounterDAL();
            DS = obj.GetPTEncounter(Criteria);
            return (DS);

        }

        public void DeletePTEncounter(PTEncounterBAL objINBAL)
        {
            PTEncounterDAL obj = new PTEncounterDAL();
            obj.DeletePTEncounter(objINBAL);
        }

    }
}