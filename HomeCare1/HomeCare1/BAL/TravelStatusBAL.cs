﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class TravelStatusBAL
    {

        public string Id { set; get; }
        public string HomeCareId { set; get; }
        public string HHTS_DATE { set; get; }
        public string HHTS_START_TIME { set; get; }
        public string HHTS_REACH_TIME { set; get; }
        public string HHTS_MILEAGE_UP { set; get; }
        public string HHTS_MILEAGE_DOWN { set; get; }
        public string HHTS_VEHICLE_NO { set; get; }
        public string HHTS_STAFF_ID { set; get; }
        public string HHTS_STAFF_NAME { set; get; }

        public string HHTS_DRIVER_ID { set; get; }
        public string HHTS_DRIVER_NAME { set; get; }
        public string UserId { set; get; }


        public string Criteria { set; get; }

        public void AddTravelStatus()
        {

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HHTS_ID", Id);
            Param.Add("HHTS_HHV_ID", HomeCareId);
            Param.Add("HHTS_DATE", HHTS_DATE);
            Param.Add("HHTS_START_TIME", HHTS_START_TIME);
            Param.Add("HHTS_REACH_TIME", HHTS_REACH_TIME);
            Param.Add("HHTS_MILEAGE_UP", HHTS_MILEAGE_UP);
            Param.Add("HHTS_MILEAGE_DOWN", HHTS_MILEAGE_DOWN);
            Param.Add("HHTS_VEHICLE_NO", HHTS_VEHICLE_NO);
            Param.Add("HHTS_STAFF_ID", HHTS_STAFF_ID);
            Param.Add("HHTS_STAFF_NAME", HHTS_STAFF_NAME);
            Param.Add("HHTS_DRIVER_ID", HHTS_DRIVER_ID);
            Param.Add("HHTS_DRIVER_NAME", HHTS_DRIVER_NAME);
            Param.Add("UserId", UserId);

            DBO.ExecuteNonQuery("HMS_SP_HomeCareTravelStatusAdd", Param);

        }

        public DataSet GetTravelStatus(string Criteria)
        {
            DataSet DS = new DataSet();

            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = DBO.ExecuteReader("HMS_SP_HomeCareTravelStatusGet", Param);
            return (DS);

        }

        public void DeleteTravelStatus()
        {
            dbOperation DBO = new dbOperation();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HHTS_ID", Id);
            DBO.ExecuteNonQuery("HMS_SP_HomeCareTravelStatusDelete", Param);

        }
    }
}