﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class PTAssessmentBAL
    {
        public string HPA_ID { set; get; }
        public string HPA_HHV_ID { set; get; }
        public string HPA_BRANCH_ID { set; get; }
        public string HPA_PT_ID { set; get; }
        public string HPA_HEALTH_PRESPASTHIST { set; get; }
        public string HPA_HEALTH_PRESPAS_PROB { set; get; }
        public string HPA_HEALTH_PRESPAS_PREVPROB { set; get; }
        public string HPA_HEALTH_ALLERGIES_MEDIC { set; get; }
        public string HPA_HEALTH_ALLERGIES_FOOD { set; get; }
        public string HPA_HEALTH_ALLERGIES_OTHER { set; get; }
        public string HPA_HEALTH_MEDICHIST_MEDICINE { set; get; }
        public string HPA_HEALTH_MEDICHIST_SLEEP { set; get; }
        public string HPA_HEALTH_MEDICHIST_PSCHOYASS { set; get; }
        public string HPA_HEALTH_PAIN_EXPRESSES { set; get; }
        public string HPA_PHY_GAST_BOWELSOUNDS { set; get; }
        public string HPA_PHY_GAST_ELIMINATION { set; get; }
        public string HPA_PHY_GAST_GENERAL { set; get; }
        public string HPA_PHY_REPRO_MALE { set; get; }
        public string HPA_PHY_REPRO_FEMALE { set; get; }
        public string HPA_PHY_REPRO_BREASTS { set; get; }
        public string HPA_PHY_GENIT_GENERAL { set; get; }
        public string HPA_PHY_SKIN_COLOR { set; get; }
        public string HPA_PHY_SKIN_TEMPERATURE { set; get; }
        public string HPA_PHY_SKIN_LESIONS { set; get; }
        public string HPA_PHY_NEURO_GENERAL { set; get; }
        public string HPA_PHY_NEURO_CONSCIO { set; get; }
        public string HPA_PHY_NEURO_ORIENTED { set; get; }
        public string HPA_PHY_NEURO_TIMERESP { set; get; }
        public string HPA_PHY_CARDIO_GENERAL { set; get; }
        public string HPA_PHY_CARDIO_PULSE { set; get; }
        public string HPA_PHY_CARDIO_PEDALPULSE { set; get; }
        public string HPA_PHY_CARDIO_EDEMA { set; get; }
        public string HPA_PHY_CARDIO_NAILBEDS { set; get; }
        public string HPA_PHY_CARDIO_CAPILLARY { set; get; }
        public string HPA_PHY_ENT_EYES { set; get; }
        public string HPA_PHY_ENT_EARS { set; get; }
        public string HPA_PHY_ENT_NOSE { set; get; }
        public string HPA_PHY_ENT_THROAT { set; get; }
        public string HPA_PHY_RESP_CHEST { set; get; }
        public string HPA_PHY_RESP_BREATHPATT { set; get; }
        public string HPA_PHY_RESP_BREATHSOUND { set; get; }
        public string HPA_RESP_BREATHCOUGH { set; get; }
        public string HPA_PHY_NUTRI_DIET { set; get; }
        public string HPA_PHY_NUTRI_APPETITE { set; get; }
        public string HPA_PHY_NUTRI_NUTRISUPPORT { set; get; }
        public string HPA_PHY_NUTRI_FEEDINGDIF { set; get; }
        public string HPA_PHY_NUTRI_WEIGHTSTATUS { set; get; }
        public string HPA_PHY_NUTRI_DIAG { set; get; }
        public string HPA_RISK_SKINRISK_SENSORY { set; get; }
        public string HPA_RISK_SKINRISK_MOISTURE { set; get; }
        public string HPA_RISK_SKINRISK_ACTIVITY { set; get; }
        public string HPA_RISK_SKINRISK_MOBILITY { set; get; }
        public string HPA_RISK_SKINRISK_NUTRITION { set; get; }
        public string HPA_RISK_SKINRISK_FRICTION { set; get; }
        public string HPA_RISK_SOCIO_LIVING { set; get; }
        public string HPA_RISK_SAFETY_GENERAL { set; get; }
        public string HPA_RISK_FUN_SELFCARING { set; get; }
        public string HPA_RISK_FUN_MUSCULOS { set; get; }
        public string HPA_RISK_FUN_EQUIPMENT { set; get; }
        public string HPA_RISK_EDU_GENERAL { set; get; }

        public string UserId { set; get; }



        public void AddPTAssessment(PTAssessmentBAL objINBAL)
        {
            PTAssessmentDAL obj = new PTAssessmentDAL();
            obj.AddPTAssessment(objINBAL);


        }

        public DataSet GetPTAssessment(string Criteria)
        {
            DataSet DS = new DataSet();

            PTAssessmentDAL obj = new PTAssessmentDAL();
            DS = obj.GetPTAssessment(Criteria);
            return (DS);

        }

        public void DeletePTAssessment(PTAssessmentBAL objINBAL)
        {
            PTAssessmentDAL obj = new PTAssessmentDAL();
            obj.DeletePTAssessment(objINBAL);
        }

    }
}