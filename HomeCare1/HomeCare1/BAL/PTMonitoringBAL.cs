﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using HomeCare1.DAL;



namespace HomeCare1.BAL
{
    public class PTMonitoringBAL
    {

        dbOperation objDB = new dbOperation();

        public string HHN_ID { set; get; }
        public string HHN_HHV_ID { set; get; }
        public string HHN_NOTES { set; get; }
        public string HHN_TYPE { set; get; }
        public string HHN_MONITORING_DATE { set; get; }


        public string UserId { set; get; }

        public string AddPTMonitoring()
        {
            string strReturnId;
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("HHN_ID", HHN_ID);
            Param.Add("HHN_HHV_ID", HHN_HHV_ID);
            Param.Add("HHN_NOTES", HHN_NOTES);
            Param.Add("HHN_TYPE", HHN_TYPE);
            Param.Add("HHN_MONITORING_DATE", HHN_MONITORING_DATE);
            Param.Add("UserId", UserId);

            strReturnId = objDB.ExecuteNonQuery("HMS_SP_HC_PT_MonitoringAdd", Param);
            return strReturnId;

        }

        public DataSet GetPTMonitoring(string Criteria)
        {
            DataSet DS = new DataSet();


            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_HC_PT_MonitoringGet", Param);
            return (DS);

        }


        public void PTMonitoringInactive(string HHN_ID)
        {
            string strSQL;

            strSQL = " UPDATE HMS_HC_PT_MONITORING SET HHN_STAtUS ='InActive'  WHERE HHN_ID='" + HHN_ID + "'";

            objDB.ExecuteNonQuery(strSQL);

        }
    }
}