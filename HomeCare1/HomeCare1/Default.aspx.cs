﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1
{
    public partial class _Default : Page
    {
        # region Variable Declaration
        connectiondb c = new connectiondb();
        dboperations dbo = new dboperations();
        # endregion

        #region Methods
        void BindBranch()
        {
            DataSet ds = new DataSet();
            ds = dbo.BranchMasterGet();
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpBranch.DataSource = ds;
                drpBranch.DataValueField = "HBM_ID";
                drpBranch.DataTextField = "HBM_ID";
                drpBranch.DataBind();

            }

        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            DataSet ds = new DataSet();
            ds = dbo.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpUsers.DataSource = ds;
                drpUsers.DataValueField = "HUM_USER_ID";
                drpUsers.DataTextField = "HUM_USER_NAME";
                drpUsers.DataBind();

            }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();
            DS = dbo.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEF_CUR_ID") == false)
                {
                    Session["HSOM_DEF_CUR_ID"] = DS.Tables[0].Rows[0]["HSOM_DEF_CUR_ID"].ToString();

                }
                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_AUTO_SL_NO") == false)
                {
                    Session["AutoSlNo"] = DS.Tables[0].Rows[0]["HSOM_AUTO_SL_NO"].ToString();

                }


                //PATIENT VISIT STATUS IS 'F' IF IT IS Y ELSE IT WILL BE 'W'
                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILING_REQUEST") == false)
                {
                    Session["FilingRequest"] = DS.Tables[0].Rows[0]["HSOM_FILING_REQUEST"].ToString();

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"].ToString();

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IdPrint"] = DS.Tables[0].Rows[0]["HSOM_ID_PRINT"].ToString();

                }
                //CLEAR THE DATA AFTER SAVING
                if (DS.Tables[0].Rows[0].IsNull("HSOM_CLEAR_AFT_SAVE") == false)
                {
                    Session["ClearAfterSave"] = DS.Tables[0].Rows[0]["HSOM_CLEAR_AFT_SAVE"].ToString();

                }

                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_SCAN_SAVE") == false)
                {
                    Session["ScanSave"] = DS.Tables[0].Rows[0]["HSOM_SCAN_SAVE"].ToString();

                }
                else
                {
                    Session["ScanSave"] = "0";
                }

                //REVISIT DAYS 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_REVISIT_DAYS") == false)
                {
                    Session["RevisitDays"] = DS.Tables[0].Rows[0]["HSOM_REVISIT_DAYS"].ToString();

                }


                //NETWORK NAME  DROPDWON IS MANDATORY OR NOT
                if (DS.Tables[0].Rows[0].IsNull("HSOM_PACKAGE_MANDITORY_PTREG") == false)
                {
                    Session["PackageManditory"] = DS.Tables[0].Rows[0]["HSOM_PACKAGE_MANDITORY_PTREG"].ToString();


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_MULTIPLEVISITADAY") == false)
                {
                    Session["MultipleVisitADay"] = DS.Tables[0].Rows[0]["HSOM_MULTIPLEVISITADAY"].ToString();


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PTNAME_MULTIPLE") == false)
                {
                    Session["PTNameMultiple"] = DS.Tables[0].Rows[0]["HSOM_PTNAME_MULTIPLE"].ToString();

                }

                if (DS.Tables[0].Rows[0].IsNull("hsom_label_copies") == false)
                {
                    Session["LabelCopies"] = DS.Tables[0].Rows[0]["hsom_label_copies"].ToString();

                }
                else
                {
                    Session["LabelCopies"] = 1;
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_TYPE") == false)
                {
                    Session["DefaultType"] = DS.Tables[0].Rows[0]["HSOM_DEFAULT_TYPE"].ToString();

                }
                else
                {
                    Session["DefaultType"] = "false";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SEARCH_OPT") == false)
                {
                    Session["SearchOption"] = DS.Tables[0].Rows[0]["HSOM_SEARCH_OPT"].ToString();

                }
                else
                {
                    Session["SearchOption"] = "AN";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILENO_MONTHYEAR") == false)
                {
                    Session["FileNoMonthYear"] = DS.Tables[0].Rows[0]["HSOM_FILENO_MONTHYEAR"].ToString();

                }
                else
                {
                    Session["FileNoMonthYear"] = "N";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_REPORT_PATH") == false)
                {
                    Session["ReportPath"] = DS.Tables[0].Rows[0]["HSOM_REPORT_PATH"].ToString();

                }
                else
                {
                    Session["ReportPath"] = "D:\\HMS\\Reports";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"].ToString();

                }
                else
                {
                    Session["TokenPrint"] = "";
                }



                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IDPrint"] = DS.Tables[0].Rows[0]["HSOM_ID_PRINT"].ToString();

                }
                else
                {
                    Session["IDPrint"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_COINS_PTREG") == false)
                {
                    Session["ConInsPTReg"] = DS.Tables[0].Rows[0]["HSOM_COINS_PTREG"].ToString();

                }
                else
                {
                    Session["ConInsPTReg"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SHOW_TOKEN") == false)
                {
                    Session["ShowToken"] = DS.Tables[0].Rows[0]["HSOM_SHOW_TOKEN"].ToString();

                }
                else
                {
                    Session["ShowToken"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DSPLY_DR_PTREG") == false)
                {
                    Session["DrNameDisplay"] = DS.Tables[0].Rows[0]["HSOM_DSPLY_DR_PTREG"].ToString();

                }
                else
                {
                    Session["DrNameDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PRIORITY_OPT_FILING") == false)
                {
                    Session["PriorityDisplay"] = DS.Tables[0].Rows[0]["HSOM_PRIORITY_OPT_FILING"].ToString();

                }
                else
                {
                    Session["PriorityDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_KNOW_FROM") == false)
                {
                    Session["KnowFromDisplay"] = DS.Tables[0].Rows[0]["HSOM_KNOW_FROM"].ToString();

                }
                else
                {
                    Session["KnowFromDisplay"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEP_WISE_PTID") == false)
                {
                    Session["DeptWisePTId"] = DS.Tables[0].Rows[0]["HSOM_DEP_WISE_PTID"].ToString();

                }
                else
                {
                    Session["DeptWisePTId"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    Session["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    Session["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    Session["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    Session["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    Session["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    Session["AppointmentEnd"] = "21";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ECLAIM") == false)
                {
                    Session["HSOM_ECLAIM"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ECLAIM"]);

                }
                else
                {
                    Session["HSOM_ECLAIM"] = "";
                }


            }

        }


        void BindStaffData()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";
            StaffMasterBAL objStaff = new StaffMasterBAL();
            DS = objStaff.GetStaffMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Session["User_DeptID"] = DS.Tables[0].Rows[0]["HSFM_DEPT_ID"].ToString();
            }
        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND ( SCREENNAME='EMR' OR  SCREENNAME='HMS' OR SCREENNAME='MEDIPLUS' OR    SCREENNAME='ECLAIM' ) ";

            DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("*", "SCREENCUSTOMIZATION", Criteria, "");





            Session["PWD_EXPIRY_DAYS"] = "0";

            Session["HMS_AUTHENTICATION"] = "0";
            Session["HMS_ENABLE_LOGFILE"] = "0";

            Session["PWD_ENCRYPT"] = "0";
            Session["PWD_MINIMUM_AGE"] = "0";

            Session["PWD_FORMAT_REQUIRED"] = "0";

            Session["PWD_MIN_LENGTH"] = "0";
            Session["PWD_MAX_LENGTH"] = "0";
            Session["PWD_NUM_MIN_LENGTH"] = "0";
            Session["PWD_LETTER_MIN_LENGTH"] = "0";




            Session["LOGIN_ACTIVE_DIRECTORY_USER"] = "0";
            Session["ACTIVE_DIRECTORY_NAME"] = "";


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_EXPIRY_DAYS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_EXPIRY_DAYS"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_AUTHENTICATION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_AUTHENTICATION"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_ENABLE_LOGFILE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_ENABLE_LOGFILE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_ENCRYPT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_ENCRYPT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MINIMUM_AGE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MINIMUM_AGE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_FORMAT_REQUIRED")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_FORMAT_REQUIRED"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MAX_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MAX_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_NUM_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_NUM_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_LETTER_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_LETTER_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_PREV_NOTREPEAT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_PREV_NOTREPEAT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }




                    if (Convert.ToString(DR["SEGMENT"]) == "LOGIN_ACTIVE_DIRECTORY_USER")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["LOGIN_ACTIVE_DIRECTORY_USER"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "ACTIVE_DIRECTORY_NAME")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["ACTIVE_DIRECTORY_NAME"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }



                }
            }



        }


        # endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSystemOption();
            if (!IsPostBack)
            {
                //Session.RemoveAll();

                Session["Branch_ID"] = null;
                Session["User_ID"] = null;
                Session["User_Code"] = null;
                Session["User_Name"] = null;
                Session["User_Active"] = null;
                Session["Roll_Id"] = null;
                Session["HomeCareId"] = null;

                BindScreenCustomization();
                txtPassword.Focus();
                BindBranch();
                BindUsers();

            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "' AND   HUM_USER_PASS= '" + txtPassword.Text.Trim() + "'";

            string EncryPassword = "";
            if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
            {
                CommonBAL objCom = new CommonBAL();
                EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtPassword.Text.Trim()));

                //if (GlobalValues.FileDescription == "SMCH")
                //{
                //    Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";
                //}
                //else
                //{
                Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";

                // }

            }
            else
            {
                Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "' AND   HUM_USER_PASS= '" + txtPassword.Text.Trim() + "'";

            }

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = dbo.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Session["Branch_ID"] = drpBranch.SelectedValue;
                Session["User_ID"] = ds.Tables[0].Rows[0]["HUM_USER_ID"].ToString();
                Session["User_Code"] = ds.Tables[0].Rows[0]["HUM_REMARK"].ToString();
                Session["User_Name"] = ds.Tables[0].Rows[0]["HUM_USER_NAME"].ToString();
                Session["User_Active"] = ds.Tables[0].Rows[0]["HUM_STATUS"].ToString();
                Session["Roll_Id"] = ds.Tables[0].Rows[0]["HGP_ROLL_ID"].ToString();

                BindStaffData();

                if (Session["User_Active"].ToString() == "A")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Login Sucess');", true);
                    // Response.Redirect("Welcome.aspx");
                    Response.Redirect("HomeCare/WaitingList.aspx");
                    ds.Clear();
                    ds.Dispose();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('User Inactive');", true);
                    ds.Clear();
                    ds.Dispose();
                }

            }
            else
            {
                txtPassword.Focus();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Login Failed');", true);
                ds.Clear();
                ds.Dispose();
            }

        }
    }
}