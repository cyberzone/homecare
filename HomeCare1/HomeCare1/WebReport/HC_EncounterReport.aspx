﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReport/Report.Master" AutoEventWireup="true" CodeBehind="HC_EncounterReport.aspx.cs" Inherits="HomeCare1.WebReport.HC_EncounterReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" Runat="Server">
     <style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        table, th, td
        {
            border: 1px solid #dcdcdc;
            height: 25px;
        }

        .BoldStyle
        {
            font-weight: bold;
        }
    </style>
    <div style="margin: 0px auto; width: 800px">
          <img style="padding:1px;" src="Content/images/HC_Report_Logo.PNG" />
        <br />
        <span class="PageHeader" style="font-size:18px;" >Patient Encounter </span>

        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Visit Date:</td>
                <td>
                    <asp:Label ID="lblVisitDate" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">Begin Date:</td>
                <td>
                    <asp:Label ID="lblBeginDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">End Date:</td>
                <td>
                    <asp:Label ID="lblEndDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1 BoldStyle" colspan="6">Patient Name :&nbsp;
                    <asp:Label ID="lblPTName" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1 BoldStyle">File No : 
                 <asp:Label ID="lblPTID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1 BoldStyle">Nationality : 
                 <asp:Label ID="lblNationality" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Age :
                    <asp:Label ID="lblAge" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    Y
                     <asp:Label ID="lblAge1" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    M

                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Emirates ID : 
                     <asp:Label ID="lblEmiratesID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Phone No :
                    <asp:Label ID="lblMobile" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Sex :
                    <asp:Label ID="lblSex" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Provider Name :
             <asp:Label ID="lblProviderName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Pollicy Type :
                    <asp:Label ID="lblPolicyType" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Policy No : 
                    <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Reference Doctor :
                    <asp:Label ID="lblRefBy" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">License No :
                    <asp:Label ID="lblRefLicNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Department :
                    <asp:Label ID="lblRefDept" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Facility Name :
                    <asp:Label ID="lblRefFacilityName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                 <td class="lblCaption1  BoldStyle">Authorization No :
                    <asp:Label ID="lblAuthorizationNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">

                    <asp:Label ID="Label4" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
        </table>
        <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Vital">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Vital Signs</span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">
               
                <tr>
                    <td class="lblCaption1  BoldStyle">
             <asp:gridview id="gvVital" runat="server" autogeneratecolumns="False" 
                        enablemodelvalidation="True" width="100%" >
                                 <HeaderStyle CssClass="GridRow"  Font-Bold="true"  />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-width="150px">
                                    <ItemTemplate>
                                            <asp:Label ID="lblVitalDate" CssClass="GridRow"   runat="server" Text='<%# Bind("VitalDateTime") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Weight">
                                    <ItemTemplate>
                                            <asp:Label ID="lblWeight" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_WEIGHT") %>'  ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Height">
                                    <ItemTemplate>
                                            <asp:Label ID="lblHeight" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_HEIGHT") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Temprature">
                                    <ItemTemplate>
                                            <asp:Label ID="lblTemperatureF" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_TEMPERATURE_F") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BMI">
                                    <ItemTemplate>
                                            <asp:Label ID="lblBMI" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_BMI") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pulse">
                                    <ItemTemplate>
                                            <asp:Label ID="lblPulse" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_PULSE") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Respiration">
                                    <ItemTemplate>
                                            <asp:Label ID="lblRespiration" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_RESPIRATION") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                              
                            </Columns>
                              

    </asp:gridview>
</td>
                </tr>
            </table>
        </div>
          <br />
         <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Surgeries">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Present Complaint  </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPresentComplaient" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PastMedicHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">PastMedical History </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPastMedicalHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_SurgicalHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Surgical History </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblSurgicallHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PsychoSocEcoHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Psycho socio- economic History	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPsychoSocEcoHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_FamilyHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Family History	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblFamilyHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PhysicalAssess">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Physical Assessment	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPhysicalAssessment" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_ReviewOfSys">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Review Of System	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblReviewOfSystem" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Investigations">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Investigations	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblInvestigations" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PrincDiag">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Principal Diagnosis	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPrincipalDiagnosis" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_SecDiag">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Secondary  Diagnosis	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblSecondaryDiagnosis" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_TreatPlan">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Treatment Plan </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblTreatmentPlan" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Procedure">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Procedure </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblHCEncounter_Procedure" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Treatment">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Treatment </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblTreatment" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_FollowUpNotes">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">FollowUp Notes </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblFollowUpNotes" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>

    </div>
</asp:Content>


