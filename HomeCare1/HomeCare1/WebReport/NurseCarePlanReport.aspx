﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReport/Report.Master" AutoEventWireup="true" CodeBehind="NurseCarePlanReport.aspx.cs" Inherits="HomeCare1.WebReport.NurseCarePlanReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" Runat="Server">
<style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        table, th, td
        {
            border: 1px solid #dcdcdc;
            height: 25px;
        }

        .BoldStyle
        {
            font-weight: bold;
        }
    </style>
    <div style="margin: 0px auto; width: 800px">
          <img style="padding:1px;" src="Content/images/HC_Report_Logo.PNG" />
        <br />
        <span class="PageHeader" style="font-size:18px;" >Nurseing Care Plan </span>

        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Visit Date:</td>
                <td>
                    <asp:Label ID="lblVisitDate" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">Begin Date:</td>
                <td>
                    <asp:Label ID="lblBeginDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">End Date:</td>
                <td>
                    <asp:Label ID="lblEndDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1 BoldStyle" colspan="6">Patient Name :&nbsp;
                    <asp:Label ID="lblPTName" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1 BoldStyle">File No : 
                 <asp:Label ID="lblPTID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1 BoldStyle">Nationality : 
                 <asp:Label ID="lblNationality" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Age :
                    <asp:Label ID="lblAge" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    Y
                     <asp:Label ID="lblAge1" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    M

                </td>
            </tr>
            <tr>
                 <td class="lblCaption1  BoldStyle">Emirates ID : 
                     <asp:Label ID="lblEmiratesID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Phone No :
                    <asp:Label ID="lblMobile" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Sex :
                    <asp:Label ID="lblSex" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Provider Name :
             <asp:Label ID="lblProviderName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Pollicy Type :
                    <asp:Label ID="lblPolicyType" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Policy No : 
                    <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Reference Doctor :
                    <asp:Label ID="lblRefBy" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">License No :
                    <asp:Label ID="lblRefLicNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Department :
                    <asp:Label ID="lblRefDept" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Facility Name :
                    <asp:Label ID="lblRefFacilityName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                 <td class="lblCaption1  BoldStyle">Authorization No :
                    <asp:Label ID="lblAuthorizationNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">

                    <asp:Label ID="Label4" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <div contenteditable="true" runat="server" visible="false" id="divHC_NurseCarePlan">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Nursing Care Plan </span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">

                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvNursingCarePlan" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                <HeaderStyle CssClass="GridRow"  Font-Bold="true"   />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Assessment">
                                    <ItemTemplate>
                                       <asp:Label ID="lblAssessment" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_ASSESSMENT") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nursing Diagnosis" >
                                    <ItemTemplate>
                                         <asp:Label ID="lblAnalysis" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_ANALYSIS") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Implementation Plan" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblPlanning" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_PLANNING") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Evaluation" >
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_EVALUATION") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                             

                        </asp:gridview>



                    </td>
                </tr>
            </table>
        </div>
        <br />
    </div>
</asp:Content>

