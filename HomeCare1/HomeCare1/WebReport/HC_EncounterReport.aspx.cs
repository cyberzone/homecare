﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.WebReport
{
    public partial class HC_EncounterReport : System.Web.UI.Page
    {
        #region Methods
        public void BindHomeCareReg()
        {


            string Criteria = " 1=1 ";// AND HHV_BRANCH_ID = '" + Convert.ToString(ViewState["Branch_ID"]) + "'";
            Criteria += " AND HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
                //lblHCID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);
                lblRefBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_NAME"]);
                lblVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_VISIT_DATEDesc"]);
                lblBeginDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_BEGIN_DATEDesc"]);
                lblEndDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_END_DATEDesc"]);


                lblRefLicNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_LICENSE_NO"]);
                lblRefDept.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_DEPARTMENT"]);
                lblRefFacilityName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_FACILITY"]);
                lblAuthorizationNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_AUTHORIZATION_NO"]);

                //lblDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_DIAGNOSIS"]);
                //lblProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PROCEDURES"]);
                //lblSurgery.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_SURGERY"]);

                BindPTDetails();
                //ScheduleServiceBind();
                //SchedulePhyServiceBind();
                BindEncounter();
                BindVital();
            }

        }


        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + lblPTID.Text + "' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                    lblNationality.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);

                lblProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                lblSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                lblMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                lblAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE1"]);
                lblEmiratesID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                lblPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                lblPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);


            }

        }

        void BindEncounter()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPE_HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "'";

            PTEncounterBAL objPTEnc = new PTEncounterBAL();
            ds = objPTEnc.GetPTEncounter(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["HPE_ID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPE_ID"]);




                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRESENT_COMPLAINT"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PRESENT_COMPLAINT") == false)
                {
                    divHCEncounter_Surgeries.Visible = true;
                    lblPresentComplaient.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRESENT_COMPLAINT"]);

                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_MEDICAL_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PAST_MEDICAL_HISTORY") == false)
                {
                    divHCEncounter_PastMedicHist.Visible = true;
                    lblPastMedicalHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_MEDICAL_HISTORY"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_SURGICAL_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PAST_SURGICAL_HISTORY") == false)
                {
                    divHCEncounter_SurgicalHist.Visible = true;
                    lblSurgicallHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_SURGICAL_HISTORY"]);
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PSY_SOC_ECONOMIC_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PSY_SOC_ECONOMIC_HISTORY") == false)
                {
                    divHCEncounter_PsychoSocEcoHist.Visible = true;
                    lblPsychoSocEcoHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PSY_SOC_ECONOMIC_HISTORY"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_FAMILY_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_FAMILY_HISTORY") == false)
                {
                    divHCEncounter_FamilyHist.Visible = true;
                    lblFamilyHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_FAMILY_HISTORY"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PHYSICAL_ASSESSMENT"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PHYSICAL_ASSESSMENT") == false)
                {
                    divHCEncounter_PhysicalAssess.Visible = true;
                    lblPhysicalAssessment.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PHYSICAL_ASSESSMENT"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_REVIEW_OF_SYSTEMS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_REVIEW_OF_SYSTEMS") == false)
                {
                    divHCEncounter_ReviewOfSys.Visible = true;
                    lblReviewOfSystem.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_REVIEW_OF_SYSTEMS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_INVESTIGATIONS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_INVESTIGATIONS") == false)
                {
                    divHCEncounter_Investigations.Visible = true;
                    lblInvestigations.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_INVESTIGATIONS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRINCIPAL_DIAGNOSIS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PRINCIPAL_DIAGNOSIS") == false)
                {
                    divHCEncounter_PrincDiag.Visible = true;
                    lblPrincipalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRINCIPAL_DIAGNOSIS"]);
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_SECONDARY_DIAGNOSIS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_SECONDARY_DIAGNOSIS") == false)
                {
                    divHCEncounter_SecDiag.Visible = true;
                    lblSecondaryDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_SECONDARY_DIAGNOSIS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT_PLAN"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_TREATMENT_PLAN") == false)
                {
                    divHCEncounter_TreatPlan.Visible = true;
                    lblTreatmentPlan.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT_PLAN"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PROCEDURE"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PROCEDURE") == false)
                {
                    divHCEncounter_Procedure.Visible = true;
                    lblHCEncounter_Procedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PROCEDURE"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_TREATMENT") == false)
                {
                    divHCEncounter_Treatment.Visible = true;
                    lblTreatment.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_FOLLOWUPNOTES"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_FOLLOWUPNOTES") == false)
                {
                    divHCEncounter_FollowUpNotes.Visible = true;
                    lblFollowUpNotes.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_FOLLOWUPNOTES"]);
                }

            }



        }

        void BindVital()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPV_HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "'";

            PTVitalSignsBAL objPTVital = new PTVitalSignsBAL();
            ds = objPTVital.GetPTVitalSigns(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHCEncounter_Vital.Visible = true;
                gvVital.DataSource = ds;
                gvVital.DataBind();

            }
            else
            {
                gvVital.DataBind();
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["HomeCareId"] = "0";
                ViewState["HomeCareId"] = Request.QueryString["HomeCareId"].ToString();
                BindHomeCareReg();


            }
        }
    }
}