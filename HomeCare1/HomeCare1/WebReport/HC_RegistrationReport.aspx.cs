﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.WebReport
{
    public partial class HC_RegistrationReport : System.Web.UI.Page
    {
        #region Methods
        public void BindHomeCareReg()
        {


            string Criteria = " 1=1 ";// AND HHV_BRANCH_ID = '" + Convert.ToString(ViewState["Branch_ID"]) + "'";
            Criteria += " AND HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
                // lblHCID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);
                lblRefBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_NAME"]);
                lblVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_VISIT_DATEDesc"]);
                lblBeginDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_BEGIN_DATEDesc"]);
                lblEndDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_END_DATEDesc"]);


                lblRefLicNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_LICENSE_NO"]);
                lblRefDept.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_DEPARTMENT"]);
                lblRefFacilityName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_FACILITY"]);
                lblAuthorizationNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_AUTHORIZATION_NO"]);


                if (Convert.ToString(ds.Tables[0].Rows[0]["HHV_DIAGNOSIS"]) != "" && ds.Tables[0].Rows[0].IsNull("HHV_DIAGNOSIS") == false)
                {
                    divHCReg_Diagnosis.Visible = true;
                    lblDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_DIAGNOSIS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HHV_PROCEDURES"]) != "" && ds.Tables[0].Rows[0].IsNull("HHV_PROCEDURES") == false)
                {
                    divHCReg_Procedures.Visible = true;
                    lblProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PROCEDURES"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HHV_SURGERY"]) != "" && ds.Tables[0].Rows[0].IsNull("HHV_SURGERY") == false)
                {
                    divHCReg_Surgeries.Visible = true;
                    lblSurgery.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_SURGERY"]);
                }



                BindPTDetails();
                ScheduleServiceBind();
                SchedulePhyServiceBind();

            }

        }

        void ScheduleServiceBind()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHSS_TYPE = 'Nursing'";
            Criteria += " AND HHSS_HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";

            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.ScheduleServiceGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHCReg_NursingServices.Visible = true;
                gvNursingService.DataSource = ds;
                gvNursingService.DataBind();

            }
            else
            {
                gvNursingService.DataBind();
            }

        }

        void SchedulePhyServiceBind()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHSS_TYPE = 'Physiotherapy'";
            Criteria += " AND HHSS_HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";

            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.ScheduleServiceGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHCReg_PhysiotherapyServices.Visible = true;

                gvPhyService.DataSource = ds;
                gvPhyService.DataBind();

            }
            else
            {
                gvPhyService.DataBind();
            }

        }

        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + lblPTID.Text + "' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                    lblNationality.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);

                lblProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                lblSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                lblMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                lblAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE1"]);
                lblEmiratesID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                lblPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                lblPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);


            }

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["HomeCareId"] = "0";
                ViewState["HomeCareId"] = Request.QueryString["HomeCareId"].ToString();
                BindHomeCareReg();


            }
        }
    }
}