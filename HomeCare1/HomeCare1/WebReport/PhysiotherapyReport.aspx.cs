﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.WebReport
{
    public partial class PhysiotherapyReport : System.Web.UI.Page
    {
        #region Methods
        public void BindHomeCareReg()
        {


            string Criteria = " 1=1 ";// AND HHV_BRANCH_ID = '" + Convert.ToString(ViewState["Branch_ID"]) + "'";
            Criteria += " AND HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
                // lblHCID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);
                lblRefBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_NAME"]);
                lblVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_VISIT_DATEDesc"]);
                lblBeginDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_BEGIN_DATEDesc"]);
                lblEndDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_END_DATEDesc"]);


                lblRefLicNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_LICENSE_NO"]);
                lblRefDept.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_DEPARTMENT"]);
                lblRefFacilityName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_FACILITY"]);
                lblAuthorizationNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_AUTHORIZATION_NO"]);

                //lblDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_DIAGNOSIS"]);
                //lblProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PROCEDURES"]);
                //lblSurgery.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_SURGERY"]);

                BindPTDetails();
                //ScheduleServiceBind();
                //SchedulePhyServiceBind();
                BindPhysicalTherapyEval();

            }

        }

        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + lblPTID.Text + "' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                    lblNationality.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);

                lblProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                lblSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                lblMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                lblAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE1"]);
                lblEmiratesID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                lblPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                lblPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);

            }

        }

        void BindPhysicalTherapyEval()
        {
            PhysicalTherapyEvaluationBAL objPTEval = new PhysicalTherapyEvaluationBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHPH_HHV_ID   = '" + Convert.ToString(Session["HomeCareId"]) + "' ";
            DS = objPTEval.GetPhysicalTherapyEvaluation(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                divPhyTherapy.Visible = true;

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CHIEF_COMPLAINT"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_CHIEF_COMPLAINT") == false)
                {
                    divPhyTherapyChiefCoplaint.Visible = true;
                    lblPhyTherapyChiefCoplaint.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CHIEF_COMPLAINT"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_DATEDesc"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_INJURY_DATEDesc") == false)
                {
                    lblPhyTherapyInj_Surg_Date.Visible = true;
                    lblPhyTherapyInjuryDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_DATEDesc"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SURGERY_DATEDesc"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_SURGERY_DATEDesc") == false)
                {
                    lblPhyTherapyInj_Surg_Date.Visible = true;
                    lblPhyTherapySurgeryDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SURGERY_DATEDesc"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_REMARKS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_INJURY_REMARKS") == false)
                {
                    divPhyTherapyInjuryRemarks.Visible = true;
                    lblPhyTherapyInjuryRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_REMARKS"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_DATEDesc"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_THERAPY_DATEDesc") == false)
                {
                    divPhyTherapyReceived.Visible = true;
                    lblPhyTherapyReceivedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_DATEDesc"]);

                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_VISIT_COUNT"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_THERAPY_VISIT_COUNT") == false)
                {
                    divPhyTherapyReceived.Visible = true;

                    lblPhyTherapyHowmany.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_VISIT_COUNT"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_CONDITION") == false)
                {
                    divPhyTherapyCondition.Visible = true;
                    lblPhyTherapyCondition.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SYMPTOMS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_SYMPTOMS") == false)
                {
                    divPhyTherapyCondition.Visible = true;
                    lblPhyTherapySymptoms.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SYMPTOMS"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAIN_BEST"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PAIN_BEST") == false)
                {
                    divPhyTherapyBest_Worst.Visible = true;
                    lblPhyTherapyAtBest.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAIN_BEST"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAINT_WORST"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PAINT_WORST") == false)
                {
                    divPhyTherapyBest_Worst.Visible = true;
                    lblPhyTherapyAtWorst.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAINT_WORST"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION_BETTER"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_CONDITION_BETTER") == false)
                {
                    radAtBest.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION_BETTER"]);

                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION_WORSE"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_CONDITION_WORSE") == false)
                {

                    radAtWorst.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION_WORSE"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_MED_INTERVENTION"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PREV_MED_INTERVENTION") == false)
                {

                    lblPrevMedIntervention.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_MED_INTERVENTION"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_MED_INTERVENTION_OTHER"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PREV_MED_INTERVENTION_OTHER") == false)
                {

                    lblPrevIntOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_MED_INTERVENTION_OTHER"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_GOALS_TOBE_ACHIEVED"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_GOALS_TOBE_ACHIEVED") == false)
                {

                    lblGoals.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_GOALS_TOBE_ACHIEVED"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_SURGERIES"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PREV_SURGERIES") == false)
                {

                    lblPrevSurgeries.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_SURGERIES"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_MEDICATIONS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_MEDICATIONS") == false)
                {

                    txtMedications.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_MEDICATIONS"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_ALLERGIES"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_ALLERGIES") == false)
                {

                    txtAllergies.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_ALLERGIES"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_OTHER"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_OTHER") == false)
                {

                    txtOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_OTHER"]);
                }

                string strMedicalInfo = "";



                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_OTHER"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_OTHER") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_DIFFICULTY_SWALLOWING"]) == true)
                    {
                        strMedicalInfo += "Difficulty Swallowing ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_MOTION_SICKNESS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_MOTION_SICKNESS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_MOTION_SICKNESS"]) == true)
                    {
                        strMedicalInfo += "Motion Sickness ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_STROKE"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_STROKE") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_STROKE"]) == true)
                    {
                        strMedicalInfo += "Stroke ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_ARTHRITIS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_ARTHRITIS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_ARTHRITIS"]) == true)
                    {
                        strMedicalInfo += "Arthritis ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_FEVER_CHILLS_SWEATS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_FEVER_CHILLS_SWEATS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_FEVER_CHILLS_SWEATS"]) == true)
                    {
                        strMedicalInfo += "Fever/Chills/Sweats ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_OSTEOPOROSIS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_OSTEOPOROSIS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_OSTEOPOROSIS"]) == true)
                    {
                        strMedicalInfo += "Osteoporosis ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_HIGH_BLOOD_PRESSURE"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_HIGH_BLOOD_PRESSURE") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HIGH_BLOOD_PRESSURE"]) == true)
                    {
                        strMedicalInfo += "High Blood Pressure ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_UNEXPLAINED_WEIGHT_LOSS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_UNEXPLAINED_WEIGHT_LOSS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_UNEXPLAINED_WEIGHT_LOSS"]) == true)
                    {
                        strMedicalInfo += "Unexplained Weight Loss ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_ANEMIA"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_ANEMIA") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_ANEMIA"]) == true)
                    {
                        strMedicalInfo += "Anemia ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_HEART_TROUBLE"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_HEART_TROUBLE") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HEART_TROUBLE"]) == true)
                    {
                        strMedicalInfo += "Heart Trouble ,";
                    }
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_BLOOD_CLOTS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_BLOOD_CLOTS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_BLOOD_CLOTS"]) == true)
                    {
                        strMedicalInfo += "Blood Clots ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_BLEEDING_PROBLEMS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_BLEEDING_PROBLEMS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_BLEEDING_PROBLEMS"]) == true)
                    {
                        strMedicalInfo += "Bleeding Problems ,";
                    }
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PACEMAKER"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PACEMAKER") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_PACEMAKER"]) == true)
                    {
                        strMedicalInfo += "Pacemaker ,";
                    }
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SHORTNESS_OF_BREATH"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_SHORTNESS_OF_BREATH") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_SHORTNESS_OF_BREATH"]) == true)
                    {
                        strMedicalInfo += "Shortness Of Breath ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_HIV_HEPATITIS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_HIV_HEPATITIS") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HIV_HEPATITIS"]) == true)
                    {
                        strMedicalInfo += "HIV/Hepatitis ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_EPILEPSY_SEIZURES"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_EPILEPSY_SEIZURES") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_EPILEPSY_SEIZURES"]) == true)
                    {
                        strMedicalInfo += "Epilepsy/Seizures ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_HISTORY_SMOKING"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_HISTORY_SMOKING") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HISTORY_SMOKING"]) == true)
                    {
                        strMedicalInfo += "History Of Smoking ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_HISTORY_ALCOHOL_ABUSE"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_HISTORY_ALCOHOL_ABUSE") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HISTORY_ALCOHOL_ABUSE"]) == true)
                    {
                        strMedicalInfo += "Hist.Of Alcohol Abuse ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_HISTORY_DRUG_ABUSE"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_HISTORY_DRUG_ABUSE") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HISTORY_DRUG_ABUSE"]) == true)
                    {
                        strMedicalInfo += "Hist. Of Drug Abuse ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_DIABETES"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_DIABETES") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_DIABETES"]) == true)
                    {
                        strMedicalInfo += "Diabetes ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_DEPRESSION_ANXIETY"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_DEPRESSION_ANXIETY") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_DEPRESSION_ANXIETY"]) == true)
                    {
                        strMedicalInfo += "Depression/Anxiety ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_MYOFASCIAL_PAIN"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_MYOFASCIAL_PAIN") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_MYOFASCIAL_PAIN"]) == true)
                    {
                        strMedicalInfo += "Myofascial Pain ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_FIBROMYALGIA"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_FIBROMYALGIA") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_FIBROMYALGIA"]) == true)
                    {
                        strMedicalInfo += "Fibromyalgia ,";
                    }
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREGNANCY"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PREGNANCY") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_PREGNANCY"]) == true)
                    {
                        strMedicalInfo += "Pregnancy ,";
                    }
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CANCER"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_CANCER") == false)
                {
                    if (Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_CANCER"]) == true)
                    {
                        strMedicalInfo += "Cancer ,";
                    }
                }



                lblMedicalInfo.Text = strMedicalInfo;


            }

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["HomeCareId"] = "0";
                ViewState["HomeCareId"] = Request.QueryString["HomeCareId"].ToString();
                BindHomeCareReg();


            }
        }
    }
}