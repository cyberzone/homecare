﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.WebReport
{
    public partial class HC_PatientSummary : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindHomeCareReg()
        {
            divHCReg_Diagnosis.Visible = false;
            divHCReg_Procedures.Visible = false;
            divHCReg_Surgeries.Visible = false;

            string Criteria = " 1=1 ";// AND HHV_BRANCH_ID = '" + Convert.ToString(ViewState["Branch_ID"]) + "'";
            Criteria += " AND HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
                // lblHCID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);
                lblRefBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_NAME"]);
                lblVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_VISIT_DATEDesc"]);
                lblBeginDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_BEGIN_DATEDesc"]);
                lblEndDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_END_DATEDesc"]);


                lblRefLicNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_LICENSE_NO"]);
                lblRefDept.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_DEPARTMENT"]);
                lblRefFacilityName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_FACILITY"]);
                lblAuthorizationNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_AUTHORIZATION_NO"]);

                if (Convert.ToString(ds.Tables[0].Rows[0]["HHV_DIAGNOSIS"]) != "" && ds.Tables[0].Rows[0].IsNull("HHV_DIAGNOSIS") == false)
                {
                    divHCReg_Diagnosis.Visible = true;
                    lblDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_DIAGNOSIS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HHV_PROCEDURES"]) != "" && ds.Tables[0].Rows[0].IsNull("HHV_PROCEDURES") == false)
                {
                    divHCReg_Procedures.Visible = true;
                    lblProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PROCEDURES"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HHV_SURGERY"]) != "" && ds.Tables[0].Rows[0].IsNull("HHV_SURGERY") == false)
                {
                    divHCReg_Surgeries.Visible = true;
                    lblSurgery.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_SURGERY"]);
                }

                BindPTDetails();
                ScheduleServiceBind();
                SchedulePhyServiceBind();

                BindEncounter();
                BindVital();
                HomeCareNotesBind("PROGRESS");
                HomeCareNotesBind("EMERGENCY");
                HomeCareNotesBind("ADDITIONAL");

                BindNursingCarePlan();
                BindAssessment();

                BindPhysicalTherapyEval();

            }

        }

        void ScheduleServiceBind()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHSS_TYPE = 'Nursing'";
            Criteria += " AND HHSS_HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";
            divHCReg_NursingServices.Visible = false;
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.ScheduleServiceGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHCReg_NursingServices.Visible = true;
                gvNursingService.DataSource = ds;
                gvNursingService.DataBind();

            }
            else
            {
                gvNursingService.DataBind();
            }

        }

        void SchedulePhyServiceBind()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHSS_TYPE = 'Physiotherapy'";
            Criteria += " AND HHSS_HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";

            divHCReg_PhysiotherapyServices.Visible = false;
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.ScheduleServiceGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHCReg_PhysiotherapyServices.Visible = true;
                gvPhyService.DataSource = ds;
                gvPhyService.DataBind();

            }
            else
            {
                gvPhyService.DataBind();
            }

        }

        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + lblPTID.Text + "' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                    lblNationality.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);

                lblProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                lblSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                lblMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                lblAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE1"]);
                lblEmiratesID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                lblPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                lblPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);


            }

        }

        void BindEncounter()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPE_HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "'";

            PTEncounterBAL objPTEnc = new PTEncounterBAL();
            ds = objPTEnc.GetPTEncounter(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHCEncounter.Visible = true;

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_AREA"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_AREA") == false)
                {
                    divHCEncounterAreaTop.Visible = true;
                    divHCEncounterArea.Visible = true;
                    lblHCEncounterArea.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_AREA"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_ARRIVAL_TIMEDesc"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_ARRIVAL_TIMEDesc") == false)
                {
                    divHCEncounterAreaTop.Visible = true;
                    divHCEncounterArea.Visible = true;
                    lblHCEncounterArrivalDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_ARRIVAL_TIMEDesc"]);
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_VISIT_DATEDesc"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_VISIT_DATEDesc") == false)
                {
                    divHCEncounterAreaTop.Visible = true;
                    divHCEncounterArrivalDate.Visible = true;
                    lblHCEncounterVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_VISIT_DATEDesc"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPE_ASSESSMENT_DONE_CODE") == false)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_ASSESSMENT_DONE_CODE"]) != "")
                    {
                        divHCEncounterAreaTop.Visible = true;
                        lblHCEncountertAsseDoneBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_ASSESSMENT_DONE_CODE"]) + " ~ " + Convert.ToString(ds.Tables[0].Rows[0]["HPE_ASSESSMENT_DONE_NAME"]);

                    }
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_DIAGNOSIS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_DIAGNOSIS") == false)
                {
                    divHCEncounterProvisionalDiag.Visible = true;
                    lblHCEncounterProvisionalDiag.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_DIAGNOSIS"]);
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_CONDITION"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_CONDITION") == false)
                {
                    divHCEncounterCondition.Visible = true;
                    divHCEncounterCondUpon.Visible = true;
                    lblHCEncounterCondUponVisit.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_CONDITION"]);
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_CONDITION_OTH"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_CONDITION_OTH") == false)
                {
                    divHCEncounterCondition.Visible = true;
                    divHCEncounterCondUpon.Visible = true;
                    lblHCEncounterCondOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_CONDITION_OTH"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_INFOBTAINDFROM"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_INFOBTAINDFROM") == false)
                {
                    divHCEncounterCondition.Visible = true;
                    divHCEncounterInfoObtFrom.Visible = true;
                    lblHCEncounterInfoObtFrom.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_INFOBTAINDFROM"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_INFOBTAINDFROM_OTH"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_INFOBTAINDFROM_OTH") == false)
                {
                    divHCEncounterCondition.Visible = true;
                    divHCEncounterInfoObtFrom.Visible = true;
                    lblHCEncounterInfoObtOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_INFOBTAINDFROM_OTH"]);
                }



                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_EXPLI_GIVEN"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_EXPLI_GIVEN") == false)
                {
                    divHCEncounterCondition.Visible = true;
                    divHCEncounterExpGiven.Visible = true;
                    lblHCEncounterExpGiven.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_EXPLI_GIVEN"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_EXPLI_GIVEN_OTH"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_EXPLI_GIVEN_OTH") == false)
                {
                    divHCEncounterCondition.Visible = true;
                    divHCEncounterExpGiven.Visible = true;
                    lblHCEncounterExpOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_EXPLI_GIVEN_OTH"]);
                }



                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRESENT_COMPLAINT"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PRESENT_COMPLAINT") == false)
                {
                    divHCEncounter_Surgeries.Visible = true;
                    lblPresentComplaient.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRESENT_COMPLAINT"]);

                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_MEDICAL_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PAST_MEDICAL_HISTORY") == false)
                {
                    divHCEncounter_PastMedicHist.Visible = true;
                    lblPastMedicalHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_MEDICAL_HISTORY"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_SURGICAL_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PAST_SURGICAL_HISTORY") == false)
                {
                    divHCEncounter_SurgicalHist.Visible = true;
                    lblSurgicallHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_SURGICAL_HISTORY"]);
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PSY_SOC_ECONOMIC_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PSY_SOC_ECONOMIC_HISTORY") == false)
                {
                    divHCEncounter_PsychoSocEcoHist.Visible = true;
                    lblPsychoSocEcoHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PSY_SOC_ECONOMIC_HISTORY"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_FAMILY_HISTORY"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_FAMILY_HISTORY") == false)
                {
                    divHCEncounter_FamilyHist.Visible = true;
                    lblFamilyHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_FAMILY_HISTORY"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PHYSICAL_ASSESSMENT"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PHYSICAL_ASSESSMENT") == false)
                {
                    divHCEncounter_PhysicalAssess.Visible = true;
                    lblPhysicalAssessment.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PHYSICAL_ASSESSMENT"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_REVIEW_OF_SYSTEMS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_REVIEW_OF_SYSTEMS") == false)
                {
                    divHCEncounter_ReviewOfSys.Visible = true;
                    lblReviewOfSystem.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_REVIEW_OF_SYSTEMS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_INVESTIGATIONS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_INVESTIGATIONS") == false)
                {
                    divHCEncounter_Investigations.Visible = true;
                    lblInvestigations.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_INVESTIGATIONS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRINCIPAL_DIAGNOSIS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PRINCIPAL_DIAGNOSIS") == false)
                {
                    divHCEncounter_PrincDiag.Visible = true;
                    lblPrincipalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRINCIPAL_DIAGNOSIS"]);
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_SECONDARY_DIAGNOSIS"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_SECONDARY_DIAGNOSIS") == false)
                {
                    divHCEncounter_SecDiag.Visible = true;
                    lblSecondaryDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_SECONDARY_DIAGNOSIS"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT_PLAN"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_TREATMENT_PLAN") == false)
                {
                    divHCEncounter_TreatPlan.Visible = true;
                    lblTreatmentPlan.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT_PLAN"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_PROCEDURE"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_PROCEDURE") == false)
                {
                    divHCEncounter_Procedure.Visible = true;
                    lblHCEncounter_Procedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PROCEDURE"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_TREATMENT") == false)
                {
                    divHCEncounter_Treatment.Visible = true;
                    lblTreatment.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT"]);
                }
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_FOLLOWUPNOTES"]) != "" && ds.Tables[0].Rows[0].IsNull("HPE_FOLLOWUPNOTES") == false)
                {
                    divHCEncounter_FollowUpNotes.Visible = true;
                    lblFollowUpNotes.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_FOLLOWUPNOTES"]);
                }

            }



        }

        void BindVital()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPV_HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "'";
            divHCEncounter_Vital.Visible = false;
            PTVitalSignsBAL objPTVital = new PTVitalSignsBAL();
            ds = objPTVital.GetPTVitalSigns(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHCEncounter_Vital.Visible = true;
                gvVital.DataSource = ds;
                gvVital.DataBind();

            }
            else
            {
                gvVital.DataBind();
            }
        }

        void HomeCareNotesBind(string strType)
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHN_TYPE = '" + strType + "'";
            Criteria += " AND HHN_HHV_ID = '" + Convert.ToString(ViewState["HomeCareId"]) + "' ";


            PTMonitoringBAL objPT = new PTMonitoringBAL();
            ds = objPT.GetPTMonitoring(Criteria);

            if (strType == "PROGRESS")
            {
                divHC_Monitoring_ProgressNotes.Visible = false;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divHC_Monitoring_ProgressNotes.Visible = true;
                    gvProgNotes.DataSource = ds;
                    gvProgNotes.DataBind();
                }
                else
                {
                    gvProgNotes.DataBind();
                }

            }

            if (strType == "EMERGENCY")
            {
                divHC_Monitoring_EmergencyNotes.Visible = false;

                if (ds.Tables[0].Rows.Count > 0)
                {
                    divHC_Monitoring_EmergencyNotes.Visible = true;
                    gvEmerNotes.DataSource = ds;
                    gvEmerNotes.DataBind();
                }
                else
                {
                    gvEmerNotes.DataBind();
                }

            }

            if (strType == "ADDITIONAL")
            {
                divHC_Monitoring_AdditionalNotes.Visible = false;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divHC_Monitoring_AdditionalNotes.Visible = true;
                    gvAddiNotes.DataSource = ds;
                    gvAddiNotes.DataBind();
                }
                else
                {
                    gvAddiNotes.DataBind();
                }

            }


        }

        void BindNursingCarePlan()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HNCP_HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "'";
            divHC_NurseCarePlan.Visible = false;
            NursingCarePlanBAL objNCP = new NursingCarePlanBAL();
            ds = objNCP.GetNursingCarePlan(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                divHC_NurseCarePlan.Visible = true;
                gvNursingCarePlan.DataSource = ds;
                gvNursingCarePlan.DataBind();

            }
            else
            {
                gvNursingCarePlan.DataBind();
            }
        }

        void BindAssessment()
        {
            PTAssessmentBAL obj = new PTAssessmentBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPA_HHV_ID   = '" + Convert.ToString(Session["HomeCareId"]) + "' ";
            DS = obj.GetPTAssessment(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                divHCAssessment.Visible = true;
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPASTHIST"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_PRESPASTHIST") == false)
                {
                    divHCAss_PresPastProb.Visible = true;
                    divHCAss_Health_PresPastHist.Visible = true;
                    lblHC_ASS_Health_PresPastHist.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPASTHIST"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PROB"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_PRESPAS_PROB") == false)
                {
                    divHCAss_Health_PresPastHist.Visible = true;
                    lblPresPastProblem.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PROB"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PREVPROB"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_PRESPAS_PREVPROB") == false)
                {
                    divHCAss_PresPastProb.Visible = true;
                    divHCAss_Health_PresPastSurgeries.Visible = true;
                    lblPresPastSurgeries.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PREVPROB"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_MEDIC"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_ALLERGIES_MEDIC") == false)
                {
                    divHCAss_Health_Alergies.Visible = true;
                    divHCAss_Health_AlergiesMedication.Visible = true;
                    lblAlergiesMedication.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_MEDIC"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_FOOD"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_ALLERGIES_FOOD") == false)
                {
                    divHCAss_Health_Alergies.Visible = true;
                    divHCAss_Health_AlergiesFood.Visible = true;
                    lblAlergiesFood.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_FOOD"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_OTHER"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_ALLERGIES_OTHER") == false)
                {
                    divHCAss_Health_Alergies.Visible = true;
                    divHCAss_Health_AlergiesOther.Visible = true;
                    lblAlergiesOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_OTHER"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_MEDICINE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_MEDICHIST_MEDICINE") == false)
                {
                    divHCAss_Health_MedicHist.Visible = true;
                    divHCAss_Health_MedicHist_Medicine.Visible = true;
                    lblHC_ASS_Health_MedicHist_Medicine.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_MEDICINE"]);

                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_SLEEP"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_MEDICHIST_SLEEP") == false)
                {
                    divHCAss_Health_MedicHist.Visible = true;
                    divHCAss_Health_MedicHist_Sleep.Visible = true;
                    lblHC_ASS_Health_MedicHist_Sleep.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_SLEEP"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_PSCHOYASS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_MEDICHIST_PSCHOYASS") == false)
                {
                    divHCAss_Health_MedicHist.Visible = true;
                    divHCAss_Health_MedicHist_PschoyAss.Visible = true;

                    lblHC_ASS_Health_MedicHist_PschoyAss.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_PSCHOYASS"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PAIN_EXPRESSES"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_HEALTH_PAIN_EXPRESSES") == false)
                {
                    divHCAss_Health_MedicHist.Visible = true;
                    divHCAss_Health_MedicHist_PainAssYes.Visible = true;
                    lblPainAssYes.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PAIN_EXPRESSES"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_BOWELSOUNDS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_GAST_BOWELSOUNDS") == false)
                {
                    divHC_ASS_Phy_Gast.Visible = true;
                    divHC_ASS_Phy_Gast_BowelSounds.Visible = true;
                    lblHC_ASS_Phy_Gast_BowelSounds.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_BOWELSOUNDS"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_ELIMINATION"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_GAST_ELIMINATION") == false)
                {
                    divHC_ASS_Phy_Gast.Visible = true;
                    divHC_ASS_Phy_Gast_Elimination.Visible = true;
                    lblHC_ASS_Phy_Gast_Elimination.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_ELIMINATION"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_GENERAL"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_GAST_GENERAL") == false)
                {
                    divHC_ASS_Phy_Gast.Visible = true;
                    divHC_ASS_Phy_Gast_General.Visible = true;
                    lblHC_ASS_Phy_Gast_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_GENERAL"]);
                }



                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_MALE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_REPRO_MALE") == false)
                {
                    divHC_ASS_Phy_Repro.Visible = true;
                    divHC_ASS_Phy_Repro_Male.Visible = true;
                    lblHC_ASS_Phy_Repro_Male.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_MALE"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_FEMALE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_REPRO_FEMALE") == false)
                {
                    divHC_ASS_Phy_Repro.Visible = true;
                    divHC_ASS_Phy_Repro_Female.Visible = true;
                    lblHC_ASS_Phy_Repro_Female.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_FEMALE"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_BREASTS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_REPRO_BREASTS") == false)
                {
                    divHC_ASS_Phy_Repro.Visible = true;
                    divHC_ASS_Phy_Repro_Breasts.Visible = true;
                    lblHC_ASS_Phy_Repro_Breasts.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_BREASTS"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GENIT_GENERAL"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_GENIT_GENERAL") == false)
                {
                    divHC_ASS_Phy_Genit_General.Visible = true;
                    lblHC_ASS_Phy_Genit_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GENIT_GENERAL"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_COLOR"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_SKIN_COLOR") == false)
                {
                    divHC_ASS_Phy_Skin.Visible = true;
                    divHC_ASS_Phy_Skin_Color.Visible = true;
                    lblHC_ASS_Phy_Skin_Color.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_COLOR"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_TEMPERATURE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_SKIN_TEMPERATURE") == false)
                {
                    divHC_ASS_Phy_Skin.Visible = true;
                    divHC_ASS_Phy_Skin_Temperature.Visible = true;
                    lblHC_ASS_Phy_Skin_Temperature.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_TEMPERATURE"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_LESIONS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_SKIN_LESIONS") == false)
                {
                    divHC_ASS_Phy_Skin.Visible = true;
                    divHC_ASS_Phy_Skin_Lesions.Visible = true;
                    lblHC_ASS_Phy_Skin_Lesions.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_LESIONS"]);

                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_GENERAL"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NEURO_GENERAL") == false)
                {
                    divHC_ASS_Phy_Neuro.Visible = true;
                    divHC_ASS_Phy_Neuro_General.Visible = true;
                    lblHC_ASS_Phy_Neuro_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_GENERAL"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_CONSCIO"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NEURO_CONSCIO") == false)
                {
                    divHC_ASS_Phy_Neuro.Visible = true;
                    divHC_ASS_Phy_Neuro_Conscio.Visible = true;
                    lblHC_ASS_Phy_Neuro_Conscio.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_CONSCIO"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_ORIENTED"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NEURO_ORIENTED") == false)
                {
                    divHC_ASS_Phy_Neuro.Visible = true;
                    divHC_ASS_Phy_Neuro_Oriented.Visible = true;
                    lblHC_ASS_Phy_Neuro_Oriented.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_ORIENTED"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_TIMERESP"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NEURO_TIMERESP") == false)
                {
                    divHC_ASS_Phy_Neuro.Visible = true;
                    divHC_ASS_Phy_Neuro_TimeResp.Visible = true;
                    lblHC_ASS_Phy_Neuro_TimeResp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_TIMERESP"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_GENERAL"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_CARDIO_GENERAL") == false)
                {
                    divHC_ASS_Phy_Cardio.Visible = true;
                    divHC_ASS_Phy_Cardio_General.Visible = true;
                    lblHC_ASS_Phy_Cardio_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_GENERAL"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PULSE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_CARDIO_PULSE") == false)
                {
                    divHC_ASS_Phy_Cardio.Visible = true;
                    divHC_ASS_Phy_Cardio_Pulse.Visible = true;
                    lblHC_ASS_Phy_Cardio_Pulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PULSE"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PEDALPULSE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_CARDIO_PEDALPULSE") == false)
                {
                    divHC_ASS_Phy_Cardio.Visible = true;
                    divHC_ASS_Phy_Cardio_PedalPulse.Visible = true;
                    lblHC_ASS_Phy_Cardio_PedalPulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PEDALPULSE"]);

                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_EDEMA"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_CARDIO_EDEMA") == false)
                {
                    divHC_ASS_Phy_Cardio.Visible = true;
                    divHC_ASS_Phy_Cardio_Edema.Visible = true;
                    lblHC_ASS_Phy_Cardio_Edema.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_EDEMA"]);

                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_NAILBEDS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_CARDIO_NAILBEDS") == false)
                {
                    divHC_ASS_Phy_Cardio.Visible = true;
                    divHC_ASS_Phy_Cardio_NailBeds.Visible = true;
                    lblHC_ASS_Phy_Cardio_NailBeds.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_NAILBEDS"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_CAPILLARY"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_CARDIO_CAPILLARY") == false)
                {
                    divHC_ASS_Phy_Cardio.Visible = true;
                    divHC_ASS_Phy_Cardio_Capillary.Visible = true;
                    lblHC_ASS_Phy_Cardio_Capillary.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_CAPILLARY"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EYES"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_ENT_EYES") == false)
                {
                    divHC_ASS_Phy_Ent.Visible = true;
                    divHC_ASS_Phy_Ent_Eyes.Visible = true;
                    lblHC_ASS_Phy_Ent_Eyes.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EYES"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EARS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_ENT_EARS") == false)
                {
                    divHC_ASS_Phy_Ent.Visible = true;
                    divHC_ASS_Phy_Ent_Ears.Visible = true;
                    lblHC_ASS_Phy_Ent_Ears.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EARS"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_NOSE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_ENT_NOSE") == false)
                {
                    divHC_ASS_Phy_Ent.Visible = true;
                    divHC_ASS_Phy_Ent_Nose.Visible = true;
                    lblHC_ASS_Phy_Ent_Nose.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_NOSE"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_THROAT"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_ENT_THROAT") == false)
                {
                    divHC_ASS_Phy_Ent.Visible = true;
                    divHC_ASS_Phy_Ent_Throat.Visible = true;
                    lblHC_ASS_Phy_Ent_Throat.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_THROAT"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_CHEST"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_RESP_CHEST") == false)
                {
                    divHC_ASS_Phy_Resp.Visible = true;
                    divHC_ASS_Phy_Resp_Chest.Visible = true;
                    lblHC_ASS_Phy_Resp_Chest.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_CHEST"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHPATT"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_RESP_BREATHPATT") == false)
                {
                    divHC_ASS_Phy_Resp.Visible = true;
                    divHC_ASS_Phy_Resp_BreathPatt.Visible = true;
                    lblHC_ASS_Phy_Resp_BreathPatt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHPATT"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHSOUND"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_RESP_BREATHSOUND") == false)
                {
                    divHC_ASS_Phy_Resp.Visible = true;
                    divHC_ASS_Phy_Resp_BreathSound.Visible = true;
                    lblHC_ASS_Phy_Resp_BreathSound.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHSOUND"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RESP_BREATHCOUGH"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RESP_BREATHCOUGH") == false)
                {
                    divHC_ASS_Phy_Resp.Visible = true;
                    divHC_ASS_Phy_Resp_BreathCough.Visible = true;
                    lblHC_ASS_Phy_Resp_BreathCough.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RESP_BREATHCOUGH"]);
                }



                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIET"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NUTRI_DIET") == false)
                {
                    divHC_ASS_Phy_Nutri.Visible = true;
                    divHC_ASS_Phy_Nutri_Diet.Visible = true;
                    lblHC_ASS_Phy_Nutri_Diet.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIET"]);

                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_APPETITE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NUTRI_APPETITE") == false)
                {
                    divHC_ASS_Phy_Nutri.Visible = true;
                    divHC_ASS_Phy_Nutri_Appetite.Visible = true;
                    lblHC_ASS_Phy_Nutri_Appetite.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_APPETITE"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_NUTRISUPPORT"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NUTRI_NUTRISUPPORT") == false)
                {
                    divHC_ASS_Phy_Nutri.Visible = true;
                    divHC_ASS_Phy_Nutri_NutriSupport.Visible = true;
                    lblHC_ASS_Phy_Nutri_NutriSupport.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_NUTRISUPPORT"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_FEEDINGDIF"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NUTRI_FEEDINGDIF") == false)
                {
                    divHC_ASS_Phy_Nutri.Visible = true;
                    divHC_ASS_Phy_Nutri_FeedingDif.Visible = true;
                    lblHC_ASS_Phy_Nutri_FeedingDif.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_FEEDINGDIF"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_WEIGHTSTATUS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NUTRI_WEIGHTSTATUS") == false)
                {
                    divHC_ASS_Phy_Nutri.Visible = true;
                    divHC_ASS_Phy_Nutri_WeightStatus.Visible = true;
                    lblHC_ASS_Phy_Nutri_WeightStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_WEIGHTSTATUS"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIAG"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_PHY_NUTRI_DIAG") == false)
                {
                    divHC_ASS_Phy_Nutri.Visible = true;
                    divHC_ASS_Phy_Nutri_Diag.Visible = true;
                    lblHC_ASS_Phy_Nutri_Diag.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIAG"]);
                }

                //-------------------------------

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_SENSORY"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SKINRISK_SENSORY") == false)
                {
                    divHC_ASS_Risk_SkinRisk.Visible = true;
                    divHC_ASS_Risk_SkinRisk_Sensory.Visible = true;
                    lblHC_ASS_Risk_SkinRisk_Sensory.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_SENSORY"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOISTURE"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SKINRISK_MOISTURE") == false)
                {
                    divHC_ASS_Risk_SkinRisk.Visible = true;
                    divHC_ASS_Risk_SkinRisk_Moisture.Visible = true;
                    lblHC_ASS_Risk_SkinRisk_Moisture.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOISTURE"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_ACTIVITY"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SKINRISK_ACTIVITY") == false)
                {
                    divHC_ASS_Risk_SkinRisk.Visible = true;
                    divHC_ASS_Risk_SkinRisk_Activity.Visible = true;
                    lblHC_ASS_Risk_SkinRisk_Activity.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_ACTIVITY"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOBILITY"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SKINRISK_MOBILITY") == false)
                {
                    divHC_ASS_Risk_SkinRisk.Visible = true;
                    divHC_ASS_Risk_SkinRisk_Mobility.Visible = true;
                    lblHC_ASS_Risk_SkinRisk_Mobility.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOBILITY"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_NUTRITION"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SKINRISK_NUTRITION") == false)
                {
                    divHC_ASS_Risk_SkinRisk.Visible = true;
                    divHC_ASS_Risk_SkinRisk_Nutrition.Visible = true;
                    lblHC_ASS_Risk_SkinRisk_Nutrition.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_NUTRITION"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_FRICTION"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SKINRISK_FRICTION") == false)
                {
                    divHC_ASS_Risk_SkinRisk.Visible = true;
                    divHC_ASS_Risk_SkinRisk_Friction.Visible = true;
                    lblHC_ASS_Risk_SkinRisk_Friction.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_FRICTION"]);
                }
                //-----------------------------
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SOCIO_LIVING"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SOCIO_LIVING") == false)
                {
                    divHC_ASS_Risk_Socio_Living.Visible = true;
                    lblHC_ASS_Risk_Socio_Living.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SOCIO_LIVING"]);
                }
                //-----------------------------

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SAFETY_GENERAL"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_SAFETY_GENERAL") == false)
                {
                    divHC_ASS_Risk_Safety_General.Visible = true;
                    lblHC_ASS_Risk_Safety_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SAFETY_GENERAL"]);
                }
                //-------------------------------

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_SELFCARING"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_FUN_SELFCARING") == false)
                {
                    divHC_ASS_Risk_Fun.Visible = true;
                    divHC_ASS_Risk_Fun_SelfCaring.Visible = true;
                    lblHC_ASS_Risk_Fun_SelfCaring.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_SELFCARING"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_MUSCULOS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_FUN_MUSCULOS") == false)
                {
                    divHC_ASS_Risk_Fun.Visible = true;
                    divHC_ASS_Risk_Fun_Musculos.Visible = true;
                    lblHC_ASS_Risk_Fun_Musculos.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_MUSCULOS"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_MUSCULOS"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_FUN_MUSCULOS") == false)
                {
                    divHC_ASS_Risk_Fun.Visible = true;
                    divHC_ASS_Risk_Fun_Equipment.Visible = true;
                    lblHC_ASS_Risk_Fun_Equipment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_EQUIPMENT"]);
                }
                //-------------------------------

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_EDU_GENERAL"]) != "" && DS.Tables[0].Rows[0].IsNull("HPA_RISK_EDU_GENERAL") == false)
                {
                    divHC_ASS_Risk_Edu_General.Visible = true;
                    lblHC_ASS_Risk_Edu_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_EDU_GENERAL"]);
                }

            }

        }

        void BindPhysicalTherapyEval()
        {
            PhysicalTherapyEvaluationBAL objPTEval = new PhysicalTherapyEvaluationBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHPH_HHV_ID   = '" + Convert.ToString(Session["HomeCareId"]) + "' ";
            DS = objPTEval.GetPhysicalTherapyEvaluation(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                divPhyTherapy.Visible = true;

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CHIEF_COMPLAINT"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_CHIEF_COMPLAINT") == false)
                {
                    divPhyTherapyChiefCoplaint.Visible = true;
                    lblPhyTherapyChiefCoplaint.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CHIEF_COMPLAINT"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_DATEDesc"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_INJURY_DATEDesc") == false)
                {
                    lblPhyTherapyInj_Surg_Date.Visible = true;
                    lblPhyTherapyInjuryDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_DATEDesc"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SURGERY_DATEDesc"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_SURGERY_DATEDesc") == false)
                {
                    lblPhyTherapyInj_Surg_Date.Visible = true;
                    lblPhyTherapySurgeryDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SURGERY_DATEDesc"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_REMARKS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_INJURY_REMARKS") == false)
                {
                    divPhyTherapyInjuryRemarks.Visible = true;
                    lblPhyTherapyInjuryRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_REMARKS"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_DATEDesc"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_THERAPY_DATEDesc") == false)
                {
                    divPhyTherapyReceived.Visible = true;
                    lblPhyTherapyReceivedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_DATEDesc"]);

                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_VISIT_COUNT"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_THERAPY_VISIT_COUNT") == false)
                {
                    divPhyTherapyReceived.Visible = true;

                    lblPhyTherapyHowmany.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_VISIT_COUNT"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_CONDITION") == false)
                {
                    divPhyTherapyCondition.Visible = true;
                    lblPhyTherapyCondition.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION"]);
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SYMPTOMS"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_SYMPTOMS") == false)
                {
                    divPhyTherapyCondition.Visible = true;
                    lblPhyTherapySymptoms.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SYMPTOMS"]);
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAIN_BEST"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PAIN_BEST") == false)
                {
                    divPhyTherapyBest_Worst.Visible = true;
                    lblPhyTherapyAtWorst.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAIN_BEST"]);
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAINT_WORST"]) != "" && DS.Tables[0].Rows[0].IsNull("HHPH_PAINT_WORST") == false)
                {
                    divPhyTherapyBest_Worst.Visible = true;
                    lblPhyTherapyAtWorst.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAINT_WORST"]);
                }

            }

        }


        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    ViewState["HomeCareId"] = "0";
                    ViewState["HomeCareId"] = Request.QueryString["HomeCareId"].ToString();
                    BindHomeCareReg();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        #endregion
    }
}