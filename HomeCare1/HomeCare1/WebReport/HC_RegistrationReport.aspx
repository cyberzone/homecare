﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReport/Report.Master" AutoEventWireup="true" CodeBehind="HC_RegistrationReport.aspx.cs" Inherits="HomeCare1.WebReport.HC_RegistrationReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" Runat="Server">
 <style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        table, th, td
        {
            border: 1px solid #dcdcdc;
            height: 25px;
        }

        .BoldStyle
        {
            font-weight: bold;
        }
    </style>
    <div style="margin: 0px auto; width: 800px">
          <img style="padding:1px;" src="Content/images/HC_Report_Logo.PNG" />
        <br />
        <span class="PageHeader" style="font-size:18px;" >Patient Details</span>

        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Visit Date:</td>
                <td>
                    <asp:Label ID="lblVisitDate" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">Begin Date:</td>
                <td>
                    <asp:Label ID="lblBeginDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">End Date:</td>
                <td>
                    <asp:Label ID="lblEndDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1 BoldStyle" colspan="6">Patient Name :&nbsp;
                    <asp:Label ID="lblPTName" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1 BoldStyle">File No : 
                 <asp:Label ID="lblPTID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1 BoldStyle">Nationality : 
                 <asp:Label ID="lblNationality" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Age :
                    <asp:Label ID="lblAge" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    Y
                     <asp:Label ID="lblAge1" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    M

                </td>
            </tr>
            <tr>
                  <td class="lblCaption1  BoldStyle">Emirates ID : 
                     <asp:Label ID="lblEmiratesID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Phone No :
                    <asp:Label ID="lblMobile" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Sex :
                    <asp:Label ID="lblSex" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Provider Name :
             <asp:Label ID="lblProviderName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Pollicy Type :
                    <asp:Label ID="lblPolicyType" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Policy No : 
                    <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
           
            <tr>
                <td class="lblCaption1  BoldStyle">Reference Doctor :
                    <asp:Label ID="lblRefBy" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">License No :
                    <asp:Label ID="lblRefLicNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Department :
                    <asp:Label ID="lblRefDept" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Facility Name :
                    <asp:Label ID="lblRefFacilityName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Authorization No :
                    <asp:Label ID="lblAuthorizationNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">

                    <asp:Label ID="Label4" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
        </table>
        <div contenteditable="true" runat="server" visible="false" id="divHCReg_Diagnosis">

            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">Diagnosis </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:label id="lblDiagnosis" runat="server" cssclass="lblCaption1"></asp:label>
                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHCReg_Procedures">
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">Procedures </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:label id="lblProcedure" runat="server" cssclass="lblCaption1"></asp:label>
                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHCReg_Surgeries">
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">ADL </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:label id="lblSurgery" runat="server" cssclass="lblCaption1"></asp:label>
                    </td>
                </tr>
            </table>
        </div>
         <div contenteditable="true" runat="server" visible="false" id="divHCReg_NursingServices">
            <br />
             <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle"> Nursing Services </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                      <asp:GridView ID="gvNursingService" runat="server"   AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%"    >
                                <HeaderStyle CssClass="GridRow"  Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                       <asp:Label ID="lblServId" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_SERV_ID") %>' ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Services">
                                    <ItemTemplate>
                                         <asp:Label ID="lblServName" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_SERV_NAME") %>' ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Staff ID">
                                    <ItemTemplate>
                                       <asp:Label ID="lblStaffId" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_ID") %>' ></asp:Label>
                                 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                         <asp:Label ID="lblStaffName" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_STAFF_NAME") %>' ></asp:Label>

                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="License#">
                                    <ItemTemplate>
                                         <asp:Label ID="lblLicenseNo" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>'  ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                  <asp:TemplateField HeaderText="Days">
                                    <ItemTemplate>
                                         <asp:Label ID="lblNoOfDays" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_NOOFDAYS") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Effective Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartDate" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_START_DATEDesc") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                         <asp:Label ID="lblEndDate" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_END_DATEDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Authorization#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthNo" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                             

    </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>

         <div contenteditable="true" runat="server" visible="false" id="divHCReg_PhysiotherapyServices">
            <br />
             <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle"> Physiotherapy  Services </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                      <asp:GridView ID="gvPhyService" runat="server"   AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%"    >
                                <HeaderStyle CssClass="GridRow"  Font-Bold="true"  />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Code"  >
                                    <ItemTemplate>
                                       <asp:Label ID="lblServId" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_SERV_ID") %>' ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Services">
                                    <ItemTemplate>
                                         <asp:Label ID="lblServName" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_SERV_NAME") %>' ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Staff ID">
                                    <ItemTemplate>
                                       <asp:Label ID="lblStaffId" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_ID") %>' ></asp:Label>
                                 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                         <asp:Label ID="lblStaffName" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_STAFF_NAME") %>' ></asp:Label>

                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="License#">
                                    <ItemTemplate>
                                         <asp:Label ID="lblLicenseNo" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>'  ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                  <asp:TemplateField HeaderText="Days">
                                    <ItemTemplate>
                                         <asp:Label ID="lblNoOfDays" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_NOOFDAYS") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Effective Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartDate" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_START_DATEDesc") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                         <asp:Label ID="lblEndDate" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_END_DATEDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Authorization#">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                             

    </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>


