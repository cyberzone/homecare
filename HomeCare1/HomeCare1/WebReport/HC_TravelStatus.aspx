﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReport/Report.Master" AutoEventWireup="true" CodeBehind="HC_TravelStatus.aspx.cs" Inherits="HomeCare1.WebReport.HC_TravelStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" Runat="Server">
<style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        table, th, td
        {
            border: 1px solid #dcdcdc;
            height: 25px;
        }

        .BoldStyle
        {
            font-weight: bold;
        }
    </style>
    <div style="margin: 0px auto; width: 800px">
          <img style="padding:1px;" src="Content/images/HC_Report_Logo.PNG" />
        <br />
        <span class="PageHeader" style="font-size:18px;" >Travel Status </span>

        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Visit Date:</td>
                <td>
                    <asp:Label ID="lblVisitDate" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">Begin Date:</td>
                <td>
                    <asp:Label ID="lblBeginDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">End Date:</td>
                <td>
                    <asp:Label ID="lblEndDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1 BoldStyle" colspan="6">Patient Name :&nbsp;
                    <asp:Label ID="lblPTName" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1 BoldStyle">File No : 
                 <asp:Label ID="lblPTID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1 BoldStyle">Nationality : 
                 <asp:Label ID="lblNationality" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Age :
                    <asp:Label ID="lblAge" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    Y
                     <asp:Label ID="lblAge1" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    M

                </td>
            </tr>
            <tr>
                 <td class="lblCaption1  BoldStyle">Emirates ID : 
                     <asp:Label ID="lblEmiratesID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Phone No :
                    <asp:Label ID="lblMobile" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Sex :
                    <asp:Label ID="lblSex" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Provider Name :
             <asp:Label ID="lblProviderName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Pollicy Type :
                    <asp:Label ID="lblPolicyType" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Policy No : 
                    <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Reference Doctor :
                    <asp:Label ID="lblRefBy" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">License No :
                    <asp:Label ID="lblRefLicNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Department :
                    <asp:Label ID="lblRefDept" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Facility Name :
                    <asp:Label ID="lblRefFacilityName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                 <td class="lblCaption1  BoldStyle">Authorization No :
                    <asp:Label ID="lblAuthorizationNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">

                    <asp:Label ID="Label4" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <div contenteditable="true">
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">Travel Status </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">

                        <asp:GridView ID="gvTravelStatus" runat="server" AutoGenerateColumns="False"
                            EnableModelValidation="True" Width="100%">
                            <HeaderStyle CssClass="GridRow" Font-Bold="true" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>
                               
                                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_DATEDesc") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Start Time" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartTime" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_START_TIME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reach Time" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReachTime" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_REACH_TIME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mileage Up" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUp" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_MILEAGE_UP") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mileage Down" HeaderStyle-Width="70px">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDown" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_MILEAGE_DOWN") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Vehicle No" >
                                    <ItemTemplate>
                                       <asp:Label ID="lblVehicleNo" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_VEHICLE_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Assigned Staff" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblStaffId" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_STAFF_ID") %>'></asp:Label>&nbsp;-&nbsp;
                                        <asp:Label ID="lblStaffName" CssClass="GridRow"  runat="server" Text='<%# Bind("HHTS_STAFF_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Driver" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblDriverId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_DRIVER_ID") %>'></asp:Label>&nbsp;-&nbsp;
                                        <asp:Label ID="lblDriverName" CssClass="GridRow"  runat="server" Text='<%# Bind("HHTS_DRIVER_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>


                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <br />
    </div>
</asp:Content>


