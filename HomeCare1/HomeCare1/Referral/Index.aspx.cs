﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.Referral
{
    public partial class Index : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        HC_PTReferral objRef = new HC_PTReferral();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        DataSet GetReferal()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND (Segment = 'REFERENCETYPE') AND (ISNULL(Value, '') <> '')  AND (Status = 1)";
            objCom = new CommonBAL();

            DS = objCom.EMR_DYNAMIC_CONTENT(Criteria);
            ReferenceType.DataSource = DS;
            ReferenceType.DataValueField = "Value";
            ReferenceType.DataTextField = "Value";
            ReferenceType.DataBind();


            return DS;
        }

        DataSet GetStaff()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.GetStateMaster(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                InDoctorID.DataSource = DS;
                InDoctorID.DataValueField = "HSFM_STAFF_ID";
                InDoctorID.DataTextField = "FullName";
                InDoctorID.DataBind();
            }

            return DS;
        }

        void BindReferral()
        {
            objRef = new HC_PTReferral();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " and HPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPR_ID='" + Convert.ToString(Session["HomeCareId"]) + "'";
            DS = objRef.HCPTReferenceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPR_TYPE"]) != null && Convert.ToString(DS.Tables[0].Rows[0]["HPR_TYPE"]).Equals("2"))
                {
                    OutType.Checked = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "EnableOutsideDr();", true);
                }
                else
                {
                    InType.Checked = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "EnableInsideDr();", true);
                }



                if (DS.Tables[0].Rows[0].IsNull("HPR_REFTYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPR_REFTYPE"]) != "")
                {
                    for (int intCount = 0; intCount < ReferenceType.Items.Count; intCount++)
                    {
                        if (ReferenceType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HPR_REFTYPE"]))
                        {
                            ReferenceType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_REFTYPE"]);
                            goto ForPayer;
                        }

                    }
                }

            ForPayer: ;





                InDoctorID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_DR_CODE"]);
                OutHospital.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_OUT_HOSPITAL"]);
                OutDoctor.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_OUT_DRNAME"]);
                KinName.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_KIN_NAME"]);
                KinAddress.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_KIN_ADDRESS"]);
                KinPhone.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_KIN_PHONE"]);
                Remarks.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_REMARKS"]);
                Diagnosis.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_PRO_DIAG"]);
                BeforeTransfer.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_BEFORE_TRANSFER"]);
                AfterTransfer.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_AFTER_TRANSFER"]);
                Recommended.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_MED_RECOMMENDED"]);
                ManagementTransfer.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_MANAGEMENT_TRANFER"]);


                Currentstatusofpatient.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_PATIENT_CURRENT_STATUS"]);
                Specialendorsement.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPR_SPECIAL_ENDORSEMENT"]);





            }

        }







        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["HomeCareId"] == null) { Response.Redirect("../HomeCare/WaitingList.aspx"); }

            try
            {
                if (!IsPostBack)
                {


                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");


                    GetReferal();
                    GetStaff();

                    BindReferral();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Referral.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objRef = new HC_PTReferral();
                objRef.branchid = Convert.ToString(Session["Branch_ID"]);
                objRef.patientmasterid = Convert.ToString(Session["HomeCareId"]);
                objRef.referencetype = ReferenceType.Value;
                objRef.remarks = Remarks.Value;
                objRef.type = InType.Checked == true ? "1" : "2";
                objRef.indoctorid = InDoctorID.Value;
                objRef.outhospital = OutHospital.Value;
                objRef.outdoctor = OutDoctor.Value;
                objRef.kinname = KinName.Value;
                objRef.kinaddress = KinAddress.Value;
                objRef.kinphone = KinPhone.Value;
                objRef.diagnosis = Diagnosis.Value;
                objRef.recommended = Recommended.Value;
                objRef.managementtransfer = ManagementTransfer.Value;
                objRef.beforetransfer = BeforeTransfer.Value;
                objRef.aftertransfer = AfterTransfer.Value;
                objRef.PatientCurrentstatus = Currentstatusofpatient.Value;
                objRef.SpecialEndorsement = Specialendorsement.Value;
                objRef.templatecode = "";
                objRef.HCPTReferenceAdd();



                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Referral.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}