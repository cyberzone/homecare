﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="HomeCare1.Referral.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />
    <style type="text/css">
        .box-title
        {
            border-bottom: 2px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <style type="text/css">
        #ReferralDiv
        {
             width: 90%;
        }

            #ReferralDiv label
            {
                font-weight: bold;
            }

            #ReferralDiv textarea
            {
                height: 60px;
                width: 100%;
            }

        #kinAddress
        {
            height: 40px !important;
        }

        #TopLeftDiv
        {
            float: left;
            width: 450px;
        }

        #TopRightDiv
        {
            float: right;
        }

            #TopRightDiv.KinDetails
            {
            }

        #MiddleDiv
        {
            height: 38%;
            float: left;
        }

        #BottomLeftDiv
        {
            float: left;
            height: 29%;
            width: 50%;
        }

        #BottomRightDiv
        {
            float: left;
            height: 29%;
            width: 50%;
        }

        #ReferralFooter
        {
            float: right;
        }
    </style>
    
<script language="javascript" type="text/javascript">


    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }

    function ShowAlertMessage(msg) {

        alert(msg);
    }

</script>

    <script language="javascript" type="text/javascript">

        function EnableOutsideDr() {

            document.getElementById('<%=OutDoctor.ClientID%>').disabled = false;
            document.getElementById('<%=KinName.ClientID%>').disabled = false;
            document.getElementById('<%=KinAddress.ClientID%>').disabled = false;
            document.getElementById('<%=KinPhone.ClientID%>').disabled = false;

            document.getElementById('<%=BeforeTransfer.ClientID%>').disabled = false;
            document.getElementById('<%=AfterTransfer.ClientID%>').disabled = false;
            document.getElementById('<%=Recommended.ClientID%>').disabled = false;
            document.getElementById('<%=ManagementTransfer.ClientID%>').disabled = false;
            document.getElementById('<%=Currentstatusofpatient.ClientID%>').disabled = false;
            document.getElementById('<%=Specialendorsement.ClientID%>').disabled = false;

        }
        function EnableInsideDr() {

            document.getElementById('<%=OutDoctor.ClientID%>').disabled = true;
            document.getElementById('<%=KinName.ClientID%>').disabled = true;
            document.getElementById('<%=KinAddress.ClientID%>').disabled = true;
            document.getElementById('<%=KinPhone.ClientID%>').disabled = true;

            document.getElementById('<%=BeforeTransfer.ClientID%>').disabled = true;
            document.getElementById('<%=AfterTransfer.ClientID%>').disabled = true;
            document.getElementById('<%=Recommended.ClientID%>').disabled = true;
            document.getElementById('<%=ManagementTransfer.ClientID%>').disabled = true;

            document.getElementById('<%=Currentstatusofpatient.ClientID%>').disabled = true;
            document.getElementById('<%=Specialendorsement.ClientID%>').disabled = true;


        }
    </script>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

       
    
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>
      <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="PageHeader">Referal</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <div id="patient-referral-container">

        <div id="ReferralDiv">
            <div id="TopLeftDiv">
                <fieldset class="fieldset" style="width:400px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Reference Details</legend>
                    <table class="table">
                        <tr>
                            <td>
                                <label for="ReferenceType" class="lblCaption1">Reference Type</label></td>
                            <td colspan="2">
                                <select id="ReferenceType" runat="server" name="ReferenceType">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" id="InType" runat="server" name="Type" value="1" onclick="EnableInsideDr();" checked="true" /></td>
                            <td>
                                <label for="InType" class="lblCaption1">Internal Hospital</label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label for="InDoctorID" class="lblCaption1">Doctor</label></td>
                            <td>
                                <select name="InDoctorID" runat="server" id="InDoctorID" class="lbl" style="width:100%">
                                </select>
                            </td>
                            <td>
                                <!--<input type="button" value="Fix Appointment" />-->
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" name="Type" id="OutType" runat="server" value="2" onclick="EnableOutsideDr();" />
                            </td>
                            <td>
                                <label for="OutType" class="lblCaption1">Outside</label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label for="OutHospital" class="lblCaption1">Clinic/Hospital</label></td>
                            <td>
                                <input type="text" name="OutHospital" id="OutHospital" runat="server" value="" style="width:99%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="OutDoctor" class="lblCaption1">Doctor</label></td>
                            <td>
                                <input type="text" id="OutDoctor" name="OutDoctor" runat="server" class="outsidehosp" disabled="disabled" style="width:99%" /></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="TopRightDiv">
                <fieldset class="KinDetails" style="width:400px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Kin Details</legend>
                    <table class="table spacy">
                        <tr>
                            <td>
                                <label for="KinName" class="lblCaption1">Name</label></td>
                            <td>
                                <input type="text" id="KinName" runat="server" name="KinName" style="width:99%;" class="outsidehosp" disabled="disabled" /></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="KinAddress" class="lblCaption1">Address</label></td>
                            <td>
                                <textarea id="KinAddress" runat="server" class="outsidehosp" name="KinAddress" disabled="disabled" style="width:99%;resize:none;"></textarea></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="KinPhone" class="lblCaption1">Phone</label></td>
                            <td>
                                <input type="text" id="KinPhone" runat="server" class="outsidehosp" style="width:99%;" name="KinPhone" disabled="disabled" /></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="MiddleDiv">
                <table class="table">
                    <tr>
                        <td>
                            <label for="Remarks" class="lblCaption1">Reason for Referral</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Remarks" name="Remarks" runat="server" rows="3" cols="140" style="resize:none;"></textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="Diagnosis" class="lblCaption1">Provisional Diagnosis</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Diagnosis" name="Diagnosis" runat="server" rows="3" cols="140" style="resize:none;"> </textarea></td>
                    </tr>
                   
                </table>
            </div>
            <div id="BottomLeftDiv">
                <table class="table">
                    <tr>
                        <td>
                            <label for="BeforeTransfer" class="lblCaption1">Patient Condition Before Transfer</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="BeforeTransfer" name="BeforeTransfer" runat="server" class="outsidehosp" rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="AfterTransfer" class="lblCaption1">Patient Condition During Transfer</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="AfterTransfer" name="AfterTransfer" runat="server" class="outsidehosp" rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                      <tr>
                        <td>
                            <label for="AfterTransfer" class="lblCaption1">Current status of patient</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Currentstatusofpatient" name="Currentstatusofpatient" runat="server" class="outsidehosp" rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>

                   
                </table>
            </div>
            <div id="BottomRightDiv">
                <table class="table">
                    <tr>
                        <td>
                            <label for="Recommended" class="lblCaption1">Recommended Medications</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Recommended" name="Recommended" runat="server" class="outsidehosp" rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="ManagementTransfer" class="lblCaption1">Management During Transfer</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="ManagementTransfer" name="ManagementTransfer" runat="server" class="outsidehosp" rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"> </textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="BeforeTransfer" class="lblCaption1">Special endorsement</label></td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="Specialendorsement" name="Specialendorsement" runat="server" class="outsidehosp" rows="3" cols="70" style="width: 95%;resize:none;" disabled="disabled"></textarea></td>
                    </tr>
                </table>
            </div>
            <div style="clear: both">&nbsp;</div>

        </div>
      
        <br /><br />

</asp:Content>


