﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.HomeCare
{
    public partial class ServicessLookup : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindData()
        {
            string Criteria = Convert.ToString(ViewState["CategoryType"]);
            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            ds = dbo.GetHaadServicessList(Criteria, SearchFilter);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

            }
            else
            {
                gvServicess.DataBind();
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            // if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);


                    ViewState["CategoryType"] = Convert.ToString(Request.QueryString["CategoryType"]);

                    BindData();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ServicessLookup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      DR_ICD_MasterPopup.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}