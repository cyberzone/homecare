﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class HomeCareDtls : System.Web.UI.MasterPage
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }
        public void BindHomeCareReg()
        {

            string Criteria = " 1=1  AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "'";    //BAL.GlobelValues.HomeCare_ID  + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
                lblHCID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);
                lblRefBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_NAME"]);
                lblBeginDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_BEGIN_DATEDesc"]);
                lblEndDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_END_DATEDesc"]);

                BindPTDetails();
                BindPatientPhoto();

            }
            else
            {
                Clear();
            }

        }

        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + lblPTID.Text + "' ";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPTName.Text = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                if (Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "0")
                    lblNationality.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);

                lblProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                lblSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                lblMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                lblAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE1"]);
                lblEmiratesID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_IQAMA_NO"]);
                lblPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                lblPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
            }

        }

        void BindPatientPhoto()
        {
            dboperations objOperations = new dboperations();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_BRANCH_ID='" + Session["Branch_ID"] + "' AND  HPP_PT_ID='" + lblPTID.Text + "'";
            DataSet DS = new DataSet();
            DS = objOperations.PatientPhotoGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
                {
                    imgFront.Visible = true;
                    Session["HTI_IMAGE1"] = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD"];
                    imgFront.ImageUrl = "~/DisplayCardImage.aspx";
                }

            }



        }

        void Clear()
        {

            lblPTID.Text = "";
            lblHCID.Text = "";
            lblRefBy.Text = "";
            lblBeginDt.Text = "";
            lblEndDt.Text = "";

            lblPTName.Text = "";

            lblNationality.Text = "";

            lblProviderName.Text = "";
            lblSex.Text = "";
            lblMobile.Text = "";
            lblAge.Text = "";
            lblAge1.Text = "";
            lblEmiratesID.Text = "";
            lblPolicyNo.Text = "";
            lblPolicyType.Text = "";
            imgFront.Visible = false;


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            //if (!IsPostBack)
            //{

            if (Convert.ToString(Session["HomeCareId"]) == "" || Convert.ToString(Session["HomeCareId"]) == null)
            {
                goto FunEnd;
            }

            try
            {
                BindHomeCareReg();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareDtls.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        //}
        FunEnd: ;
        }
    }
}