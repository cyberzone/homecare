﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.HomeCare
{
    public partial class HomeCare : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblLoginUser.Text = "Welcome " + Convert.ToString(Session["User_Name"]);

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Code"]) != "")
                {
                    DataSet DS = new DataSet();
                    dboperations dbo = new dboperations();
                    string Criteria = " 1=1  ";
                    Criteria += " AND HSP_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

                    DS = dbo.StaffPhotoGet(Criteria);

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        if (DS.Tables[0].Rows[0].IsNull("HSP_PHOTO") == false)
                        {
                            imgStaffPhoto.ImageUrl = "../DisplayUserProfileImage.aspx";
                        }

                    }
                }


            }
        }
    }
}