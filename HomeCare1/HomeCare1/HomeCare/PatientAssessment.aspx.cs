﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class PatientAssessment : System.Web.UI.Page
    {

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindHomeCareReg()
        {

            string Criteria = " 1=1 AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                hidPTID.Value = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
            }

        }

        void ModifyAssessment()
        {
            PTAssessmentBAL obj = new PTAssessmentBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPA_HHV_ID   = '" + Convert.ToString(Session["HomeCareId"]) + "' ";
            DS = obj.GetPTAssessment(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["HPA_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HPA_ID"]);
                txtHC_ASS_Health_PresPastHist.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPASTHIST"]);
                txtPresPastProblem.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PROB"]);
                txtPresPastSurgeries.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PREVPROB"]);
                txtAlergiesMedication.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_MEDIC"]);
                txtAlergiesFood.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_FOOD"]);
                txtAlergiesOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_OTHER"]);
                txtHC_ASS_Health_MedicHist_Medicine.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_MEDICINE"]);
                txtHC_ASS_Health_MedicHist_Sleep.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_SLEEP"]);
                txtHC_ASS_Health_MedicHist_PschoyAss.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_PSCHOYASS"]);
                radPainAssYes.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PAIN_EXPRESSES"]);
                txtHC_ASS_Phy_Gast_BowelSounds.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_BOWELSOUNDS"]);
                txtHC_ASS_Phy_Gast_Elimination.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_ELIMINATION"]);
                txtHC_ASS_Phy_Gast_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_GENERAL"]);
                txtHC_ASS_Phy_Repro_Male.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_MALE"]);
                txtHC_ASS_Phy_Repro_Female.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_FEMALE"]);
                txtHC_ASS_Phy_Repro_Breasts.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_BREASTS"]);
                txtHC_ASS_Phy_Genit_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GENIT_GENERAL"]);
                txtHC_ASS_Phy_Skin_Color.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_COLOR"]);
                txtHC_ASS_Phy_Skin_Temperature.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_TEMPERATURE"]);
                txtHC_ASS_Phy_Skin_Lesions.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_LESIONS"]);
                txtHC_ASS_Phy_Neuro_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_GENERAL"]);
                txtHC_ASS_Phy_Neuro_Conscio.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_CONSCIO"]);
                txtHC_ASS_Phy_Neuro_Oriented.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_ORIENTED"]);
                txtHC_ASS_Phy_Neuro_TimeResp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_TIMERESP"]);
                txtHC_ASS_Phy_Cardio_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_GENERAL"]);
                txtHC_ASS_Phy_Cardio_Pulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PULSE"]);
                txtHC_ASS_Phy_Cardio_PedalPulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PEDALPULSE"]);
                txtHC_ASS_Phy_Cardio_Edema.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_EDEMA"]);
                txtHC_ASS_Phy_Cardio_NailBeds.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_NAILBEDS"]);
                txtHC_ASS_Phy_Cardio_Capillary.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_CAPILLARY"]);
                txtHC_ASS_Phy_Ent_Eyes.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EYES"]);
                txtHC_ASS_Phy_Ent_Ears.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EARS"]);
                txtHC_ASS_Phy_Ent_Nose.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_NOSE"]);
                txtHC_ASS_Phy_Ent_Throat.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_THROAT"]);
                txtHC_ASS_Phy_Resp_Chest.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_CHEST"]);
                txtHC_ASS_Phy_Resp_BreathPatt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHPATT"]);
                txtHC_ASS_Phy_Resp_BreathSound.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHSOUND"]);
                txtHC_ASS_Phy_Resp_BreathCough.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RESP_BREATHCOUGH"]);
                txtHC_ASS_Phy_Nutri_Diet.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIET"]);
                txtHC_ASS_Phy_Nutri_Appetite.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_APPETITE"]);
                txtHC_ASS_Phy_Nutri_NutriSupport.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_NUTRISUPPORT"]);
                txtHC_ASS_Phy_Nutri_FeedingDif.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_FEEDINGDIF"]);
                txtHC_ASS_Phy_Nutri_WeightStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_WEIGHTSTATUS"]);
                txtHC_ASS_Phy_Nutri_Diag.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIAG"]);
                txtHC_ASS_Risk_SkinRisk_Sensory.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_SENSORY"]);
                txtHC_ASS_Risk_SkinRisk_Moisture.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOISTURE"]);
                txtHC_ASS_Risk_SkinRisk_Activity.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_ACTIVITY"]);
                txtHC_ASS_Risk_SkinRisk_Mobility.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOBILITY"]);
                txtHC_ASS_Risk_SkinRisk_Nutrition.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_NUTRITION"]);
                txtHC_ASS_Risk_SkinRisk_Friction.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_FRICTION"]);
                txtHC_ASS_Risk_Socio_Living.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SOCIO_LIVING"]);
                txtHC_ASS_Risk_Safety_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SAFETY_GENERAL"]);
                txtHC_ASS_Risk_Fun_SelfCaring.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_SELFCARING"]);
                txtHC_ASS_Risk_Fun_Musculos.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_MUSCULOS"]);
                txtHC_ASS_Risk_Fun_Equipment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_EQUIPMENT"]);
                txtHC_ASS_Risk_Edu_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_EDU_GENERAL"]);

            }


        }

        void Clear()
        {
            ViewState["HPA_ID"] = "0";
            txtHC_ASS_Health_PresPastHist.Text = "";
            txtPresPastProblem.Text = "";
            txtPresPastSurgeries.Text = "";
            txtAlergiesMedication.Text = "";
            txtAlergiesFood.Text = "";
            txtAlergiesOther.Text = "";
            txtHC_ASS_Health_MedicHist_Medicine.Text = "";
            txtHC_ASS_Health_MedicHist_Sleep.Text = "";
            txtHC_ASS_Health_MedicHist_PschoyAss.Text = "";
            radPainAssYes.SelectedIndex = -1;
            txtHC_ASS_Phy_Gast_BowelSounds.Text = "";
            txtHC_ASS_Phy_Gast_Elimination.Text = "";
            txtHC_ASS_Phy_Gast_General.Text = "";
            txtHC_ASS_Phy_Repro_Male.Text = "";
            txtHC_ASS_Phy_Repro_Female.Text = "";
            txtHC_ASS_Phy_Repro_Breasts.Text = "";
            txtHC_ASS_Phy_Genit_General.Text = "";
            txtHC_ASS_Phy_Skin_Color.Text = "";
            txtHC_ASS_Phy_Skin_Temperature.Text = "";
            txtHC_ASS_Phy_Skin_Lesions.Text = "";
            txtHC_ASS_Phy_Neuro_General.Text = "";
            txtHC_ASS_Phy_Neuro_Conscio.Text = "";
            txtHC_ASS_Phy_Neuro_Oriented.Text = "";
            txtHC_ASS_Phy_Neuro_TimeResp.Text = "";
            txtHC_ASS_Phy_Cardio_General.Text = "";
            txtHC_ASS_Phy_Cardio_Pulse.Text = "";
            txtHC_ASS_Phy_Cardio_PedalPulse.Text = "";
            txtHC_ASS_Phy_Cardio_Edema.Text = "";
            txtHC_ASS_Phy_Cardio_NailBeds.Text = "";
            txtHC_ASS_Phy_Cardio_Capillary.Text = "";
            txtHC_ASS_Phy_Ent_Eyes.Text = "";
            txtHC_ASS_Phy_Ent_Ears.Text = "";
            txtHC_ASS_Phy_Ent_Nose.Text = "";
            txtHC_ASS_Phy_Ent_Throat.Text = "";
            txtHC_ASS_Phy_Resp_Chest.Text = "";
            txtHC_ASS_Phy_Resp_BreathPatt.Text = "";
            txtHC_ASS_Phy_Resp_BreathSound.Text = "";
            txtHC_ASS_Phy_Resp_BreathCough.Text = "";
            txtHC_ASS_Phy_Nutri_Diet.Text = "";
            txtHC_ASS_Phy_Nutri_Appetite.Text = "";
            txtHC_ASS_Phy_Nutri_FeedingDif.Text = "";
            txtHC_ASS_Phy_Nutri_WeightStatus.Text = "";
            txtHC_ASS_Phy_Nutri_Diag.Text = "";
            txtHC_ASS_Risk_SkinRisk_Sensory.Text = "";
            txtHC_ASS_Risk_SkinRisk_Moisture.Text = "";
            txtHC_ASS_Risk_SkinRisk_Activity.Text = "";
            txtHC_ASS_Risk_SkinRisk_Mobility.Text = "";
            txtHC_ASS_Risk_SkinRisk_Nutrition.Text = "";
            txtHC_ASS_Risk_SkinRisk_Friction.Text = "";
            txtHC_ASS_Risk_Socio_Living.Text = "";
            txtHC_ASS_Risk_Safety_General.Text = "";
            txtHC_ASS_Risk_Fun_SelfCaring.Text = "";
            txtHC_ASS_Risk_Fun_Musculos.Text = "";
            txtHC_ASS_Risk_Fun_Equipment.Text = "";
            txtHC_ASS_Risk_Edu_General.Text = "";
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    ViewState["HPA_ID"] = "0";

                    BindHomeCareReg();
                    ModifyAssessment();
                }

                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PTAssessmentBAL obj = new PTAssessmentBAL();
                obj.HPA_ID = Convert.ToString(ViewState["HPA_ID"]);
                obj.HPA_HHV_ID = Convert.ToString(Session["HomeCareId"]);
                obj.HPA_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                obj.HPA_PT_ID = hidPTID.Value;
                obj.HPA_HEALTH_PRESPASTHIST = txtHC_ASS_Health_PresPastHist.Text.Trim();
                obj.HPA_HEALTH_PRESPAS_PROB = txtPresPastProblem.Text.Trim();
                obj.HPA_HEALTH_PRESPAS_PREVPROB = txtPresPastSurgeries.Text.Trim();
                obj.HPA_HEALTH_ALLERGIES_MEDIC = txtAlergiesMedication.Text.Trim();
                obj.HPA_HEALTH_ALLERGIES_FOOD = txtAlergiesFood.Text.Trim();
                obj.HPA_HEALTH_ALLERGIES_OTHER = txtAlergiesOther.Text.Trim();
                obj.HPA_HEALTH_MEDICHIST_MEDICINE = txtHC_ASS_Health_MedicHist_Medicine.Text.Trim();
                obj.HPA_HEALTH_MEDICHIST_SLEEP = txtHC_ASS_Health_MedicHist_Sleep.Text.Trim();
                obj.HPA_HEALTH_MEDICHIST_PSCHOYASS = txtHC_ASS_Health_MedicHist_PschoyAss.Text.Trim();
                obj.HPA_HEALTH_PAIN_EXPRESSES = Convert.ToString(radPainAssYes.SelectedValue);
                obj.HPA_PHY_GAST_BOWELSOUNDS = txtHC_ASS_Phy_Gast_BowelSounds.Text.Trim();
                obj.HPA_PHY_GAST_ELIMINATION = txtHC_ASS_Phy_Gast_Elimination.Text.Trim();
                obj.HPA_PHY_GAST_GENERAL = txtHC_ASS_Phy_Gast_General.Text.Trim();
                obj.HPA_PHY_REPRO_MALE = txtHC_ASS_Phy_Repro_Male.Text.Trim();
                obj.HPA_PHY_REPRO_FEMALE = txtHC_ASS_Phy_Repro_Female.Text.Trim();
                obj.HPA_PHY_REPRO_BREASTS = txtHC_ASS_Phy_Repro_Breasts.Text.Trim();
                obj.HPA_PHY_GENIT_GENERAL = txtHC_ASS_Phy_Genit_General.Text.Trim();
                obj.HPA_PHY_SKIN_COLOR = txtHC_ASS_Phy_Skin_Color.Text.Trim();
                obj.HPA_PHY_SKIN_TEMPERATURE = txtHC_ASS_Phy_Skin_Temperature.Text.Trim();
                obj.HPA_PHY_SKIN_LESIONS = txtHC_ASS_Phy_Skin_Lesions.Text.Trim();
                obj.HPA_PHY_NEURO_GENERAL = txtHC_ASS_Phy_Neuro_General.Text.Trim();
                obj.HPA_PHY_NEURO_CONSCIO = txtHC_ASS_Phy_Neuro_Conscio.Text.Trim();
                obj.HPA_PHY_NEURO_ORIENTED = txtHC_ASS_Phy_Neuro_Oriented.Text.Trim();
                obj.HPA_PHY_NEURO_TIMERESP = txtHC_ASS_Phy_Neuro_TimeResp.Text.Trim();
                obj.HPA_PHY_CARDIO_GENERAL = txtHC_ASS_Phy_Cardio_General.Text.Trim();
                obj.HPA_PHY_CARDIO_PULSE = txtHC_ASS_Phy_Cardio_Pulse.Text.Trim();
                obj.HPA_PHY_CARDIO_PEDALPULSE = txtHC_ASS_Phy_Cardio_PedalPulse.Text.Trim();
                obj.HPA_PHY_CARDIO_EDEMA = txtHC_ASS_Phy_Cardio_Edema.Text.Trim();
                obj.HPA_PHY_CARDIO_NAILBEDS = txtHC_ASS_Phy_Cardio_NailBeds.Text.Trim();
                obj.HPA_PHY_CARDIO_CAPILLARY = txtHC_ASS_Phy_Cardio_Capillary.Text.Trim();
                obj.HPA_PHY_ENT_EYES = txtHC_ASS_Phy_Ent_Eyes.Text.Trim();
                obj.HPA_PHY_ENT_EARS = txtHC_ASS_Phy_Ent_Ears.Text.Trim();
                obj.HPA_PHY_ENT_NOSE = txtHC_ASS_Phy_Ent_Nose.Text.Trim();
                obj.HPA_PHY_ENT_THROAT = txtHC_ASS_Phy_Ent_Throat.Text.Trim();
                obj.HPA_PHY_RESP_CHEST = txtHC_ASS_Phy_Resp_Chest.Text.Trim();
                obj.HPA_PHY_RESP_BREATHPATT = txtHC_ASS_Phy_Resp_BreathPatt.Text.Trim();
                obj.HPA_PHY_RESP_BREATHSOUND = txtHC_ASS_Phy_Resp_BreathSound.Text.Trim();
                obj.HPA_RESP_BREATHCOUGH = txtHC_ASS_Phy_Resp_BreathCough.Text.Trim();
                obj.HPA_PHY_NUTRI_DIET = txtHC_ASS_Phy_Nutri_Diet.Text.Trim();
                obj.HPA_PHY_NUTRI_APPETITE = txtHC_ASS_Phy_Nutri_Appetite.Text.Trim();
                obj.HPA_PHY_NUTRI_NUTRISUPPORT = txtHC_ASS_Phy_Nutri_NutriSupport.Text.Trim();
                obj.HPA_PHY_NUTRI_FEEDINGDIF = txtHC_ASS_Phy_Nutri_FeedingDif.Text.Trim();
                obj.HPA_PHY_NUTRI_WEIGHTSTATUS = txtHC_ASS_Phy_Nutri_WeightStatus.Text.Trim();
                obj.HPA_PHY_NUTRI_DIAG = txtHC_ASS_Phy_Nutri_Diag.Text.Trim();
                obj.HPA_RISK_SKINRISK_SENSORY = txtHC_ASS_Risk_SkinRisk_Sensory.Text.Trim();
                obj.HPA_RISK_SKINRISK_MOISTURE = txtHC_ASS_Risk_SkinRisk_Moisture.Text.Trim();
                obj.HPA_RISK_SKINRISK_ACTIVITY = txtHC_ASS_Risk_SkinRisk_Activity.Text.Trim();
                obj.HPA_RISK_SKINRISK_MOBILITY = txtHC_ASS_Risk_SkinRisk_Mobility.Text.Trim();
                obj.HPA_RISK_SKINRISK_NUTRITION = txtHC_ASS_Risk_SkinRisk_Nutrition.Text.Trim();
                obj.HPA_RISK_SKINRISK_FRICTION = txtHC_ASS_Risk_SkinRisk_Friction.Text.Trim();
                obj.HPA_RISK_SOCIO_LIVING = txtHC_ASS_Risk_Socio_Living.Text.Trim();
                obj.HPA_RISK_SAFETY_GENERAL = txtHC_ASS_Risk_Safety_General.Text.Trim();
                obj.HPA_RISK_FUN_SELFCARING = txtHC_ASS_Risk_Fun_SelfCaring.Text.Trim();
                obj.HPA_RISK_FUN_MUSCULOS = txtHC_ASS_Risk_Fun_Musculos.Text.Trim();
                obj.HPA_RISK_FUN_EQUIPMENT = txtHC_ASS_Risk_Fun_Equipment.Text.Trim();
                obj.HPA_RISK_EDU_GENERAL = txtHC_ASS_Risk_Edu_General.Text.Trim();
                obj.UserId = Convert.ToString(Session["User_Code"]);

                obj.AddPTAssessment(obj);

                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientAssessment.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                PTAssessmentBAL objPTA = new PTAssessmentBAL();
                objPTA.HPA_ID = Convert.ToString(ViewState["HPA_ID"]);
                objPTA.UserId = Convert.ToString(Session["User_Code"]);
                objPTA.DeletePTAssessment(objPTA);


                Clear();

                lblStatus.Text = "Assessment Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientAssessment.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSaveTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSave = new Button();
                btnSave = (Button)sender;

                string strPath = "";
                if (btnSave.CommandName == "HC_HEALTH_PRESPAS_PROB")
                {
                    Session["Template"] = txtPresPastProblem.Text.Trim();
                    strPath = "TemplatesPopup.aspx?PageName=PatientAssessment&CtrlName=HC_HEALTH_PRESPAS_PROB";
                }
                if (btnSave.CommandName == "HC_HEALTH_PRESPAS_PREVPROB")
                {
                    Session["Template"] = txtPresPastSurgeries.Text.Trim();
                    strPath = "TemplatesPopup.aspx?PageName=PatientAssessment&CtrlName=HC_HEALTH_PRESPAS_PREVPROB";
                }

                if (btnSave.CommandName == "HC_HEALTH_ALLERGIES_MEDIC")
                {
                    Session["Template"] = txtAlergiesMedication.Text.Trim();
                    strPath = "TemplatesPopup.aspx?PageName=PatientAssessment&CtrlName=HC_HEALTH_ALLERGIES_MEDIC";
                }

                if (btnSave.CommandName == "HC_HEALTH_ALLERGIES_FOOD")
                {
                    Session["Template"] = txtAlergiesFood.Text.Trim();
                    strPath = "TemplatesPopup.aspx?PageName=PatientAssessment&CtrlName=HC_HEALTH_ALLERGIES_FOOD";
                }

                if (btnSave.CommandName == "HC_HEALTH_ALLERGIES_OTHER")
                {
                    Session["Template"] = txtAlergiesOther.Text.Trim();
                    strPath = "TemplatesPopup.aspx?PageName=PatientAssessment&CtrlName=HC_HEALTH_ALLERGIES_OTHER";
                }
                if (btnSave.CommandName == "HC_HEALTH_MEDICHIST_MEDICINE")
                {
                    Session["Template"] = txtHC_ASS_Health_MedicHist_Medicine.Text.Trim();
                    strPath = "TemplatesPopup.aspx?PageName=PatientAssessment&CtrlName=HC_HEALTH_MEDICHIST_MEDICINE";
                }



                ScriptManager.RegisterStartupScript(this, this.GetType(), "Templates", "window.open('" + strPath + "','_new','top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no');", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientAssessment.btnSaveTemplate_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        #endregion
    }
}