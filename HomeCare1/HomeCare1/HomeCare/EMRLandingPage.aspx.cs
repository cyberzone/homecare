﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;
namespace HomeCare1.HomeCare
{
    public partial class EMRLandingPage : System.Web.UI.Page
    {
        void GetHomeCareDtls()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                ViewState["FileID"] = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
                ViewState["VisitDate"] = Convert.ToString(ds.Tables[0].Rows[0]["HHV_VISIT_DATEDesc"]);



            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                string strPageName = Request.QueryString["PageName"];

                if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }

                GetHomeCareDtls();

                string strFileID = Convert.ToString(ViewState["FileID"]);
                string strVisitDate = Convert.ToString(ViewState["VisitDate"]);


                string strUserID = Convert.ToString(Session["User_ID"]);
                string strBranchID = Convert.ToString(Session["Branch_ID"]);


                //strUserID = "GNRL";
                //  strUserID = "DR.MOHAMME";
                //  strFileID = "07552";

                string strURL = "";


                if (strPageName == "Scans")
                {
                    // strURL = @"http://localhost:50787/ByPass/ByPass?Url=http://localhost:50787/HomeCareScans/Index&UserID=" + strUserID + @"&Date=" + strVisitDate + "&BranchID=" + strBranchID + "&FileNumber=" + strFileID;
                    // strURL = @"" + GlobelValues.EMR_Path + "/ByPass/ByPass?Url=" + GlobelValues.EMR_Path + "/HomeCareScans/Index&UserID=" + strUserID + @"&Date=" + strVisitDate + "&BranchID=" + strBranchID + "&FileNumber=" + strFileID;
                    strURL = @"" + GlobelValues.EMR_Path + "/ByPass/ByPassWithFileNumber?Url=" + GlobelValues.EMR_Path + "/HomeCareScans/Index&UserID=" + strUserID + @"&Date=" + strVisitDate + "&BranchID=" + strBranchID + "&FileNumber=" + strFileID;


                }
                else if (strPageName == "Scaning")
                {
                    //strURL = @"http://localhost:50787/ByPass/ByPass?Url=http://localhost:50787/HomeCareScans/List&UserID=" + strUserID + @"&Date=" + strVisitDate + "&BranchID=" + strBranchID + "&FileNumber=" + strFileID;
                    //  strURL = @"" + GlobelValues.EMR_Path + "/ByPass/ByPass?Url=" + GlobelValues.EMR_Path + "/HomeCareScans/List&UserID=" + strUserID + @"&Date=" + strVisitDate + "&BranchID=" + strBranchID + "&FileNumber=" + strFileID;
                    strURL = @"" + GlobelValues.EMR_Path + "/ByPass/ByPassWithFileNumber?Url=" + GlobelValues.EMR_Path + "/HomeCareScans/List&UserID=" + strUserID + @"&Date=" + strVisitDate + "&BranchID=" + strBranchID + "&FileNumber=" + strFileID;


                }



                // strURL = @"http://localhost:50787/ByPass/ByPass?Url=http://localhost:50787/HomeCareScans/Index&UserID=DR.MOHAMME&Date=22/02/2014&BranchID=MAIN&FileNumber=07552";

                this.iFrmEmr.Attributes["src"] = strURL;

            }

        }
    }
}