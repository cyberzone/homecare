﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class PhysicalTherapy : System.Web.UI.Page
    {
        # region Variable Declaration
        connectiondb c = new connectiondb();

        clsHomeCare objHC = new clsHomeCare();
        PhysicalTherapyEvaluationBAL objPTEval = new PhysicalTherapyEvaluationBAL();

        public static string strSessionBranchId = "", strDrName = "";

        #endregion

        # region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        //void PatientDataBind()
        //{
        //    string Criteria = " 1=1 AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
        //    Criteria += " AND HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "' ";

        //    DataSet ds = new DataSet();

        //    ds = objHC.HomeCareVisitGet(Criteria);
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
        //        txtFName.Text = ds.Tables[0].Rows[0]["HPM_PT_FNAME"].ToString();
        //        txtMName.Text = ds.Tables[0].Rows[0]["HPM_PT_MNAME"].ToString();
        //        txtLName.Text = ds.Tables[0].Rows[0]["HPM_PT_LNAME"].ToString();

        //        txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
        //        txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);



        //        txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
        //        txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
        //        txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

        //        lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
        //        txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

        //    }

        //}

        void ModifyPhysicalTherapyEval()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHPH_HHV_ID   = '" + Convert.ToString(Session["HomeCareId"]) + "' ";
            DS = objPTEval.GetPhysicalTherapyEvaluation(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                //txtHomeCareId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_HHV_ID"]);
                txtChiefCoplaint.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CHIEF_COMPLAINT"]);
                txtInjuryDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_DATEDesc"]);
                txtSugeryDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SURGERY_DATEDesc"]);
                txtInjuryRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_INJURY_REMARKS"]);
                radTherapyReceived.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_RECEIVED"]);
                txtTherapyDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_DATEDesc"]);
                txtTherapyVisitCount.Text = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_THERAPY_VISIT_COUNT"]);
                radcondition.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION"]);
                radSymptoms.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_SYMPTOMS"]);
                radAtBest.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAIN_BEST"]);
                radAtWorst.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PAINT_WORST"]);


                string strConditionBetter = "";
                strConditionBetter = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION_BETTER"]);

                string[] arrConditionBetter = strConditionBetter.Split(',');

                if (arrConditionBetter.Length > 0)
                {

                    for (int i = 0; i < arrConditionBetter.Length; i++)
                    {
                        if (arrConditionBetter[i] == "Bending")
                            chkBetterBending.Checked = true;

                        if (arrConditionBetter[i] == "Movement")
                            chkBetterMovement.Checked = true;
                        if (arrConditionBetter[i] == "Rest")
                            chkBetterRest.Checked = true;

                        if (arrConditionBetter[i] == "InAm")
                            chkBetterInAm.Checked = true;
                        if (arrConditionBetter[i] == "Sitting")
                            chkBetterSitting.Checked = true;
                        if (arrConditionBetter[i] == "Standing")
                            chkBetterStanding.Checked = true;
                        if (arrConditionBetter[i] == "Heat")
                            chkBetterHeat.Checked = true;
                        if (arrConditionBetter[i] == "AsDay")
                            chkBetterAsDay.Checked = true;
                        if (arrConditionBetter[i] == "Rising")
                            chkBetterRising.Checked = true;
                        if (arrConditionBetter[i] == "Walking")
                            chkBetterWalking.Checked = true;
                        if (arrConditionBetter[i] == "ICE")
                            chkBetterICE.Checked = true;
                        if (arrConditionBetter[i] == "InPM")
                            chkBetterInPM.Checked = true;
                        if (arrConditionBetter[i] == "ChangingPosit")
                            chkBetterChangingPosit.Checked = true;
                        if (arrConditionBetter[i] == "Lying")
                            chkBetterLying.Checked = true;
                        if (arrConditionBetter[i] == "Medication")
                            chkBetterMedication.Checked = true;
                        if (arrConditionBetter[i] == "Cough")
                            chkBetterCough.Checked = true;



                    }
                }

                string strConditionWorse = "";
                strConditionWorse = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_CONDITION_WORSE"]);

                string[] arrConditionWorse = strConditionWorse.Split(',');

                if (arrConditionWorse.Length > 0)
                {

                    for (int i = 0; i < arrConditionWorse.Length; i++)
                    {
                        if (arrConditionWorse[i] == "Bending")
                            chkWorseBending.Checked = true;
                        if (arrConditionWorse[i] == "Movement")
                            chkWorseMovement.Checked = true;
                        if (arrConditionWorse[i] == "Rest")
                            chkWorseRest.Checked = true;
                        if (arrConditionWorse[i] == "Sneeze")
                            chkWorseSneeze.Checked = true;
                        if (arrConditionWorse[i] == "Sitting")
                            chkWorseSitting.Checked = true;
                        if (arrConditionWorse[i] == "Standing")
                            chkWorseStanding.Checked = true;
                        if (arrConditionWorse[i] == "Stairs")
                            chkWorseStairs.Checked = true;
                        if (arrConditionWorse[i] == "Breath")
                            chkWorseBreath.Checked = true;
                        if (arrConditionWorse[i] == "Rising")
                            chkWorseRising.Checked = true;
                        if (arrConditionWorse[i] == "Walking")
                            chkWorseWalking.Checked = true;
                        if (arrConditionWorse[i] == "Cough")
                            chkWorseCough.Checked = true;
                        if (arrConditionWorse[i] == "Medication")
                            chkWorseMedication.Checked = true;

                    }

                }

                string strPrevMedIntervention = "";
                strPrevMedIntervention = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_MED_INTERVENTION"]);

                string[] arrPrevMedIntervention = strPrevMedIntervention.Split(',');

                if (arrPrevMedIntervention.Length > 0)
                {

                    for (int i = 0; i < arrPrevMedIntervention.Length; i++)
                    {
                        if (arrPrevMedIntervention[i] == "Xray")
                            chkPrevIntXray.Checked = true;
                        if (arrPrevMedIntervention[i] == "CATScan")
                            chkPrevIntCATScan.Checked = true;
                        if (arrPrevMedIntervention[i] == "Injections")
                            chkPrevIntInjections.Checked = true;
                    }

                }

                txtPrevIntOther.Value = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_MED_INTERVENTION_OTHER"]);
                txtGoals.Value = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_GOALS_TOBE_ACHIEVED"]);
                chkDifficultySwallowing.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_DIFFICULTY_SWALLOWING"]);
                chkMotionSickness.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_MOTION_SICKNESS"]);
                chkStroke.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_STROKE"]);
                chkArthritis.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_ARTHRITIS"]);
                chkFeverChillsSweats.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_FEVER_CHILLS_SWEATS"]);
                chkOsteoporosis.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_OSTEOPOROSIS"]);
                chkHighBP.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HIGH_BLOOD_PRESSURE"]);
                chkUnexplainedWeightLoss.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_UNEXPLAINED_WEIGHT_LOSS"]);
                chkAnemia.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_ANEMIA"]);
                chkHeartTrouble.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HEART_TROUBLE"]);
                chkBloodClots.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_BLOOD_CLOTS"]);
                chkBleedingProblems.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_BLEEDING_PROBLEMS"]);
                chkPacemaker.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_PACEMAKER"]);
                chkShortnessOfBreath.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_SHORTNESS_OF_BREATH"]);
                chkHIVHepatitis.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HIV_HEPATITIS"]);
                chkEpilepsySeizures.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_EPILEPSY_SEIZURES"]);
                chkHistorySmoking.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HISTORY_SMOKING"]);
                chkHistoryAlcoholAbuse.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HISTORY_ALCOHOL_ABUSE"]);
                chkHistoryDrugAbuse.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_HISTORY_DRUG_ABUSE"]);
                chkDiabetes.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_DIABETES"]);
                chkDepressionAnxiety.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_DEPRESSION_ANXIETY"]);
                chkMyofascialPain.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_MYOFASCIAL_PAIN"]);
                chkFibromyalgia.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_FIBROMYALGIA"]);
                chkPregnancy.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_PREGNANCY"]);
                chkcancer.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HHPH_CANCER"]);
                txtPrevSurgeries.Value = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_PREV_SURGERIES"]);
                txtOther.Value = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_OTHER"]);
                txtMedications.Value = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_MEDICATIONS"]);
                txtAllergies.Value = Convert.ToString(DS.Tables[0].Rows[0]["HHPH_ALLERGIES"]);


                // PatientDataBind();
            }
        }

        void Clear()
        {
            //txtFileNo.Text = "";
            //txtFName.Text = "";
            //txtMName.Text = "";
            //txtLName.Text = "";
            //txtDOB.Text = "";
            //txtSex.Text = "";
            //txtPhone1.Text = "";
            //txtMobile1.Text = "";
            //txtMobile2.Text = "";
            //lblIDCaption.Text = "";
            //txtEmiratesID.Text = "";

            txtChiefCoplaint.Text = "";
            txtInjuryDate.Text = "";
            txtSugeryDate.Text = "";
            txtInjuryRemarks.Text = "";
            radTherapyReceived.SelectedIndex = -1;
            txtTherapyDate.Text = "";
            txtTherapyVisitCount.Text = "";
            radcondition.SelectedIndex = -1;
            radSymptoms.SelectedIndex = -1;
            radAtBest.SelectedIndex = -1;
            radAtWorst.SelectedIndex = -1;

            chkBetterBending.Checked = false;
            chkBetterMovement.Checked = false;
            chkBetterRest.Checked = false;
            chkBetterInAm.Checked = false;
            chkBetterSitting.Checked = false;
            chkBetterStanding.Checked = false;
            chkBetterHeat.Checked = false;
            chkBetterAsDay.Checked = false;
            chkBetterRising.Checked = false;
            chkBetterWalking.Checked = false;
            chkBetterICE.Checked = false;
            chkBetterInPM.Checked = false;
            chkBetterChangingPosit.Checked = false;
            chkBetterLying.Checked = false;
            chkBetterMedication.Checked = false;
            chkBetterCough.Checked = false;


            chkWorseBending.Checked = false;
            chkWorseMovement.Checked = false;
            chkWorseRest.Checked = false;
            chkWorseSneeze.Checked = false;
            chkWorseSitting.Checked = false;
            chkWorseStanding.Checked = false;
            chkWorseStairs.Checked = false;
            chkWorseBreath.Checked = false;
            chkWorseRising.Checked = false;
            chkWorseWalking.Checked = false;
            chkWorseCough.Checked = false;
            chkWorseMedication.Checked = false;

            chkPrevIntXray.Checked = false;
            chkPrevIntCATScan.Checked = false;
            chkPrevIntInjections.Checked = false;

            txtPrevIntOther.Value = "";
            txtGoals.Value = "";
            chkDifficultySwallowing.Checked = false;
            chkMotionSickness.Checked = false;
            chkStroke.Checked = false;
            chkArthritis.Checked = false;
            chkFeverChillsSweats.Checked = false;
            chkOsteoporosis.Checked = false;
            chkHighBP.Checked = false;
            chkUnexplainedWeightLoss.Checked = false;
            chkAnemia.Checked = false;
            chkHeartTrouble.Checked = false;
            chkBloodClots.Checked = false;
            chkBleedingProblems.Checked = false;
            chkPacemaker.Checked = false;
            chkShortnessOfBreath.Checked = false;
            chkHIVHepatitis.Checked = false;
            chkEpilepsySeizures.Checked = false;
            chkHistorySmoking.Checked = false;
            chkHistoryAlcoholAbuse.Checked = false;
            chkHistoryDrugAbuse.Checked = false;
            chkDiabetes.Checked = false;
            chkDepressionAnxiety.Checked = false;
            chkMyofascialPain.Checked = false;
            chkFibromyalgia.Checked = false;
            chkPregnancy.Checked = false;
            chkcancer.Checked = false;
            txtPrevSurgeries.Value = "";
            txtOther.Value = "";
            txtMedications.Value = "";
            txtAllergies.Value = "";

            btnDelete.Visible = false;
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='HCPHYTHE' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnClear.Enabled = false;
                btnDelete.Enabled = false;
            }


            if (strPermission == "7")
            {
                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {

                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {
                    string PatientId = "", Id = "";


                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                    Id = Convert.ToString(Session["HomeCareId"]);

                    if (PatientId != " " && PatientId != null)
                    {
                        //txtFileNo.Text = Convert.ToString(PatientId);
                        //PatientDataBind();
                    }

                    if (Id != " " && Id != null)
                    {
                        // hidHHPH_ID.Value = Convert.ToString(HHPH_ID);

                        ModifyPhysicalTherapyEval();
                        // PatientDataBind();
                        btnDelete.Visible = true;
                    }
                    else
                    {


                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strConditionBetter = "";
                if (chkBetterBending.Checked == true)
                    strConditionBetter += "Bending,";
                if (chkBetterMovement.Checked == true)
                    strConditionBetter += "Movement,";
                if (chkBetterRest.Checked == true)
                    strConditionBetter += "Rest,";
                if (chkBetterInAm.Checked == true)
                    strConditionBetter += "InAm,";
                if (chkBetterSitting.Checked == true)
                    strConditionBetter += "Sitting,";
                if (chkBetterStanding.Checked == true)
                    strConditionBetter += "Standing,";

                if (chkBetterHeat.Checked == true)
                    strConditionBetter += "Heat,";
                if (chkBetterAsDay.Checked == true)
                    strConditionBetter += "AsDay,";
                if (chkBetterRising.Checked == true)
                    strConditionBetter += "Rising,";
                if (chkBetterWalking.Checked == true)
                    strConditionBetter += "Walking,";
                if (chkBetterICE.Checked == true)
                    strConditionBetter += "ICE,";
                if (chkBetterInPM.Checked == true)
                    strConditionBetter += "InPM,";

                if (chkBetterChangingPosit.Checked == true)
                    strConditionBetter += "ChangingPosit,";
                if (chkBetterLying.Checked == true)
                    strConditionBetter += "Lying,";
                if (chkBetterMedication.Checked == true)
                    strConditionBetter += "Medication,";
                if (chkBetterCough.Checked == true)
                    strConditionBetter += "Cough,";

                if (strConditionBetter.Length > 1)
                    strConditionBetter = strConditionBetter.Substring(0, strConditionBetter.Length - 1);

                string strConditionWorse = "";
                if (chkWorseBending.Checked == true)
                    strConditionWorse += "Bending,";
                if (chkWorseMovement.Checked == true)
                    strConditionWorse += "Movement,";
                if (chkWorseRest.Checked == true)
                    strConditionWorse += "Rest,";
                if (chkWorseSneeze.Checked == true)
                    strConditionWorse += "Sneeze,";
                if (chkWorseSitting.Checked == true)
                    strConditionWorse += "Sitting,";
                if (chkWorseStanding.Checked == true)
                    strConditionWorse += "Standing,";

                if (chkWorseStairs.Checked == true)
                    strConditionWorse += "Stairs,";
                if (chkWorseBreath.Checked == true)
                    strConditionWorse += "Breath,";
                if (chkWorseRising.Checked == true)
                    strConditionWorse += "Rising,";
                if (chkWorseWalking.Checked == true)
                    strConditionWorse += "Walking,";
                if (chkWorseCough.Checked == true)
                    strConditionWorse += "Cough,";
                if (chkWorseMedication.Checked == true)
                    strConditionWorse += "Medication,";

                if (strConditionWorse.Length > 1)
                    strConditionWorse = strConditionWorse.Substring(0, strConditionWorse.Length - 1);


                string strPrevMedIntervention = "";
                if (chkPrevIntXray.Checked == true)
                    strPrevMedIntervention += "Xray,";
                if (chkPrevIntCATScan.Checked == true)
                    strPrevMedIntervention += "CATScan,";
                if (chkPrevIntInjections.Checked == true)
                    strPrevMedIntervention += "Injections,";
                if (strPrevMedIntervention.Length > 1)
                    strPrevMedIntervention = strPrevMedIntervention.Substring(0, strPrevMedIntervention.Length - 1);


                PhysicalTherapyEvaluationBAL objPTEval = new PhysicalTherapyEvaluationBAL();
                objPTEval.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPTEval.HHV_ID = Convert.ToString(Session["HomeCareId"]);
                objPTEval.CHIEF_COMPLAINT = txtChiefCoplaint.Text.Trim();
                objPTEval.INJURY_DATE = txtInjuryDate.Text.Trim();
                objPTEval.SURGERY_DATE = txtSugeryDate.Text.Trim();
                objPTEval.INJURY_REMARKS = txtInjuryRemarks.Text.Trim();
                objPTEval.THERAPY_RECEIVED = Convert.ToString(radTherapyReceived.SelectedValue);
                objPTEval.THERAPY_DATE = txtTherapyDate.Text.Trim();
                objPTEval.THERAPY_VISIT_COUNT = txtTherapyVisitCount.Text.Trim();
                objPTEval.CONDITION = radcondition.SelectedValue;
                objPTEval.SYMPTOMS = radSymptoms.SelectedValue;
                objPTEval.PAIN_BEST = radAtBest.SelectedValue;
                objPTEval.PAINT_WORST = radAtWorst.SelectedValue;
                objPTEval.CONDITION_BETTER = strConditionBetter;
                objPTEval.CONDITION_WORSE = strConditionWorse;
                objPTEval.PREV_MED_INTERVENTION = strPrevMedIntervention;
                objPTEval.PREV_MED_INTERVENTION_OTHER = txtPrevIntOther.Value.Trim();
                objPTEval.GOALS_TOBE_ACHIEVED = txtGoals.Value.Trim();
                objPTEval.DIFFICULTY_SWALLOWING = Convert.ToString(chkDifficultySwallowing.Checked);
                objPTEval.MOTION_SICKNESS = Convert.ToString(chkMotionSickness.Checked);
                objPTEval.STROKE = Convert.ToString(chkStroke.Checked);
                objPTEval.ARTHRITIS = Convert.ToString(chkArthritis.Checked);
                objPTEval.FEVER_CHILLS_SWEATS = Convert.ToString(chkFeverChillsSweats.Checked);
                objPTEval.OSTEOPOROSIS = Convert.ToString(chkOsteoporosis.Checked);
                objPTEval.HIGH_BLOOD_PRESSURE = Convert.ToString(chkHighBP.Checked);
                objPTEval.UNEXPLAINED_WEIGHT_LOSS = Convert.ToString(chkUnexplainedWeightLoss.Checked);
                objPTEval.ANEMIA = Convert.ToString(chkAnemia.Checked);
                objPTEval.HEART_TROUBLE = Convert.ToString(chkHeartTrouble.Checked);
                objPTEval.BLOOD_CLOTS = Convert.ToString(chkBloodClots.Checked);
                objPTEval.BLEEDING_PROBLEMS = Convert.ToString(chkBleedingProblems.Checked);
                objPTEval.PACEMAKER = Convert.ToString(chkPacemaker.Checked);
                objPTEval.SHORTNESS_OF_BREATH = Convert.ToString(chkShortnessOfBreath.Checked);
                objPTEval.HIV_HEPATITIS = Convert.ToString(chkHIVHepatitis.Checked);
                objPTEval.EPILEPSY_SEIZURES = Convert.ToString(chkEpilepsySeizures.Checked);
                objPTEval.HISTORY_SMOKING = Convert.ToString(chkHistorySmoking.Checked);
                objPTEval.HISTORY_ALCOHOL_ABUSE = Convert.ToString(chkHistoryAlcoholAbuse.Checked);
                objPTEval.HISTORY_DRUG_ABUSE = Convert.ToString(chkHistoryDrugAbuse.Checked);
                objPTEval.DIABETES = Convert.ToString(chkDiabetes.Checked);
                objPTEval.DEPRESSION_ANXIETY = Convert.ToString(chkDepressionAnxiety.Checked);
                objPTEval.MYOFASCIAL_PAIN = Convert.ToString(chkMyofascialPain.Checked);
                objPTEval.FIBROMYALGIA = Convert.ToString(chkFibromyalgia.Checked);
                objPTEval.PREGNANCY = Convert.ToString(chkPregnancy.Checked);
                objPTEval.CANCER = Convert.ToString(chkcancer.Checked);
                objPTEval.PREV_SURGERIES = txtPrevSurgeries.Value.Trim();
                objPTEval.OTHER = txtOther.Value.Trim();
                objPTEval.MEDICATIONS = txtMedications.Value.Trim();
                objPTEval.ALLERGIES = txtAllergies.Value.Trim();
                objPTEval.UserId = Convert.ToString(Session["User_Code"]);
                objPTEval.AddPhysicalTherapyEvaluation(objPTEval);

                //Clear();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


                // FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PhysicalTherapy.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                PhysicalTherapyEvaluationBAL objPTEval = new PhysicalTherapyEvaluationBAL();
                objPTEval.HHV_ID = Convert.ToString(Session["HomeCareId"]);
                objPTEval.DeleteTravelStatus(objPTEval);

                Clear();
                lblStatus.Text = "Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PhysicalTherapy.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PhysicalTherapy.btnClear_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}