﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeCareReports.aspx.cs" Inherits="HomeCare1.HomeCare.HomeCareReports" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div>
         <table >
        <tr>
            <td class="PageHeader">
                <asp:Label ID="lblReportHeader" runat="server"  CssClass="PageHeader" Text="Report"></asp:Label>
            </td>
        </tr>
    </table>
        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
            <tr>
                <td>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </td>
            </tr>

        </table>

        <table width="900px" cellpadding="5" cellspacing="5">
            <tr>
                <td style="width: 150px; height: 30px;">
                    <asp:Label ID="Label1" runat="server" CssClass="label"
                        Text="From"></asp:Label><span style="color:red;">* </span>
                </td>
                <td style="width: 400px" colspan="3">
                    <asp:TextBox ID="txtFromDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                        Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                    &nbsp;
                    <asp:Label ID="Label2" runat="server" CssClass="label"
                        Text="To"></asp:Label>

                    <asp:TextBox ID="txtToDate" runat="server" Width="75px" MaxLength="10" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                        Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                </td>

            </tr>

            <tr>
                <td style="width: 100px; height: 30px;">
                    <asp:Label ID="Label8" runat="server" CssClass="label"
                        Text="Company"></asp:Label>

                </td>
                <td colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#cccccc" Width="70px" MaxLength="10" onKeyUp="DoUpper(this);" onblur="return CompIdSelected()" OnTextChanged="txtCompany_TextChanged"></asp:TextBox>
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" Width="350px" MaxLength="50" onKeyUp="DoUpper(this);" onblur="return CompNameSelected()" OnTextChanged="txtCompanyName_TextChanged"></asp:TextBox>

                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtCompany" MinimumPrefixLength="1" ServiceMethod="GetCompany"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtCompanyName" MinimumPrefixLength="1" ServiceMethod="GetCompanyName"></asp:AutoCompleteExtender>

                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td style="width: 100px; height: 30px;">
                    <asp:Label ID="Label20" runat="server" CssClass="label" Text="Doctor"></asp:Label>
                </td>
                <td class="auto-style28" colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDoctorID" runat="server" CssClass="label"  BorderColor="#CCCCCC" BorderWidth="1" Width="70px" AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()" onKeyUp="DoUpper(this);"  ></asp:TextBox>
                            <asp:TextBox ID="txtDoctorName" runat="server" CssClass="label" BorderColor="#CCCCCC" BorderWidth="1" AutoPostBack="true" OnTextChanged="txtDoctorName_TextChanged" Width="350px" MaxLength="50" onblur="return DRNameSelected()" onKeyUp="DoUpper(this);" ></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"></asp:AutoCompleteExtender>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"></asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

            </tr>
            <tr>
                <td style="width: 100px; height: 30px;" class="label">Remarks</td>
                <td>
                    <asp:Button ID="btnReport" runat="server" Text="Report" Width="100px" CssClass="button orange small" OnClick="btnReport_Click" OnClientClick="return Val();" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
