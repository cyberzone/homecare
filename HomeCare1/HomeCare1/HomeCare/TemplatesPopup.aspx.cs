﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.HomeCare
{
    public partial class TemplatesPopup : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {

            TemplatesBAL obj = new TemplatesBAL();
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ET_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ET_TYPE ='" + hidCtrlName.Value + "'";
            ds = obj.GetEMRTemplates(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvTemplates.DataSource = ds;
                gvTemplates.DataBind();


            }
            else
            {
                gvTemplates.DataBind();
            }


        }
        void Clear()
        {
            txtName.Text = "";
            Session["Template"] = "";
            ViewState["Template"] = "";
            btnSave.Visible = false;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    // hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    hidCtrlData.Value = Convert.ToString(Request.QueryString["CtrlData"]);
                    string strMode;
                    strMode = Convert.ToString(Request.QueryString["Mode"]);
                    if (strMode == "Show")
                    {
                        divSave.Visible = false;

                    }

                    ViewState["Template"] = Convert.ToString(Session["Template"]);
                    Session["Template"] = "";


                    BindGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      TemplatesPopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void gvTemplates_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvTemplates.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TemplatesPopup.gvTemplates_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteTemplates_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblBranchID, lblType, lblName;
                lblBranchID = (Label)gvScanCard.Cells[0].FindControl("lblBranchID");
                lblType = (Label)gvScanCard.Cells[0].FindControl("lblType");
                lblName = (Label)gvScanCard.Cells[0].FindControl("lblName");

                TemplatesBAL obj = new TemplatesBAL();
                obj.ET_BRANCH_ID = lblBranchID.Text.Trim();
                obj.ET_TYPE = lblType.Text.Trim();
                obj.ET_NAME = lblName.Text.Trim();
                obj.DeleteEMRTemplates(obj);

                lblStatus.Text = "Template Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindGrid();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.DeleteStaff_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                TemplatesBAL obj = new TemplatesBAL();
                obj.ET_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                obj.ET_TYPE = hidCtrlName.Value;
                obj.ET_NAME = txtName.Text.Trim(); ;
                obj.ET_TEMPLATE = Convert.ToString(hidCtrlData.Value);
                obj.ET_DR_CODE = Convert.ToString(Session["User_Code"]);
                obj.ET_DEP_ID = Convert.ToString(Session["User_DeptID"]);

                if (chkApply.Checked == true)
                {
                    obj.ET_APPLY = "1";
                }
                else
                {
                    obj.ET_APPLY = "0";
                }

                obj.AddEMRTemplates(obj);
                BindGrid();
                Clear();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TemplatesPopup.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }
    }
}