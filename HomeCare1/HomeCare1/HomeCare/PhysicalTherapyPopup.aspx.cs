﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.HomeCare
{
    public partial class PhysicalTherapyPopup : System.Web.UI.Page
    {
        PhysicalTherapyEvaluationBAL objPTEval = new PhysicalTherapyEvaluationBAL();
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("~/HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";

            if (txtHomeCareId.Text.Trim() != "")
            {

                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HHPH_ID like '" + txtHomeCareId.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HHPH_ID  like '%" + txtHomeCareId.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HHPH_ID   = '" + txtHomeCareId.Text.Trim() + "' ";
                }

            }

            if (txtSrcName.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PT_FNAME like '" + txtSrcName.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PT_FNAME + ' ' +isnull(HPM_PT_MNAME,'') + ' '  + isnull(HPM_PT_LNAME,'')   like '%" + txtSrcName.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PT_FNAME + ' ' +isnull(HPM_PT_MNAME,'') + ' '  + isnull(HPM_PT_LNAME,'')   = '" + txtSrcName.Text.Trim() + "' ";
                }

            }

            if (txtSrcFileNo.Text.Trim() != "")
            {


                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PT_ID like '" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PT_ID  like '%" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PT_ID   = '" + txtSrcFileNo.Text.Trim() + "' ";
                }



            }

            lblTotal.Text = "0";
            ds = objPTEval.GetPhysicalTherapyEvaluation(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPhysicalTherapy.DataSource = ds;
                gvPhysicalTherapy.DataBind();
                lblTotal.Text = Convert.ToString(ds.Tables[0].Rows.Count);

            }
            else
            {
                gvPhysicalTherapy.DataBind();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    hidPageName.Value = Request.QueryString["PageName"].ToString();
                    ViewState["CtrlName"] = Request.QueryString["CtrlName"].ToString();
                    ViewState["Value"] = Request.QueryString["Value"].ToString();


                    if (ViewState["CtrlName"] != "")
                    {
                        if (ViewState["CtrlName"].ToString() == "HomeCareId")
                        {
                            txtHomeCareId.Text = ViewState["Value"].ToString();
                        }

                    }

                    BindGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PhysicalTherapyPopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void gvPhysicalTherapy_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvPhysicalTherapy.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PhysicalTherapyPopup.gvHCVisit_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["CtrlName"] = "";
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PhysicalTherapyPopup.gvHCVisit_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}