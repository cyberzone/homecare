﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class WaitingList : System.Web.UI.Page
    {
        clsHomeCare objHC = new clsHomeCare();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (txtHomeCareId.Text.Trim() != "")
            {

                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HHV_ID like '" + txtHomeCareId.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HHV_ID  like '%" + txtHomeCareId.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HHV_ID   = '" + txtHomeCareId.Text.Trim() + "' ";
                }

            }

            if (txtSrcName.Text.Trim() != "")
            {
                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PT_FNAME like '" + txtSrcName.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PT_FNAME + ' ' +isnull(HPM_PT_MNAME,'') + ' '  + isnull(HPM_PT_LNAME,'')   like '%" + txtSrcName.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PT_FNAME + ' ' +isnull(HPM_PT_MNAME,'') + ' '  + isnull(HPM_PT_LNAME,'')   = '" + txtSrcName.Text.Trim() + "' ";
                }

            }

            if (txtSrcFileNo.Text.Trim() != "")
            {


                if (Convert.ToString(Session["SearchOption"]).ToUpper() == "ST")
                {
                    Criteria += " AND HPM_PT_ID like '" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "AN")
                {
                    Criteria += " AND HPM_PT_ID  like '%" + txtSrcFileNo.Text.Trim() + "%' ";
                }
                else if (Convert.ToString(Session["SearchOption"]).ToUpper() == "EX")
                {
                    Criteria += " AND HPM_PT_ID   = '" + txtSrcFileNo.Text.Trim() + "' ";
                }



            }



            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HHV_VISIT_DATE,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HHV_VISIT_DATE,101),101) <= '" + strForToDate + "'";
            }



            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvHCVisit.DataSource = ds;
                gvHCVisit.DataBind();


            }
            else
            {
                gvHCVisit.DataBind();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                try
                {
                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    BindGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      WaitingList.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void gvHCVisit_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvHCVisit.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      WaitingList.gvHCVisit_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnFind_Click(object sender, EventArgs e)
        {
            ViewState["CtrlName"] = "";
            BindGrid();


        }

        protected void Select_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;




            Label lblHHVId = (Label)gvScanCard.Cells[0].FindControl("lblHHVId");
            Label lblPT_ID = (Label)gvScanCard.Cells[0].FindControl("lblPT_ID");
            Label lblVisitDate = (Label)gvScanCard.Cells[0].FindControl("lblVisitDate");
            Session["HomeCareId"] = lblHHVId.Text.Trim();
            GlobelValues.HomeCare_ID = lblHHVId.Text.Trim();
            Session["HHV_PT_ID"] = lblPT_ID.Text.Trim();
            Session["HHV_VISIT_DATE"] = lblVisitDate.Text.Trim();


            Response.Redirect("HomeCareRegistration.aspx?HHV_ID=" + lblHHVId.Text.Trim());


        }


        protected void btnNewReg_Click(object sender, EventArgs e)
        {

            Response.Redirect("HomeCareRegistration.aspx?");
        }
    }
}