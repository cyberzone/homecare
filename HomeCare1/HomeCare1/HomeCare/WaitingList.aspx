﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCare.master" AutoEventWireup="true" CodeBehind="WaitingList.aspx.cs" Inherits="HomeCare1.HomeCare.WaitingList" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table >
        <tr>
            <td class="PageHeader">Waiting List
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>

            <td>
                <asp:updatepanel id="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:updatepanel>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td class="label" style="height: 30px;">Home Care ID
            </td>
            <td>
                <asp:textbox id="txtHomeCareId" runat="server" borderwidth="1px"  borderColor="#cccccc"  width="100px"></asp:textbox>
            </td>
            <td class="label" style="height: 30px;">File No

            </td>
            <td>
                <asp:textbox id="txtSrcFileNo" runat="server" borderwidth="1px"  borderColor="#cccccc" width="100px" maxlength="10"></asp:textbox>
            </td>
            <td class="label" style="height: 30px;">Name 
            </td>

            <td>
                <asp:textbox id="txtSrcName" runat="server"  borderwidth="1px"  borderColor="#cccccc"  width="200px"></asp:textbox>
                    <asp:button id="btnFind" runat="server" style="padding-left:5px;padding-right:5px;width:60px;" cssclass="button orange small"
                    onclick="btnFind_Click" text="Refresh"   />
            </td>
           
         
        </tr>
        <tr>
            <td class="label" style="height: 30px;">Visit Date
            </td>
            <td>
                <asp:textbox id="txtFromDate" runat="server" width="100px" cssclass="label" borderwidth="1px"  BorderColor="#cccccc" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                    enabled="True" targetcontrolid="txtFromDate" format="dd/MM/yyyy">
                                        </asp:calendarextender>
                <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtFromDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

            </td>

            <td class="label" style="height: 30px;">To
            </td>
            <td>
                <asp:textbox id="txtToDate" runat="server" width="100px" cssclass="label" borderwidth="1px"  borderColor="#cccccc" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                <asp:calendarextender id="txtToDate_CalendarExtender" runat="server"
                    enabled="True" targetcontrolid="txtToDate" format="dd/MM/yyyy">
                                        </asp:calendarextender>
                <asp:maskededitextender id="MaskedEditExtender2" runat="server" enabled="true" targetcontrolid="txtToDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

            </td>
        </tr>
    </table>

    <div style="padding-top: 0px; width: 100%; height: 450px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

        <asp:gridview id="gvHCVisit" runat="server" autogeneratecolumns="False" allowpaging="true"
            enablemodelvalidation="True" width="100%" pagesize="200" onpageindexchanging="gvHCVisit_PageIndexChanging">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                     <asp:TemplateField HeaderText="Home Care ID">
                        <ItemTemplate>
                             <asp:Label ID="lblHHVId" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("HHV_ID") %>'></asp:Label>
                            <asp:Label ID="lblPT_ID" runat="server"  Text='<%# Bind("HHV_PT_ID") %>' Visible="false"> </asp:Label>
                             <asp:Label ID="lblVisitDate" runat="server"  Text='<%# Bind("HHV_VISIT_DATEDesc") %>' Visible="false"> </asp:Label>

                             <asp:LinkButton ID="lnkHHV_ID" runat="server" OnClick="Select_Click" Text='<%# Bind("HHV_ID") %>'> </asp:LinkButton>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="File No">
                        <ItemTemplate>
                              <asp:LinkButton ID="lnkPT_ID" runat="server" OnClick="Select_Click" Text='<%# Bind("HHV_PT_ID") %>'> </asp:LinkButton>
                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                         
                            <asp:LinkButton ID="lnkName" runat="server" OnClick="Select_Click" Text='<%# Bind("FullName") %>'> </asp:LinkButton>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Visit Date">
                        <ItemTemplate>
                         
                            <asp:LinkButton ID="lnkVisitDt" runat="server" OnClick="Select_Click" Text='<%# Bind("HHV_VISIT_DATEDesc") %>'> </asp:LinkButton>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Begin Date">
                        <ItemTemplate>
                           
                            <asp:LinkButton ID="lnkBegDt" runat="server" OnClick="Select_Click" Text='<%# Bind("HHV_BEGIN_DATEDesc") %>'> </asp:LinkButton>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <ItemTemplate>
                         
                            <asp:LinkButton ID="lnkEndDt" runat="server" OnClick="Select_Click" Text='<%# Bind("HHV_END_DATEDesc") %>'> </asp:LinkButton>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />
            </asp:gridview>
    </div>
    <table width="930px;">
        <tr>
            <td class="style2" align="left">
              <asp:Button ID="btnNewReg" runat="server" style="padding-left:5px;padding-right:5px;width:100px;"  CssClass="button orange small"   OnClick="btnNewReg_Click"  Text="New Registration" />

            </td>
        </tr>
    </table>
</asp:Content>
