﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class PatientEncounter : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }

                drpArrHour.Items.Insert(index, Convert.ToString(strHour));
                drpArrHour.Items[index].Value = Convert.ToString(strHour);

                drpVisitHour.Items.Insert(index, Convert.ToString(strHour));
                drpVisitHour.Items[index].Value = Convert.ToString(strHour);




                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;




            drpArrMin.Items.Insert(0, Convert.ToString("00"));
            drpArrMin.Items[0].Value = Convert.ToString("00");

            drpVisiMin.Items.Insert(0, Convert.ToString("00"));
            drpVisiMin.Items[0].Value = Convert.ToString("00");



            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpArrMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpArrMin.Items[index].Value = Convert.ToString(j - intCount);

                drpVisiMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpVisiMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }


        void BindEncounter()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPE_HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "'";

            PTEncounterBAL objPTEnc = new PTEncounterBAL();
            ds = objPTEnc.GetPTEncounter(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["HPE_ID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPE_ID"]);
                txtArea.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_AREA"]);
                txtVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_VISIT_DATEDesc"]);
                txtArrivalDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_ARRIVAL_TIMEDesc"]);
                txtProvisionalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_DIAGNOSIS"]);

                radConditionUponVisit.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPE_CONDITION"]);
                txtConditionOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_CONDITION_OTH"]);

                string strSTHour = Convert.ToString(ds.Tables[0].Rows[0]["ArrivalTime"]);

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpArrHour.SelectedValue = arrSTHour[0];
                    drpArrMin.SelectedValue = arrSTHour[1];

                }

                string strVisitHour = Convert.ToString(ds.Tables[0].Rows[0]["VisitTime"]);
                string[] arrVisitHour = strVisitHour.Split(':');
                if (arrVisitHour.Length > 1)
                {
                    drpVisitHour.SelectedValue = arrVisitHour[0];
                    drpVisiMin.SelectedValue = arrVisitHour[1];

                }

                radInfoObtainedFrom.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPE_INFOBTAINDFROM"]);
                txtInfoObtainedOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_INFOBTAINDFROM_OTH"]);

                radExplanationGiven.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPE_EXPLI_GIVEN"]);
                txtExplanationGivenOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_EXPLI_GIVEN_OTH"]);

                if (ds.Tables[0].Rows[0].IsNull("HPE_ASSESSMENT_DONE_CODE") == false)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["HPE_ASSESSMENT_DONE_CODE"]) != "")
                    {
                        txtAssessmentDoneBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_ASSESSMENT_DONE_CODE"]) + " ~ " + Convert.ToString(ds.Tables[0].Rows[0]["HPE_ASSESSMENT_DONE_NAME"]);

                    }
                }

                txtPresentComplaient.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRESENT_COMPLAINT"]);
                txtPastMedicalHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_MEDICAL_HISTORY"]);
                txtSurgicallHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PAST_SURGICAL_HISTORY"]);
                txtPsychoSocEcoHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PSY_SOC_ECONOMIC_HISTORY"]);
                txtFamilyHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_FAMILY_HISTORY"]);
                txtPhysicalAssessment.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PHYSICAL_ASSESSMENT"]);
                txtReviewOfSystem.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_REVIEW_OF_SYSTEMS"]);
                txtInvestigations.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_INVESTIGATIONS"]);
                txtPrincipalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PRINCIPAL_DIAGNOSIS"]);
                txtSecondaryDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_SECONDARY_DIAGNOSIS"]);
                txtTreatmentPlan.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT_PLAN"]);
                txtProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_PROCEDURE"]);
                txtTreatment.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_TREATMENT"]);
                txtFollowUpNotes.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPE_FOLLOWUPNOTES"]);

            }

        }

        void BindHomeCareReg()
        {

            string Criteria = " 1=1 AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            //Criteria += " AND HHV_ID = '" + hidHHV_ID.Value + "' ";
            Criteria += " AND HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                hidPTID.Value = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
            }

        }

        [System.Web.Services.WebMethod]
        public static string[] GetStaffName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_STAFF_ID +' '  + HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        void Clear()
        {
            ViewState["HPE_ID"] = "0";
            txtArea.Text = "";
            txtArrivalDate.Text = "";
            drpArrHour.SelectedIndex = 0;
            drpArrMin.SelectedIndex = 0;
            txtVisitDate.Text = "";


            txtProvisionalDiagnosis.Text = "";
            radConditionUponVisit.SelectedIndex = -1;
            txtConditionOther.Text = "";
            radInfoObtainedFrom.SelectedIndex = -1;
            txtInfoObtainedOther.Text = "";

            radExplanationGiven.SelectedIndex = -1;
            txtExplanationGivenOther.Text = "";
            txtAssessmentDoneBy.Text = "";

            txtPresentComplaient.Text = "";
            txtPastMedicalHistory.Text = "";
            txtSurgicallHistory.Text = "";
            txtPsychoSocEcoHistory.Text = "";
            txtFamilyHistory.Text = "";
            txtPhysicalAssessment.Text = "";
            txtReviewOfSystem.Text = "";
            txtInvestigations.Text = "";
            txtPrincipalDiagnosis.Text = "";
            txtSecondaryDiagnosis.Text = "";
            txtTreatmentPlan.Text = "";
            txtProcedure.Text = "";
            txtTreatment.Text = "";
            txtFollowUpNotes.Text = "";

        }

        void SaveEncounter()
        {

            try
            {
                PTEncounterBAL objPTEnc = new PTEncounterBAL();
                objPTEnc.HPE_ID = Convert.ToString(ViewState["HPE_ID"]);
                objPTEnc.HPE_HHV_ID = Convert.ToString(Session["HomeCareId"]);
                objPTEnc.HPE_AREA = txtArea.Text.Trim();
                if (txtArrivalDate.Text.Trim() != "")
                {
                    objPTEnc.HPE_ARRIVAL_TIME = txtArrivalDate.Text.Trim() + " " + drpArrHour.SelectedValue + ":" + drpArrMin.SelectedValue + ":00";
                }
                else
                {
                    objPTEnc.HPE_ARRIVAL_TIME = "";
                }
                if (txtVisitDate.Text.Trim() != "")
                {
                    objPTEnc.HPE_VISIT_DATE = txtVisitDate.Text.Trim() + " " + drpVisitHour.SelectedValue + ":" + drpVisiMin.SelectedValue + ":00";
                }
                else
                {
                    objPTEnc.HPE_VISIT_DATE = "";
                }
                objPTEnc.HPE_DIAGNOSIS = txtProvisionalDiagnosis.Text.Trim();
                objPTEnc.HPE_CONDITION = Convert.ToString(radConditionUponVisit.SelectedValue);
                objPTEnc.HPE_CONDITION_OTH = txtConditionOther.Text.Trim();
                objPTEnc.HPE_INFOBTAINDFROM = Convert.ToString(radInfoObtainedFrom.SelectedValue);
                objPTEnc.HPE_INFOBTAINDFROM_OTH = txtInfoObtainedOther.Text.Trim();

                objPTEnc.HPE_EXPLI_GIVEN = Convert.ToString(radExplanationGiven.SelectedValue);
                objPTEnc.HPE_EXPLI_GIVEN_OTH = txtExplanationGivenOther.Text.Trim();

                string strAssessmentDoneby = txtAssessmentDoneBy.Text.Trim();
                string[] arrAssessmentDoneby = strAssessmentDoneby.Split('~');

                if (arrAssessmentDoneby.Length > 1)
                {
                    objPTEnc.HPE_ASSESSMENT_DONE_CODE = arrAssessmentDoneby[0].Trim();
                    objPTEnc.HPE_ASSESSMENT_DONE_NAME = arrAssessmentDoneby[1].Trim();
                }
                else
                {
                    objPTEnc.HPE_ASSESSMENT_DONE_CODE = "";
                    objPTEnc.HPE_ASSESSMENT_DONE_NAME = "";
                }

                objPTEnc.HPE_PRESENT_COMPLAINT = txtPresentComplaient.Text.Trim();
                objPTEnc.HPE_PAST_MEDICAL_HISTORY = txtPastMedicalHistory.Text.Trim();
                objPTEnc.HPE_PAST_SURGICAL_HISTORY = txtSurgicallHistory.Text.Trim();
                objPTEnc.HPE_PSY_SOC_ECONOMIC_HISTORY = txtPsychoSocEcoHistory.Text.Trim();
                objPTEnc.HPE_FAMILY_HISTORY = txtFamilyHistory.Text.Trim();
                objPTEnc.HPE_PHYSICAL_ASSESSMENT = txtPhysicalAssessment.Text.Trim();
                objPTEnc.HPE_REVIEW_OF_SYSTEMS = txtReviewOfSystem.Text.Trim();
                objPTEnc.HPE_INVESTIGATIONS = txtInvestigations.Text.Trim();
                objPTEnc.HPE_PRINCIPAL_DIAGNOSIS = txtPrincipalDiagnosis.Text.Trim();
                objPTEnc.HPE_SECONDARY_DIAGNOSIS = txtSecondaryDiagnosis.Text.Trim();
                objPTEnc.HPE_TREATMENT_PLAN = txtTreatmentPlan.Text.Trim();
                objPTEnc.HPE_PROCEDURE = txtProcedure.Text.Trim();
                objPTEnc.HPE_TREATMENT = txtTreatment.Text.Trim();
                objPTEnc.HPE_FOLLOWUPNOTES = txtFollowUpNotes.Text.Trim();
                objPTEnc.UserId = Convert.ToString(Session["User_Code"]);

                string HPE_ID;
                HPE_ID = objPTEnc.AddPTEncounter(objPTEnc);
                ViewState["HPE_ID"] = HPE_ID;


                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }



        void BindAssessment()
        {
            PTAssessmentBAL obj = new PTAssessmentBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPA_HHV_ID   = '" + Convert.ToString(Session["HomeCareId"]) + "' ";
            DS = obj.GetPTAssessment(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["HPA_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HPA_ID"]);
                txtHC_ASS_Health_PresPastHist.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPASTHIST"]);
                txtPresPastProblem.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PROB"]);
                txtPresPastSurgeries.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PRESPAS_PREVPROB"]);
                txtAlergiesMedication.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_MEDIC"]);
                txtAlergiesFood.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_FOOD"]);
                txtAlergiesOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_ALLERGIES_OTHER"]);
                txtHC_ASS_Health_MedicHist_Medicine.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_MEDICINE"]);
                txtHC_ASS_Health_MedicHist_Sleep.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_SLEEP"]);
                txtHC_ASS_Health_MedicHist_PschoyAss.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_MEDICHIST_PSCHOYASS"]);
                radPainAssYes.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HPA_HEALTH_PAIN_EXPRESSES"]);
                txtHC_ASS_Phy_Gast_BowelSounds.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_BOWELSOUNDS"]);
                txtHC_ASS_Phy_Gast_Elimination.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_ELIMINATION"]);
                txtHC_ASS_Phy_Gast_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GAST_GENERAL"]);
                txtHC_ASS_Phy_Repro_Male.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_MALE"]);
                txtHC_ASS_Phy_Repro_Female.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_FEMALE"]);
                txtHC_ASS_Phy_Repro_Breasts.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_REPRO_BREASTS"]);
                txtHC_ASS_Phy_Genit_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_GENIT_GENERAL"]);
                txtHC_ASS_Phy_Skin_Color.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_COLOR"]);
                txtHC_ASS_Phy_Skin_Temperature.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_TEMPERATURE"]);
                txtHC_ASS_Phy_Skin_Lesions.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_SKIN_LESIONS"]);
                txtHC_ASS_Phy_Neuro_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_GENERAL"]);
                txtHC_ASS_Phy_Neuro_Conscio.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_CONSCIO"]);
                txtHC_ASS_Phy_Neuro_Oriented.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_ORIENTED"]);
                txtHC_ASS_Phy_Neuro_TimeResp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NEURO_TIMERESP"]);
                txtHC_ASS_Phy_Cardio_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_GENERAL"]);
                txtHC_ASS_Phy_Cardio_Pulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PULSE"]);
                txtHC_ASS_Phy_Cardio_PedalPulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_PEDALPULSE"]);
                txtHC_ASS_Phy_Cardio_Edema.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_EDEMA"]);
                txtHC_ASS_Phy_Cardio_NailBeds.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_NAILBEDS"]);
                txtHC_ASS_Phy_Cardio_Capillary.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_CARDIO_CAPILLARY"]);
                txtHC_ASS_Phy_Ent_Eyes.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EYES"]);
                txtHC_ASS_Phy_Ent_Ears.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_EARS"]);
                txtHC_ASS_Phy_Ent_Nose.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_NOSE"]);
                txtHC_ASS_Phy_Ent_Throat.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_ENT_THROAT"]);
                txtHC_ASS_Phy_Resp_Chest.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_CHEST"]);
                txtHC_ASS_Phy_Resp_BreathPatt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHPATT"]);
                txtHC_ASS_Phy_Resp_BreathSound.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_RESP_BREATHSOUND"]);
                txtHC_ASS_Phy_Resp_BreathCough.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RESP_BREATHCOUGH"]);
                txtHC_ASS_Phy_Nutri_Diet.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIET"]);
                txtHC_ASS_Phy_Nutri_Appetite.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_APPETITE"]);
                txtHC_ASS_Phy_Nutri_NutriSupport.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_NUTRISUPPORT"]);
                txtHC_ASS_Phy_Nutri_FeedingDif.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_FEEDINGDIF"]);
                txtHC_ASS_Phy_Nutri_WeightStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_WEIGHTSTATUS"]);
                txtHC_ASS_Phy_Nutri_Diag.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_PHY_NUTRI_DIAG"]);
                txtHC_ASS_Risk_SkinRisk_Sensory.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_SENSORY"]);
                txtHC_ASS_Risk_SkinRisk_Moisture.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOISTURE"]);
                txtHC_ASS_Risk_SkinRisk_Activity.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_ACTIVITY"]);
                txtHC_ASS_Risk_SkinRisk_Mobility.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_MOBILITY"]);
                txtHC_ASS_Risk_SkinRisk_Nutrition.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_NUTRITION"]);
                txtHC_ASS_Risk_SkinRisk_Friction.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SKINRISK_FRICTION"]);
                txtHC_ASS_Risk_Socio_Living.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SOCIO_LIVING"]);
                txtHC_ASS_Risk_Safety_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_SAFETY_GENERAL"]);
                txtHC_ASS_Risk_Fun_SelfCaring.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_SELFCARING"]);
                txtHC_ASS_Risk_Fun_Musculos.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_MUSCULOS"]);
                txtHC_ASS_Risk_Fun_Equipment.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_FUN_EQUIPMENT"]);
                txtHC_ASS_Risk_Edu_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPA_RISK_EDU_GENERAL"]);

            }


        }

        void ClearAssessment()
        {
            ViewState["HPA_ID"] = "0";
            txtHC_ASS_Health_PresPastHist.Text = "";
            txtPresPastProblem.Text = "";
            txtPresPastSurgeries.Text = "";
            txtAlergiesMedication.Text = "";
            txtAlergiesFood.Text = "";
            txtAlergiesOther.Text = "";
            txtHC_ASS_Health_MedicHist_Medicine.Text = "";
            txtHC_ASS_Health_MedicHist_Sleep.Text = "";
            txtHC_ASS_Health_MedicHist_PschoyAss.Text = "";
            radPainAssYes.SelectedIndex = -1;
            txtHC_ASS_Phy_Gast_BowelSounds.Text = "";
            txtHC_ASS_Phy_Gast_Elimination.Text = "";
            txtHC_ASS_Phy_Gast_General.Text = "";
            txtHC_ASS_Phy_Repro_Male.Text = "";
            txtHC_ASS_Phy_Repro_Female.Text = "";
            txtHC_ASS_Phy_Repro_Breasts.Text = "";
            txtHC_ASS_Phy_Genit_General.Text = "";
            txtHC_ASS_Phy_Skin_Color.Text = "";
            txtHC_ASS_Phy_Skin_Temperature.Text = "";
            txtHC_ASS_Phy_Skin_Lesions.Text = "";
            txtHC_ASS_Phy_Neuro_General.Text = "";
            txtHC_ASS_Phy_Neuro_Conscio.Text = "";
            txtHC_ASS_Phy_Neuro_Oriented.Text = "";
            txtHC_ASS_Phy_Neuro_TimeResp.Text = "";
            txtHC_ASS_Phy_Cardio_General.Text = "";
            txtHC_ASS_Phy_Cardio_Pulse.Text = "";
            txtHC_ASS_Phy_Cardio_PedalPulse.Text = "";
            txtHC_ASS_Phy_Cardio_Edema.Text = "";
            txtHC_ASS_Phy_Cardio_NailBeds.Text = "";
            txtHC_ASS_Phy_Cardio_Capillary.Text = "";
            txtHC_ASS_Phy_Ent_Eyes.Text = "";
            txtHC_ASS_Phy_Ent_Ears.Text = "";
            txtHC_ASS_Phy_Ent_Nose.Text = "";
            txtHC_ASS_Phy_Ent_Throat.Text = "";
            txtHC_ASS_Phy_Resp_Chest.Text = "";
            txtHC_ASS_Phy_Resp_BreathPatt.Text = "";
            txtHC_ASS_Phy_Resp_BreathSound.Text = "";
            txtHC_ASS_Phy_Resp_BreathCough.Text = "";
            txtHC_ASS_Phy_Nutri_Diet.Text = "";
            txtHC_ASS_Phy_Nutri_Appetite.Text = "";
            txtHC_ASS_Phy_Nutri_FeedingDif.Text = "";
            txtHC_ASS_Phy_Nutri_WeightStatus.Text = "";
            txtHC_ASS_Phy_Nutri_Diag.Text = "";
            txtHC_ASS_Risk_SkinRisk_Sensory.Text = "";
            txtHC_ASS_Risk_SkinRisk_Moisture.Text = "";
            txtHC_ASS_Risk_SkinRisk_Activity.Text = "";
            txtHC_ASS_Risk_SkinRisk_Mobility.Text = "";
            txtHC_ASS_Risk_SkinRisk_Nutrition.Text = "";
            txtHC_ASS_Risk_SkinRisk_Friction.Text = "";
            txtHC_ASS_Risk_Socio_Living.Text = "";
            txtHC_ASS_Risk_Safety_General.Text = "";
            txtHC_ASS_Risk_Fun_SelfCaring.Text = "";
            txtHC_ASS_Risk_Fun_Musculos.Text = "";
            txtHC_ASS_Risk_Fun_Equipment.Text = "";
            txtHC_ASS_Risk_Edu_General.Text = "";
        }


        void SaveAssessment()
        {

            PTAssessmentBAL obj = new PTAssessmentBAL();
            obj.HPA_ID = Convert.ToString(ViewState["HPA_ID"]);
            obj.HPA_HHV_ID = Convert.ToString(Session["HomeCareId"]);
            obj.HPA_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            obj.HPA_PT_ID = hidPTID.Value;
            obj.HPA_HEALTH_PRESPASTHIST = txtHC_ASS_Health_PresPastHist.Text.Trim();
            obj.HPA_HEALTH_PRESPAS_PROB = txtPresPastProblem.Text.Trim();
            obj.HPA_HEALTH_PRESPAS_PREVPROB = txtPresPastSurgeries.Text.Trim();
            obj.HPA_HEALTH_ALLERGIES_MEDIC = txtAlergiesMedication.Text.Trim();
            obj.HPA_HEALTH_ALLERGIES_FOOD = txtAlergiesFood.Text.Trim();
            obj.HPA_HEALTH_ALLERGIES_OTHER = txtAlergiesOther.Text.Trim();
            obj.HPA_HEALTH_MEDICHIST_MEDICINE = txtHC_ASS_Health_MedicHist_Medicine.Text.Trim();
            obj.HPA_HEALTH_MEDICHIST_SLEEP = txtHC_ASS_Health_MedicHist_Sleep.Text.Trim();
            obj.HPA_HEALTH_MEDICHIST_PSCHOYASS = txtHC_ASS_Health_MedicHist_PschoyAss.Text.Trim();
            obj.HPA_HEALTH_PAIN_EXPRESSES = Convert.ToString(radPainAssYes.SelectedValue);
            obj.HPA_PHY_GAST_BOWELSOUNDS = txtHC_ASS_Phy_Gast_BowelSounds.Text.Trim();
            obj.HPA_PHY_GAST_ELIMINATION = txtHC_ASS_Phy_Gast_Elimination.Text.Trim();
            obj.HPA_PHY_GAST_GENERAL = txtHC_ASS_Phy_Gast_General.Text.Trim();
            obj.HPA_PHY_REPRO_MALE = txtHC_ASS_Phy_Repro_Male.Text.Trim();
            obj.HPA_PHY_REPRO_FEMALE = txtHC_ASS_Phy_Repro_Female.Text.Trim();
            obj.HPA_PHY_REPRO_BREASTS = txtHC_ASS_Phy_Repro_Breasts.Text.Trim();
            obj.HPA_PHY_GENIT_GENERAL = txtHC_ASS_Phy_Genit_General.Text.Trim();
            obj.HPA_PHY_SKIN_COLOR = txtHC_ASS_Phy_Skin_Color.Text.Trim();
            obj.HPA_PHY_SKIN_TEMPERATURE = txtHC_ASS_Phy_Skin_Temperature.Text.Trim();
            obj.HPA_PHY_SKIN_LESIONS = txtHC_ASS_Phy_Skin_Lesions.Text.Trim();
            obj.HPA_PHY_NEURO_GENERAL = txtHC_ASS_Phy_Neuro_General.Text.Trim();
            obj.HPA_PHY_NEURO_CONSCIO = txtHC_ASS_Phy_Neuro_Conscio.Text.Trim();
            obj.HPA_PHY_NEURO_ORIENTED = txtHC_ASS_Phy_Neuro_Oriented.Text.Trim();
            obj.HPA_PHY_NEURO_TIMERESP = txtHC_ASS_Phy_Neuro_TimeResp.Text.Trim();
            obj.HPA_PHY_CARDIO_GENERAL = txtHC_ASS_Phy_Cardio_General.Text.Trim();
            obj.HPA_PHY_CARDIO_PULSE = txtHC_ASS_Phy_Cardio_Pulse.Text.Trim();
            obj.HPA_PHY_CARDIO_PEDALPULSE = txtHC_ASS_Phy_Cardio_PedalPulse.Text.Trim();
            obj.HPA_PHY_CARDIO_EDEMA = txtHC_ASS_Phy_Cardio_Edema.Text.Trim();
            obj.HPA_PHY_CARDIO_NAILBEDS = txtHC_ASS_Phy_Cardio_NailBeds.Text.Trim();
            obj.HPA_PHY_CARDIO_CAPILLARY = txtHC_ASS_Phy_Cardio_Capillary.Text.Trim();
            obj.HPA_PHY_ENT_EYES = txtHC_ASS_Phy_Ent_Eyes.Text.Trim();
            obj.HPA_PHY_ENT_EARS = txtHC_ASS_Phy_Ent_Ears.Text.Trim();
            obj.HPA_PHY_ENT_NOSE = txtHC_ASS_Phy_Ent_Nose.Text.Trim();
            obj.HPA_PHY_ENT_THROAT = txtHC_ASS_Phy_Ent_Throat.Text.Trim();
            obj.HPA_PHY_RESP_CHEST = txtHC_ASS_Phy_Resp_Chest.Text.Trim();
            obj.HPA_PHY_RESP_BREATHPATT = txtHC_ASS_Phy_Resp_BreathPatt.Text.Trim();
            obj.HPA_PHY_RESP_BREATHSOUND = txtHC_ASS_Phy_Resp_BreathSound.Text.Trim();
            obj.HPA_RESP_BREATHCOUGH = txtHC_ASS_Phy_Resp_BreathCough.Text.Trim();
            obj.HPA_PHY_NUTRI_DIET = txtHC_ASS_Phy_Nutri_Diet.Text.Trim();
            obj.HPA_PHY_NUTRI_APPETITE = txtHC_ASS_Phy_Nutri_Appetite.Text.Trim();
            obj.HPA_PHY_NUTRI_NUTRISUPPORT = txtHC_ASS_Phy_Nutri_NutriSupport.Text.Trim();
            obj.HPA_PHY_NUTRI_FEEDINGDIF = txtHC_ASS_Phy_Nutri_FeedingDif.Text.Trim();
            obj.HPA_PHY_NUTRI_WEIGHTSTATUS = txtHC_ASS_Phy_Nutri_WeightStatus.Text.Trim();
            obj.HPA_PHY_NUTRI_DIAG = txtHC_ASS_Phy_Nutri_Diag.Text.Trim();
            obj.HPA_RISK_SKINRISK_SENSORY = txtHC_ASS_Risk_SkinRisk_Sensory.Text.Trim();
            obj.HPA_RISK_SKINRISK_MOISTURE = txtHC_ASS_Risk_SkinRisk_Moisture.Text.Trim();
            obj.HPA_RISK_SKINRISK_ACTIVITY = txtHC_ASS_Risk_SkinRisk_Activity.Text.Trim();
            obj.HPA_RISK_SKINRISK_MOBILITY = txtHC_ASS_Risk_SkinRisk_Mobility.Text.Trim();
            obj.HPA_RISK_SKINRISK_NUTRITION = txtHC_ASS_Risk_SkinRisk_Nutrition.Text.Trim();
            obj.HPA_RISK_SKINRISK_FRICTION = txtHC_ASS_Risk_SkinRisk_Friction.Text.Trim();
            obj.HPA_RISK_SOCIO_LIVING = txtHC_ASS_Risk_Socio_Living.Text.Trim();
            obj.HPA_RISK_SAFETY_GENERAL = txtHC_ASS_Risk_Safety_General.Text.Trim();
            obj.HPA_RISK_FUN_SELFCARING = txtHC_ASS_Risk_Fun_SelfCaring.Text.Trim();
            obj.HPA_RISK_FUN_MUSCULOS = txtHC_ASS_Risk_Fun_Musculos.Text.Trim();
            obj.HPA_RISK_FUN_EQUIPMENT = txtHC_ASS_Risk_Fun_Equipment.Text.Trim();
            obj.HPA_RISK_EDU_GENERAL = txtHC_ASS_Risk_Edu_General.Text.Trim();
            obj.UserId = Convert.ToString(Session["User_Code"]);

            obj.AddPTAssessment(obj);

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='HCPTENOC' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnDelete.Enabled = false;

            }

            if (strPermission == "3")
            {
                btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();

                try
                {
                    ViewState["HPE_ID"] = "0";


                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtVisitDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtArrivalDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    BindTime();
                    BindHomeCareReg();
                    BindEncounter();
                    BindAssessment();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveEncounter();
                SaveAssessment();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                PTEncounterBAL objPTEnc = new PTEncounterBAL();
                objPTEnc.HPE_ID = Convert.ToString(ViewState["HPE_ID"]);
                objPTEnc.UserId = Convert.ToString(Session["User_Code"]);
                objPTEnc.DeletePTEncounter(objPTEnc);

                Clear();

                lblStatus.Text = "Encounter Data Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }




        #endregion
    }
}