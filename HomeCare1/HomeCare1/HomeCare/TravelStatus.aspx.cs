﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class TravelStatus : System.Web.UI.Page
    {
        # region Variable Declaration
        connectiondb c = new connectiondb();

        clsHomeCare objHC = new clsHomeCare();

        public static string strSessionBranchId = "", strDrName = "";
        #endregion

        # region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTravelStatus()
        {
            TravelStatusBAL objTS = new TravelStatusBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHTS_HHV_ID='" + Convert.ToString(Session["HomeCareId"]) + "'";

            DS = objTS.GetTravelStatus(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTravelStatus.DataSource = DS;
                gvTravelStatus.DataBind();
            }
            else
            {
                gvTravelStatus.DataBind();
            }


        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }

                drpSTHour.Items.Insert(index, Convert.ToString(strHour));
                drpSTHour.Items[index].Value = Convert.ToString(strHour);

                drpFIHour.Items.Insert(index, Convert.ToString(strHour));
                drpFIHour.Items[index].Value = Convert.ToString(strHour);

                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;




            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");

            drpFIMin.Items.Insert(0, Convert.ToString("00"));
            drpFIMin.Items[0].Value = Convert.ToString("00");

            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);

                drpFIMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpFIMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void Clear()
        {
            txtVisitDate.Text = "";
            drpSTHour.SelectedIndex = 0;
            drpSTMin.SelectedIndex = 0;
            drpFIHour.SelectedIndex = 0;
            drpFIMin.SelectedIndex = 0;

            txtMileageUp.Text = "";
            txtMileageDown.Text = "";
            txtVehicleNo.Text = "";

            txtDriverId.Text = "";
            txtDriverName.Text = "";
            txtStaffID.Text = "";
            txtStaffName.Text = "";

            ViewState["HHTS_ID"] = "0";

            if (Convert.ToString(ViewState["TravelStatusIndex"]) != "")
            {
                gvTravelStatus.Rows[Convert.ToInt32(ViewState["TravelStatusIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["TravelStatusIndex"] = "";
        }

        string GetStaffName(string StaffId)
        {
            string strStaffName = "";
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID  ='" + StaffId + "'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strStaffName = ds.Tables[0].Rows[0]["FullName"].ToString();
            }

            return strStaffName;
        }
        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            strDrName = "";
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDriverId(string prefixText)
        {
            strDrName = "";
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HSFM_CATEGORY ='Others'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDriverName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HSFM_CATEGORY ='Others'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        #endregion


        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='HCTRASTA' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {
                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {
                    ViewState["HHTS_ID"] = "0";
                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    if (Convert.ToString(Request.QueryString["Message"]) == "Save")
                    {
                        lblStatus.Text = "Data Saved.";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }


                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtVisitDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTime();

                    string StaffId = "", Id = "";

                    StaffId = Convert.ToString(Request.QueryString["StaffId"]);
                    Id = Convert.ToString(Session["HomeCareId"]);

                    if (StaffId != " " && StaffId != null)
                    {
                        txtStaffID.Text = Convert.ToString(StaffId);
                        // PatientDataBind();
                    }

                    if (Id != " " && Id != null)
                    {
                        BindTravelStatus();
                    }
                    else
                    {


                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      TravelStatus.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void SelectTravelStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                if (Convert.ToString(ViewState["TravelStatusIndex"]) != "")
                {
                    gvTravelStatus.Rows[Convert.ToInt32(ViewState["TravelStatusIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["TravelStatusIndex"] = gvScanCard.RowIndex;


                gvTravelStatus.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


                Label lblTSId, lblDate, lblStartTime, lblReachTime, lblUp, lblDown, lblVehicleNo, lblStaffId, lblDriverId;

                lblTSId = (Label)gvScanCard.Cells[0].FindControl("lblTSId");
                lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");
                lblStartTime = (Label)gvScanCard.Cells[0].FindControl("lblStartTime");
                lblReachTime = (Label)gvScanCard.Cells[0].FindControl("lblReachTime");
                lblUp = (Label)gvScanCard.Cells[0].FindControl("lblUp");
                lblDown = (Label)gvScanCard.Cells[0].FindControl("lblDown");
                lblVehicleNo = (Label)gvScanCard.Cells[0].FindControl("lblVehicleNo");
                lblStaffId = (Label)gvScanCard.Cells[0].FindControl("lblStaffId");
                lblDriverId = (Label)gvScanCard.Cells[0].FindControl("lblDriverId");


                ViewState["HHTS_ID"] = lblTSId.Text;
                txtVisitDate.Text = lblDate.Text;

                string strSTHour = lblStartTime.Text;
                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 0)
                {
                    drpSTHour.SelectedValue = arrSTHour[0];
                    drpSTMin.SelectedValue = arrSTHour[1];
                }


                string strFIHour = lblReachTime.Text;
                string[] arrFIHour = strFIHour.Split(':');
                if (arrFIHour.Length > 0)
                {
                    drpFIHour.SelectedValue = arrFIHour[0];
                    drpFIMin.SelectedValue = arrFIHour[1];
                }

                txtMileageUp.Text = lblUp.Text;
                txtMileageDown.Text = lblDown.Text;
                txtVehicleNo.Text = lblVehicleNo.Text;
                txtStaffID.Text = lblStaffId.Text;
                txtDriverId.Text = lblDriverId.Text;
                if (txtStaffID.Text.Trim() != "")
                    txtStaffName.Text = GetStaffName(txtStaffID.Text.Trim());

                if (txtDriverId.Text.Trim() != "")
                    txtDriverName.Text = GetStaffName(txtDriverId.Text.Trim());

              
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TravelStatus.SelectTravelStatus_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteTravelStatus_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblTSId;
                    lblTSId = (Label)gvScanCard.Cells[0].FindControl("lblTSId");

                    TravelStatusBAL objTS = new TravelStatusBAL();
                    objTS.Id = lblTSId.Text;
                    objTS.DeleteTravelStatus();

                    Clear();
                    lblStatus.Text = "Data Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    BindTravelStatus();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TravelStatus.DeleteTravelStatus_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvTravelStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                    lblSerial.Text = ((gvTravelStatus.PageIndex * gvTravelStatus.PageSize) + e.Row.RowIndex + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Notification.gvNotification_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                TravelStatusBAL objTS = new TravelStatusBAL();
                objTS.HomeCareId = Convert.ToString(Session["HomeCareId"]);
                objTS.Id = Convert.ToString(ViewState["HHTS_ID"]);
                objTS.HHTS_DATE = txtVisitDate.Text.Trim();
                objTS.HHTS_START_TIME = drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue;
                objTS.HHTS_REACH_TIME = drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue;
                objTS.HHTS_MILEAGE_UP = txtMileageUp.Text.Trim();
                objTS.HHTS_MILEAGE_DOWN = txtMileageDown.Text.Trim();
                objTS.HHTS_VEHICLE_NO = txtVehicleNo.Text.Trim();
                objTS.HHTS_STAFF_ID = txtStaffID.Text.Trim();
                objTS.HHTS_STAFF_NAME = txtStaffName.Text.Trim();
                objTS.HHTS_DRIVER_ID = txtDriverId.Text.Trim();
                objTS.HHTS_DRIVER_NAME = txtDriverName.Text.Trim();
                objTS.UserId = Convert.ToString(Session["User_Code"]);
                objTS.AddTravelStatus();

                Clear();
                BindTravelStatus();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


                Response.Redirect("TravelStatus.aspx?Message=Save");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TravelStatus.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TravelStatus.btnClear_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        #endregion
    }
}