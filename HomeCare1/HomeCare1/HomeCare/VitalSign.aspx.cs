﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class VitalSign : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }



                drpVitalHour.Items.Insert(index, Convert.ToString(strHour));
                drpVitalHour.Items[index].Value = Convert.ToString(strHour);



                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;






            drpVitalMin.Items.Insert(0, Convert.ToString("00"));
            drpVitalMin.Items[0].Value = Convert.ToString("00");




            for (int j = AppointmentInterval; j < 60; j++)
            {


                drpVitalMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpVitalMin.Items[index].Value = Convert.ToString(j - intCount);



                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void BindVital()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPV_HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "'";

            PTVitalSignsBAL objPTVital = new PTVitalSignsBAL();
            ds = objPTVital.GetPTVitalSigns(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvVital.DataSource = ds;
                gvVital.DataBind();

            }
            else
            {
                gvVital.DataBind();
            }
        }


        void ClearVital()
        {
            txtVitalDate.Text = "";
            drpVitalHour.SelectedIndex = 0;
            drpVitalMin.SelectedIndex = 0;
            txtWeight.Text = "";
            txtHeight.Text = "";
            txtBMI.Text = "";
            txtTemperatureF.Text = "";
            txtTemperatureC.Text = "";
            txtPulse.Text = "";
            txtRespiration.Text = "";
            txtBPSystolic.Text = "";
            txtBPDiastolic.Text = "";
            txtSPO2.Text = "";

            ViewState["HPV_ID"] = "0";

            if (Convert.ToString(ViewState["EncSelectIndex"]) != "")
            {
                gvVital.Rows[Convert.ToInt32(ViewState["EncSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["EncSelectIndex"] = "";
        }

        void SetPermission()
        {
            string Criteria = " 1=1 AND HRT_SCREEN_ID='HCVITAL' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSaveVital.Enabled = false;
                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {
                btnSaveVital.Enabled = false;

            }


            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {
                    ViewState["HPV_ID"] = "0";

                    if (Convert.ToString(Request.QueryString["Message"]) == "Save")
                    {
                        lblStatus.Text = "Data Saved.";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtVitalDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTime();
                    BindVital();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearVital();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSaveVital_Click(object sender, EventArgs e)
        {
            try
            {

                PTVitalSignsBAL objPTVital = new PTVitalSignsBAL();
                objPTVital.HPV_ID = Convert.ToString(ViewState["HPV_ID"]);
                objPTVital.HPV_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPTVital.HPV_HHV_ID = Convert.ToString(Session["HomeCareId"]);
                objPTVital.HPV_PT_ID = hidPTID.Value;

                if (txtVitalDate.Text.Trim() != "")
                {
                    objPTVital.HPV_DATE = txtVitalDate.Text.Trim() + " " + drpVitalHour.SelectedValue + ":" + drpVitalMin.SelectedValue + ":00";
                }
                else
                {
                    objPTVital.HPV_DATE = "";
                }

                objPTVital.HPV_WEIGHT = txtWeight.Text.Trim();
                objPTVital.HPV_HEIGHT = txtHeight.Text.Trim();
                objPTVital.HPV_BMI = txtBMI.Text.Trim();
                objPTVital.HPV_TEMPERATURE_F = txtTemperatureF.Text.Trim();
                objPTVital.HPV_TEMPERATURE_C = txtTemperatureC.Text.Trim();
                objPTVital.HPV_PULSE = txtPulse.Text.Trim();
                objPTVital.HPV_RESPIRATION = txtRespiration.Text.Trim();
                objPTVital.HPV_BP_SYSTOLIC = txtBPSystolic.Text.Trim();
                objPTVital.HPV_BP_DIASTOLIC = txtBPDiastolic.Text.Trim();
                objPTVital.HPV_SPO2 = txtSPO2.Text.Trim();
                objPTVital.UserId = Convert.ToString(Session["User_Code"]);

                objPTVital.AddPTVitalSigns();

                BindVital();
                ClearVital();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                Response.Redirect("VitalSign.aspx?Message=Save");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear();
                if (Convert.ToString(ViewState["EncSelectIndex"]) != "")
                {
                    gvVital.Rows[Convert.ToInt32(ViewState["EncSelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["EncSelectIndex"] = gvScanCard.RowIndex;


                gvVital.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


                Label lblHPVId, lblVitalDate, lblVitalTime, lblWeight, lblHeight, lblBMI, lblTemperatureF, lblTemperatureC, lblPulse, lblRespiration, lblSystolic, lblDiastolic, lblSPO2;

                lblHPVId = (Label)gvScanCard.Cells[0].FindControl("lblHPVId");
                lblVitalDate = (Label)gvScanCard.Cells[0].FindControl("lblVitalDate");
                lblVitalTime = (Label)gvScanCard.Cells[0].FindControl("lblVitalTime");
                lblWeight = (Label)gvScanCard.Cells[0].FindControl("lblWeight");
                lblHeight = (Label)gvScanCard.Cells[0].FindControl("lblHeight");
                lblBMI = (Label)gvScanCard.Cells[0].FindControl("lblBMI");
                lblTemperatureF = (Label)gvScanCard.Cells[0].FindControl("lblTemperatureF");
                lblTemperatureC = (Label)gvScanCard.Cells[0].FindControl("lblTemperatureC");
                lblPulse = (Label)gvScanCard.Cells[0].FindControl("lblPulse");
                lblRespiration = (Label)gvScanCard.Cells[0].FindControl("lblRespiration");
                lblSystolic = (Label)gvScanCard.Cells[0].FindControl("lblSystolic");
                lblDiastolic = (Label)gvScanCard.Cells[0].FindControl("lblDiastolic");
                lblSPO2 = (Label)gvScanCard.Cells[0].FindControl("lblSPO2");


                ViewState["HPV_ID"] = lblHPVId.Text;
                txtVitalDate.Text = lblVitalDate.Text;

                string strSTHour = lblVitalTime.Text;

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpVitalHour.SelectedValue = arrSTHour[0];
                    drpVitalMin.SelectedValue = arrSTHour[1];

                }



                txtWeight.Text = lblWeight.Text;
                txtHeight.Text = lblHeight.Text;
                txtBMI.Text = lblBMI.Text;
                txtTemperatureF.Text = lblTemperatureF.Text;
                txtTemperatureC.Text = lblTemperatureC.Text;
                txtPulse.Text = lblPulse.Text;
                txtRespiration.Text = lblRespiration.Text;
                txtBPSystolic.Text = lblSystolic.Text;
                txtBPDiastolic.Text = lblDiastolic.Text;
                txtSPO2.Text = lblSPO2.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteVitial_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblHPVId;
                    lblHPVId = (Label)gvScanCard.Cells[0].FindControl("lblHPVId");


                    PTVitalSignsBAL objPTVital = new PTVitalSignsBAL();
                    objPTVital.HPV_ID = Convert.ToString(lblHPVId.Text);
                    objPTVital.UserId = Convert.ToString(Session["User_Code"]);
                    objPTVital.DeletePTVitalSigns();

                    lblStatus.Text = "VitalSign Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    ClearVital(); ;
                    BindVital();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.DeleteSchServ_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvVital_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvVital.PageIndex * gvVital.PageSize) + e.Row.RowIndex + 1).ToString();
            }
        }
    }
}