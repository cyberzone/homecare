﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class CoordinatorStaffAssign : System.Web.UI.Page
    {
        # region Variable Declaration
        connectiondb c = new connectiondb();
        dboperations dbo = new dboperations();

        public static string strSessionBranchId = "", strDrName = "";
        #endregion

        # region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }

                drpSTHour.Items.Insert(index, Convert.ToString(strHour));
                drpSTHour.Items[index].Value = Convert.ToString(strHour);

                drpFIHour.Items.Insert(index, Convert.ToString(strHour));
                drpFIHour.Items[index].Value = Convert.ToString(strHour);

                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;




            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");

            drpFIMin.Items.Insert(0, Convert.ToString("00"));
            drpFIMin.Items[0].Value = Convert.ToString("00");

            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);

                drpFIMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpFIMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["HPM_PT_FNAME"].ToString();
                txtPlace.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                txtMobile.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();

            }

        }

        void BindData()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HSAM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text.Trim() != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HSAM_DATE,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text.Trim() != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HSAM_DATE,101),101) <= '" + strForToDate + "'";
            }


            if (txtSrcStaffID.Text.Trim() != "")
            {
                Criteria += " AND HSAM_STAFF_ID='" + txtSrcStaffID.Text.Trim() + "'";

            }

            if (drpSrcStatus.SelectedIndex != 0)
            {
                Criteria += " AND HSAM_STATUS='" + drpSrcStatus.SelectedValue + "'";

            }


            clsHCStaffAssignMonitor objSAM = new clsHCStaffAssignMonitor();
            ds = objSAM.HCStaffAssignMonitorGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvStaffAssign.DataSource = ds;
                gvStaffAssign.DataBind();

            }
            else
            {
                gvStaffAssign.DataBind();
            }
        }

        void GetServiceMasterName(string ServID, out string ServName)
        {

            ServName = "";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND  HSM_HAAD_CODE  ='" + ServID + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            txtServName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServName = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
            }

        }

        void Clear()
        {
            txtFileNo.Text = "";
            txtFName.Text = "";
            txtPlace.Text = "";
            txtMobile.Text = "";
            txtVisitDate.Text = "";
            drpSTHour.SelectedIndex = 0;
            drpFIHour.SelectedIndex = 0;
            txtStaffID.Text = "";
            txtStaffName.Text = "";
            drpStatus.SelectedIndex = 0;
            txtComment.Text = "";
            txtServCode.Text = "";
            txtServName.Text = "";

            ViewState["HSAM_ID"] = "0";

            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                gvStaffAssign.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["SelectIndex"] = "";
        }

        void ClearSearch()
        {
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtSrcStaffID.Text = "";
            txtSrcStaffName.Text = "";
            drpSrcStatus.SelectedIndex = 0;

        }

        void PatientDtlsClear()
        {

            txtFName.Text = "";
            txtPlace.Text = "";
            txtMobile.Text = "";

        }

        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            strDrName = "";
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            //    Criteria += " AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            //   Criteria += " AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID  like '" + prefixText + "%' ";
            dboperations dbo = new dboperations();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_HAAD_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            dboperations dbo = new dboperations();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_HAAD_CODE"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='HCTRASTA' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {
                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }

        #region Events


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //    SetPermission();
                try
                {
                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    string PatientId = "";
                    ViewState["HSAM_ID"] = "0";

                    if (Convert.ToString(Request.QueryString["Message"]) == "Save")
                    {
                        lblStatus.Text = "Data Saved.";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }

                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);

                    if (PatientId != " " && PatientId != null)
                    {
                        txtFileNo.Text = Convert.ToString(PatientId);
                        PatientDataBind();
                    }


                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtVisitDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTime();

                    BindData();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      CoordinatorStaffAssign.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PatientDtlsClear();
                PatientDataBind();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                clsHCStaffAssignMonitor objSAM = new clsHCStaffAssignMonitor();

                objSAM.HSAM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objSAM.HSAM_ID = Convert.ToString(ViewState["HSAM_ID"]);
                objSAM.HSAM_PT_ID = txtFileNo.Text.Trim();
                objSAM.HSAM_PT_NAME = txtFName.Text.Trim();
                objSAM.HSAM_PLACE = txtPlace.Text.Trim();
                objSAM.HSAM_MOBILE = txtMobile.Text.Trim();
                objSAM.HSAM_DATE = txtVisitDate.Text.Trim();
                objSAM.HSAM_START_TIME = drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue;
                objSAM.HSAM_END_TIME = drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue;
                objSAM.HSAM_STAFF_ID = txtStaffID.Text.Trim();
                objSAM.HSAM_STAFF_NAME = txtStaffName.Text.Trim();
                objSAM.HSAM_STATUS = drpStatus.SelectedValue;
                objSAM.HSAM_ASSIGNER = Convert.ToString(Session["User_ID"]);
                objSAM.HSAM_COMMENT = txtComment.Text.Trim();
                objSAM.HSAM_PROCEDURE_CODE = txtServCode.Text.Trim();
                objSAM.HSAM_PROCEDURE_DESC = txtServName.Text.Trim();


                objSAM.UserID = Convert.ToString(Session["User_ID"]);
                objSAM.HCStaffAssignMonitorAdd();

                Clear();
                ClearSearch();
                BindData();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


                Response.Redirect("CoordinatorStaffAssign.aspx?Message=Save");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CoordinatorStaffAssign.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            try
            {
                Clear();
                ClearSearch();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CoordinatorStaffAssign.btnClear_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvStaffAssign.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;

                gvStaffAssign.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


                Label lblHSAM_ID = (Label)gvScanCard.Cells[0].FindControl("lblHSAM_ID");
                Label lblPTId = (Label)gvScanCard.Cells[0].FindControl("lblPTId");
                Label lblPTName = (Label)gvScanCard.Cells[0].FindControl("lblPTName");
                Label lblPlace = (Label)gvScanCard.Cells[0].FindControl("lblPlace");
                Label lblMobile = (Label)gvScanCard.Cells[0].FindControl("lblMobile");
                Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");
                Label lblStartTime = (Label)gvScanCard.Cells[0].FindControl("lblStartTime");
                Label lblReachTime = (Label)gvScanCard.Cells[0].FindControl("lblReachTime");
                Label lblStaffID = (Label)gvScanCard.Cells[0].FindControl("lblStaffID");
                Label lblStaffName = (Label)gvScanCard.Cells[0].FindControl("lblStaffName");
                Label lblgvStatus = (Label)gvScanCard.Cells[0].FindControl("lblgvStatus");
                Label lblAssigner = (Label)gvScanCard.Cells[0].FindControl("lblAssigner");
                Label lblComment = (Label)gvScanCard.Cells[0].FindControl("lblComment");
                Label lblProdedureCode = (Label)gvScanCard.Cells[0].FindControl("lblProdedureCode");
                Label lblProdedureDesc = (Label)gvScanCard.Cells[0].FindControl("lblProdedureDesc");
                ViewState["HSAM_ID"] = lblHSAM_ID.Text;

                txtFileNo.Text = lblPTId.Text.Trim();
                txtFName.Text = lblPTName.Text.Trim();
                txtPlace.Text = lblPlace.Text;
                txtMobile.Text = lblMobile.Text;
                txtVisitDate.Text = lblDate.Text;

                string strSTHour = lblStartTime.Text;
                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 0)
                {
                    drpSTHour.SelectedValue = arrSTHour[0];
                    drpSTMin.SelectedValue = arrSTHour[1];
                }


                string strFIHour = lblReachTime.Text;
                string[] arrFIHour = strFIHour.Split(':');
                if (arrFIHour.Length > 0)
                {
                    drpFIHour.SelectedValue = arrFIHour[0];
                    drpFIMin.SelectedValue = arrFIHour[1];
                }

                txtStaffID.Text = lblStaffID.Text;
                txtStaffName.Text = lblStaffName.Text;


                drpStatus.SelectedValue = lblgvStatus.Text.Trim();
                txtComment.Text = lblComment.Text;
                txtServCode.Text = lblProdedureCode.Text;
                txtServName.Text = lblProdedureDesc.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCare_CoordinatorStaffAssign.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;



                    Label lblHSAM_ID = (Label)gvScanCard.Cells[0].FindControl("lblHSAM_ID");

                    clsHCStaffAssignMonitor objSAM = new clsHCStaffAssignMonitor();

                    objSAM.HSAM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objSAM.HSAM_ID = lblHSAM_ID.Text.Trim();// Convert.ToString(ViewState["HSAM_ID"]);
                    objSAM.UserID = Convert.ToString(Session["User_ID"]);
                    objSAM.HCStaffAssignMonitorDelete();

                    ViewState["SelectIndex"] = "";
                    Clear();
                    BindData();
                    lblStatus.Text = "Data Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCare_CoordinatorStaffAssign.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            string ServID = "", ServName = "", strServType = "";
            string HaadCode = "";

            decimal decServPrice = 0;

            GetServiceMasterName(txtServCode.Text.Trim(), out  ServName);

            txtServName.Text = ServName;


        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {

                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCare_CoordinatorStaffAssign.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
        #endregion
    }
}