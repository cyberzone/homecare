﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.HomeCare
{
    public partial class HomeCareReports : System.Web.UI.Page
    {
        dboperations dbo = new dboperations();
        static string strSessionBranchId;

        void TextFileWriting(string strContent)
        {
            try
            {
                string strName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strName) == true)
                {
                    oWrite = File.AppendText(strName);
                }
                else
                {
                    oWrite = File.CreateText(strName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }




        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            if (!IsPostBack)
            {
                try
                {
                    string strPageHeader = "";
                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);
                    strPageHeader = Convert.ToString(Request.QueryString["HC_CreditBillStatement"]);

                    switch (strPageHeader)
                    {
                        case "CreditBillStatement":
                            {
                                lblReportHeader.Text = "Company Credit Bill Statement";
                                break;
                            }
                        default:
                            lblReportHeader.Text = "Report";
                            break;

                    }

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString());
                    TextFileWriting("HomeCare_HomeCareReports.aspx_Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }



        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";

            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_NAME"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["FullName"].ToString() + "~" + ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        protected void txtCompany_TextChanged(object sender, EventArgs e)
        {
            string strName = txtCompany.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[0];
                txtCompanyName.Text = arrName[1];

            }
        }

        protected void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            string strName = txtCompanyName.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtCompany.Text = arrName[1];
                txtCompanyName.Text = arrName[0];

            }
        }

        protected void txtDoctorID_TextChanged(object sender, EventArgs e)
        {

            string strDoctorID = txtDoctorID.Text;
            if (strDoctorID.Length > 1)
            {
                if (strDoctorID.Substring(strDoctorID.Length - 1, 1) == "~")
                {
                    strDoctorID = strDoctorID.Substring(0, strDoctorID.Length - 1);
                }
            }
            txtDoctorID.Text = strDoctorID;

            string strName = txtDoctorID.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[0];
                txtDoctorName.Text = arrName[1];

            }



        }

        protected void txtDoctorName_TextChanged(object sender, EventArgs e)
        {
            string strName = txtDoctorName.Text;
            string[] arrName = strName.Split('~');


            if (arrName.Length > 1)
            {
                txtDoctorID.Text = arrName[1];
                txtDoctorName.Text = arrName[0];

            }


        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string strForStartDate = "";
            string strForToDate = "";
            string dtFrom = txtFromDate.Text.Trim();
            string dtTo = txtToDate.Text.Trim();

            try
            {

                string strReport = "rptRemittance.rpt";



                string strStartDate = txtFromDate.Text;
                string[] arrDate = strStartDate.Split('/');


                if (arrDate.Length > 1)
                {
                    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                }




                string strTotDate = txtToDate.Text;
                string[] arrToDate = strTotDate.Split('/');

                if (arrToDate.Length > 1)
                {
                    strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
                }



                //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowRemittance('" + strReport + "','" + DBString + "','" + txtCompany.Text.Trim() + "','" + txtDoctorID.Text.Trim() + "','" + strForStartDate + "','" + strForToDate + "');", true);
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowRemittancePDF('" + txtCompany.Text.Trim() + "','" + txtDoctorID.Text.Trim() + "','" + strForStartDate + "','" + strForToDate + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString());
                TextFileWriting("HomeCare_HomeCareReports.aspx_btnReport_Click");
                TextFileWriting("FromDate" + dtFrom + " After Convert " + strForStartDate);
                TextFileWriting("ToDate" + dtTo + " After Convert " + strForToDate);
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}