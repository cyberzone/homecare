﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.HomeCare
{
    public partial class NurseCarePlan : System.Web.UI.Page
    {

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HNCP_HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "'";

            NursingCarePlanBAL objNCP = new NursingCarePlanBAL();
            ds = objNCP.GetNursingCarePlan(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvNursingCarePlan.DataSource = ds;
                gvNursingCarePlan.DataBind();

            }
            else
            {
                gvNursingCarePlan.DataBind();
            }
        }

        void BindHomeCareReg()
        {

            string Criteria = " 1=1 AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HHV_ID = '" + Convert.ToString(Session["HomeCareId"]) + "' ";

            DataSet ds = new DataSet();
            clsHomeCare objHC = new clsHomeCare();
            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                hidPTID.Value = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
            }

        }

        void Clear()
        {
            txtAssessment.Text = "";
            txtAnalyis.Text = "";
            txtPlanning.Text = "";
            txtEvaluation.Text = "";
            ViewState["HNCP_ID"] = "0";

            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                gvNursingCarePlan.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["SelectIndex"] = "";

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='HCNURCAPL' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {
                btnSave.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {
                    ViewState["HNCP_ID"] = "0";

                    if (Convert.ToString(Request.QueryString["Message"]) == "Save")
                    {
                        lblStatus.Text = "Data Saved.";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }

                    BindHomeCareReg();
                    BindData();
                    //BindEncounter();
                    //BindVital();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      NurseCarePlan.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAssessment.Text.Trim() == "" && txtAnalyis.Text.Trim() == "" && txtPlanning.Text.Trim() == "" && txtEvaluation.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                NursingCarePlanBAL objNCP = new NursingCarePlanBAL();

                objNCP.HNCP_ID = Convert.ToString(ViewState["HNCP_ID"]);
                objNCP.HNCP_HHV_ID = Convert.ToString(Session["HomeCareId"]);
                objNCP.HNCP_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objNCP.HNCP_PT_ID = hidPTID.Value;
                objNCP.HNCP_ASSESSMENT = txtAssessment.Text.Trim();
                objNCP.HNCP_ANALYSIS = txtAnalyis.Text.Trim();
                objNCP.HNCP_PLANNING = txtPlanning.Text.Trim();
                objNCP.HNCP_EVALUATION = txtEvaluation.Text.Trim();
                objNCP.UserId = Convert.ToString(Session["User_ID"]);
                objNCP.AddNursingCarePlan();

                BindData();
                Clear();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;
 
                Response.Redirect("NurseCarePlan.aspx?Message=Save");
            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NurseCarePlan.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvNursingCarePlan.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["SelectIndex"] = gvScanCard.RowIndex;


                gvNursingCarePlan.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


                Label lblHNCPId, lblAssessment, lblAnalysis, lblPlanning, lblEvaluation;

                lblHNCPId = (Label)gvScanCard.Cells[0].FindControl("lblHNCPId");
                lblAssessment = (Label)gvScanCard.Cells[0].FindControl("lblAssessment");
                lblAnalysis = (Label)gvScanCard.Cells[0].FindControl("lblAnalysis");
                lblPlanning = (Label)gvScanCard.Cells[0].FindControl("lblPlanning");
                lblEvaluation = (Label)gvScanCard.Cells[0].FindControl("lblEvaluation");

                txtAssessment.Text = lblAssessment.Text;
                txtAnalyis.Text = lblAnalysis.Text;
                txtPlanning.Text = lblPlanning.Text;
                txtEvaluation.Text = lblEvaluation.Text;


                ViewState["HNCP_ID"] = lblHNCPId.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NurseCarePlan.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblHNCPId;

                    lblHNCPId = (Label)gvScanCard.Cells[0].FindControl("lblHNCPId");

                    NursingCarePlanBAL objNCP = new NursingCarePlanBAL();
                    objNCP.HNCP_ID = lblHNCPId.Text;
                    objNCP.UserId = Convert.ToString(Session["User_ID"]);
                    objNCP.DeleteNursingCarePlan();

                    ViewState["SelectIndex"] = "";
                    Clear();
                    BindData();
                    lblStatus.Text = "Data Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NurseCarePlan.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void gvNursingCarePlan_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvNursingCarePlan.PageIndex * gvNursingCarePlan.PageSize) + e.Row.RowIndex + 1).ToString();
            }
        }
    }
}