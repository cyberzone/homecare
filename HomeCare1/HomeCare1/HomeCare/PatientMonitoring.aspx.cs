﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class PatientMonitoring : System.Web.UI.Page
    {
         # region Variable Declaration
    connectiondb c = new connectiondb();
    dboperations dbo = new dboperations();
    clsHomeCare objHC = new clsHomeCare();


    #endregion

    # region Methods

    void TextFileWriting(string strContent)
    {
        try
        {
            string strFileName = Server.MapPath("../HMSLog.txt");

            StreamWriter oWrite;

            if (File.Exists(strFileName) == true)
            {
                oWrite = File.AppendText(strFileName);
            }
            else
            {
                oWrite = File.CreateText(strFileName);
                //oWrite.WriteLine(strContent);
                oWrite.WriteLine();
            }

            oWrite.WriteLine(strContent);
            //  oWrite.WriteLine();
            oWrite.Close();
        }
        catch (Exception ex)
        {

        }

    }

    void HomeCareNotesBind(string strType)
    {

        DataSet ds = new DataSet();
        string Criteria = " 1=1 ";
        Criteria += " AND HHN_TYPE = '" + strType + "'";
        Criteria += " AND HHN_HHV_ID = '" + hidHHV_ID.Value + "'";


        PTMonitoringBAL objPT = new PTMonitoringBAL();
        ds = objPT.GetPTMonitoring(Criteria);

        if (strType == "PROGRESS")
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvProgNotes.DataSource = ds;
                gvProgNotes.DataBind();
            }
            else
            {
                gvProgNotes.DataBind();
            }

        }

        if (strType == "EMERGENCY")
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvEmerNotes.DataSource = ds;
                gvEmerNotes.DataBind();
            }
            else
            {
                gvEmerNotes.DataBind();
            }

        }

        if (strType == "ADDITIONAL")
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAddiNotes.DataSource = ds;
                gvAddiNotes.DataBind();
            }
            else
            {
                gvAddiNotes.DataBind();
            }

        }


    }

    void BindTime()
    {
        int AppointmentInterval = 15;
        int AppointmentStart = 0;
        int AppointmentEnd = 23;
        int index = 0;

        for (int i = AppointmentStart; i <= AppointmentEnd; i++)
        {
            string strHour = Convert.ToString(index);
            if (index <= 9)
            {
                strHour = "0" + Convert.ToString(index);
            }

            drpProgHour.Items.Insert(index, Convert.ToString(strHour));
            drpProgHour.Items[index].Value = Convert.ToString(strHour);

            drpEmerHour.Items.Insert(index, Convert.ToString(strHour));
            drpEmerHour.Items[index].Value = Convert.ToString(strHour);

            drpAddiHour.Items.Insert(index, Convert.ToString(strHour));
            drpAddiHour.Items[index].Value = Convert.ToString(strHour);



            index = index + 1;

        }
        index = 1;
        int intCount = 0;
        Int32 intMin = AppointmentInterval;




        drpProgMin.Items.Insert(0, Convert.ToString("00"));
        drpProgMin.Items[0].Value = Convert.ToString("00");

        drpEmerMin.Items.Insert(0, Convert.ToString("00"));
        drpEmerMin.Items[0].Value = Convert.ToString("00");

        drpAddiMin.Items.Insert(0, Convert.ToString("00"));
        drpAddiMin.Items[0].Value = Convert.ToString("00");




        for (int j = AppointmentInterval; j < 60; j++)
        {

            drpProgMin.Items.Insert(index, Convert.ToString(j - intCount));
            drpProgMin.Items[index].Value = Convert.ToString(j - intCount);

            drpEmerMin.Items.Insert(index, Convert.ToString(j - intCount));
            drpEmerMin.Items[index].Value = Convert.ToString(j - intCount);

            drpAddiMin.Items.Insert(index, Convert.ToString(j - intCount));
            drpAddiMin.Items[index].Value = Convert.ToString(j - intCount);



            j = j + AppointmentInterval;
            index = index + 1;
            intCount = intCount + 1;

        }

    }


    void ClearProgNotes()
    {
        txtProgressNotes.Text = "";
        txtProgressNotes.Enabled = true;
      //  txtProgDate.Text = "";
        drpProgHour.SelectedIndex = 0;
        drpProgMin.SelectedIndex = 0;
        if (Convert.ToString(ViewState["ProgNotesSelectIndex"]) != "")
        {
            gvProgNotes.Rows[Convert.ToInt32(ViewState["ProgNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
        }
        ViewState["ProgNotesSelectIndex"] = "";
        ViewState["HHN_ID"] = "0";

    }

    void ClearEmerNotes()
    {
        txtEmergencyNotes.Text = "";
        txtEmergencyNotes.Enabled = true;
      //  txtEmerDate.Text = "";
        drpEmerHour.SelectedIndex = 0;
        drpEmerMin.SelectedIndex = 0;

        if (Convert.ToString(ViewState["EmerNotesSelectIndex"]) != "")
        {
            gvEmerNotes.Rows[Convert.ToInt32(ViewState["EmerNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
        }
        ViewState["EmerNotesSelectIndex"] = "";
        ViewState["HHN_ID"] = "0";
    }


    void ClearAddiNotes()
    {
        txtAdditionalNotes.Text = "";
        txtAdditionalNotes.Enabled = true;
       // txtAddiDate.Text = "";
        drpAddiHour.SelectedIndex = 0;
        drpAddiMin.SelectedIndex = 0;
        if (Convert.ToString(ViewState["AddiNotesSelectIndex"]) != "")
        {
            gvAddiNotes.Rows[Convert.ToInt32(ViewState["AddiNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
        }
        ViewState["AddiNotesSelectIndex"] = "";
        ViewState["HHN_ID"] = "0";
    }

    void SetPermission()
    {
        //HCREG,HCPTENOC
        string Criteria = " 1=1 AND HRT_SCREEN_ID='HCPTMONI' ";
        Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

        dboperations dbo = new dboperations();
        DataSet ds = new DataSet();
        ds = dbo.RollTransGet(Criteria);

        string strPermission = "0";
        if (ds.Tables[0].Rows.Count > 0)
        {
            strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

        }

        if (strPermission == "1")
        {
            btnSaveProgNotes.Enabled = false;
            btnClearProgNotes.Enabled = false;
            btnSaveEmerNotes.Enabled = false;
            btnClearEmerNotes.Enabled = false;
            btnSaveAddiNotes.Enabled = false;
            btnClearAddiNotes.Enabled = false;

        }


        if (strPermission == "7")
        {
            btnSaveProgNotes.Enabled = false;
            btnSaveEmerNotes.Enabled = false;
            btnSaveAddiNotes.Enabled = false;

        }

        hidPermission.Value = strPermission;
        if (strPermission == "0")
        {
            Response.Redirect("../Home.aspx");
        }
    }


    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["HomeCareId"] == null) { Response.Redirect("WaitingList.aspx"); }
        lblStatus.Text = "";

        if (!IsPostBack)
        {

            if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                SetPermission();
            try
            {
                ViewState["HHN_ID"] = "0";
                // hidHHV_ID.Value = "HC00001";

                if (Convert.ToString(Request.QueryString["Message"]) == "Save")
                {
                    lblStatus.Text = "Data Saved.";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                string TabName = Convert.ToString(Request.QueryString["TabName"]) ;

                if (TabName == "ProgressNotes")
                {
                    TabContainer1.ActiveTab = TabProgressNotes;
                }
                else if (TabName == "EmergencyNotes")
                {
                    TabContainer1.ActiveTab = TabEmergencyNotes;
                }
                else if (TabName == "AdditionalNotes")
                {
                    TabContainer1.ActiveTab = TabAdditionalNotes;
                }

                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtProgDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtEmerDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtAddiDate.Text = strFromDate.ToString("dd/MM/yyyy");



                BindTime();

                string HHV_ID = "";

                HHV_ID = Convert.ToString(Session["HomeCareId"]);

                if (HHV_ID != " " && HHV_ID != null)
                {
                    hidHHV_ID.Value = Convert.ToString(HHV_ID);

                }

                HomeCareNotesBind("PROGRESS");
                HomeCareNotesBind("EMERGENCY");
                HomeCareNotesBind("ADDITIONAL");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }

    protected void SelectProgNotes_Click(object sender, EventArgs e)
    {
        try
        {
            //Clear();
            txtProgressNotes.Enabled = false;

            if (Convert.ToString(ViewState["ProgNotesSelectIndex"]) != "")
            {
                gvProgNotes.Rows[Convert.ToInt32(ViewState["ProgNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;


            gvProgNotes.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


            Label lblProgId, lblProgressNotes, lblProgDate, lblProgTime;

            lblProgId = (Label)gvScanCard.Cells[0].FindControl("lblProgId");
            lblProgressNotes = (Label)gvScanCard.Cells[0].FindControl("lblProgressNotes");
            lblProgDate = (Label)gvScanCard.Cells[0].FindControl("lblProgDate");
            lblProgTime = (Label)gvScanCard.Cells[0].FindControl("lblProgTime");

            ViewState["HHN_ID"] = lblProgId.Text;
            txtProgressNotes.Text = lblProgressNotes.Text;
            txtProgDate.Text = lblProgDate.Text;


            string strSTHour = Convert.ToString(lblProgTime.Text);

            string[] arrSTHour = strSTHour.Split(':');
            if (arrSTHour.Length > 1)
            {
                drpProgHour.SelectedValue = arrSTHour[0];
                drpProgMin.SelectedValue = arrSTHour[1];

            }

        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.SelectProgNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }

    }

    protected void btnSaveProgNotes_Click(object sender, EventArgs e)
    {
        try
        {
            if (hidHHV_ID.Value == "0")
            {
                goto FunEnd;
            }

            if (txtProgressNotes.Enabled == false)
            {
                goto FunEnd;
            }

            PTMonitoringBAL objPT = new PTMonitoringBAL();
            objPT.HHN_ID = Convert.ToString(ViewState["HHN_ID"]);
            objPT.HHN_HHV_ID = hidHHV_ID.Value;
            objPT.HHN_NOTES = txtProgressNotes.Text.Trim();
            objPT.HHN_TYPE = "PROGRESS";
            objPT.HHN_MONITORING_DATE = txtProgDate.Text.Trim() + " " + drpProgHour.SelectedValue + ":" + drpProgMin.SelectedValue + ":00";
            objPT.UserId = Convert.ToString(Session["User_Code"]);
            objPT.AddPTMonitoring();


            ClearProgNotes();
            HomeCareNotesBind("PROGRESS");
            lblStatus.Text = "Data Saved.";
            lblStatus.ForeColor = System.Drawing.Color.Green;

            Response.Redirect("PatientMonitoring.aspx?Message=Save&TabName=ProgressNotes");
        FunEnd: ;


        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.btnSaveProgNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }
    }

    protected void DeleteProgNotes_Click(object sender, EventArgs e)
    {
        try
        {
            string strPermission = hidPermission.Value;
            if (strPermission == "7" || strPermission == "9")
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblProgId;
                lblProgId = (Label)gvScanCard.Cells[0].FindControl("lblProgId");

                PTMonitoringBAL objPT = new PTMonitoringBAL();
                objPT.PTMonitoringInactive(lblProgId.Text);

                lblStatus.Text = "Progress Notes Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                ClearProgNotes();
                HomeCareNotesBind("PROGRESS");

            }
        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.DeleteProgNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }

    }

    protected void btnClearProgNotes_Click(object sender, EventArgs e)
    {
        ClearProgNotes();
    }

    protected void gvProgNotes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            Label lblSerial = (Label)e.Row.FindControl("lblSerial");
            lblSerial.Text = ((gvProgNotes.PageIndex * gvProgNotes.PageSize) + e.Row.RowIndex + 1).ToString();

            //BELOW CODING FOR STRIKE THROUGH OPTION
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            ImageButton DeleteProgNotes;
            DeleteProgNotes = (ImageButton)e.Row.FindControl("DeleteProgNotes");
            if (lblStatus.Text == "InActive")
            {
                e.Row.Cells[0].Font.Strikeout = true;
                e.Row.Cells[1].Font.Strikeout = true;
                e.Row.Cells[2].Font.Strikeout = true;
                e.Row.Cells[3].Font.Strikeout = true;
                DeleteProgNotes.Visible = false;
            }


        }
    }


    protected void SelectgvEmerNotes_Click(object sender, EventArgs e)
    {
        try
        {
            //Clear();
            txtEmergencyNotes.Enabled = false;
            if (Convert.ToString(ViewState["EmerNotesSelectIndex"]) != "")
            {
                gvEmerNotes.Rows[Convert.ToInt32(ViewState["EmerNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["EmerNotesSelectIndex"] = gvScanCard.RowIndex;


            gvEmerNotes.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


            Label lblEmerId, lblEmergencyNotes, lblEmerDate, lblEmerTime;

            lblEmerId = (Label)gvScanCard.Cells[0].FindControl("lblEmerId");
            lblEmergencyNotes = (Label)gvScanCard.Cells[0].FindControl("lblEmergencyNotes");
            lblEmerDate = (Label)gvScanCard.Cells[0].FindControl("lblEmerDate");
            lblEmerTime = (Label)gvScanCard.Cells[0].FindControl("lblEmerTime");

            ViewState["HHN_ID"] = lblEmerId.Text;
            txtEmergencyNotes.Text = lblEmergencyNotes.Text;
            txtEmerDate.Text = lblEmerDate.Text;

            string strSTHour = Convert.ToString(lblEmerTime.Text);

            string[] arrSTHour = strSTHour.Split(':');
            if (arrSTHour.Length > 1)
            {
                drpEmerHour.SelectedValue = arrSTHour[0];
                drpEmerMin.SelectedValue = arrSTHour[1];

            }


        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.SelectProgNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }

    }

    protected void btnSaveEmerNotes_Click(object sender, EventArgs e)
    {
        try
        {

            if (hidHHV_ID.Value == "0")
            {
                goto FunEnd;
            }

            if (txtEmergencyNotes.Enabled == false)
            {
                goto FunEnd;
            }

            PTMonitoringBAL objPT = new PTMonitoringBAL();
            objPT.HHN_ID = Convert.ToString(ViewState["HHN_ID"]);
            objPT.HHN_HHV_ID = hidHHV_ID.Value;
            objPT.HHN_NOTES = txtEmergencyNotes.Text.Trim();
            objPT.HHN_TYPE = "EMERGENCY";
            objPT.HHN_MONITORING_DATE = txtEmerDate.Text.Trim() + " " + drpEmerHour.SelectedValue + ":" + drpEmerMin.SelectedValue + ":00";
            objPT.UserId = Convert.ToString(Session["User_Code"]);
            objPT.AddPTMonitoring();


            ClearEmerNotes();
            HomeCareNotesBind("EMERGENCY");
            lblStatus.Text = "Data Saved.";
            lblStatus.ForeColor = System.Drawing.Color.Green;

            Response.Redirect("PatientMonitoring.aspx?Message=Save&TabName=EmergencyNotes");
        FunEnd: ;


        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.btnSaveEmerNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }
    }

    protected void DeleteEmerNotes_Click(object sender, EventArgs e)
    {
        try
        {
            string strPermission = hidPermission.Value;
            if (strPermission == "7" || strPermission == "9")
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblEmerId;
                lblEmerId = (Label)gvScanCard.Cells[0].FindControl("lblEmerId");

                PTMonitoringBAL objPT = new PTMonitoringBAL();
                objPT.PTMonitoringInactive(lblEmerId.Text);


                lblStatus.Text = "Emergency Notes Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                ClearEmerNotes();
                HomeCareNotesBind("EMERGENCY");
            }

        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.DeleteEmerNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }

    }

    protected void btnClearEmerNotes_Click(object sender, EventArgs e)
    {
        ClearEmerNotes();
    }

    protected void gvEmerNotes_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblSerial = (Label)e.Row.FindControl("lblSerial");
            lblSerial.Text = ((gvEmerNotes.PageIndex * gvEmerNotes.PageSize) + e.Row.RowIndex + 1).ToString();


            //BELOW CODING FOR STRIKE THROUGH OPTION
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            ImageButton DeleteProgNotes;
            DeleteProgNotes = (ImageButton)e.Row.FindControl("DeleteEmeriNotes");
            if (lblStatus.Text == "InActive")
            {
                e.Row.Cells[0].Font.Strikeout = true;
                e.Row.Cells[1].Font.Strikeout = true;
                e.Row.Cells[2].Font.Strikeout = true;
                e.Row.Cells[3].Font.Strikeout = true;
                DeleteProgNotes.Visible = false;
            }
        }
    }


    protected void SelectgvAddiNotes_Click(object sender, EventArgs e)
    {
        try
        {
            //Clear();
            txtAdditionalNotes.Enabled = false;
            if (Convert.ToString(ViewState["AddiNotesSelectIndex"]) != "")
            {
                gvAddiNotes.Rows[Convert.ToInt32(ViewState["AddiNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["AddiNotesSelectIndex"] = gvScanCard.RowIndex;


            gvAddiNotes.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


            Label lblAddiId, lblAdditionalNotes, lblAddiDate, lblAddiTime;

            lblAddiId = (Label)gvScanCard.Cells[0].FindControl("lblAddiId");
            lblAdditionalNotes = (Label)gvScanCard.Cells[0].FindControl("lblAdditionalNotes");
            lblAddiDate = (Label)gvScanCard.Cells[0].FindControl("lblAddiDate");
            lblAddiTime = (Label)gvScanCard.Cells[0].FindControl("lblAddiTime");

            ViewState["HHN_ID"] = lblAddiId.Text;
            txtAdditionalNotes.Text = lblAdditionalNotes.Text;
            txtAddiDate.Text = lblAddiDate.Text;


            string strSTHour = Convert.ToString(lblAddiTime.Text);

            string[] arrSTHour = strSTHour.Split(':');
            if (arrSTHour.Length > 1)
            {
                drpAddiHour.SelectedValue = arrSTHour[0];
                drpAddiMin.SelectedValue = arrSTHour[1];

            }


            Response.Redirect("PatientMonitoring.aspx?Message=Save&TabName=AdditionalNotes");

        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.SelectProgNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }

    }

    protected void btnSaveAddiNotes_Click(object sender, EventArgs e)
    {
        try
        {
            if (hidHHV_ID.Value == "0")
            {
                goto FunEnd;
            }

            if (txtAdditionalNotes.Enabled == false)
            {
                goto FunEnd;
            }


            PTMonitoringBAL objPT = new PTMonitoringBAL();
            objPT.HHN_ID = Convert.ToString(ViewState["HHN_ID"]);
            objPT.HHN_HHV_ID = hidHHV_ID.Value;
            objPT.HHN_NOTES = txtAdditionalNotes.Text.Trim();
            objPT.HHN_TYPE = "ADDITIONAL";
            objPT.HHN_MONITORING_DATE = txtAddiDate.Text.Trim() + " " + drpAddiHour.SelectedValue + ":" + drpAddiMin.SelectedValue + ":00";
            objPT.UserId = Convert.ToString(Session["User_Code"]);
            objPT.AddPTMonitoring();


            ClearAddiNotes();
            HomeCareNotesBind("ADDITIONAL");
            lblStatus.Text = "Data Saved.";
            lblStatus.ForeColor = System.Drawing.Color.Green;


        FunEnd: ;


        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.btnSaveAddiNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }
    }

    protected void DeleteAddiNotes_Click(object sender, EventArgs e)
    {
        try
        {
            string strPermission = hidPermission.Value;
            if (strPermission == "7" || strPermission == "9")
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblAddiId;
                lblAddiId = (Label)gvScanCard.Cells[0].FindControl("lblAddiId");

                PTMonitoringBAL objPT = new PTMonitoringBAL();
                objPT.PTMonitoringInactive(lblAddiId.Text);

                lblStatus.Text = "Additional Notes Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                ClearAddiNotes();
                HomeCareNotesBind("ADDITIONAL");

            }
        }
        catch (Exception ex)
        {
            TextFileWriting("-----------------------------------------------");
            TextFileWriting(System.DateTime.Now.ToString() + "      PatientMonitoring.DeleteAddiNotes_Click");
            TextFileWriting(ex.Message.ToString());
        }

    }

    protected void btnClearAddiNotes_Click(object sender, EventArgs e)
    {
        ClearAddiNotes();
    }

    protected void gvAddiNotes_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblSerial = (Label)e.Row.FindControl("lblSerial");
            lblSerial.Text = ((gvAddiNotes.PageIndex * gvAddiNotes.PageSize) + e.Row.RowIndex + 1).ToString();


            //BELOW CODING FOR STRIKE THROUGH OPTION
            Label lblStatus = (Label)e.Row.FindControl("lblStatus");
            ImageButton DeleteAddiNotes;
            DeleteAddiNotes = (ImageButton)e.Row.FindControl("DeleteAddiNotes");
            if (lblStatus.Text == "InActive")
            {
                e.Row.Cells[0].Font.Strikeout = true;
                e.Row.Cells[1].Font.Strikeout = true;
                e.Row.Cells[2].Font.Strikeout = true;
                e.Row.Cells[3].Font.Strikeout = true;
                DeleteAddiNotes.Visible = false;
            }
        }
    }



    protected void btnSaveTemplate_Click(object sender, EventArgs e)
    {
        Button btnSave = new Button();
        btnSave = (Button)sender;

        string strPath = "";
        if (btnSave.CommandName == "HC_ProgressNotes")
        {
            Session["Template"] = txtProgressNotes.Text.Trim();
            strPath = "TemplatesPopup.aspx?PageName=PatientMonitoring&CtrlName=HC_ProgressNotes";
        }
        if (btnSave.CommandName == "HC_EmergencyNotes")
        {
            Session["Template"] = txtEmergencyNotes.Text.Trim();
            strPath = "TemplatesPopup.aspx?PageName=PatientMonitoring&CtrlName=HC_EmergencyNotes";
        }

        if (btnSave.CommandName == "HC_AdditionalNotes")
        {
            Session["Template"] = txtAdditionalNotes.Text.Trim();
            strPath = "TemplatesPopup.aspx?PageName=PatientMonitoring&CtrlName=HC_AdditionalNotes";
        }



        ScriptManager.RegisterStartupScript(this, this.GetType(), "Templates", "window.open('" + strPath + "','_new','top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no');", true);


    }

    #endregion

    }
}