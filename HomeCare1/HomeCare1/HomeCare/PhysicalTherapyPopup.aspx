﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PhysicalTherapyPopup.aspx.cs" Inherits="HomeCare1.HomeCare.PhysicalTherapyPopup" %>

<%@ register namespace="AjaxControlToolkit" assembly="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function passvalue(HHPH_ID) {
            opener.location.href = "PhysicalTherapy.aspx?HHPH_ID=" + HHPH_ID;
            opener.focus();
            window.close();

        }
    </script>
</head>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <asp:hiddenfield id="hidPageName" runat="server" />
            <table width="930px;">
                <tr>
                    <td class="label" style="height: 30px;"> Id 
                    </td>
                    <td>
                        <asp:textbox id="txtHomeCareId" runat="server" bordercolor="#e3f7ef" borderstyle="Groove" width="100px"></asp:textbox>
                    </td>
                    <td class="label" style="height: 30px;">Name 
                    </td>
                    <td>
                        <asp:textbox id="txtSrcName" runat="server" bordercolor="#e3f7ef" borderstyle="Groove" width="200px"></asp:textbox>
                    </td>
                    <td class="label" style="height: 30px;">File No

                    </td>
                    <td>
                        <asp:textbox id="txtSrcFileNo" runat="server" bordercolor="#e3f7ef" borderstyle="Groove" width="100px" maxlength="10"></asp:textbox>
                    </td>
                    <td>

                        <asp:button id="btnFind" runat="server" cssclass="button1"
                            onclick="btnFind_Click" text=" Find " />

                    </td>
                </tr>
            </table>
     <div style="padding-top: 0px; width: 930px; height: 450px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:gridview id="gvPhysicalTherapy" runat="server" autogeneratecolumns="False" allowpaging="true"
                    enablemodelvalidation="True" width="99%" pagesize="200" onpageindexchanging="gvPhysicalTherapy_PageIndexChanging">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                     <asp:TemplateField HeaderText="Id">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHPH_ID")  %>')">
                                <asp:Label ID="Label3" CssClass="label" runat="server" Text='<%# Bind("HHPH_ID") %>' Width="50px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="File No">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHPH_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHPH_PT_ID") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHPH_ID")  %>')">
                                <asp:Label ID="Label2" CssClass="label" runat="server" Text='<%# Bind("FullName") %>' Width="300px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHPH_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHPH_CREATED_DATEDesc") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Injury Date">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHPH_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHPH_INJURY_DATEDesc") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surgery Date">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHPH_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHPH_SURGERY_DATEDesc") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />
            </asp:gridview>
            </div>
             <table width="930px;">
                <tr>
                    <td class="style2" align="left">
                        <asp:label id="Label9" runat="server" cssclass="label" Font-Bold="true"
                            text="Selected Records:"></asp:label>
                        &nbsp;
                                    <asp:label id="lblTotal" runat="server" cssclass="label" font-bold="true"></asp:label>
                    </td>
                </tr>
            </table>
    </div>
    </form>
</body>
</html>
