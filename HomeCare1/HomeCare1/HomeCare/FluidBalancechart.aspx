﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.master" AutoEventWireup="true" CodeBehind="FluidBalancechart.aspx.cs" Inherits="HomeCare1.HomeCare.FluidBalancechart" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

     <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
        function IntakeTotalCal() {

            var vOral = document.getElementById("<%=txtOral.ClientID%>").value;
            var vIV = document.getElementById("<%=txtIV.ClientID%>").value;

            var vOther = document.getElementById("<%=txtOralOther.ClientID%>").value;

            if (vOral == '') {

                vOral = 0;
            }

            if (vIV == '') {

                vIV = 0;
            }

            if (vOther == '') {

                vOther = 0;
            }

            var vIntakeTotal = parseFloat(vOral) + parseFloat(vIV) + parseFloat(vOther);

            document.getElementById("<%=txtTotalInput.ClientID%>").value = vIntakeTotal;
        }

        function OutPutTotalCal() {

            var vUrin = document.getElementById("<%=txtUrine.ClientID%>").value;
            var vVomit = document.getElementById("<%=txtVomit.ClientID%>").value;
            var vNGAspiration = document.getElementById("<%=txtNGAspiration.ClientID%>").value;
            var vOther = document.getElementById("<%=txtOther.ClientID%>").value;

            if (vUrin == '') {

                vUrin = 0;
            }

            if (vVomit == '') {

                vVomit = 0;
            }

            if (vNGAspiration == '') {

                vNGAspiration = 0;
            }


            if (vOther == '') {

                vOther = 0;
            }

            var vIntakeTotal = parseFloat(vUrin) + parseFloat(vVomit) + parseFloat(vNGAspiration) + parseFloat(vOther);

            document.getElementById("<%=txtTotalOutput.ClientID%>").value = vIntakeTotal;
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

      <input type="hidden" id="hidPermission" runat="server" value="9" />
     <table>
        <tr>
            <td class="PageHeader">Fluid Balance chart
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>
 
    <table style="width: 100%">
        <tr>
            <td class="lblCaption1"  style="width:100px;" >Date & Time
            </td>
           <td class="lblCaption1"  style="width:150px;" >
                <asp:UpdatePanel ID="updatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtAssDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                            Enabled="True" TargetControlID="txtAssDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpFluHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                        <asp:DropDownList ID="drpFluMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
             <td class="lblCaption1"  style="width:100px;" >
                   Result&nbsp;:&nbsp;&nbsp;&nbsp;
            </td>
            <td style="width:300px;" class="lblCaption1">
                 <asp:UpdatePanel runat="server" ID="updatePanel13">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpResult" runat="server" CssClass="lblCaption1" Width="150px" >
                            <asp:ListItem Text="--- Select ---" Value="" Selected="True" ></asp:ListItem>
                            <asp:ListItem Text="Positive" Value="Positive" ></asp:ListItem>
                            <asp:ListItem Text="Negative" Value="Negative"></asp:ListItem>
                        </asp:DropDownList>
                          </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <fieldset>
        <legend class="lblCaption1">INTAKE</legend>
        <table style="width: 100%">
            <tr>
                <td class="lblCaption1" style="width:250px;"  >Nature of food
                </td>
                <td class="lblCaption1" >Oral
                                </td>
                 <td class="lblCaption1">I.V.
                                </td>
                 <td class="lblCaption1"> Other Method
                                </td>
                  
                 <td class="lblCaption1"> Total
                                </td>
                 </tr>
            <tr>
                <td style="width:250px;" >
                    <asp:UpdatePanel ID="updatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtNatueIfFood" runat="server" Width="200px"  CssClass="label" ></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                
                <td>
                    <asp:UpdatePanel ID="updatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtOral" runat="server" Width="100px"  CssClass="label" onkeypress="return OnlyNumeric(event);"  onkeyup="IntakeTotalCal();"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
           
               
                <td>
                    <asp:UpdatePanel ID="updatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtIV" runat="server" Width="100px"  CssClass="label" onkeypress="return OnlyNumeric(event);" onkeyup="IntakeTotalCal();"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
               
                <td>
                    <asp:UpdatePanel ID="updatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtOralOther" runat="server" Width="100px"  CssClass="label" onkeypress="return OnlyNumeric(event);" onkeyup="IntakeTotalCal();"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                
                <td>
                    <asp:UpdatePanel ID="updatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTotalInput" runat="server" Width="100px"  CssClass="label" onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
        </table>
    </fieldset>

     <fieldset>
        <legend class="lblCaption1">OUTPUT</legend>
        <table style="width: 100%">
            <tr>
                <td class="lblCaption1"style="width:250px;"  >Urine
                </td>

                <td class="lblCaption1"  >Vomit
                </td>
                <td class="lblCaption1">N.G Aspiration
                </td>
                <td class="lblCaption1"> Other
                </td>
                 <td class="lblCaption1"> Total
                                </td>
            </tr>
            <tr>
                <td style="width:250px;" >
                    <asp:UpdatePanel ID="updatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtUrine" runat="server" Width="100px"  CssClass="label"  onkeypress="return OnlyNumeric(event);" onkeyup="OutPutTotalCal()"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td>
                    <asp:UpdatePanel ID="updatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtVomit" runat="server" Width="100px"  CssClass="label"  onkeypress="return OnlyNumeric(event);" onkeyup="OutPutTotalCal()"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
               
                <td>
                    <asp:UpdatePanel ID="updatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtNGAspiration" runat="server" Width="100px"  CssClass="label"  onkeypress="return OnlyNumeric(event);" onkeyup="OutPutTotalCal()"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                
                <td>
                    <asp:UpdatePanel ID="updatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtOther" runat="server" Width="100px"  CssClass="label" onkeypress="return OnlyNumeric(event);"  onkeyup="OutPutTotalCal()"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                  <td>
                    <asp:UpdatePanel ID="updatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTotalOutput" runat="server" Width="100px"  CssClass="label" onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
             
        </table>
    </fieldset>
    <table>
        <tr>
            <td  >
               
                <asp:UpdatePanel runat="server" ID="updatePanel11">
                    <ContentTemplate>
                        <asp:Button ID="btnAdd" runat="server" CssClass="orange" Width="100px" Text="Add" OnClick="btnAdd_Click" Style="width: 100px; border-radius: 5px 5px 5px 5px" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table> <br /> <br />
       <span class="lblCaption1">Note: 1.All measurements in ML </span>
    <br />
    <asp:UpdatePanel ID="updatePanel24" runat="server">
        <ContentTemplate>

            <asp:GridView ID="gvFluidBalance" runat="server" AutoGenerateColumns="False" 
                EnableModelValidation="True" Width="100%" GridLines="none">
                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
               
                <RowStyle CssClass="GridRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Date & Time">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkCode" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblFliidBalID" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_FLUBAL_ID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblgvAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_DATEDesc") %>'></asp:Label>
                                <asp:Label ID="lblgvAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_DATE_TIMEDesc") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nature of food" HeaderStyle-Width="200px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkNatureoffood" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblNatureoffood" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_NATURE_OF_FOOD") %>'></asp:Label>

                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Oral" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkOral" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvOral" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_ROUTE_ORAL") %>'></asp:Label>

                            </asp:LinkButton>
                        </ItemTemplate>
                         
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="I.V." HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkIV" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvIV" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_ROUTE_IV") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                          
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Other Method" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRouteOther" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvRouteOther" CssClass="GridRow"  runat="server" Text='<%# Bind("HFBC_ROUTE_OTHER") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                          
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Urine" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkUrine" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvUrine" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_URINE") %>'></asp:Label>

                            </asp:LinkButton>
                             </ItemTemplate>

                            
                       
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vomit" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkVomit" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvVomit" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_VOMIT") %>'></asp:Label>

                            </asp:LinkButton>
                        </ItemTemplate>
                              
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="N.G Aspiration" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkGAspiration" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvNGAspiration" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_NG_ASPIRATION") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                              
                      
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Other" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkOutputOthe" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvOutputOther" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_OTHER") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                          
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Input" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkTotalInput" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvTotalInput" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_TOTAL_INPUT") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                              
                      
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Total Output" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkTotalOutput" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvTotalOutput" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_TOTAL_OUTPUT") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                              
                      
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Result" HeaderStyle-Width="100px">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkResult" runat="server" OnClick="FluidBalance_Click">
                                <asp:Label ID="lblgvResult" CssClass="GridRow" runat="server" Text='<%# Bind("HFBC_RESULT") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                              
                      
                    </asp:TemplateField>
                </Columns>


            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <br />
 
</asp:Content>


