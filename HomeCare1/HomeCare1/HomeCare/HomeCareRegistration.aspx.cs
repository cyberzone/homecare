﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class HomeCareRegistration : System.Web.UI.Page
    {
        # region Variable Declaration
        connectiondb c = new connectiondb();
        dboperations dbo = new dboperations();
        clsHomeCare objHC = new clsHomeCare();

        public static string strSessionBranchId = "", strDrName = "";

        #endregion

        # region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = dbo.retun_patient_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["HPM_PT_FNAME"].ToString();
                txtMName.Text = ds.Tables[0].Rows[0]["HPM_PT_MNAME"].ToString();
                txtLName.Text = ds.Tables[0].Rows[0]["HPM_PT_LNAME"].ToString();

                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);
                txtMonth.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);
                hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();
                txtNationality.Text = ds.Tables[0].Rows[0]["HPM_NATIONALITY"].ToString();

                txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                    lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();



                txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                hidDepID.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);


                GetPatientVisit();

            }

        }

        void ScheduleServiceBind()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHSS_TYPE = 'Nursing'";
            Criteria += " AND HHSS_HHV_ID = '" + txtHomeCareId.Text.Trim() + "'";

            ds = objHC.ScheduleServiceGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvNursingService.DataSource = ds;
                gvNursingService.DataBind();

            }
            else
            {
                gvNursingService.DataBind();
            }

        }


        void SchedulePhyServiceBind()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HHSS_TYPE = 'Physiotherapy'";
            Criteria += " AND HHSS_HHV_ID = '" + txtHomeCareId.Text.Trim() + "'";

            ds = objHC.ScheduleServiceGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPhyService.DataSource = ds;
                gvPhyService.DataBind();

            }
            else
            {
                gvPhyService.DataBind();
            }

        }

        void GetServiceDetails(string ColtrolName, string ColtrolValue, string Type)
        {


            string Criteria = " 1=1 ";
            Criteria += " AND  HSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (ColtrolName == "Code")
            {
                txtServName.Text = "";
                Criteria += " AND HSM_SERV_ID = '" + ColtrolValue + "' ";

            }

            if (ColtrolName == "Name")
            {
                txtServCode.Text = "";
                Criteria += " AND HSM_NAME = '" + ColtrolValue + "' ";
            }

            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (Type == "Nursing")
                {
                    txtServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);
                    txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                }

                if (Type == "Physio")
                {
                    txtPhyServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);
                    txtPhyServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                }
            }


        }

        void GetStaffDetails(string ColtrolName, string ColtrolValue, string Type)
        {


            string Criteria = " 1=1 ";
            Criteria += " AND  HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HSFM_CATEGORY='Doctors'";

            if (ColtrolName == "Code")
            {
                Criteria += " AND HSFM_STAFF_ID = '" + ColtrolValue + "' ";

            }

            if (ColtrolName == "Name")
            {
                Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + ColtrolValue + "%' ";
            }

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.retun_doctor_detailss(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (Type == "Nursing")
                {
                    txtNurServStaffId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STAFF_ID"]);
                    txtNurServStaffName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
                    txtNurServStaffLicNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);

                }
                if (Type == "Physio")
                {
                    txtPhyServStaffId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_STAFF_ID"]);
                    txtPhyServStaffName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
                    txtPhyServStaffLicNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);


                }
            }


        }

        Boolean CheckHomeCareVisit()
        {

            string Criteria = " 1=1 AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            //Criteria += " AND HHV_ID = '" + hidHHV_ID.Value + "' ";
            Criteria += " AND HHV_ID = '" + txtHomeCareId.Text.Trim() + "' ";

            DataSet ds = new DataSet();

            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;

            }
            return false;
        }


        Boolean CheckDuplicateService()
        {
            if (gvNursingService.Rows.Count == 0)
                return true;
            for (int i = 0; i <= gvNursingService.Rows.Count - 1; i++)
            {

                Label lblServId, lblStartDate;
                lblServId = (Label)gvNursingService.Rows[i].Cells[0].FindControl("lblServId");
                lblStartDate = (Label)gvNursingService.Rows[i].Cells[0].FindControl("lblStartDate");


                if (lblServId.Text == txtServCode.Text.Trim() && lblStartDate.Text == txtServStDate.Text)
                {
                    return false;
                }

            }
            return true;
        }

        Boolean CheckDuplicatePhyService()
        {
            if (gvPhyService.Rows.Count == 0)
                return true;
            for (int i = 0; i <= gvPhyService.Rows.Count - 1; i++)
            {

                Label lblServId, lblStartDate;
                lblServId = (Label)gvPhyService.Rows[i].Cells[0].FindControl("lblServId");
                lblStartDate = (Label)gvPhyService.Rows[i].Cells[0].FindControl("lblStartDate");


                if (lblServId.Text == txtServCode.Text.Trim() && lblStartDate.Text == txtServStDate.Text)
                {
                    return false;
                }

            }
            return true;
        }


        void ModifyHCVisit()
        {
            string Criteria = " 1=1 AND HHV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            //Criteria += " AND HHV_ID = '" + hidHHV_ID.Value + "' ";
            Criteria += " AND HHV_ID = '" + txtHomeCareId.Text.Trim() + "' ";

            DataSet ds = new DataSet();

            ds = objHC.HomeCareVisitGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Session["HomeCareId"] = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);
                GlobelValues.HomeCare_ID = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);

                txtHomeCareId.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_ID"]);
                txtFileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PT_ID"]);
                txtVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_VISIT_DATEDesc"]);
                txtBeginDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_BEGIN_DATEDesc"]);
                txtEndDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_END_DATEDesc"]);
                txtRefDrName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_NAME"]);
                txtRefDrLicenseNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_LICENSE_NO"]);
                txtRefDrDepartment.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_DEPARTMENT"]);
                txtRefDrFacility.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_REF_DR_FACILITY"]);

                txtProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_PROCEDURES"]);
                txtSurgery.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_SURGERY"]);


                txtAuthorizationNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_AUTHORIZATION_NO"]);
                txtExtAuthorizationNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_EXT_AUTHORIZATION_NO"]);
                txtExtBeginDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_EXT_BEGIN_DATEDesc"]);
                txtExtEndDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["HHV_EXT_END_DATEDesc"]);

                hidEmrID.Value = Convert.ToString(ds.Tables[0].Rows[0]["HHV_EMP_ID"]);

                PatientDataBind();
                ScheduleServiceBind();
                SchedulePhyServiceBind();
                BindDiagnosis();

                //HomeCareDtls obj = new HomeCareDtls();
                //obj.BindHomeCareReg();



            }
            else
            {
                lblStatus.Text = "No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }


        }

        void Clear()
        {
            hidHHV_ID.Value = "0";
            ViewState["HHSS_Id"] = "0";
            ViewState["PhyHHSS_Id"] = "0";
            txtVisitDate.Text = "";
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtVisitDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtBeginDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtServStDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtServEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtServNoOfDays.Text = "1";

            txtPhyServStDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtPhyServEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtPhyServNoOfDays.Text = "1";

            txtRefDrName.Text = "";
            txtRefDrLicenseNo.Text = "";
            txtRefDrDepartment.Text = "";
            txtRefDrFacility.Text = "";

            txtProcedure.Text = "";
            txtSurgery.Text = "";

            txtAuthorizationNo.Text = "";
            txtExtAuthorizationNo.Text = "";
            txtExtBeginDate.Text = "";
            txtExtEndDate.Text = "";

            btnDelete.Visible = false;

            txtFileNo.Text = "";
            PatientDtlsClear();
            ClearServ();
            gvNursingService.DataBind();


        }

        void PatientDtlsClear()
        {

            txtFName.Text = "";
            txtMName.Text = "";
            txtLName.Text = "";

            txtDOB.Text = "";
            txtAge.Text = "";
            txtMonth.Text = "";

            txtSex.Text = "";

            txtAddress.Text = "";
            txtPoBox.Text = "";

            txtArea.Text = "";
            txtCity.Text = "";
            txtNationality.Text = "";

            txtPhone1.Text = "";
            txtMobile1.Text = "";
            txtMobile2.Text = "";

            lblIDCaption.Text = "EmiratesId";
            txtEmiratesID.Text = "";
            hidAgeType.Value = "";
            hidInsCode.Value = "";
            hidInsName.Value = "";
            hidDepID.Value = "";


        }

        void ClearServ()
        {
            txtServCode.Text = "";
            txtServName.Text = "";
            txtNurServStaffId.Text = "";
            txtNurServStaffName.Text = "";
            txtNurServStaffLicNo.Text = "";
            txtNurServAuthNo.Text = "";

            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtServStDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtServEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtServNoOfDays.Text = "1";




            if (Convert.ToString(ViewState["ServSelectIndex"]) != "")
            {
                gvNursingService.Rows[Convert.ToInt32(ViewState["ServSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["ServSelectIndex"] = "";
            ViewState["HHSS_Id"] = "0";
        }

        void ClearPhyServ()
        {
            txtPhyServCode.Text = "";
            txtPhyServName.Text = "";
            txtPhyServStaffId.Text = "";
            txtPhyServStaffName.Text = "";
            txtPhyServStaffLicNo.Text = "";
            txtPhyServAuthNo.Text = "";

            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());

            txtPhyServStDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtPhyServEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtPhyServNoOfDays.Text = "1";



            if (Convert.ToString(ViewState["ServPhySelectIndex"]) != "")
            {
                gvPhyService.Rows[Convert.ToInt32(ViewState["ServPhySelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["ServPhySelectIndex"] = "";
            ViewState["PhyHHSS_Id"] = "0";
        }

        void GetPatientVisit()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            Criteria += " AND HPV_PT_ID='" + txtFileNo.Text + "'";

            //if (Convert.ToString(ViewState["DEP_WISE_VISIT_TYPE"]) == "1")
            //{
            //    if (txtDepName.Text != "")
            //    {
            //        Criteria += " AND HPV_DEP_NAME='" + txtDepName.Text.Trim() + "'";
            //    }
            //}

            DS = new DataSet();
            DS = dbo.PatientVisitGet(Criteria);

            hidVisitType.Value = "New";
            if (DS.Tables[0].Rows.Count > 0)
            {
                hidVisitType.Value = "Established";

            }

        }


        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + hidEmrID.Value + "'";

            DataSet DS = new DataSet();
            clsWebReport objWebrpt = new clsWebReport();
            DS = objWebrpt.DiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();
            }
            else
            {
                gvDiagnosis.DataBind();
            }

        }

        void GetDiagnosisName()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  DIM_ICD_ID  ='" + txtDiagCode.Text.Trim() + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.DrICDMasterGet(Criteria, "10");

            txtDiagName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDiagName.Text = Convert.ToString(DS.Tables[0].Rows[0]["DIM_ICD_DESCRIPTION"]);
            }

        }

        void GetDiagnosisCode()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  DIM_ICD_DESCRIPTION   ='" + txtDiagName.Text.Trim() + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.DrICDMasterGet(Criteria, "10");

            txtDiagCode.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDiagCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["DIM_ICD_ID"]);
            }

        }


        void SetPermission()
        {
            string Criteria = " 1=1 AND HRT_SCREEN_ID='HCREG' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Enabled = false;
                btnDelete.Enabled = false;

                btnServAdd.Enabled = false;
                btnPhyServAdd.Enabled = false;
                btnPTRegPopup.Enabled = false;
                btnDiagAdd.Enabled = false;

                btnClear.Enabled = false;
                btnServClear.Enabled = false;
                btnPhyServClear.Enabled = false;
            }

            if (strPermission == "3")
            {
                btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;


                btnServAdd.Enabled = false;
                btnPhyServAdd.Enabled = false;
                btnPTRegPopup.Enabled = false;
                btnDiagAdd.Enabled = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Home.aspx");
            }
        }
        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            strDrName = "";
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1  AND HSFM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HSFM_STAFF_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["FullName"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    SetPermission();
                try
                {
                    ViewState["HHSS_Id"] = "0";
                    ViewState["PhyHHSS_Id"] = "0";
                    ViewState["HHS_Id"] = "0";
                    hidEmrID.Value = "0";

                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtVisitDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtBeginDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtServStDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtServEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtServNoOfDays.Text = "1";

                    txtPhyServStDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtPhyServEndDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtPhyServNoOfDays.Text = "1";



                    //string AutoSlNo = Convert.ToString(Session["AutoSlNo"]);
                    //switch (AutoSlNo)
                    //{
                    //    case "A":
                    chkManual.Checked = false;
                    chkManual.Visible = false;
                    txtHomeCareId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "HOMECARERE");

                    //        break;
                    //    case "M":
                    //        chkManual.Checked = true;
                    //        chkManual.Visible = false;
                    //        // txtCompCode.ReadOnly = false;
                    //        break;
                    //    case "B":
                    //        chkManual.Visible = true;
                    //        txtHomeCareId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "HOMECARERE");
                    //        break;
                    //    default:
                    //        break;

                    //}

                    string PatientId = "", HHV_ID = "";


                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                    HHV_ID = Convert.ToString(Request.QueryString["HHV_ID"]);

                    if (PatientId != " " && PatientId != null)
                    {
                        txtFileNo.Text = Convert.ToString(PatientId);
                        PatientDataBind();
                    }
                    else
                    {


                    }
                    if (HHV_ID != " " && HHV_ID != null)
                    {
                        txtHomeCareId.Text = Convert.ToString(HHV_ID);
                        ModifyHCVisit();
                        //  btnDelete.Visible = true;
                    }
                    else
                    {


                    }

                    if (Convert.ToString(Session["HomeCareId"]) != "" && Convert.ToString(Session["HomeCareId"]) != null)
                    {
                        txtHomeCareId.Text = Convert.ToString(Session["HomeCareId"]);
                        ModifyHCVisit();
                        // btnDelete.Visible = true;
                    }
                    else
                    {


                    }



                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void chkManual_CheckedChanged(object sender, EventArgs e)
        {
            if (chkManual.Checked == true)
            {
                txtHomeCareId.Text = "";
            }
            else
            {
                txtHomeCareId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "HOMECARERE");

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                dbOperation DBO = new dbOperation();
                IDictionary<string, string> Param = new Dictionary<string, string>();

                Param.Add("HHV_BRANCH_ID", Convert.ToString(Session["Branch_ID"]));
                Param.Add("HHV_ID", txtHomeCareId.Text.Trim());
                Param.Add("HHV_PT_ID", txtFileNo.Text.Trim());
                Param.Add("HHV_VISIT_DATE", txtVisitDate.Text.Trim());
                Param.Add("HHV_BEGIN_DATE", txtBeginDate.Text.Trim());
                Param.Add("HHV_END_DATE", txtEndDate.Text.Trim());
                Param.Add("HHV_REF_DR_NAME", txtRefDrName.Text.Trim());
                Param.Add("HHV_REF_DR_LICENSE_NO", txtRefDrLicenseNo.Text.Trim());
                Param.Add("HHV_REF_DR_DEPARTMENT", txtRefDrDepartment.Text.Trim());
                Param.Add("HHV_REF_DR_FACILITY", txtRefDrFacility.Text.Trim());
                Param.Add("HHV_DIAGNOSIS", "");
                Param.Add("HHV_PROCEDURES", txtProcedure.Text.Trim());
                Param.Add("HHV_SURGERY", txtSurgery.Text.Trim());
                Param.Add("UserId", Convert.ToString(Session["User_Code"]));
                Param.Add("IsManual", Convert.ToString(chkManual.Checked));
                Param.Add("HIN_SCRN_ID", "HOMECARERE");

                Param.Add("HHV_AUTHORIZATION_NO", txtAuthorizationNo.Text.Trim());
                Param.Add("HHV_EXT_AUTHORIZATION_NO ", txtExtAuthorizationNo.Text.Trim());
                Param.Add("HHV_EXT_BEGIN_DATE", txtExtBeginDate.Text.Trim());
                Param.Add("HHV_EXT_END_DATE", txtExtEndDate.Text.Trim());

                string strReturnHCVId;
                strReturnHCVId = DBO.ExecuteNonQueryReturn("HMS_SP_HomeCareVisitAdd", Param);

                txtHomeCareId.Text = strReturnHCVId;
                Session["HomeCareId"] = txtHomeCareId.Text;

                /*

                EMR_PTMasterBAL objEMR_PT = new EMR_PTMasterBAL();

                objEMR_PT.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objEMR_PT.EPM_ID = hidEmrID.Value;
                objEMR_PT.EPM_DATE = txtVisitDate.Text.Trim();
                objEMR_PT.EPM_PT_ID = txtFileNo.Text.Trim();
                objEMR_PT.EPM_PT_NAME = txtFName.Text.Trim() + " " + txtMName.Text.Trim() + " " + txtLName.Text.Trim();
                objEMR_PT.EPM_AGE = txtAge.Text.Trim();
                objEMR_PT.EPM_AGE_TYPE = hidAgeType.Value;

                objEMR_PT.EPM_DR_CODE = "00010";
                objEMR_PT.EPM_DR_NAME = "DR. ATIQUL RAHAMAN";
                objEMR_PT.EPM_INS_CODE = hidInsCode.Value;
                objEMR_PT.EPM_INS_NAME = hidInsName.Value;

                objEMR_PT.EPM_START_DATE = txtBeginDate.Text.Trim();
                objEMR_PT.EPM_END_DATE = txtEndDate.Text.Trim();
                objEMR_PT.EPM_DEP_ID = hidDepID.Value;
                objEMR_PT.EPM_DEP_NAME = hidDepID.Value;
                objEMR_PT.EMR_PT_TYPE = hidVisitType.Value;
                objEMR_PT.UserId = Convert.ToString(Session["User_ID"]);

                string EMR_ID = "";
                objEMR_PT.AddEMR_PTMaster(out EMR_ID);
                hidEmrID.Value = EMR_ID;
                */
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

            Response.Redirect("HomeCareRegistration.aspx?HHV_ID=" + txtHomeCareId.Text);

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                objHC.HomeCareVisitDelete(txtHomeCareId.Text.Trim());
                txtHomeCareId.Text = "";
                Clear();
                //lblStatus.Text = "Data Deleted";
                //lblStatus.ForeColor = System.Drawing.Color.Green;
                //txtHomeCareId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "HOMECARERE");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Deleted.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

            Session["HomeCareId"] = null;
            Response.Redirect("HomeCareRegistration.aspx");

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            //try
            //{
            txtHomeCareId.Text = "";
            Clear();
            //txtHomeCareId.Text = dbo.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "HOMECARERE");
            Session["HomeCareId"] = null;
            Response.Redirect("HomeCareRegistration.aspx");


            //}
            //catch (Exception ex)
            //{
            //    TextFileWriting("-----------------------------------------------");
            //    TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.btnClear_Click");
            //    TextFileWriting(ex.Message.ToString());
            //}
        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear();
                if (Convert.ToString(ViewState["ServSelectIndex"]) != "")
                {
                    gvNursingService.Rows[Convert.ToInt32(ViewState["ServSelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ServSelectIndex"] = gvScanCard.RowIndex;


                gvNursingService.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


                Label lblHHSSId, lblServId, lblServName, lblStaffId, lblStaffName, lblLicenseNo, lblNoOfDays, lblStartDate, lblEndDate, lblAuthNo;

                lblHHSSId = (Label)gvScanCard.Cells[0].FindControl("lblHHSSId");
                lblServId = (Label)gvScanCard.Cells[0].FindControl("lblServId");
                lblServName = (Label)gvScanCard.Cells[0].FindControl("lblServName");
                lblStaffId = (Label)gvScanCard.Cells[0].FindControl("lblStaffId");
                lblStaffName = (Label)gvScanCard.Cells[0].FindControl("lblStaffName");
                lblLicenseNo = (Label)gvScanCard.Cells[0].FindControl("lblLicenseNo");

                lblNoOfDays = (Label)gvScanCard.Cells[0].FindControl("lblNoOfDays");
                lblStartDate = (Label)gvScanCard.Cells[0].FindControl("lblStartDate");
                lblEndDate = (Label)gvScanCard.Cells[0].FindControl("lblEndDate");
                lblAuthNo = (Label)gvScanCard.Cells[0].FindControl("lblAuthNo");


                ViewState["HHSS_Id"] = lblHHSSId.Text;
                txtServCode.Text = lblServId.Text;
                txtServName.Text = lblServName.Text;
                txtNurServStaffId.Text = lblStaffId.Text;
                txtNurServStaffName.Text = lblStaffName.Text;
                txtNurServStaffLicNo.Text = lblLicenseNo.Text;

                txtServNoOfDays.Text = lblNoOfDays.Text;
                txtServStDate.Text = lblStartDate.Text;
                txtServEndDate.Text = lblEndDate.Text;
                txtNurServAuthNo.Text = lblAuthNo.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvNursingService_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvNursingService.PageIndex * gvNursingService.PageSize) + e.Row.RowIndex + 1).ToString();
            }
        }



        protected void btnServAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //if (txtHomeCareId.Text.Trim() == "0")
                //{
                //    goto FunEnd;
                //}

                if (txtHomeCareId.Text.Trim() == "")
                {
                    goto FunEnd;
                }
                if (CheckHomeCareVisit() == false)
                {
                    lblStatus.Text = "Please Save the Home Care Details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                //if (CheckDuplicateService() == false)
                //{
                //    lblStatus.Text = "This Service already added for same day";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;
                //}


                dbOperation DBO = new dbOperation();
                IDictionary<string, string> Param = new Dictionary<string, string>();
                Param.Add("HHSS_ID", Convert.ToString(ViewState["HHSS_Id"]));
                Param.Add("HHSS_HHV_ID", txtHomeCareId.Text.Trim());
                Param.Add("HHSS_PT_ID", txtFileNo.Text.Trim());
                Param.Add("HHSS_BRANCH_ID", Convert.ToString(Session["Branch_ID"]));
                Param.Add("HHSS_SERV_ID", txtServCode.Text.Trim());
                Param.Add("HHSS_SERV_NAME", txtServName.Text.Trim());
                Param.Add("HHSS_STAFF_ID", txtNurServStaffId.Text.Trim());
                Param.Add("HHSS_STAFF_NAME", txtNurServStaffName.Text.Trim());
                Param.Add("HHSS_STAFF_MOH_NO", txtNurServStaffLicNo.Text.Trim());
                Param.Add("HHSS_AUTHORIZATION_NO", txtNurServAuthNo.Text.Trim());
                Param.Add("HHSS_NOOFDAYS", txtServNoOfDays.Text.Trim());
                Param.Add("HHSS_START_DATE", txtServStDate.Text.Trim());
                Param.Add("HHSS_END_DATE", txtServEndDate.Text.Trim());
                Param.Add("HHV_VISIT_DATE", txtVisitDate.Text.Trim());

                Param.Add("HHSS_TYPE", "Nursing");
                Param.Add("USER_ID", Convert.ToString(Session["User_Code"]));
                DBO.ExecuteNonQuery("HMS_SP_HC_ScheduleServAdd", Param);

                ClearServ();
                ScheduleServiceBind();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.btnServAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteSchServ_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblHHSSId, lblServId;
                    lblHHSSId = (Label)gvScanCard.Cells[0].FindControl("lblHHSSId");
                    lblServId = (Label)gvScanCard.Cells[0].FindControl("lblServId");

                    objHC.ScheduleServiceDelete(txtHomeCareId.Text.Trim(), lblHHSSId.Text);

                    lblStatus.Text = "Schedulte Service Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    ClearServ();
                    ScheduleServiceBind();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.DeleteSchServ_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void btnServClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearServ();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.btnServClear_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void PhySelect_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear();
                if (Convert.ToString(ViewState["ServPhySelectIndex"]) != "")
                {
                    gvPhyService.Rows[Convert.ToInt32(ViewState["ServPhySelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ServPhySelectIndex"] = gvScanCard.RowIndex;


                gvPhyService.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.Orange;


                Label lblHHSSId, lblServId, lblServName, lblStaffId, lblStaffName, lblLicenseNo, lblNoOfDays, lblStartDate, lblEndDate, lblAuthNo; ;

                lblHHSSId = (Label)gvScanCard.Cells[0].FindControl("lblHHSSId");
                lblServId = (Label)gvScanCard.Cells[0].FindControl("lblServId");
                lblServName = (Label)gvScanCard.Cells[0].FindControl("lblServName");
                lblStaffId = (Label)gvScanCard.Cells[0].FindControl("lblStaffId");
                lblStaffName = (Label)gvScanCard.Cells[0].FindControl("lblStaffName");
                lblLicenseNo = (Label)gvScanCard.Cells[0].FindControl("lblLicenseNo");


                lblNoOfDays = (Label)gvScanCard.Cells[0].FindControl("lblNoOfDays");
                lblStartDate = (Label)gvScanCard.Cells[0].FindControl("lblStartDate");
                lblEndDate = (Label)gvScanCard.Cells[0].FindControl("lblEndDate");
                lblAuthNo = (Label)gvScanCard.Cells[0].FindControl("lblAuthNo");





                ViewState["PhyHHSS_Id"] = lblHHSSId.Text;
                txtPhyServCode.Text = lblServId.Text;
                txtPhyServName.Text = lblServName.Text;
                txtPhyServStaffId.Text = lblStaffId.Text;
                txtPhyServStaffName.Text = lblStaffName.Text;
                txtPhyServStaffLicNo.Text = lblLicenseNo.Text;

                txtPhyServNoOfDays.Text = lblNoOfDays.Text;
                txtPhyServStDate.Text = lblStartDate.Text;
                txtPhyServEndDate.Text = lblEndDate.Text;
                txtPhyServAuthNo.Text = lblAuthNo.Text;



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void gvPhyService_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvPhyService.PageIndex * gvPhyService.PageSize) + e.Row.RowIndex + 1).ToString();
            }
        }

        protected void btnPhyServAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //if (txtHomeCareId.Text.Trim() == "0")
                //{
                //    goto FunEnd;
                //}

                if (txtHomeCareId.Text.Trim() == "")
                {
                    goto FunEnd;
                }
                if (CheckHomeCareVisit() == false)
                {
                    lblStatus.Text = "Please Save the Home Care Details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                //if (CheckDuplicatePhyService() == false)
                //{
                //    lblStatus.Text = "This Service already added for same day";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;
                //}


                dbOperation DBO = new dbOperation();
                IDictionary<string, string> Param = new Dictionary<string, string>();
                Param.Add("HHSS_ID", Convert.ToString(ViewState["PhyHHSS_Id"]));
                Param.Add("HHSS_HHV_ID", txtHomeCareId.Text.Trim());
                Param.Add("HHSS_PT_ID", txtFileNo.Text.Trim());
                Param.Add("HHSS_BRANCH_ID", Convert.ToString(Session["Branch_ID"]));
                Param.Add("HHSS_SERV_ID", txtPhyServCode.Text.Trim());
                Param.Add("HHSS_SERV_NAME", txtPhyServName.Text.Trim());
                Param.Add("HHSS_STAFF_ID", txtPhyServStaffId.Text.Trim());
                Param.Add("HHSS_STAFF_NAME", txtPhyServStaffName.Text.Trim());
                Param.Add("HHSS_STAFF_MOH_NO", txtPhyServStaffLicNo.Text.Trim());
                Param.Add("HHSS_AUTHORIZATION_NO", txtPhyServAuthNo.Text.Trim());
                Param.Add("HHSS_NOOFDAYS", txtPhyServNoOfDays.Text.Trim());
                Param.Add("HHSS_START_DATE", txtPhyServStDate.Text.Trim());
                Param.Add("HHSS_END_DATE", txtPhyServEndDate.Text.Trim());
                Param.Add("HHV_VISIT_DATE", txtVisitDate.Text.Trim());

                Param.Add("HHSS_TYPE", "Physiotherapy");
                Param.Add("USER_ID", Convert.ToString(Session["User_Code"]));
                DBO.ExecuteNonQuery("HMS_SP_HC_ScheduleServAdd", Param);

                ClearPhyServ();
                SchedulePhyServiceBind();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.btnServAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeletePhyServ_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblHHSSId, lblServId;
                    lblHHSSId = (Label)gvScanCard.Cells[0].FindControl("lblHHSSId");
                    lblServId = (Label)gvScanCard.Cells[0].FindControl("lblServId");

                    objHC.ScheduleServiceDelete(txtHomeCareId.Text.Trim(), lblHHSSId.Text);

                    lblStatus.Text = "Schedulte Service Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    ClearPhyServ();
                    SchedulePhyServiceBind();

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.DeleteSchServ_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnPhyServClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearPhyServ();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.btnServClear_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }




        protected void txtHomeCareId_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    Clear();
            //    ModifyHCVisit();

            //}
            //catch (Exception ex)
            //{
            //    TextFileWriting("-----------------------------------------------");
            //    TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtHomeCareId_TextChanged");
            //    TextFileWriting(ex.Message.ToString());
            //}
            Session["HomeCareId"] = null;
            Response.Redirect("HomeCareRegistration.aspx?HHV_ID=" + txtHomeCareId.Text);

        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PatientDtlsClear();
                PatientDataBind();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnDamanGCPrint_Click(object sender, EventArgs e)
        {
            string strFormula = " {HMS_PATIENT_MASTER.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'";
            Session["ReportFormula"] = strFormula;

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ReportPopup('" + txtFileNo.Text.Trim() + "');", true);

            //  Response.Redirect(@"..\CReports\ReportsView.aspx?ReportName=" + strReport + "&SelectionFormula={GeneralConsentView.HPM_PT_ID}='" + txtFileNo.Text.Trim() + "'");




        }


        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtServCode.Text.Trim() != "")
                    GetServiceDetails("Code", txtServCode.Text.Trim(), "Nursing");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtServName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtServName.Text.Trim() != "")
                    GetServiceDetails("Name", txtServName.Text.Trim(), "Nursing");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtServStDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtServStDate.Text.Trim() != "" && txtServEndDate.Text.Trim() != "")
                {
                    DateTime Date1 = Convert.ToDateTime(txtServStDate.Text.Trim());
                    DateTime Date2 = Convert.ToDateTime(txtServEndDate.Text.Trim());
                    TimeSpan Diff = Date2.Date - Date1.Date;
                    txtServNoOfDays.Text = Convert.ToString(Diff.Days + 1);



                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtServStDate_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtNurServStaffId_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtNurServStaffId.Text.Trim() != "")
                    GetStaffDetails("Code", txtNurServStaffId.Text.Trim(), "Nursing");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtNurServStaffId_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtNurServStaffName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtNurServStaffName.Text.Trim() != "")
                    GetStaffDetails("Name", txtNurServStaffName.Text.Trim(), "Nursing");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtNurServStaffName_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }



        protected void txtPhyServCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtPhyServCode.Text.Trim() != "")
                    GetServiceDetails("Code", txtPhyServCode.Text.Trim(), "Physio");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtPhyServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtPhyServName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtPhyServName.Text.Trim() != "")
                    GetServiceDetails("Name", txtPhyServName.Text.Trim(), "Physio");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtPhyServName_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtPhyServStDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtPhyServStDate.Text.Trim() != "" && txtPhyServEndDate.Text.Trim() != "")
                {
                    DateTime Date1 = Convert.ToDateTime(txtPhyServStDate.Text.Trim());
                    DateTime Date2 = Convert.ToDateTime(txtPhyServEndDate.Text.Trim());
                    TimeSpan Diff = Date2.Date - Date1.Date;
                    txtPhyServNoOfDays.Text = Convert.ToString(Diff.Days + 1);



                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtPhyServStDate_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void txtPhyServStaffId_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtPhyServStaffId.Text.Trim() != "")
                    GetStaffDetails("Code", txtPhyServStaffId.Text.Trim(), "Physio");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtPhyServStaffId_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtPhyServStaffName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtPhyServStaffName.Text.Trim() != "")
                    GetStaffDetails("Name", txtPhyServStaffName.Text.Trim(), "Physio");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtPhyServStaffName_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnDiagAdd_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtHomeCareId.Text.Trim() == "")
                {
                    goto FunEnd;
                }
                if (CheckHomeCareVisit() == false)
                {
                    lblStatus.Text = "Please Save the Home Care Details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                EMR_PT_DiagnosisBAL objEMRDiag = new EMR_PT_DiagnosisBAL();

                objEMRDiag.EPD_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objEMRDiag.EPD_ID = hidEmrID.Value;
                objEMRDiag.EPD_DIAG_CODE = txtDiagCode.Text.Trim(); ;
                objEMRDiag.EPD_DIAG_NAME = txtDiagName.Text.Trim();
                objEMRDiag.AddEMR_PTDiagnosis();


                BindDiagnosis();

                txtDiagCode.Text = "";
                txtDiagName.Text = "";

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteDiag_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;

                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblDiagCode;

                    lblDiagCode = (Label)gvScanCard.FindControl("lblDiagCode");

                    EMR_PT_DiagnosisBAL objEMRDiag = new EMR_PT_DiagnosisBAL();

                    objEMRDiag.EPD_ID = hidEmrID.Value;
                    objEMRDiag.EPD_DIAG_CODE = lblDiagCode.Text;
                    objEMRDiag.DeleteEMR_PTDiagnosis();

                    BindDiagnosis();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void txtDiagCode_TextChanged(object sender, EventArgs e)
        {

            GetDiagnosisName();

        }

        protected void txtDiagName_TextChanged(object sender, EventArgs e)
        {

            GetDiagnosisCode();

        }


        protected void btnPTRegPopup_Click(object sender, EventArgs e)
        {
            try
            {

                Session["HomeCareId"] = null;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "PTPopup();", true);



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnPTRegPopup_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        #endregion
    }
}