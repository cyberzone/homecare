﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;

namespace HomeCare1.HomeCare
{
    public partial class FluidBalancechart : System.Web.UI.Page
    {
        HC_FluidBalancechart objFluid = new HC_FluidBalancechart();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpFluHour.DataSource = DS;
                drpFluHour.DataTextField = "Name";
                drpFluHour.DataValueField = "Code";
                drpFluHour.DataBind();
            }

            drpFluHour.Items.Insert(0, "00");
            drpFluHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet("5");
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpFluMin.DataSource = DS;
                drpFluMin.DataTextField = "Name";
                drpFluMin.DataValueField = "Code";
                drpFluMin.DataBind();
            }

            drpFluMin.Items.Insert(0, "00");
            drpFluMin.Items[0].Value = "00";

        }

        void BindGrid()
        {
            DataSet DS = new DataSet();
            objFluid = new HC_FluidBalancechart();



            string Criteria = " 1=1 and HFBC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HFBC_ID='" + Convert.ToString(Session["HomeCareId"]) + "'";

            DS = objFluid.FluidBalanceChartGet(Criteria);


            gvFluidBalance.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvFluidBalance.DataSource = DS;
                gvFluidBalance.DataBind();

            }

        }

        void CalculateTotalScore()
        {
            Decimal intTotalInput = 0, intTotalOutPut = 0;

            string strResult = "Positive ";
            for (int intCurRow = 0; intCurRow < gvFluidBalance.Rows.Count; intCurRow++)
            {
                Label lblgvOral = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvOral");
                Label lblgvIV = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvIV");
                Label lblgvRouteOther = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvRouteOther");

                Label lblgvUrine = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvUrine");
                Label lblgvVomit = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvVomit");
                Label lblgvNGAspiration = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvNGAspiration");
                Label lblgvOutputOther = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvOutputOther");

                if (lblgvOral.Text.Trim() != "")
                {
                    intTotalInput += Convert.ToDecimal(lblgvOral.Text);

                }
                if (lblgvIV.Text.Trim() != "")
                {
                    intTotalInput += Convert.ToDecimal(lblgvIV.Text);
                }

                if (lblgvRouteOther.Text.Trim() != "")
                {
                    intTotalInput += Convert.ToDecimal(lblgvRouteOther.Text);
                }



                if (lblgvUrine.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvUrine.Text);
                }

                if (lblgvVomit.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvVomit.Text);
                }
                if (lblgvNGAspiration.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvNGAspiration.Text);
                }

                if (lblgvOutputOther.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvOutputOther.Text);
                }
            }
            TextBox txtgvTotalInput = (TextBox)gvFluidBalance.FooterRow.FindControl("txtgvTotalInput");
            TextBox txtgvTotalOutput = (TextBox)gvFluidBalance.FooterRow.FindControl("txtgvTotalOutput");
            TextBox txtgvResult = (TextBox)gvFluidBalance.FooterRow.FindControl("txtgvResult");


            txtgvTotalInput.Text = Convert.ToString(intTotalInput);
            txtgvTotalOutput.Text = Convert.ToString(intTotalOutPut);
            txtgvResult.Text = strResult;





        }

        void Clear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvFluidBalanceSelectIndex"]) != "" && Convert.ToString(ViewState["gvFluidBalanceSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvFluidBalanceSelectIndex"]);
                gvFluidBalance.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }

            ViewState["HFBC_FLUBAL_ID"] = "";
            ViewState["gvFluidBalanceSelectIndex"] = "";


            CommonBAL objCom = new CommonBAL();
            string strDate = "", strTime = ""; ;
            strDate = objCom.fnGetDate("dd/MM/yyyy");
            strTime = objCom.fnGetDate("hh:mm:ss");
            txtAssDate.Text = strDate;



            txtNatueIfFood.Text = "";

            txtOral.Text = "";
            txtIV.Text = "";
            txtOralOther.Text = "";

            txtUrine.Text = "";
            txtVomit.Text = "";
            txtNGAspiration.Text = "";
            txtOther.Text = "";

            txtTotalInput.Text = "";
            txtTotalOutput.Text = "";
            drpResult.SelectedIndex = 0;

            ViewState["HFBC_FLUBAL_ID"] = "";
        }

        //void SetPermission()
        //{
        //    //HCREG,HCPTENOC
        //    string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_NURSE_CAREPLAN' ";
        //    Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

        //    CommonBAL objCom = new CommonBAL();
        //    DataSet ds = new DataSet();
        //    ds = objCom.RollTransGet(Criteria);

        //    string strPermission = "0";
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

        //    }

        //    if (strPermission == "1")
        //    {

        //        btnAdd.Visible = false;
        //        // btnDeleteFav.Visible = false;
        //        // btnClear.Visible = false;



        //    }

        //    if (strPermission == "5")
        //    {
        //        //btnDeleteFav.Visible = false;

        //    }

        //    if (strPermission == "7")
        //    {
        //        btnAdd.Visible = false;
        //        //  btnAddFav.Visible = false;
        //        //btnProcUpdate.Visible = false;

        //    }

        //    hidPermission.Value = strPermission;
        //    if (strPermission == "0")
        //    {
        //        Response.Redirect("../Common/PermissionDenied.aspx");
        //    }
        //}
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["HomeCareId"] == null) { Response.Redirect("../HomeCare/WaitingList.aspx"); }

            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                //{
                //    SetPermission();
                //}
                try
                {
                    if (Convert.ToString(Request.QueryString["Message"]) == "Save")
                    {
                        lblStatus.Text = "Data Saved.";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }

                    this.Page.Title = "FluidBalancechart";
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "";
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    BindTime();

                    txtAssDate.Text = strDate;

                    BindGrid();
                    //  CalculateTotalScore();
                    ViewState["HFBC_FLUBAL_ID"] = "";
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //Decimal decTotalInput=0,decTotalOutPut=0;
                // string strResult = "Positive";

                //if (txtOral.Text.Trim() != "")
                //{
                //    decTotalInput += Convert.ToDecimal(txtOral.Text.Trim());
                //}
                //if (txtIV.Text.Trim() != "")
                //{
                //    decTotalInput += Convert.ToDecimal(txtIV.Text.Trim());
                //}
                //if (txtOralOther.Text.Trim() != "")
                //{
                //    decTotalInput += Convert.ToDecimal(txtOralOther.Text.Trim());
                //}

                //if (txtUrine.Text.Trim() != "")
                //{
                //    decTotalOutPut += Convert.ToDecimal(txtUrine.Text.Trim());
                //}

                //if (txtVomit.Text.Trim() != "")
                //{
                //    decTotalOutPut += Convert.ToDecimal(txtVomit.Text.Trim());
                //}

                //if (txtNGAspiration.Text.Trim() != "")
                //{
                //    decTotalOutPut += Convert.ToDecimal(txtNGAspiration.Text.Trim());
                //}

                //if (txtOther.Text.Trim() != "")
                //{
                //    decTotalOutPut += Convert.ToDecimal(txtOther.Text.Trim());
                //}

                objFluid = new HC_FluidBalancechart();
                objFluid.BranchID = Convert.ToString(Session["Branch_ID"]);
                objFluid.EMRID = Convert.ToString(Session["HomeCareId"]);
                objFluid.PTID = Convert.ToString(Session["HHV_PT_ID"]);

                if (Convert.ToString(ViewState["HFBC_FLUBAL_ID"]) == "")
                {
                    objFluid.HFBC_FLUBAL_ID = "0";
                }
                else
                {
                    objFluid.HFBC_FLUBAL_ID = Convert.ToString(ViewState["HFBC_FLUBAL_ID"]);
                }
                objFluid.HFBC_DATE = txtAssDate.Text.Trim() + " " + drpFluHour.SelectedValue + ":" + drpFluMin.SelectedValue + ":00";

                objFluid.HFBC_NATURE_OF_FOOD = txtNatueIfFood.Text;

                objFluid.HFBC_ROUTE_ORAL = txtOral.Text;
                objFluid.HFBC_ROUTE_IV = txtIV.Text;
                objFluid.HFBC_ROUTE_OTHER = txtOralOther.Text;

                objFluid.HFBC_URINE = txtUrine.Text;
                objFluid.HFBC_VOMIT = txtVomit.Text;
                objFluid.HFBC_NG_ASPIRATION = txtNGAspiration.Text;
                objFluid.HFBC_OTHER = txtOther.Text;


                objFluid.HFBC_TOTAL_INPUT = txtTotalInput.Text.Trim();
                objFluid.HFBC_TOTAL_OUTPUT = txtTotalOutput.Text.Trim();
                objFluid.HFBC_RESULT = drpResult.SelectedValue;

                objFluid.UserID = Convert.ToString(Session["User_ID"]);
                objFluid.FluidBalanceChartAdd();
                Clear();
                BindGrid();
                // CalculateTotalScore();
                //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

                Response.Redirect("FluidBalancechart.aspx?Message=Save");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void FluidBalance_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvFluidBalanceSelectIndex"] = gvScanCard.RowIndex;
                gvFluidBalance.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblFliidBalID = (Label)gvScanCard.Cells[0].FindControl("lblFliidBalID");

                Label lblgvAssDate = (Label)gvScanCard.Cells[0].FindControl("lblgvAssDate");
                Label lblgvAssTime = (Label)gvScanCard.Cells[0].FindControl("lblgvAssTime");

                Label lblNatureoffood = (Label)gvScanCard.Cells[0].FindControl("lblNatureoffood");
                Label lblgvOral = (Label)gvScanCard.Cells[0].FindControl("lblgvOral");
                Label lblgvIV = (Label)gvScanCard.Cells[0].FindControl("lblgvIV");
                Label lblgvRouteOther = (Label)gvScanCard.Cells[0].FindControl("lblgvRouteOther");

                Label lblgvUrine = (Label)gvScanCard.Cells[0].FindControl("lblgvUrine");
                Label lblgvVomit = (Label)gvScanCard.Cells[0].FindControl("lblgvVomit");
                Label lblgvNGAspiration = (Label)gvScanCard.Cells[0].FindControl("lblgvNGAspiration");
                Label lblgvOutputOther = (Label)gvScanCard.Cells[0].FindControl("lblgvOutputOther");


                Label lblgvTotalInput = (Label)gvScanCard.Cells[0].FindControl("lblgvTotalInput");
                Label lblgvTotalOutput = (Label)gvScanCard.Cells[0].FindControl("lblgvTotalOutput");
                Label lblgvResult = (Label)gvScanCard.Cells[0].FindControl("lblgvResult");



                ViewState["HFBC_FLUBAL_ID"] = lblFliidBalID.Text;


                txtAssDate.Text = lblgvAssDate.Text;


                string strIA_TIME_IN = lblgvAssTime.Text;

                string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                if (arrIA_TIME_IN.Length > 1)
                {
                    drpFluHour.SelectedValue = arrIA_TIME_IN[0];
                    drpFluMin.SelectedValue = arrIA_TIME_IN[1];

                }

                txtNatueIfFood.Text = lblNatureoffood.Text;

                txtOral.Text = lblgvOral.Text;
                txtIV.Text = lblgvIV.Text;
                txtOralOther.Text = lblgvRouteOther.Text;

                txtUrine.Text = lblgvUrine.Text;
                txtVomit.Text = lblgvVomit.Text;
                txtNGAspiration.Text = lblgvNGAspiration.Text;
                txtOther.Text = lblgvOutputOther.Text;

                txtTotalInput.Text = lblgvTotalInput.Text;
                txtTotalOutput.Text = lblgvTotalOutput.Text;
                drpResult.SelectedValue = lblgvResult.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "ReAssessment_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}