﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCare.Master" AutoEventWireup="true" CodeBehind="CoordinatorStaffAssign.aspx.cs" Inherits="HomeCare1.HomeCare.CoordinatorStaffAssign" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

      <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

        

        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function PatientPopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=HCStaffAssign&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
                win.focus();
            }
            return true;

        }




        function SaveVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



            if (document.getElementById('<%=txtFileNo.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter File No";
                   return false;

               }

               if (document.getElementById('<%=txtVisitDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Date";
                return false;

            }

            if (isDate(document.getElementById('<%=txtVisitDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtToDate.ClientID%>').focus()
                return false;
            }

            if (document.getElementById('<%=txtStaffID.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please select the Staff ID";
                return false;

            }



            return true;
        }




        function DRIdSelected() {
            if (document.getElementById('<%=txtStaffID.ClientID%>').value != "") {
                  var Data = document.getElementById('<%=txtStaffID.ClientID%>').value;
                  var Data1 = Data.split('~');
                  if (Data1.length > 1) {
                      document.getElementById('<%=txtStaffID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtStaffName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtStaffName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtStaffName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtStaffID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtStaffName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function SrcDRIdSelected() {
            if (document.getElementById('<%=txtSrcStaffID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtSrcStaffID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtSrcStaffID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtSrcStaffName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function SrcDRNameSelected() {
            if (document.getElementById('<%=txtSrcStaffName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtSrcStaffName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtSrcStaffID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtSrcStaffName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function ServIdSelected() {
            if (document.getElementById('<%=txtServCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td class="PageHeader">Staff Assigning and Monitoring
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
    <table width="500px" cellspacing="0" cellpadding="0">
        <tr>
            <td class="lblCaption1" style="height: 30px;">Visit Date
            </td>
            <td>
                <asp:TextBox ID="txtFromDate" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="Calendarextender1" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

            </td>

            <td class="lblCaption1" style="height: 30px;">To
            </td>
            <td>
                <asp:TextBox ID="txtToDate" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="txtToDate_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                <asp:Button ID="btnFind" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                    OnClick="btnFind_Click" Text="Refresh" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Assigned Staff  
            </td>
            <td>
                <asp:TextBox ID="txtSrcStaffID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="105px" onblur="return SrcDRIdSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtSrcStaffID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"></asp:AutoCompleteExtender>

            </td>
            <td colspan="3">
                <asp:TextBox ID="txtSrcStaffName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="400px" MaxLength="50" onblur="return SrcDRNameSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtSrcStaffName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"></asp:AutoCompleteExtender>

            </td>

        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Status
            </td>
            <td colspan="3">
                <asp:DropDownList ID="drpSrcStatus" runat="server" CssClass="label" Width="150px" BorderWidth="1px" BorderColor="#CCCCCC">
                    <asp:ListItem Value="" Selected="true">---All---</asp:ListItem>
                    <asp:ListItem Value="I">InProgress</asp:ListItem>
                    <asp:ListItem Value="P">Pending</asp:ListItem>
                    <asp:ListItem Value="C">Completed</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto; border-color: #cccccc; border-style: solid; border-width: thin;">
        <asp:GridView ID="gvStaffAssign" runat="server" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="100%" PageSize="200">
            <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
            <RowStyle CssClass="GridRow" />
            <Columns>

                <asp:TemplateField HeaderText="File No">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEmergencyNotes" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblComment" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_COMMENT") %>' Visible="false"></asp:Label>
                           
                            <asp:Label ID="lblHSAM_ID" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_ID") %>' Visible="false"></asp:Label>

                            <asp:Label ID="lblPTId" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_PT_ID") %>'></asp:Label>

                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_PT_NAME") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Place">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblPlace" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_PLACE") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Mobile No">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEvaluation" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblMobile" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_MOBILE") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_DATEDesc") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="St Time">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblStartTime" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_START_TIME") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reach Time ">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton5" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblReachTime" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_END_TIME") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Staff ID">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton6" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblStaffID" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_STAFF_ID") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Staff Name">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton7" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblStaffName" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_STAFF_NAME") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                  <asp:TemplateField HeaderText="Prodedure">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton10" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblProdedureDesc" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_PROCEDURE_DESC") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblProdedureCode" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_PROCEDURE_CODE") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton8" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblgvStatus" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_STATUS") %>' Visible="false"></asp:Label>
                            <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_STATUSDesc") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Assigner">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton9" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblAssigner" CssClass="GridRow" runat="server" Text='<%# Bind("HSAM_ASSIGNER") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="70px">
                    <ItemTemplate>
                        <asp:ImageButton ID="Delete" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')" ImageUrl="~/Images/icon_delete.png"
                            OnClick="Delete_Click" />&nbsp;&nbsp;
                                                
                    </ItemTemplate>
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

        </asp:GridView>
    </div>
    <br />
    <table width="50%" border="0">
        <tr>
            <td class="lblCaption1" style="height: 30px; width: 150px;">File No<span style="color: red;">* </span>

            </td>
            <td style="height: 30px; width: 170px;">
                <asp:TextBox ID="txtFileNo" runat="server" Width="150px" MaxLength="10" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="Red" ondblclick="return PatientPopup('FileNo',this.value);" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>

            </td>
            <td class="lblCaption1" style="height: 30px;">Name
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtFName" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('Name',this.value);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Place
            </td>
            <td style="height: 30px; width: 150px;">
                <asp:TextBox ID="txtPlace" runat="server" Width="150px" MaxLength="10" CssClass="label" BorderColor="#CCCCCC" BorderWidth="1px" ondblclick="return PatientPopup('FileNo',this.value);" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>

            </td>
            <td class="lblCaption1" style="height: 30px;">Mobile
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtMobile" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('Name',this.value);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Date<span style="color: red;">* </span>
            </td>
            <td style="height: 30px; width: 150px;">
                <asp:TextBox ID="txtVisitDate" runat="server" Width="105px" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtVisitDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtVisitDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

            </td>
            <td class="lblCaption1" style="height: 30px;">Start Time
            </td>
            <td class="lblCaption1">
                <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                Reach Time
               <asp:DropDownList ID="drpFIHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                :
               <asp:DropDownList ID="drpFIMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Assigned Staff  
            </td>
            <td>
                <asp:TextBox ID="txtStaffID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="105px" onblur="return DRIdSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtStaffID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"></asp:AutoCompleteExtender>

            </td>
            <td colspan="2">
                <asp:TextBox ID="txtStaffName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="400px" MaxLength="50" onblur="return DRNameSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtStaffName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"></asp:AutoCompleteExtender>

            </td>
        </tr>

        <tr>
             <td class="lblCaption1" style="height: 30px;">Procedure 
            </td>
            <td style="width: 120px">
                
                <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" onblur="return ServIdSelected()"></asp:TextBox>
            </td>
            <td style="width: 480px">
                <asp:TextBox ID="txtServName" runat="server" Width="400px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                <div id="divwidth" style="visibility: hidden;"></div>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                </asp:AutoCompleteExtender>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                </asp:AutoCompleteExtender>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Status
            </td>
            <td>
                <asp:DropDownList ID="drpStatus" runat="server" CssClass="label" Width="150px" BorderWidth="1px" BorderColor="#CCCCCC">
                    <asp:ListItem Value="I" Selected="true">InProgress</asp:ListItem>
                    <asp:ListItem Value="P">Pending</asp:ListItem>
                    <asp:ListItem Value="C">Completed</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>

       
        <tr>
            <td class="lblCaption1" style="height: 30px;">Comment
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtComment" runat="server" Width="100%" CssClass="label" BorderWidth="1px" Height="50px" TextMode="MultiLine" BorderColor="#cccccc"></asp:TextBox>

            </td>
        </tr>
    </table>
    <table width="50%">
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
                <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnClear_Click" Text="Clear" />

            </td>
        </tr>
    </table>
</asp:Content>
