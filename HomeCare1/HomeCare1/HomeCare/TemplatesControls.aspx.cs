﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.HomeCare
{
    public partial class TemplatesControls : System.Web.UI.Page
    {
        public StringBuilder strData = new StringBuilder();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindControls()
        {

            TemplatesBAL obj = new TemplatesBAL();
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ET_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ET_TYPE ='" + hidCtrlName.Value + "'";
            ds = obj.GetEMRTemplates(Criteria);

            strData.Append(" <ul >");


            if (ds.Tables[0].Rows.Count > 0)
            {
                for (Int32 i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    strData.Append("<li class='lblCaption1 Content'>");
                    strData.Append("<input type='checkbox' id='chk" + Convert.ToString(ds.Tables[0].Rows[i]["ET_TEMPLATE"]).Trim() + "' value='" + Convert.ToString(ds.Tables[0].Rows[i]["ET_TEMPLATE"]).Trim() + "'/>");
                    strData.Append("<label for='chk" + Convert.ToString(ds.Tables[0].Rows[i]["ET_TEMPLATE"]).Trim() + "'>" + Convert.ToString(ds.Tables[0].Rows[i]["ET_TEMPLATE"]).Trim() + " </label>");
                    strData.Append("</li>");

                }

            }

            strData.Append(" </ul>");

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    //string strMode;
                    //strMode = Convert.ToString(Request.QueryString["Mode"]);
                    //if (strMode == "Show")
                    //{


                    //}

                    //ViewState["Template"] = Convert.ToString(Session["Template"]);
                    //Session["Template"] = "";

                    BindControls();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      TemplatesPopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }
    }
}