﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.master" AutoEventWireup="true" CodeBehind="PhysicalTherapy.aspx.cs" Inherits="HomeCare1.HomeCare.PhysicalTherapy" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function PhysicalTherapyPopup(CtrlName, strValue) {
            var win = window.open("PhysicalTherapyPopup.aspx?PageName=PhysicalTherapy&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=570,width=950,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();


        }



        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strInjuryDate = document.getElementById('<%=txtInjuryDate.ClientID%>').value
            if (/\S+/.test(strInjuryDate) == true) {
                if (isDate(document.getElementById('<%=txtInjuryDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtInjuryDate.ClientID%>').focus()
                    return false;
                }
            }

            var strSugeryDate = document.getElementById('<%=txtSugeryDate.ClientID%>').value
            if (/\S+/.test(strSugeryDate) == true) {
                if (isDate(document.getElementById('<%=txtSugeryDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtSugeryDate.ClientID%>').focus()
                    return false;
                }
            }
            var strTherapyDate = document.getElementById('<%=txtTherapyDate.ClientID%>').value
            if (/\S+/.test(strTherapyDate) == true) {
                if (isDate(document.getElementById('<%=txtTherapyDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtTherapyDate.ClientID%>').focus()
                    return false;
                }
            }



        }
    </script>

</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
    <div >
   
    <table >
        <tr>
            <td class="PageHeader">Physical Therapy Evaluation
               
            </td>
        </tr>
    </table>

    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <asp:HiddenField ID="hidHHPH_ID" runat="server" Value="0" />
         <table width="95%">
             <tr>
                <td style="float: right; padding-right: 20px;">
                    <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="button orange small" Width="70px" OnClick="btnDelete_Click" Visible="false"  Text="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"   />
                    <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnClear_Click" Text="Clear" />
                </td>
             </tr>
          </table>
        
         <table width="100%" cellpadding="5px" cellspacing="5px">
              <tr>
                                <td class="lblCaption1" style="width:50% ;height:30px;">  Date Of Injury : 
                                                           <asp:TextBox ID="txtInjuryDate" runat="server" Width="75px" CssClass="label" BackColor="#ffffff" BorderWidth="1px" BorderColor="#CCCCCC"           ></asp:TextBox>
                                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtInjuryDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtInjuryDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                </td>

                                <td class="lblCaption1" style="width:50%">Date of Surgery :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                       <asp:TextBox ID="txtSugeryDate" runat="server" Width="75px" CssClass="label" BackColor="#ffffff" BorderWidth="1px" BorderColor="#CCCCCC"           ></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtSugeryDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtSugeryDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

 
                                </td>
                            </tr>

               <tr>
                                 <td class="lblCaption1" >
                                     <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1">  Chief Complaint / Ailment / Injury: </span><br />
                             <asp:TextBox ID="txtChiefCoplaint" runat="server"  CssClass="label"  BorderWidth="1px" BorderColor="#CCCCCC"     Width="100%"  Height="50px"  TextMode="MultiLine"      ></asp:TextBox>
                                     </div>
                                 </td>
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                            <span class="lblCaption1" style="width:150px;">Briefly Describe How you were Injured:</span>
                                            <asp:TextBox ID="txtInjuryRemarks" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#CCCCCC"     Width="100%"  Height="50px"  TextMode="MultiLine"      ></asp:TextBox>

                                        </div>

                                  </td>

                             </tr>

             <tr>
                                 <td class="lblCaption1" >
                                     <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1">    Have you received therapy for this condition? : </span><br />
                                          <asp:RadioButtonList ID="radTherapyReceived" runat="server" CssClass="label"  Width="150px"  RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="True"  ></asp:ListItem>
                                            <asp:ListItem Text="No" Value="False"  ></asp:ListItem>
                                        </asp:RadioButtonList>
                                       <br />
                                          When
                                         <asp:TextBox ID="txtTherapyDate" runat="server" Width="75px"  CssClass="label" BackColor="#ffffff"  BorderWidth="1px" BorderColor="#CCCCCC"          ></asp:TextBox>
                                         <asp:CalendarExtender ID="CalendarExtender2" runat="server"  TargetControlID="txtTherapyDate"  Format="dd/MM/yyyy" ></asp:CalendarExtender>
                                         <asp:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="txtTherapyDate" Mask="99/99/9999"  MaskType="Date"></asp:MaskedEditExtender>
                                          &nbsp;&nbsp;&nbsp;&nbsp; How many Visits?
                                           <asp:TextBox ID="txtTherapyVisitCount" runat="server" Width="50px" MaxLength="3"  CssClass="label" BackColor="#ffffff"  BorderWidth="1px" BorderColor="#CCCCCC"    onkeypress="return OnlyNumeric(event);"  ></asp:TextBox>
                                       
                                          </div>
                                 </td>
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                            <span class="lblCaption1" style="width:150px;"> Has your condition been getting :</span>
                                                <asp:RadioButtonList ID="radcondition" runat="server" CssClass="label" Width="215px" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Worse" Value="Worse"></asp:ListItem>
                                            <asp:ListItem Text="Same" Value="Same"  ></asp:ListItem>
                                            <asp:ListItem Text="Better" Value="Better"  ></asp:ListItem>
                                        </asp:RadioButtonList>
                                            <br />
                                                Are You symptoms :
                                             <asp:RadioButtonList ID="radSymptoms" runat="server" CssClass="label" Width="170px" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Constant" Value="Constant"></asp:ListItem>
                                            <asp:ListItem Text="Intermittent" Value="Intermittent"  ></asp:ListItem>
                                
                                        </asp:RadioButtonList>
                                        </div>

                                  </td>

                             </tr>
              <tr>
                                 <td class="lblCaption1" >
                                     <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1">   Mark the number that best corresponds to your pain : </span><br />
                                          At Best :
                                         <br />
                                          <asp:RadioButtonList ID="radAtBest" runat="server" CssClass="label" Width="400px" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="1" Value="1"  ></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"  ></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"  ></asp:ListItem>
                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="7" Value="7"  ></asp:ListItem>
                                            <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="9" Value="9"  ></asp:ListItem>
                                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                               
                                        </asp:RadioButtonList>
                                         <br />  At Worst :
                                          <asp:RadioButtonList ID="radAtWorst" runat="server" CssClass="label" Width="400px" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1" Value="1"  ></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"  ></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"  ></asp:ListItem>
                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="7" Value="7"  ></asp:ListItem>
                                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="9" Value="9"  ></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                               
                                            </asp:RadioButtonList>

                                     </div>
                                 </td>
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                            <span class="lblCaption1" style="width:150px;"> What decreases/makes your condition better? (mark all that apply):</span>
                                            <br />
                                              <asp:CheckBox id="chkBetterBending" runat="server" CssClass="label" width="150px" Text="Bending" ></asp:CheckBox>
                                                <asp:CheckBox id="chkBetterMovement" runat="server" CssClass="label"  width="100px"  Text="Movement"></asp:CheckBox>
                                                <asp:CheckBox id="chkBetterRest" runat="server" CssClass="label" width="100px" Text="Rest"></asp:CheckBox>
                                                <asp:CHeckBox id="chkBetterInAm" runat="server" CssClass="label" width="150px" Text="Better In AM"></asp:CHeckBox>
                                            <br /> <br />
                                             <asp:CheckBox id="chkBetterSitting" runat="server" CssClass="label" width="150px" Text="Sitting" ></asp:CheckBox>
                                            <asp:CheckBox id="chkBetterStanding" runat="server" CssClass="label"  width="100px"  Text="Standing"></asp:CheckBox>
                                            <asp:CheckBox id="chkBetterHeat" runat="server" CssClass="label" width="100px" Text="Heat"></asp:CheckBox>
                                            <asp:CHeckBox id="chkBetterAsDay" runat="server" CssClass="label" width="200px" Text="Better Ad Day Progress"></asp:CHeckBox>
                                            <br /> <br />
                                             <asp:CheckBox id="chkBetterRising" runat="server" CssClass="label" width="150px" Text="Rising" ></asp:CheckBox>
                                            <asp:CheckBox id="chkBetterWalking" runat="server" CssClass="label"  width="100px"  Text="Walking"></asp:CheckBox>
                                            <asp:CheckBox id="chkBetterICE" runat="server" CssClass="label" width="100px" Text="ICE"></asp:CheckBox>
                                            <asp:CHeckBox id="chkBetterInPM" runat="server" CssClass="label" width="150px" Text="Better In PM"></asp:CHeckBox>
                                            <br /> <br />
                                             <asp:CheckBox id="chkBetterChangingPosit" runat="server" CssClass="label" width="150px" Text="Changing Positions" ></asp:CheckBox>
                                            <asp:CheckBox id="chkBetterLying" runat="server" CssClass="label"  width="100px"  Text="Lying"></asp:CheckBox>
                                            <asp:CHeckBox id="chkBetterMedication" runat="server" CssClass="label" width="100px" Text="Medication"></asp:CHeckBox>
                                            <asp:CheckBox id="chkBetterCough" runat="server" CssClass="label" width="200px" Text="N/A Cast Just Removed"></asp:CheckBox>


                                        </div>

                                  </td>

                             </tr>

               <tr>
                                 <td class="lblCaption1" >
                                     <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1">   What increases/makes your condition worse? (mark all that apply): </span><br />
                                        <asp:CheckBox id="chkWorseBending" runat="server" CssClass="label" width="100px" Text="Bending" ></asp:CheckBox>
                                        <asp:CheckBox id="chkWorseMovement" runat="server" CssClass="label"  width="100px"  Text="Movement"></asp:CheckBox>
                                        <asp:CheckBox id="chkWorseRest" runat="server" CssClass="label" width="100px" Text="Rest"></asp:CheckBox>
                                        <asp:CHeckBox id="chkWorseSneeze" runat="server" CssClass="label" width="100px" Text="Sneeze"></asp:CHeckBox>
                                         <br /><br />
                                           <asp:CheckBox id="chkWorseSitting" runat="server" CssClass="label" width="100px" Text="Sitting" ></asp:CheckBox>
                                            <asp:CheckBox id="chkWorseStanding" runat="server" CssClass="label"  width="100px"  Text="Standing"></asp:CheckBox>
                                            <asp:CheckBox id="chkWorseStairs" runat="server" CssClass="label" width="100px" Text="Stairs"></asp:CheckBox>
                                            <asp:CHeckBox id="chkWorseBreath" runat="server" CssClass="label" width="100px" Text="Beep Breath"></asp:CHeckBox>
                                          <br /><br />
                                          <asp:CheckBox id="chkWorseRising" runat="server" CssClass="label" width="100px" Text="Rising" ></asp:CheckBox>
                                        <asp:CheckBox id="chkWorseWalking" runat="server" CssClass="label"  width="100px"  Text="Walking"></asp:CheckBox>
                                        <asp:CheckBox id="chkWorseCough" runat="server" CssClass="label" width="100px" Text="Cough"></asp:CheckBox>
                                        <asp:CHeckBox id="chkWorseMedication" runat="server" CssClass="label" width="100px" Text="Medication"></asp:CHeckBox>

                                     </div>
                                 </td>
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                            <span class="lblCaption1" style="width:150px;">  Previous medical intervention (mark all that apply): </span> <br />
                                             <asp:CheckBox id="chkPrevIntXray" runat="server" CssClass="label" width="150px" Text="X-RAY MRI" ></asp:CheckBox>
                                            <asp:CheckBox id="chkPrevIntCATScan" runat="server" CssClass="label"  width="150px"  Text="CATSCAN"></asp:CheckBox>
                                            <asp:CheckBox id="chkPrevIntInjections" runat="server" CssClass="label" width="150px" Text="Injections"></asp:CheckBox>
                                           <br /> <br />
                                            Other:
                                          <input type="text" runat="server" id="txtPrevIntOther" class="label"  style="width:100%;border:thin;border-style:groove;border-color:#CCCCCC;" />


                                        </div>

                                  </td>

                             </tr>

             
               <tr>
                                 <td class="lblCaption1" >
                                     <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1">    What are your goals to be achieved by the end of therapy? : </span><br />
                                         <textarea id="txtGoals" runat="server" style="height:50px;width:100%;border:thin;border-style:groove;border-color:#CCCCCC;"    class="label"></textarea>


                                     </div>
                                 </td>
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1">    Previous Surgeries: </span><br />
                                         <textarea id="txtPrevSurgeries" runat="server" style="height:50px;width:100%;border:thin;border-style:groove;border-color:#CCCCCC;"    class="label"></textarea>


                                     </div>

                                  </td>

                             </tr>

            

              <tr>
                                 <td class="lblCaption1" >
                                     <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1">    Medications: </span><br />

                                                        <textarea id="txtMedications" runat="server" style="height:50px;width:100%;border:thin;border-style:groove;border-color:#CCCCCC;"    class="label"></textarea>

                                     </div>
                                 </td>
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                            <span class="lblCaption1" style="width:150px;">   Allergies:</span> <br />
               <textarea id="txtAllergies" runat="server" style="height:50px;width:100%;border:thin;border-style:groove;border-color:#CCCCCC;"    class="label"></textarea>

                                        </div>

                                  </td>

                             </tr>

               <tr>
                              
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                            <span class="lblCaption1" style="width:150px;">   Other:</span> <br />
                                         <textarea id="txtOther" runat="server" style="height:50px;width:100%;border:thin;border-style:groove;border-color:#CCCCCC;"    class="label"></textarea>


                                        </div>

                                  </td>
                      <td class="lblCaption1" >
                                    
                                 </td>

                             </tr>
             
             </table>
      <table width="100%" border="0">
            
          
          <tr>
              <td colspan="6" class="lblCaption1" style="height:30px;">
                 Medical information (mark all that apply)**this information is confidential and remains part of your chart
              </td>
          </tr>
          <tr>
              <td   style="height:30px;"></td>
              <td colspan="5">
                    <asp:CheckBox id="chkDifficultySwallowing" runat="server" CssClass="lblCaption1" width="150px" Text="Difficulty Swallowing" ></asp:CheckBox>
                    <asp:CheckBox id="chkMotionSickness" runat="server" CssClass="lblCaption1"  width="150px"  Text="Motion Sickness"></asp:CheckBox>
                    <asp:CheckBox id="chkStroke" runat="server" CssClass="lblCaption1" width="150px" Text="Stroke"></asp:CheckBox>
                   <asp:CheckBox id="chkArthritis" runat="server" CssClass="lblCaption1" width="150px" Text="Arthritis" ></asp:CheckBox>
              </td>
          </tr>
          <tr>
              <td   style="height:30px;"></td>
              <td colspan="5">
                    <asp:CheckBox id="chkFeverChillsSweats" runat="server" CssClass="lblCaption1"  width="150px"  Text="Fever/Chills/Sweats"></asp:CheckBox>
                    <asp:CheckBox id="chkOsteoporosis" runat="server" CssClass="lblCaption1" width="150px" Text="Osteoporosis"></asp:CheckBox>
                    <asp:CheckBox id="chkHighBP" runat="server" CssClass="lblCaption1"  width="150px"  Text="High Blood Pressure"></asp:CheckBox>
                    <asp:CheckBox id="chkUnexplainedWeightLoss" runat="server" CssClass="lblCaption1" width="200px" Text="Unexplained Weight Loss" ></asp:CheckBox>
              </td>
          </tr>
          <tr>
              <td   style="height:30px;"></td>
              <td colspan="5">
                    <asp:CheckBox id="chkAnemia" runat="server" CssClass="lblCaption1" width="150px" Text="Anemia"></asp:CheckBox>
                     <asp:CheckBox id="chkHeartTrouble" runat="server" CssClass="lblCaption1"  width="150px"  Text="Heart Trouble"></asp:CheckBox>
                    <asp:CheckBox id="chkBloodClots" runat="server" CssClass="lblCaption1" width="150px" Text="Blood Clots" ></asp:CheckBox>
                    <asp:CheckBox id="chkBleedingProblems" runat="server" CssClass="lblCaption1" width="150px" Text="Bleeding Problems"></asp:CheckBox>
              </td>
          </tr>
          <tr>
              <td   style="height:30px;"></td>
              <td colspan="5">
                    <asp:CheckBox id="chkPacemaker" runat="server" CssClass="lblCaption1"  width="150px"  Text="Pacemaker"></asp:CheckBox>
                    <asp:CheckBox id="chkShortnessOfBreath" runat="server" CssClass="lblCaption1" width="150px" Text="Shortness Of Breath" ></asp:CheckBox>
                    <asp:CheckBox id="chkHIVHepatitis" runat="server" CssClass="lblCaption1" width="150px" Text="HIV/Hepatitis"></asp:CheckBox>
                    <asp:CheckBox id="chkEpilepsySeizures" runat="server" CssClass="lblCaption1"  width="150px"  Text="Epilepsy/Seizures"></asp:CheckBox>
              </td>
          </tr>
          <tr>
              <td   style="height:30px;"></td>
              <td colspan="5">
                 
                    <asp:CheckBox id="chkHistorySmoking" runat="server" CssClass="lblCaption1" width="150px" Text="History Of Smoking" ></asp:CheckBox>
                    <asp:CheckBox id="chkHistoryAlcoholAbuse" runat="server" CssClass="lblCaption1" width="150px" Text="Hist.Of Alcohol Abuse"></asp:CheckBox>
                    <asp:CheckBox id="chkHistoryDrugAbuse" runat="server" CssClass="lblCaption1"  width="150px"  Text="Hist. Of Drug Abuse"></asp:CheckBox>
                    <asp:CheckBox id="chkDiabetes" runat="server" CssClass="lblCaption1" width="150px" Text="Diabetes" ></asp:CheckBox>
              </td>
          </tr>
         <tr>
              <td   style="height:30px;"></td>
              <td colspan="5">
                    <asp:CheckBox id="chkDepressionAnxiety" runat="server" CssClass="lblCaption1" width="150px" Text="Depression/Anxiety"></asp:CheckBox>
                    <asp:CheckBox id="chkMyofascialPain" runat="server" CssClass="lblCaption1"  width="150px"  Text="Myofascial Pain"></asp:CheckBox>
                    <asp:CheckBox id="chkFibromyalgia" runat="server" CssClass="lblCaption1" width="150px" Text="Fibromyalgia" ></asp:CheckBox>
                    <asp:CheckBox id="chkPregnancy" runat="server" CssClass="lblCaption1" width="150px" Text="Pregnancy"></asp:CheckBox>
                 
              </td>
          </tr>
         <tr>
              <td   style="height:30px;"></td>
              <td colspan="5">
                     <asp:CheckBox id="chkcancer" runat="server" CssClass="lblCaption1"  width="150px"  Text="Cancer"></asp:CheckBox>
                   
              </td>
          </tr> 
          
          </table>
         
    <br /><br/>

    </div>
</asp:content>

