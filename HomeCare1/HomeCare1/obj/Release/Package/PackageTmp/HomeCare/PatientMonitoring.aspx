﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.master" AutoEventWireup="true" CodeBehind="PatientMonitoring.aspx.cs" Inherits="HomeCare1.HomeCare.PatientMonitoring" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function SaveProgNotesVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strProgressNotes = document.getElementById('<%=txtProgressNotes.ClientID%>').value
            if (/\S+/.test(strProgressNotes) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Progress Notes";
                document.getElementById('<%=txtProgressNotes.ClientID%>').focus;
                return false;
            }



        }


        function SaveEmerNotesVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strEmergencyNotes = document.getElementById('<%=txtEmergencyNotes.ClientID%>').value
            if (/\S+/.test(strEmergencyNotes) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Emergency Notes";
                document.getElementById('<%=txtEmergencyNotes.ClientID%>').focus;
                return false;
            }



        }


        function SaveAddiNotesVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strAdditionalNotes = document.getElementById('<%=txtAdditionalNotes.ClientID%>').value
            if (/\S+/.test(strAdditionalNotes) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Additional Notes";
                document.getElementById('<%=txtAdditionalNotes.ClientID%>').focus;
                return false;
            }



        }




    </script>
    
    <script language="javascript" type="text/javascript">


        function TemplatesPopup(CtrlName, obj) {
            document.getElementById('hidCurElement').value = obj.getAttribute("id");
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName + "&Mode=Show", "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
            }
            return true;

        }

        function TemplateSave(CtrlName, obj) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                document.getElementById('hidCurElement').value = jQuery(obj).parent().find('textarea').attr('id');
                var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName, "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
            }
            return true;
        }

        function GetTemplateData() {
            return (document.getElementById(document.getElementById("hidCurElement").value).value);
        }

        function BindTemplate(ctrlName, strTemplate) {
            if (ctrlName != '') {
                var ctrlData = document.getElementById(document.getElementById('hidCurElement').value).value;
                document.getElementById(document.getElementById('hidCurElement').value).value = ctrlData + strTemplate;
            }

        }



    </script>

</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
     <input type="hidden" id="hidCurElement" name="hidCurElement" value="" />
    <table >
        <tr>
            <td class="PageHeader">Patient Monitoring
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
     <asp:HiddenField ID="hidHHV_ID" runat="server" Value="0" />
    
      <div  style="padding:5px; width: 100%;   overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" Progress Notes" Width="100%">
            <contenttemplate>
                    <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">

                     
                              <asp:GridView ID="gvProgNotes" runat="server"   AutoGenerateColumns="False"  OnRowDataBound="gvProgNotes_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200"   >
                                <HeaderStyle CssClass="GridHeader"  Font-Bold="True" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                   <asp:TemplateField HeaderText="No">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSerial" runat="server" OnClick="SelectProgNotes_Click" >
                                                         <asp:Label ID="lblProgId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_ID") %>' visible="false" ></asp:Label>
                                                         <asp:Label ID="lblStatus" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_STATUS") %>' visible="false" ></asp:Label>
                                                         <asp:Label ID="lblProgDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_MONITORING_DATEDesc") %>' visible="false" ></asp:Label>
                                                         <asp:Label ID="lblProgTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("MonitoringTime") %>' visible="false" ></asp:Label>
                                                        <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                                <HeaderStyle Width="40px" />

                                  </asp:TemplateField>

                                <asp:TemplateField HeaderText="Progress Notes">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkProgressNotes" runat="server" OnClick="SelectProgNotes_Click"  >
                                             <asp:Label ID="lblProgressNotes" CssClass="GridRow" runat="server" Text='<%# Bind("HHN_NOTES") %>'  ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkModUser" runat="server" OnClick="SelectProgNotes_Click" Text='<%# Bind("CreatedStaffName") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="200px" />
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Date & Time">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDate" runat="server" OnClick="SelectProgNotes_Click"  Text='<%# Bind("HHN_MONITORING_DATE_TimeDesc") %>' > </asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="DeleteProgNotes" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="DeleteProgNotes_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                               </asp:TemplateField>
                            </Columns>
                              <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

                        </asp:GridView>

                    </div>
                <br />
                 <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >  Progress Notes</span> 
                                     <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HHN_ProgressNotes', this)"  value="Template" />
                                   <br />
                                <asp:TextBox ID="txtProgressNotes" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#CCCCCC"  ondblclick="return TemplatesPopup('HC_HHN_ProgressNotes',this);"  ></asp:TextBox>
                     <br />
                                  <span class="lblCaption1" >    Date & Time : </span>  <span style="color:red;">* </span>
                                   <asp:textbox id="txtProgDate" runat="server" width="70px" height="20px" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                                    <asp:calendarextender id="CalendarExtender1" runat="server"
                                        enabled="True" targetcontrolid="txtProgDate" format="dd/MM/yyyy">
                                     </asp:calendarextender>
                                    <asp:maskededitextender id="MaskedEditExtender2" runat="server" enabled="true" targetcontrolid="txtProgDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                                    <asp:dropdownlist id="drpProgHour" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                    <asp:dropdownlist id="drpProgMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                            </div>
                 <table width="100%"">
                   
                    <tr>
                        <td>
                    <asp:Button ID="btnSaveProgNotes" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSaveProgNotes_Click" OnClientClick="return SaveProgNotesVal();" Text="Save" />
                    <asp:Button ID="btnClearProgNotes" runat="server" CssClass="button orange small" Width="100px" OnClick="btnClearProgNotes_Click" Text="Clear" />

                        </td>
                    </tr>
                </table>
                 
            </contenttemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Emergency Notes" Width="100%">
            <contenttemplate>
                <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">

                     
                              <asp:GridView ID="gvEmerNotes" runat="server"   AutoGenerateColumns="False"  OnRowDataBound="gvEmerNotes_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200"   >
                                <HeaderStyle CssClass="GridHeader"  Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No" HeaderStyle-Width="40px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SelectgvEmerNotes_Click" >
                                                         <asp:Label ID="lblEmerId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_ID") %>' visible="false" ></asp:Label>
                                                         <asp:Label ID="lblStatus" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_STATUS") %>' visible="false" ></asp:Label>
                                                        <asp:Label ID="lblEmerDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_MONITORING_DATEDesc") %>' visible="false" ></asp:Label>
                                                         <asp:Label ID="lblEmerTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("MonitoringTime") %>' visible="false" ></asp:Label>

                                                        <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                  </asp:TemplateField>

                                <asp:TemplateField HeaderText="Emergency Notes">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEmergencyNotes" runat="server" OnClick="SelectgvEmerNotes_Click" >
                                             <asp:Label ID="lblEmergencyNotes" CssClass="GridRow"  runat="server" Text='<%# Bind("HHN_NOTES") %>'   ></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name" HeaderStyle-Width="200px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton3" runat="server" OnClick="SelectgvEmerNotes_Click" Text='<%# Bind("CreatedStaffName") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Date & Time" HeaderStyle-Width="130px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton4" runat="server" OnClick="SelectProgNotes_Click"  Text='<%# Bind("HHN_MONITORING_DATE_TimeDesc") %>' > </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="70px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="DeleteEmeriNotes" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="DeleteEmerNotes_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                               </asp:TemplateField>
                            </Columns>
                              <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                        </asp:GridView>

                    </div>
                <br />
                   <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >  Emergency Notes</span> 
                                     <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HHN_EmergencyNotes', this)"  value="Template" />
                               
                                   <br />
                                <asp:TextBox ID="txtEmergencyNotes" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HHN_EmergencyNotes',this);"  ></asp:TextBox>
                    <br />
                                  <span class="lblCaption1" >    Date & Time : </span>  <span style="color:red;">* </span>
                                   <asp:textbox id="txtEmerDate" runat="server" width="70px" height="20px" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                                    <asp:calendarextender id="CalendarExtender2" runat="server"
                                        enabled="True" targetcontrolid="txtEmerDate" format="dd/MM/yyyy">
                                     </asp:calendarextender>
                                    <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtEmerDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                                    <asp:dropdownlist id="drpEmerHour" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                    <asp:dropdownlist id="drpEmerMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                            </div>

                 <table width="95%">
                   
                    <tr>
                        <td>
                    <asp:Button ID="btnSaveEmerNotes" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSaveEmerNotes_Click" OnClientClick="return SaveEmerNotesVal();" Text="Save" />
                    <asp:Button ID="btnClearEmerNotes" runat="server" CssClass="button orange small" Width="100px" OnClick="btnClearEmerNotes_Click" Text="Clear" />

                        </td>
                    </tr>
                </table>
                
            </contenttemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText=" Additional Notes " Width="100%">
            <contenttemplate>
                  <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">

                     
                              <asp:GridView ID="gvAddiNotes" runat="server"   AutoGenerateColumns="False"  OnRowDataBound="gvAddiNotes_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200"   >
                                <HeaderStyle CssClass="GridHeader"  Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                  <asp:TemplateField HeaderText="No" HeaderStyle-Width="40px" >
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="SelectgvAddiNotes_Click" >
                                                         <asp:Label ID="lblAddiId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_ID") %>' visible="false" ></asp:Label>
                                                        <asp:Label ID="lblStatus" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_STATUS") %>' visible="false" ></asp:Label>
                                                         <asp:Label ID="lblAddiDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_MONITORING_DATEDesc") %>' visible="false" ></asp:Label>
                                                         <asp:Label ID="lblAddiTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("MonitoringTime") %>' visible="false" ></asp:Label>
                                                        <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHN_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                  </asp:TemplateField>

                                <asp:TemplateField HeaderText="Additional Notes">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkAdditionalNotes" runat="server" OnClick="SelectgvAddiNotes_Click">
                                           <asp:Label ID="lblAdditionalNotes" CssClass="GridRow"  runat="server" Text='<%# Bind("HHN_NOTES") %>' ></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name" HeaderStyle-Width="200px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton7" runat="server" OnClick="SelectgvAddiNotes_Click" Text='<%# Bind("CreatedStaffName") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Date & Time" HeaderStyle-Width="130px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton8" runat="server" OnClick="SelectProgNotes_Click"  Text='<%# Bind("HHN_MONITORING_DATE_TimeDesc") %>' > </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="70px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="DeleteAddiNotes" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="DeleteAddiNotes_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                               </asp:TemplateField>
                            </Columns>
                              <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                        </asp:GridView>

                    </div>
                 <br />
                   <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >     Additional Notes</span> 
                                <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HHN_AdditionalNotes', this)"  value="Template" />
                               
                                   <br />
                                <asp:TextBox ID="txtAdditionalNotes" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HHN_AdditionalNotes',this);"  ></asp:TextBox>

            <br />
                                  <span class="lblCaption1" >    Date & Time : </span>  <span style="color:red;">* </span>
                                   <asp:textbox id="txtAddiDate" runat="server" width="70px" height="20px" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                                    <asp:calendarextender id="CalendarExtender3" runat="server"
                                        enabled="True" targetcontrolid="txtAddiDate" format="dd/MM/yyyy">
                                     </asp:calendarextender>
                                    <asp:maskededitextender id="MaskedEditExtender3" runat="server" enabled="true" targetcontrolid="txtAddiDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                                    <asp:dropdownlist id="drpAddiHour" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                    <asp:dropdownlist id="drpAddiMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                            </div>
                 <table width="95%">
                   
                    <tr>
                        <td>
                    <asp:Button ID="btnSaveAddiNotes" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSaveAddiNotes_Click" OnClientClick="return SaveAddiNotesVal();" Text="Save" />
                    <asp:Button ID="btnClearAddiNotes" runat="server" CssClass="button orange small" Width="100px" OnClick="btnClearAddiNotes_Click" Text="Clear" />

                        </td>
                    </tr>
                </table>
                

        </contenttemplate>
        </asp:TabPanel>

    </asp:TabContainer>
          </div>
</asp:content>

