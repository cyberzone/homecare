﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.master" AutoEventWireup="true" CodeBehind="NurseCarePlan.aspx.cs" Inherits="HomeCare1.HomeCare.NurseCarePlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";





        }
    </script>

    <script language="javascript" type="text/javascript">


        function TemplatesPopup(CtrlName, obj) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                document.getElementById('hidCurElement').value = obj.getAttribute("id");
                var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName + "&Mode=Show", "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
            }
            return true;

        }

        function TemplateSave(CtrlName, obj) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                document.getElementById('hidCurElement').value = jQuery(obj).parent().find('textarea').attr('id');
                var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName, "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
            }
            return true;
        }

        function GetTemplateData() {
            return (document.getElementById(document.getElementById("hidCurElement").value).value);
        }

        function BindTemplate(ctrlName, strTemplate) {
            if (ctrlName != '') {
                var ctrlData = document.getElementById(document.getElementById('hidCurElement').value).value;
                document.getElementById(document.getElementById('hidCurElement').value).value = ctrlData + strTemplate;
            }

        }



    </script>

</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
      <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidCurElement" name="hidCurElement" value="" />
    <input type="hidden" id="hidPTID" runat="server" />
       <table>
        <tr>
            <td class="PageHeader">Nursing Care Plan
               
            </td>
        </tr>
    </table>
    <br />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:updatepanel id="UpdatePanel0" runat="server">
                    <contenttemplate>
                     <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </contenttemplate>
                </asp:updatepanel>
            </td>
        </tr>
    </table>
    
  <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">
                 <asp:GridView ID="gvNursingCarePlan" runat="server"   AutoGenerateColumns="False"  OnRowDataBound="gvNursingCarePlan_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200"   >
                                <HeaderStyle CssClass="GridHeader"  Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No" HeaderStyle-Width="40px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Select_Click" >
                                                         <asp:Label ID="lblHNCPId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HNCP_ID") %>' visible="false" ></asp:Label>
                                                        <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HNCP_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                  </asp:TemplateField>

                                <asp:TemplateField HeaderText="Assessment">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEmergencyNotes" runat="server" OnClick="Select_Click" >
                                             <asp:Label ID="lblAssessment" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_ASSESSMENT") %>'   ></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nursing Diagnosis" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Select_Click" >
                                            <asp:Label ID="lblAnalysis" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_ANALYSIS") %>'   ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Implementation Plan" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton4" runat="server" OnClick="Select_Click"  >
                                            <asp:Label ID="lblPlanning" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_PLANNING") %>'   ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Evaluation" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEvaluation" runat="server" OnClick="Select_Click"  >
                                            <asp:Label ID="lblEvaluation" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_EVALUATION") %>'   ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="70px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Delete" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="Delete_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                               </asp:TemplateField>
                            </Columns>
                              <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                        </asp:GridView>

                    </div>
     <br />

    <table width="100%">
        <tr>
            <td style="width:50%" valign="top">
                            <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >   Assessment</span> 
                                      <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HCP_Assessment', this)"  value="Template" />

                                   <br />
                                <asp:TextBox ID="txtAssessment" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HCP_Assessment',this);"  ></asp:TextBox>
                            </div>
                             <br />
                             <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >   Nursing Diagnosis </span> 
                                      <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HCP_Analyis', this)"  value="Template" />
                               
                                   <br />
                                <asp:TextBox ID="txtAnalyis" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HCP_Analyis',this);"  ></asp:TextBox>
                            </div>
                             <br />

                </td>
           <td style="width:50%" valign="top">
                   <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >   Implementation Plan</span> 
                                      <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HCP_Planning', this)"  value="Template" />
                               
                                   <br />
                                <asp:TextBox ID="txtPlanning" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HCP_Planning',this);"  ></asp:TextBox>
                            </div>

                     <br />
                             <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >   Evaluation  </span> 
                                      <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HCP_Evaluation', this)"  value="Template" />
                               
                                   <br />
                                <asp:TextBox ID="txtEvaluation" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HCP_Evaluation',this);"  ></asp:TextBox>
                            </div>
                             <br />
                
            </td>
        </tr>
    </table>
   
    <table width="100%">
         <tr>
            <td>
             <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" OnClientClick="return SaveProgNotesVal();" Text="Save" />
               <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="100px" OnClick="btnClear_Click" Text="Clear" />
            </td>
        </tr>
   </table>

   
    
   

</asp:content>