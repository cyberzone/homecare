﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.master" AutoEventWireup="true" CodeBehind="PatientAssessment.aspx.cs" Inherits="HomeCare1.HomeCare.PatientAssessment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }




        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



        }
    </script>

    <script language="javascript" type="text/javascript">


        function TemplatesCtrlPopup(CtrlName, obj) {
            document.getElementById('hidCurElement').value = obj.getAttribute('id');
            var win = window.open("TemplatesControls.aspx?PageName=PatientAssessment&CtrlName=" + CtrlName, "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();

            return true;

        }

        function BindCtrlData(ctrlName, strCtrlData) {

            if (ctrlName != '') {
                var ctrlData = document.getElementById(document.getElementById('hidCurElement').value).value;

                document.getElementById(document.getElementById('hidCurElement').value).value = ctrlData + strCtrlData
            }


        }

    </script>
       

      <script language="javascript" type="text/javascript">


          function TemplatesPopup(CtrlName, obj) {
              document.getElementById('hidCurElement').value = obj.getAttribute("id");
              var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName + "&Mode=Show", "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
              win.focus();

              return true;

          }

          function TemplateSave(CtrlName, obj) {
              document.getElementById('hidCurElement').value = jQuery(obj).parent().find('textarea').attr('id');
              var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName, "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
              win.focus();
              return true;
          }

          function GetTemplateData() {
              return (document.getElementById(document.getElementById("hidCurElement").value).value);
          }

          function BindTemplate(ctrlName, strTemplate) {
              if (ctrlName != '') {
                  var ctrlData = document.getElementById(document.getElementById('hidCurElement').value).value;
                  document.getElementById(document.getElementById('hidCurElement').value).value = ctrlData + strTemplate;
              }

          }



    </script>
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
      <input type="hidden" id="hidPTID" runat="server" />
    <input type="hidden" id="hidCurElement" name="hidCurElement" value="" />
     <table >
        <tr>
            <td class="PageHeader">Patient Assessment
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    
    <div style="padding: 5px; width: 100%; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">

        <table width="95%">
            <tr>
                <td style="float: right; padding-right: 20px;">
                    <asp:button id="btnSave" runat="server" cssclass="button orange small" width="70px" onclick="btnSave_Click" onclientclick="return SaveVal();" text="Save" />
                    <asp:button id="btnDelete" runat="server" cssclass="button orange small" width="70px" onclick="btnDelete_Click" text="Delete" onclientclick="return window.confirm('Do you want to Delete?')" />
                </td>
            </tr>
        </table>

       <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0" cssclass="ajax__tab_yuitabview-theme" width="100%">
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Health History " Width="100%">
            <contenttemplate>
                  <table>
                    <tr>
                         <td valign="top" style="width:49%">
                 <fieldset>
                    <legend class="lblCaption1" style="font-weight:bold;">Present And / Or past Health Problems</legend>
                   
                    <table >
                          <tr>
                            <td class="lblCaption1">
                                 <asp:textbox id="txtHC_ASS_Health_PresPastHist" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Health_PresPastHist',this)"></asp:textbox>
                                                   
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                 <div style="padding:5px; width: 97%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >    If any problem identified (Please specify)</span> 
                                    <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_PRESPAS_PROB', this)"  value="Template" />
                               
                                   <br />
                                 <asp:TextBox ID="txtPresPastProblem" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HEALTH_PRESPAS_PROB',this);"  ></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td class="lblCaption1">
                                 <div style="padding:5px; width: 97%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >Previous Hospitalizations / If any Surgeries write</span> 
                                    
                                    <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_PRESPAS_PREVPROB', this)"  value="Template" />

                               
                                   <br />
                                 <asp:TextBox ID="txtPresPastSurgeries" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HEALTH_PRESPAS_PREVPROB',this);"  ></asp:TextBox>
                                </div>


                            </td>
                        </tr>
                        <tr>
                            <td style="height:10px;"></td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" >
                                  <div style="padding: 5px; width: 97%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1" style="font-weight:bold;">Alergies:</span> <br />
                                         Medication :
                                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_ALLERGIES_MEDIC', this)"  value="Template" />
                                        <br />
                                         <asp:textbox id="txtAlergiesMedication" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_ALLERGIES_MEDIC',this);" ></asp:textbox><br />
                                        Food  :
                                          
                                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_ALLERGIES_FOOD', this)"  value="Template" />
                                      <br />
                                           <asp:textbox id="txtAlergiesFood" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_ALLERGIES_FOOD',this);" ></asp:textbox><br />
                                       Other  :
                                        
                                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_ALLERGIES_OTHER', this)"  value="Template" />

                                      <br />
                                           <asp:textbox id="txtAlergiesOther" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_ALLERGIES_OTHER',this);" ></asp:textbox><br />

 
                                    </div>

                            </td>
                        </tr>
                    </table>
               
                </fieldset>
                         <td style="width:1%;"></td>
                         <td valign="top" style="width:49%">

                 <fieldset>
                <legend class="lblCaption1" style="font-weight:bold;">Medication History</legend>
             
                     <table>
                    <tr>
                             <td>
                     <span class="lblCaption1"  >  Medicine Name / Dosage / Frequency </span>
                         
                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_MEDICHIST_MEDICINE', this)"  value="Template" />

                         <br />
                         <asp:textbox id="txtHC_ASS_Health_MedicHist_Medicine" cssclass="label" runat="server" width="99%" height="80px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_MEDICHIST_MEDICINE',this);" ></asp:textbox>
                                         
                             </td>
                         </tr>


                         <tr>
                             <td>
                     <span class="lblCaption1"  >  Sleep / Rest </span><br />
                         <asp:textbox id="txtHC_ASS_Health_MedicHist_Sleep" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Health_MedicHist_Sleep',this)"></asp:textbox>
                                         
                             </td>
                         </tr>
                   
                         <tr>
                             <td>
                             <span class="lblCaption1"  >  Psychological Assessment </span><br />
                           <asp:textbox id="txtHC_ASS_Health_MedicHist_PschoyAss" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Health_MedicHist_PschoyAss',this)"></asp:textbox>
                             </td>
                         </tr>
                         <tr><td style="height:10px;"></td></tr>
                       <tr>

                           <td class="lblCaption1"  >
                             <span class="lblCaption1" style="font-weight:bold;" >  Pain Assessment </span><br />
                                 Patient expresses presence of pain   
                                  <asp:RadioButtonList ID="radPainAssYes" runat="server" CssClass="label"  Width="100px"  RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="True"  ></asp:ListItem>
                                        <asp:ListItem Text="No" Value="False"  ></asp:ListItem>
                                  </asp:RadioButtonList>
                                 If Yes, Complete pain assessment form
                           </td>
                       </tr>


                     </table>
                
 
            </fieldset>

                        </td>
                    </tr>
                </table>



              </contenttemplate>
        </asp:TabPanel>
         <asp:TabPanel runat="server" ID="TabPanel2" HeaderText=" Physical Assessment " Width="100%">
            <contenttemplate>
                <table>
                    <tr>
                      <td valign="top" style="width:49%">
                    <fieldset>
                 <legend class="lblCaption1" style="font-weight:bold;">Gastrointestinal</legend>
                     <table>
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Bowel Sounds </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Gast_BowelSounds" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Gast_BowelSounds',this)"></asp:textbox>
                             </td>
                         </tr>  

                         <tr>
                             <td>
                                 <span class="lblCaption1" > Elimination </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Gast_Elimination" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Gast_Elimination',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > General </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Gast_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Gast_General',this)"></asp:textbox>
                             </td>
                         </tr>  
                     </table>
                 </fieldset>

                    <fieldset>
                     <legend class="lblCaption1" style="font-weight:bold;">Skin/Integumentary    </legend>
                         <table>
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Color </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Skin_Color" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Skin_Color',this)"></asp:textbox>
                                 </td>
                             </tr>  

                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Temperature </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Skin_Temperature" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Skin_Temperature',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Lesions </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Skin_Lesions" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Skin_Lesions',this)"></asp:textbox>
                                 </td>
                             </tr>  
                         </table>
                     </fieldset>
                    <fieldset>
                     <legend class="lblCaption1" style="font-weight:bold;">Neurological        </legend>
                         <table>
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > General </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Neuro_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Neuro_General',this)"></asp:textbox>
                                 </td>
                             </tr>  

                             <tr>
                                 <td>
                                     <span class="lblCaption1" >  Level of Consciousness </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Neuro_Conscio" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Neuro_Conscio',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Oriented to </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Neuro_Oriented" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Neuro_Oriented',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Time  Responsivenes </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Neuro_TimeResp" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Neuro_TimeResp',this)"></asp:textbox>
                                 </td>
                             </tr>  


                         </table>
                     </fieldset>

                    <fieldset>
                     <legend class="lblCaption1" style="font-weight:bold;">Cardiovascular        </legend>
                         <table>
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > General </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Cardio_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Cardio_General',this)"></asp:textbox>
                                 </td>
                             </tr>  

                             <tr>
                                 <td>
                                     <span class="lblCaption1" >  Pulse </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Cardio_Pulse" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Cardio_Pulse',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Pedal Pulses </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Cardio_PedalPulse" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Cardio_PedalPulse',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Edema </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Cardio_Edema" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Cardio_Edema',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Nail beds </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Cardio_NailBeds" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Cardio_NailBeds',this)"></asp:textbox>
                                 </td>
                             </tr>  
                              <tr>
                                 <td>
                                     <span class="lblCaption1" > Capillary refill </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Cardio_Capillary" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Cardio_Capillary',this)"></asp:textbox>
                                 </td>
                             </tr>  
                         </table>
                     </fieldset>

                        </td>
                        <td style="width:1%;"></td>
                       <td valign="top" style="width:49%">

                    <fieldset>
                     <legend class="lblCaption1" style="font-weight:bold;">Reproductive</legend>
                         <table>
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Male </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Repro_Male" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Repro_Male',this)"></asp:textbox>
                                 </td>
                             </tr>  

                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Female </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Repro_Female" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Repro_Female',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Breasts </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Repro_Breasts" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Repro_Breasts',this)"></asp:textbox>
                                 </td>
                             </tr>  
                         </table>
                     </fieldset>
                   <fieldset>
                     <legend class="lblCaption1" style="font-weight:bold;">Genitourinary</legend>
                         <table>
                             <tr>
                                 <td>
                                     <asp:textbox id="txtHC_ASS_Phy_Genit_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Genit_General',this)"></asp:textbox>
                                 </td>
                             </tr>  

                         </table>
                     </fieldset>

                    <fieldset>
                     <legend class="lblCaption1" style="font-weight:bold;">EENT & Mouth                       </legend>
                         <table>
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Eyes </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Ent_Eyes" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Ent_Eyes',this)"></asp:textbox>
                                 </td>
                             </tr>  

                             <tr>
                                 <td>
                                     <span class="lblCaption1" >  Ears </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Ent_Ears" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Ent_Ears',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Nose </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Ent_Nose" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Ent_Nose',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Throat / Neck </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Ent_Throat" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Ent_Throat',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             
                         </table>
                     </fieldset>

                           <fieldset>
                     <legend class="lblCaption1" style="font-weight:bold;">Respiratory                     </legend>
                         <table>
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Chest Appearance </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Resp_Chest" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Resp_Chest',this)"></asp:textbox>
                                 </td>
                             </tr>  

                             <tr>
                                 <td>
                                     <span class="lblCaption1" >  Breath Pattern </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Resp_BreathPatt" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Resp_BreathPatt',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Breath Sound </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Resp_BreathSound" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Resp_BreathSound',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             <tr>
                                 <td>
                                     <span class="lblCaption1" > Breath Cough </span><br />
                                     <asp:textbox id="txtHC_ASS_Phy_Resp_BreathCough" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Resp_BreathCough',this)"></asp:textbox>
                                 </td>
                             </tr>  
                             
                          
                         </table>
                     </fieldset>



                     <fieldset>
                 <legend class="lblCaption1" style="font-weight:bold;">Diet / Nutrition Assessment  </legend>
                     <table>
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Diet </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Nutri_Diet" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Nutri_Diet',this)"></asp:textbox>
                             </td>
                         </tr>  

                         <tr>
                             <td>
                                 <span class="lblCaption1" >  Appetite </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Nutri_Appetite" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Nutri_Appetite',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Nutritional Support </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Nutri_NutriSupport" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Nutri_NutriSupport',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Feeding difficulties: </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Nutri_FeedingDif" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Nutri_FeedingDif',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Weight status </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Nutri_WeightStatus" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Nutri_WeightStatus',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > By Diagnosis </span><br />
                                 <asp:textbox id="txtHC_ASS_Phy_Nutri_Diag" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Phy_Nutri_Diag',this)"></asp:textbox>
                             </td>
                         </tr>  
                        
                     </table>
                 </fieldset>
                        </td>
                    </tr>
                </table>
               

            </contenttemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText=" Risk/Safety Assessment " Width="100%">
            <contenttemplate>
                <table>
                    <tr>
                     <td valign="top" style="width:49%">
                            
                     <fieldset>
                 <legend class="lblCaption1" style="font-weight:bold;">Braden Skin Risk Assessment </legend>
                     <table>
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Sensory perception </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Sensory" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Sensory',this)"></asp:textbox>
                             </td>
                         </tr>  

                         <tr>
                             <td>
                                 <span class="lblCaption1" >  Moisture </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Moisture" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Moisture',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Activity</span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Activity" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Activity',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Mobility </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Mobility" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Mobility',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Nutrition </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Nutrition" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Nutrition',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Friction / Shear </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Friction" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Friction',this)"></asp:textbox>
                             </td>
                         </tr>  
                        
                     </table>
                 </fieldset>

                <fieldset>
                 <legend class="lblCaption1" style="font-weight:bold;">Educational Assessment </legend>
                     <table>
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Patient and / or family Need Eduction On </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_Edu_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Edu_General',this)"></asp:textbox>
                             </td>
                         </tr>  
 
                        
                     </table>
                 </fieldset>
                        </td>
                   <td style="width:1%;"></td>
                       <td valign="top" style="width:49%">
                                       
                     <fieldset>
                         <legend class="lblCaption1" style="font-weight:bold;">Socioeconomic </legend>
                             <table>
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" > Living Situation </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Socio_Living" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Socio_Living',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                        
                             </table>
                         </fieldset>
                        <fieldset>
                         <legend class="lblCaption1" style="font-weight:bold;">Fall/Safety Risk </legend>
                             <table>
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" > </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Safety_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Safety_General',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                        
                             </table>
                         </fieldset>
                            <fieldset>
                         <legend class="lblCaption1" style="font-weight:bold;">Functional Assessment </legend>
                             <table>
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" >Self Caring </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Fun_SelfCaring" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Fun_SelfCaring',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" >Musculoskeletal   </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Fun_Musculos" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Fun_Musculos',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" >Use of Assisting Equipment   </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Fun_Equipment" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Fun_Equipment',this)"></asp:textbox>
                                     </td>
                                 </tr>  

                             </table>
                         </fieldset>

                        </td>
                    </tr>
                </table>
            </contenttemplate>
        </asp:TabPanel>


        </asp:tabcontainer>

 </div>
    <br />


</asp:content>


