﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.master" AutoEventWireup="true" CodeBehind="TravelStatus.aspx.cs" Inherits="HomeCare1.HomeCare.TravelStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function DRIdSelected() {
            if (document.getElementById('<%=txtStaffID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtStaffID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtStaffID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtStaffName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtStaffName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtStaffName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtStaffID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtStaffName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function DriverIdSelected() {
            if (document.getElementById('<%=txtDriverId.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDriverId.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDriverId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDriverName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DriverNameSelected() {
            if (document.getElementById('<%=txtDriverName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDriverName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDriverId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDriverName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strVisitDate = document.getElementById('<%=txtVisitDate.ClientID%>').value
            if (/\S+/.test(strVisitDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Date";
                document.getElementById('<%=txtVisitDate.ClientID%>').focus;
                return false;
            }

            if (isDate(document.getElementById('<%=txtVisitDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtVisitDate.ClientID%>').focus()
                return false;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table>
        <tr>
            <td class="PageHeader">Travel Status
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <asp:HiddenField ID="hidHHV_ID" runat="server" Value="0" />

    <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto; border-color: #cccccc; border-style: solid; border-width: thin;">


        <asp:GridView ID="gvTravelStatus" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvTravelStatus_RowDataBound"
            EnableModelValidation="True" Width="100%" PageSize="200">
            <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
            <RowStyle CssClass="GridRow" />
            <Columns>
                <asp:TemplateField HeaderText="No" HeaderStyle-Width="40px">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkSerial" runat="server" OnClick="SelectTravelStatus_Click">
                            <asp:Label ID="lblTSId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_ID") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_DATEDesc") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblStartTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_START_TIME") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblReachTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_REACH_TIME") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblUp" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_MILEAGE_UP") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblDown" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_MILEAGE_DOWN") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblStaffId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_STAFF_ID") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblDriverId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_DRIVER_ID") %>' Visible="false"></asp:Label>

                            <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_ID") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date" >
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkModUser" runat="server" OnClick="SelectTravelStatus_Click" Text='<%# Bind("HHTS_DATEDesc") %>'>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Start Time" >
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkStartTime" runat="server" OnClick="SelectTravelStatus_Click" Text='<%# Bind("HHTS_START_TIME") %>'> </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reach Time" >
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkReachTime" runat="server" OnClick="SelectTravelStatus_Click" Text='<%# Bind("HHTS_REACH_TIME") %>'> </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mileage Up">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkUp" runat="server" OnClick="SelectTravelStatus_Click" Text='<%# Bind("HHTS_MILEAGE_UP") %>'> </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mileage Down" >
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDown" runat="server" OnClick="SelectTravelStatus_Click" Text='<%# Bind("HHTS_MILEAGE_DOWN") %>'> </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vehicle No" >
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkVehicleNo" runat="server" OnClick="SelectTravelStatus_Click" > 
                            <asp:Label ID="lblVehicleNo" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_VEHICLE_NO") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Assigned Staff">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkStaff" runat="server" OnClick="SelectTravelStatus_Click">
                            <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_STAFF_ID") %>'></asp:Label>&nbsp;-&nbsp;
                             <asp:Label ID="lblStaffName" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_STAFF_NAME") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Driver">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDriver" runat="server" OnClick="SelectTravelStatus_Click">
                            <asp:Label ID="Label2" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHTS_DRIVER_ID") %>'></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblDriverName" CssClass="GridRow" runat="server" Text='<%# Bind("HHTS_DRIVER_NAME") %>'></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="70px">
                    <ItemTemplate>
                        <asp:ImageButton ID="DeletTravelStatus" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')" ImageUrl="~/Images/icon_delete.png"
                            OnClick="DeleteTravelStatus_Click" />&nbsp;&nbsp;
                                                
                    </ItemTemplate>
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

        </asp:GridView>

    </div>
    <br />
    <table width="50%" border="0">
        <tr>
            <td class="lblCaption1" style="height: 30px; width: 150px;">Date<span style="color:red;">* </span>
            </td>
            <td style="height: 30px; width: 150px;">
                <asp:TextBox ID="txtVisitDate" runat="server" Width="105px" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtVisitDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtVisitDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

            </td>
            <td class="lblCaption1" style="height: 30px;">Start Time
            </td>
            <td class="lblCaption1">
                <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                Reach Time
               <asp:DropDownList ID="drpFIHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                :
               <asp:DropDownList ID="drpFIMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Mileage Up
            </td>
            <td>
                <asp:TextBox ID="txtMileageUp" CssClass="label" runat="server" Width="105px" BorderWidth="1px" BorderColor="#cccccc" MaxLength="5" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

            </td>
            <td class="lblCaption1" style="height: 30px;">Mileage Down
            </td>
            <td>
                <asp:TextBox ID="txtMileageDown" CssClass="label" runat="server" Width="100px" BorderWidth="1px" BorderColor="#cccccc" MaxLength="5" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Vehicle No
            </td>
            <td>
                <asp:TextBox ID="txtVehicleNo" CssClass="label" runat="server" Width="105px" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>

            </td>
            <td class="lblCaption1" style="height: 30px;"> 
            </td>
            <td>
                

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Assigned Staff  
            </td>
            <td>
                <asp:TextBox ID="txtStaffID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="105px" onblur="return DRIdSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtStaffID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"></asp:AutoCompleteExtender>

            </td>
            <td colspan="2">
                <asp:TextBox ID="txtStaffName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="500px" MaxLength="50" onblur="return DRNameSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtStaffName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"></asp:AutoCompleteExtender>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="height: 30px;">Driver 
            </td>
            <td>
                <asp:TextBox ID="txtDriverId" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="105px" onblur="return DriverIdSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtDriverId" MinimumPrefixLength="1" ServiceMethod="GetDriverId"></asp:AutoCompleteExtender>

            </td>
            <td colspan="2">
                <asp:TextBox ID="txtDriverName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="500px" MaxLength="50" onblur="return DriverNameSelected()"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtDriverName" MinimumPrefixLength="1" ServiceMethod="GetDriverName"></asp:AutoCompleteExtender>

            </td>
        </tr>
    </table>
    <table width="50%">
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="70px" OnClick="btnSave_Click" OnClientClick="return SaveVal();" Text="Save" />
                <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnClear_Click" Text="Clear" />

            </td>
        </tr>
    </table>



</asp:Content>


