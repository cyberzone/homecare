﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeCare/HomeCareDtls.master" AutoEventWireup="true" CodeBehind="HomeCareRegistration.aspx.cs" Inherits="HomeCare1.HomeCare.HomeCareRegistration" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function HomeCareRegPopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("HomeCareRegPopup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=570,width=950,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
            }
            return true;

        }

        function PatientPopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
                win.focus();
            }
            return true;

        }

        function ServicePopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("../Masters/ServiceMasterLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
                win.focus();
            }
            return true;

        }

        function ReportPopup(FileNo) {
            var RptName = "MedConsent1.rpt";
            var win = window.open('../CReports/ReportsView.aspx?ReportName=' + RptName + '&SelectionFormula={GeneralConsentView.HPM_PT_ID}=\'' + FileNo + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();


        }
        function PTPopup() {
            var win = window.open("../Registration/Patient_Registration.aspx?PageName=HomeCare", "newwin1", "top=200,left=100,height=700,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
            win.focus();

        }

        function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName) {

            if (CtrlName == 'NursingService') {
                document.getElementById("<%=txtServCode.ClientID%>").value = ServCode;
                document.getElementById("<%=txtServName.ClientID%>").value = ServName;
            }

            if (CtrlName == 'PhysioService') {
                document.getElementById("<%=txtPhyServCode.ClientID%>").value = ServCode;
                document.getElementById("<%=txtPhyServName.ClientID%>").value = ServName;
            }


        }

        function NurServStaffIdSelected() {
            if (document.getElementById('<%=txtNurServStaffId.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtNurServStaffId.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtNurServStaffId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtNurServStaffName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function NurServStaffNameSelected() {
            if (document.getElementById('<%=txtNurServStaffName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtNurServStaffName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtNurServStaffId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtNurServStaffName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function PhyServStaffIdSelected() {
            if (document.getElementById('<%=txtPhyServStaffId.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtPhyServStaffId.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtPhyServStaffId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtPhyServStaffName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function PhyServStaffNameSelected() {
            if (document.getElementById('<%=txtPhyServStaffName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtPhyServStaffName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtPhyServStaffId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtPhyServStaffName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }



        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strFileNo = document.getElementById('<%=txtFileNo.ClientID%>').value
            if (/\S+/.test(strFileNo) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter File No";
                document.getElementById('<%=txtFileNo.ClientID%>').focus;
                return false;
            }

            var strVisitDate = document.getElementById('<%=txtVisitDate.ClientID%>').value
            if (/\S+/.test(strVisitDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Visit Date";
                document.getElementById('<%=txtVisitDate.ClientID%>').focus;
                return false;
            }

            var strBeginDate = document.getElementById('<%=txtBeginDate.ClientID%>').value
            if (/\S+/.test(strVisitDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Begin Date";
                document.getElementById('<%=txtBeginDate.ClientID%>').focus;
                return false;
            }

            var strEndDate = document.getElementById('<%=txtEndDate.ClientID%>').value
            if (/\S+/.test(strEndDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter End Date";
                document.getElementById('<%=txtEndDate.ClientID%>').focus;
                return false;
            }

            if (isDate(document.getElementById('<%=txtVisitDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtVisitDate.ClientID%>').focus()
                return false;
            }

            if (isDate(document.getElementById('<%=txtBeginDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtBeginDate.ClientID%>').focus()
                return false;
            }

            if (isDate(document.getElementById('<%=txtEndDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtEndDate.ClientID%>').focus()
                return false;
            }


            var strExtServStDate = document.getElementById('<%=txtExtBeginDate.ClientID%>').value
            if (/\S+/.test(strExtServStDate) == true) {
                if (isDate(document.getElementById('<%=txtExtBeginDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtExtBeginDate.ClientID%>').focus()
                    return false;
                }
            }

            var strExtServEndDate = document.getElementById('<%=txtExtEndDate.ClientID%>').value
            if (/\S+/.test(strExtServEndDate) == true) {
                if (isDate(document.getElementById('<%=txtExtEndDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtExtEndDate.ClientID%>').focus()
                    return false;
                }
            }


        }

        function checkDates(d1, d2) {
            if (d1 instanceof Date && d2 instanceof Date) {
                var today = new Date();
                today.setHours(0, 0, 0, 0);
                if (date1.getTime() < (today.getTime() + 86400000)) {
                    return false;
                }
                if (date2.getTime() < (date1.getTime() + 86400000)) {
                    return false;
                }
                return true;
            }
            return false;
        }


        function parseDate(input) {
            var parts = input.match(/(\d+)/g);
            return new Date(parts[2], parts[1] - 1, parts[0]); // months are 0-based
        }


        function SaveNurseSchServVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strServCode = document.getElementById('<%=txtServCode.ClientID%>').value
            if (/\S+/.test(strServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Code";
                document.getElementById('<%=txtServCode.ClientID%>').focus;
                return false;
            }

            var strServName = document.getElementById('<%=txtServName.ClientID%>').value
            if (/\S+/.test(strServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Name";
                document.getElementById('<%=txtServName.ClientID%>').focus;
                return false;
            }

            var strNurServStaffId = document.getElementById('<%=txtNurServStaffId.ClientID%>').value
            if (/\S+/.test(strNurServStaffId) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter staff ID";
                document.getElementById('<%=txtNurServStaffId.ClientID%>').focus;
                return false;
            }

            var strNurServStaffName = document.getElementById('<%=txtNurServStaffName.ClientID%>').value
            if (/\S+/.test(strNurServStaffName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter staff Name";
                document.getElementById('<%=txtNurServStaffName.ClientID%>').focus;
                return false;
            }



            var strServNoOfDays = document.getElementById('<%=txtServNoOfDays.ClientID%>').value
            if (/\S+/.test(strServNoOfDays) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service No Of Days";
                document.getElementById('<%=txtServNoOfDays.ClientID%>').focus;
                return false;
            }

            var strServStDate = document.getElementById('<%=txtServStDate.ClientID%>').value
            if (/\S+/.test(strServStDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Start Date";
                document.getElementById('<%=txtServStDate.ClientID%>').focus;
                return false;
            }

            var strServEndDate = document.getElementById('<%=txtServEndDate.ClientID%>').value
            if (/\S+/.test(strServEndDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service End Date";
                document.getElementById('<%=txtServEndDate.ClientID%>').focus;
                return false;
            }


            if (isDate(document.getElementById('<%=txtServStDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtServStDate.ClientID%>').focus()
                return false;
            }

            if (isDate(document.getElementById('<%=txtServEndDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtServEndDate.ClientID%>').focus()
                return false;
            }


        }


        function SavePhySchServVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strServCode = document.getElementById('<%=txtPhyServCode.ClientID%>').value
            if (/\S+/.test(strServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Code";
                document.getElementById('<%=txtPhyServCode.ClientID%>').focus;
                return false;
            }

            var strServName = document.getElementById('<%=txtPhyServName.ClientID%>').value
            if (/\S+/.test(strServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Name";
                document.getElementById('<%=txtPhyServName.ClientID%>').focus;
                return false;
            }

            var strPhyServStaffId = document.getElementById('<%=txtPhyServStaffId.ClientID%>').value
            if (/\S+/.test(strPhyServStaffId) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter staff ID";
                document.getElementById('<%=txtPhyServStaffId.ClientID%>').focus;
                return false;
            }

            var strPhyServStaffName = document.getElementById('<%=txtPhyServStaffName.ClientID%>').value
            if (/\S+/.test(strPhyServStaffName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter staff Name";
                document.getElementById('<%=txtPhyServStaffName.ClientID%>').focus;
                return false;
            }


            var strServNoOfDays = document.getElementById('<%=txtPhyServNoOfDays.ClientID%>').value
            if (/\S+/.test(strServNoOfDays) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service No Of Days";
                document.getElementById('<%=txtPhyServNoOfDays.ClientID%>').focus;
                return false;
            }

            var strServStDate = document.getElementById('<%=txtPhyServStDate.ClientID%>').value
            if (/\S+/.test(strServStDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service Start Date";
                document.getElementById('<%=txtPhyServStDate.ClientID%>').focus;
                return false;
            }

            var strServEndDate = document.getElementById('<%=txtPhyServEndDate.ClientID%>').value
            if (/\S+/.test(strServEndDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter service End Date";
                document.getElementById('<%=txtPhyServEndDate.ClientID%>').focus;
                return false;
            }


            if (isDate(document.getElementById('<%=txtPhyServStDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtPhyServStDate.ClientID%>').focus()
                return false;
            }

            if (isDate(document.getElementById('<%=txtPhyServEndDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtPhyServEndDate.ClientID%>').focus()
                return false;
            }



        }

    </script>


    <script language="javascript" type="text/javascript">
        function ServicessShow(CtrlName, CategoryType) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("ServicessLookup.aspx?CtrlName=" + CtrlName + "&CategoryType=" + CategoryType, "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
            }
            return false;

        }

        function BindServicess(HaadCode, HaadName, CtrlName) {


            if (CtrlName == 'Procedure') {
                var data = document.getElementById("<%=txtProcedure.ClientID%>").value

                if (data != "") {
                    document.getElementById("<%=txtProcedure.ClientID%>").value = data + "\n" + HaadCode + ' - ' + HaadName
                }
                else {
                    document.getElementById("<%=txtProcedure.ClientID%>").value = HaadCode + ' - ' + HaadName
                }

            }


        }

    </script>
    
    <script language="javascript" type="text/javascript">

        function DiagnosisPopup(CtrlName, CategoryType) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("../HomeCare/ServicessLookup.aspx?CtrlName=" + CtrlName + "&CategoryType=" + CategoryType, "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
            }
            return false;

        }

        function BindServicess(HaadCode, HaadName, CtrlName) {

            if (CtrlName == 'Diagnosis') {
                document.getElementById("<%=txtDiagCode.ClientID%>").value = HaadCode
                document.getElementById("<%=txtDiagName.ClientID%>").value = HaadName

            }




        }
     </script>
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td class="PageHeader">Home Care Registration
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:updatepanel id="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:updatepanel>
            </td>
        </tr>
    </table>
    <br />
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <asp:hiddenfield id="hidHHV_ID" runat="server" value="0" />
    <input type="hidden" id="hidEmrID" runat="server" />
    <div style="padding: 5px; width: 100%; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
        <table width="95%">

            <tr>
                <td style="float: left; padding-right: 20px;"></td>
                <td style="float: right; padding-right: 20px;">
                    <asp:button id="btnSave" runat="server" cssclass="button orange small" width="70px" onclick="btnSave_Click" onclientclick="return SaveVal();" text="Save" />
                    <asp:button id="btnDelete" runat="server" cssclass="button orange small" width="70px" onclick="btnDelete_Click" visible="false" text="Delete" onclientclick="return window.confirm('Do you want to Delete Home Care Visit Details?')" />
                    <asp:button id="btnClear" runat="server" style="padding-left: 5px; padding-right: 5px;" cssclass="button orange small" width="70px" onclick="btnClear_Click" text="Clear" />
                    <asp:button id="btnDamanGCPrint" visible="false" runat="server" style="padding-left: 0px; padding-right: 0px" cssclass="button orange small" width="120px" text="Consent Form Print" onclick="btnDamanGCPrint_Click"  />

                </td>
            </tr>
        </table>
        <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0" cssclass="ajax__tab_yuitabview-theme" width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText=" Home Care Details " Width="100%">
            <contenttemplate>
                <input type="hidden" id="hidAgeType" runat="server" />

                 <input type="hidden" id="hidInsCode" runat="server" />
                 <input type="hidden" id="hidInsName" runat="server" />
                 <input type="hidden" id="hidDepID" runat="server" />
                    <input type="hidden" id="hidVisitType" runat="server" />

                  <table width="100%" border="0">
                      <tr>
                          <td>
                           
                           <asp:Button ID="btnPTRegPopup" runat="server" style="padding-left:5px;padding-right:5px;width:90px;"  CssClass="button orange small"   Height="25px" Text="New Patient" OnClick="btnPTRegPopup_Click"   />

                          </td>
                      </tr>
                     
                      <tr>
                         <td class="lblCaption1" style="height:50px;">
                              File No
                          </td>
                          <td>
                               <asp:TextBox ID="txtFileNo" runat="server" Width="150px" MaxLength="10" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="Red" ondblclick="return PatientPopup('FileNo',this.value);" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"      ></asp:TextBox>

                              <asp:TextBox ID="txtHomeCareId" visible="false" runat="server" Width="150px" MaxLength="10" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="Red" ondblclick="return HomeCareRegPopup('HomeCareId',this.value);"   Readonly="false" AutoPostBack="true"   OnTextChanged="txtHomeCareId_TextChanged"  ></asp:TextBox>

                          </td>
                          <td class="lblCaption1">
                             <asp:CheckBox ID="chkManual" runat="server" CssClass="label" Text="Manual" Checked="false"  AutoPostBack="True"  OnCheckedChanged="chkManual_CheckedChanged" />
                          </td>
                          <td>

                          </td>
                      </tr>
                       <tr>
                        <td class="lblCaption1" style="height:50px;">
                          First  Name
                        </td>
                           <td>
                            <asp:TextBox ID="txtFName" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('Name',this.value);" ></asp:TextBox>

                           </td>
                            <td class="lblCaption1" style="height:50px;">
                              Middle Name
                        </td>
                         <td  > 
                               <asp:TextBox ID="txtMName" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('Name',this.value);" ></asp:TextBox>

                            </td>
                             <td class="lblCaption1" >
                           Last Name
                              </td>
                         <td  >
                           <asp:TextBox ID="txtLName" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc"  ondblclick="return PatientPopup('Name',this.value);" ></asp:TextBox>


                        </td>
                      </tr>
                      <tr>
                          
                            <td class="lblCaption1" style="height:50px;">
                        DOB
                        </td>
                           <td>
                             <asp:TextBox ID="txtDOB" runat="server" Width="75px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"      ></asp:TextBox>
                             <asp:Label ID="Label120" runat="server" CssClass="label" Text="Age"></asp:Label>
                             <asp:TextBox ID="txtAge" runat="server" Width="20px" MaxLength="3" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                             <asp:TextBox ID="txtMonth" runat="server" Width="20px" MaxLength="2" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"   ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                      Sex
                        </td>
                           <td>
                             <asp:TextBox ID="txtSex" runat="server" Width="150px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"      ></asp:TextBox>

                           </td>
                            <td  class="lblCaption1"  style="height:50px;">
                    <asp:Label ID="lblIDCaption" runat="server" CssClass="label" Text="EmiratesId"></asp:Label>
                        </td>
                           <td>
                             <asp:TextBox ID="txtEmiratesID" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" ondblclick="return PatientPopup('EmiratesID',this.value);"   BorderWidth="1px"   BorderColor="#cccccc"    ></asp:TextBox>

                           </td>

                      </tr>
                       <tr>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                       PO Box
                        </td>
                           <td>
                             <asp:TextBox ID="txtPoBox" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" ondblclick="return PatientPopup('POBox',this.value);"    BorderWidth="1px"    BorderColor="#cccccc" ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                    Address
                        </td>
                           <td colspan="3">
                             <asp:TextBox ID="txtAddress" runat="server" Width="87%" CssClass="label"   BorderWidth="1px" ReadOnly="true"  Height="30px"  TextMode="MultiLine"    BorderColor="#cccccc" ></asp:TextBox>

                           </td>
                            
                      </tr>
                       <tr>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                        City
                        </td>
                           <td>
                             <asp:TextBox ID="txtCity" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"      ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                     Area
                        </td>
                           <td>
                             <asp:TextBox ID="txtArea" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"      ></asp:TextBox>

                           </td>
                            <td  class="lblCaption1"  style="height:50px;">
                       Nationality
                        </td>
                           <td>
                             <asp:TextBox ID="txtNationality" runat="server" Width="150px" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"      ></asp:TextBox>

                           </td>

                      </tr>
                        <tr>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                      Home PH.No.
                        </td>
                           <td>
                             <asp:TextBox ID="txtPhone1" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px"  BorderColor="#cccccc" ondblclick="return PatientPopup('Phone1',this.value);"      ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                    Mob. No 1
                        </td>
                           <td>
                             <asp:TextBox ID="txtMobile1" runat="server" Width="150px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return PatientPopup('Mobile1',this.value);"    ></asp:TextBox>

                           </td>
                            <td  class="lblCaption1"  style="height:50px;">
                     Mob. No 2
                        </td>
                           <td>
                             <asp:TextBox ID="txtMobile2" runat="server" Width="150px"  CssClass="label" BackColor="#e3f7ef"  BorderWidth="1px" BorderColor="#cccccc"  ondblclick="return PatientPopup('Mobile2',this.value);"     ></asp:TextBox>

                           </td>

                      </tr>
                        <tr>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                     Reference Doctor
                        </td>
                           <td>
                             <asp:TextBox ID="txtRefDrName" runat="server" Width="150px" CssClass="label"   BorderWidth="1px"    BorderColor="#cccccc"   ></asp:TextBox>

                           </td>
                          
                            <td  class="lblCaption1"  style="height:50px;">
                    License No
                        </td>
                           <td>
                             <asp:TextBox ID="txtRefDrLicenseNo" runat="server" Width="150px" CssClass="label"  BorderWidth="1px"  BorderColor="#cccccc"   ></asp:TextBox>

                           </td>
                            <td  class="lblCaption1"  style="height:50px;">
                    Department 
                        </td>
                           <td>
                             <asp:TextBox ID="txtRefDrDepartment" runat="server" Width="150px"  CssClass="label"   BorderWidth="1px" BorderColor="#cccccc"  ></asp:TextBox>

                           </td>

                      </tr>
                      <tr>
                           <td  class="lblCaption1"  style="height:50px;">
                               Facility Name
                          </td>
                           <td colspan="3" >
                                <asp:TextBox ID="txtRefDrFacility" runat="server" Width="300px"  CssClass="label"   BorderWidth="1px" BorderColor="#cccccc" ></asp:TextBox>

                          </td>
 
                            <td  class="lblCaption1"  style="height:50px;">
                    Date Of Visit
                        </td>
                           <td>
                                  <asp:TextBox ID="txtVisitDate" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                            Enabled="True" TargetControlID="txtVisitDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtVisitDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                           </td>
                      </tr>

                        <tr>
                           <td  class="lblCaption1"  style="height:50px;">
                            Authorization No
                         </td>
                          <td>
                            <asp:TextBox ID="txtAuthorizationNo" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#cccccc" Width="150px"      ></asp:TextBox>

                         </td>
                          
                          
                            <td  class="lblCaption1"  style="height:50px;">
                    Begin Date
                        </td>
                           <td>
                                     <asp:TextBox ID="txtBeginDate" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                     <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtBeginDate" Format="dd/MM/yyyy"   >
                                      </asp:CalendarExtender>
                                     <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtBeginDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


                           </td>
                            <td  class="lblCaption1"  style="height:50px;">
                    End Date
                        </td>
                           <td>
                                       <asp:TextBox ID="txtEndDate" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                            Enabled="True" TargetControlID="txtEndDate" Format="dd/MM/yyyy"  >
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtEndDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>



                           </td>

                      </tr>
                   
                         <tr>
                         <td  class="lblCaption1"  style="height:50px;">
                              Exten.  Authorization No
                         </td>
                         <td>
                            <asp:TextBox ID="txtExtAuthorizationNo" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#cccccc" Width="150px"      ></asp:TextBox>

                         </td>
                      
                             <td  class="lblCaption1"  style="height:50px;">
                                         Exten. Begin Date
                                </td>
                         <td>
                                        <asp:TextBox ID="txtExtBeginDate" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10"  onkeypress="return OnlyNumeric(event);"  ></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender5" runat="server"
                                            Enabled="True" TargetControlID="txtExtBeginDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtExtBeginDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                       </td>
                             <td  class="lblCaption1"  style="height:50px;">
                                 Exten. End Date
                                 </td>
                             <td  class="lblCaption1"  style="height:50px;">
                                          <asp:TextBox ID="txtExtEndDate" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10"  onkeypress="return OnlyNumeric(event);"   ></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender8" runat="server"
                                            Enabled="True" TargetControlID="txtExtEndDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender9" runat="server" Enabled="true" TargetControlID="txtExtEndDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                           </td>
                  </tr>
                  </table>
               
            </contenttemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" Nursing Service " Width="100%">
            <contenttemplate>
                <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">

                     
                              <asp:GridView ID="gvNursingService" runat="server"   AutoGenerateColumns="False"  OnRowDataBound="gvNursingService_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200"   >
                                <HeaderStyle CssClass="GridHeader"  Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                   <asp:TemplateField HeaderText="No">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSerial" runat="server" OnClick="Select_Click" >
                                                         <asp:Label ID="lblHHSSId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_ID") %>' visible="false" ></asp:Label>
                                                        <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_HHV_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                  </asp:TemplateField>

                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                       <asp:Label ID="lblServId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_SERV_ID") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click" Text='<%# Bind("HHSS_SERV_ID") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Services">
                                    <ItemTemplate>
                                         <asp:Label ID="lblServName" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_SERV_NAME") %>' visible="false" ></asp:Label>

                                        <asp:LinkButton ID="lnkServName" runat="server" OnClick="Select_Click" Text='<%# Bind("HHSS_SERV_NAME") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Staff ID">
                                    <ItemTemplate>
                                       <asp:Label ID="lblStaffId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_STAFF_ID") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkStaffId" runat="server" OnClick="Select_Click" Text='<%# Bind("HHSS_STAFF_ID") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                         <asp:Label ID="lblStaffName" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_STAFF_NAME") %>' visible="false" ></asp:Label>

                                        <asp:LinkButton ID="lnkStaffName" runat="server" OnClick="Select_Click" Text='<%# Bind("HHSS_STAFF_NAME") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="License No">
                                    <ItemTemplate>
                                         <asp:Label ID="lblLicenseNo" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>' visible="false" ></asp:Label>

                                        <asp:LinkButton ID="lnkLicenseNo" runat="server" OnClick="Select_Click" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                  <asp:TemplateField HeaderText="No Of Days">
                                    <ItemTemplate>
                                         <asp:Label ID="lblNoOfDays" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_NOOFDAYS") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkNoOfDays" runat="server" OnClick="Select_Click"  Text='<%# Bind("HHSS_NOOFDAYS") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Effective Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_START_DATEDesc") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkStDate" runat="server" OnClick="Select_Click" Text='<%# Bind("HHSS_START_DATEDesc") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                         <asp:Label ID="lblEndDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_END_DATEDesc") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkEndDate" runat="server" OnClick="Select_Click" Text='<%# Bind("HHSS_END_DATEDesc") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Authorization No">
                                    <ItemTemplate>
                                         <asp:Label ID="lblAuthNo" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkAuthNo" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="DeleteSchServ" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="DeleteSchServ_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                               </asp:TemplateField>
                            </Columns>
                              <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

    </asp:GridView>

                    </div>
                 <table width="850px" border="0">
                       <tr>
                            <td  class="lblCaption1"  style="height:50px;">
                            Service Code
                           </td>
                           <td >
                             <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="Red" ondblclick="return ServicePopup('NursingService',this.value);" AutoPostBack="true"   OnTextChanged="txtServCode_TextChanged"   ></asp:TextBox>
                            <asp:TextBox ID="txtServName" runat="server" Width="400px"  CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return ServicePopup('NursingService',this.value);"  AutoPostBack="true"   OnTextChanged="txtServName_TextChanged" ></asp:TextBox>
                            </td>
                  </tr>
                     <tr>
                          <td class="lblCaption1" style="width:100px;height:30px;" >
                            Staff ID 
                        </td>
                         
                          <td>  
                
                             <asp:TextBox ID="txtNurServStaffId" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="red" Width="100px"    onblur="return NurServStaffIdSelected()" AutoPostBack="true"   OnTextChanged="txtNurServStaffId_TextChanged" ></asp:TextBox>
                             <asp:TextBox ID="txtNurServStaffName" runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc"   Width="400px" MaxLength="50" onblur="return NurServStaffNameSelected()" AutoPostBack="true"   OnTextChanged="txtNurServStaffName_TextChanged" ></asp:TextBox>
                             <asp:AutoCompleteExtender ID="AutoComNurServStaffId" runat="Server" TargetControlID="txtNurServStaffId" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"></asp:AutoCompleteExtender>
                              <asp:AutoCompleteExtender ID="AutoComNurServStaffName" runat="Server" TargetControlID="txtNurServStaffName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"></asp:AutoCompleteExtender>
                             <asp:TextBox ID="txtNurServStaffLicNo" visible="false"   runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc"   Width="100px" MaxLength="50" onblur="return NurServStaffNameSelected()" AutoPostBack="true"   OnTextChanged="txtNurServStaffName_TextChanged" ></asp:TextBox>

                           </td>

                    
                    <tr>
                          <td  class="lblCaption1"  style="height:50px;">
                             No Of Days
                        </td>
                         <td class="lblCaption1">
                            <asp:TextBox ID="txtServNoOfDays" runat="server" Width="100px" MaxLength="10" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                  Effective Date
                                      <asp:TextBox ID="txtServStDate" runat="server" Width="75px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10"  onkeypress="return OnlyNumeric(event);"  AutoPostBack="true"   OnTextChanged="txtServStDate_TextChanged" ></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender3" runat="server"
                                            Enabled="True" TargetControlID="txtServStDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtServStDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expiry Date
                                          <asp:TextBox ID="txtServEndDate" runat="server" Width="75px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10"  onkeypress="return OnlyNumeric(event);"  AutoPostBack="true"   OnTextChanged="txtServStDate_TextChanged" ></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender4" runat="server"
                                            Enabled="True" TargetControlID="txtServEndDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtServEndDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                           </td>
                     </tr>
                   </tr>
                     <tr>
                         <td class="lblCaption1" style="width:100px;height:30px;" >
                             Authorization No
                         </td>
                         <td>
                             <asp:TextBox ID="txtNurServAuthNo" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#cccccc" Width="150px"   onkeypress="return OnlyNumeric(event);"  ></asp:TextBox>
                         </td>
                     </tr>

                     </table>
                 <table width="95%">
                    <tr>
                        <td>
                    <asp:Button ID="btnServAdd" runat="server" CssClass="button orange small" Width="70px" OnClick="btnServAdd_Click"   OnClientClick="return SaveNurseSchServVal();" Text="Save" />
                    <asp:Button ID="btnServClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnServClear_Click" Text="Clear" />

                        </td>
                    </tr>
                </table>

                 
                 </contenttemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel4" HeaderText=" Physiotherapy Service " Width="100%">
            <contenttemplate>
                  <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto;border-color:#CCCCCC;border-style:solid;border-width:thin;">

                     
                              <asp:GridView ID="gvPhyService" runat="server"   AutoGenerateColumns="False"  OnRowDataBound="gvPhyService_RowDataBound"
                                EnableModelValidation="True" Width="100%" PageSize="200"   >
                                <HeaderStyle CssClass="GridHeader"  Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                   <asp:TemplateField HeaderText="No">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" OnClick="PhySelect_Click" >
                                                         <asp:Label ID="lblHHSSId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_ID") %>' visible="false" ></asp:Label>
                                                        <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_HHV_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                  </asp:TemplateField>

                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                       <asp:Label ID="lblServId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_SERV_ID") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="LinkButton5" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_SERV_ID") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Services">
                                    <ItemTemplate>
                                         <asp:Label ID="lblServName" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_SERV_NAME") %>' visible="false" ></asp:Label>

                                        <asp:LinkButton ID="lnkServName" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_SERV_NAME") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Staff ID">
                                    <ItemTemplate>
                                       <asp:Label ID="lblStaffId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_STAFF_ID") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkStaffId" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_STAFF_ID") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                         <asp:Label ID="lblStaffName" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_STAFF_NAME") %>' visible="false" ></asp:Label>

                                        <asp:LinkButton ID="lnkStaffName" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_STAFF_NAME") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="License No">
                                    <ItemTemplate>
                                         <asp:Label ID="lblLicenseNo" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>' visible="false" ></asp:Label>

                                        <asp:LinkButton ID="lnkLicenseNo" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>' >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="No Of Days">
                                    <ItemTemplate>
                                         <asp:Label ID="lblNoOfDays" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_NOOFDAYS") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="LinkButton7" runat="server" OnClick="PhySelect_Click"  Text='<%# Bind("HHSS_NOOFDAYS") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Effective Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_START_DATEDesc") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="LinkButton8" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_START_DATEDesc") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                         <asp:Label ID="lblEndDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_END_DATEDesc") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="LinkButton9" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_END_DATEDesc") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                 <asp:TemplateField HeaderText="Authorization No">
                                    <ItemTemplate>
                                         <asp:Label ID="lblAuthNo" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>' visible="false" ></asp:Label>
                                        <asp:LinkButton ID="lnkAuthNo" runat="server" OnClick="PhySelect_Click" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>'> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.png"
                                                    OnClick="DeletePhyServ_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                               </asp:TemplateField>
                            </Columns>
                              <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

    </asp:GridView>

                    </div>

                    <table width="850px" border="0">
                       <tr>
                            <td  class="lblCaption1"  style="height:50px;">
                         Service Code 
                        </td>
                           <td>
                             <asp:TextBox ID="txtPhyServCode" runat="server" Width="100px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="Red" ondblclick="return ServicePopup('PhysioService',this.value);" AutoPostBack="true"   OnTextChanged="txtPhyServCode_TextChanged"   ></asp:TextBox>
                            <asp:TextBox ID="txtPhyServName" runat="server" Width="400px"  CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" ondblclick="return ServicePopup('PhysioService',this.value);"  AutoPostBack="true"   OnTextChanged="txtPhyServName_TextChanged" ></asp:TextBox>
                            </td>
                  </tr>
                       <tr>
                          <td class="lblCaption1" style="width:100px;height:30px;" >
                            Staff ID 
                        </td>
                         
                          <td  >  
                
                             <asp:TextBox ID="txtPhyServStaffId" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="red" Width="100px"    onblur="return PhyServStaffIdSelected()" AutoPostBack="true"   OnTextChanged="txtPhyServStaffId_TextChanged" ></asp:TextBox>
                             <asp:TextBox ID="txtPhyServStaffName" runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc"   Width="400px" MaxLength="50" onblur="return PhyServStaffNameSelected()" AutoPostBack="true"   OnTextChanged="txtPhyServStaffName_TextChanged" ></asp:TextBox>
                             <asp:AutoCompleteExtender ID="AutoComPhyServStaffId" runat="Server" TargetControlID="txtPhyServStaffId" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"></asp:AutoCompleteExtender>
                              <asp:AutoCompleteExtender ID="AutoComPhyServStaffName" runat="Server" TargetControlID="txtPhyServStaffName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"></asp:AutoCompleteExtender>
                             <asp:TextBox ID="txtPhyServStaffLicNo" visible="false"  runat="server" CssClass="label"   BorderWidth="1px" BorderColor="#cccccc"   Width="100px" MaxLength="50" onblur="return NurServStaffNameSelected()" AutoPostBack="true"   OnTextChanged="txtNurServStaffName_TextChanged" ></asp:TextBox>

                           </td>

                     </tr>
                       <tr>
                          <td  class="lblCaption1"  style="height:50px;">
                             No Of Days
                          </td>
                          <td class="lblCaption1" >
                            <asp:TextBox ID="txtPhyServNoOfDays" runat="server" Width="100px" MaxLength="10" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                 Effective Date
                                  <asp:TextBox ID="txtPhyServStDate" runat="server" Width="75px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10"  onkeypress="return OnlyNumeric(event);"  AutoPostBack="true"   OnTextChanged="txtPhyServStDate_TextChanged" ></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender6" runat="server"
                                            Enabled="True" TargetControlID="txtPhyServStDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Enabled="true" TargetControlID="txtPhyServStDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Expiry Date
                                          <asp:TextBox ID="txtPhyServEndDate" runat="server" Width="75px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10"  onkeypress="return OnlyNumeric(event);"  AutoPostBack="true"   OnTextChanged="txtPhyServStDate_TextChanged" ></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender7" runat="server"
                                            Enabled="True" TargetControlID="txtPhyServEndDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender8" runat="server" Enabled="true" TargetControlID="txtPhyServEndDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            


                           </td>
                            <td  class="lblCaption1"  style="height:50px;">
                             
                            </td>
                            <td >
                           </td>
                  </tr>
                          <tr>
                         <td class="lblCaption1" style="width:100px;height:30px;" >
                             Authorization No
                         </td>
                         <td>
                             <asp:TextBox ID="txtPhyServAuthNo" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#cccccc" Width="150px"   onkeypress="return OnlyNumeric(event);"  ></asp:TextBox>
                         </td>
                     </tr>
                     </table>
                 <table width="95%">
                   
                    <tr>
                        <td>
                    <asp:Button ID="btnPhyServAdd" runat="server" CssClass="button orange small" Width="70px" OnClick="btnPhyServAdd_Click"   OnClientClick="return SavePhySchServVal();" Text="Save" />
                    <asp:Button ID="btnPhyServClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnPhyServClear_Click" Text="Clear" />

                        </td>
                    </tr>
                </table>
            </contenttemplate>
        </asp:TabPanel>
       
        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText=" Patient Clinical Details " Width="100%">
            <contenttemplate>

                 <table width="950px" border="0">
                     <tr>
                         <td colspan="2">
                             <div style="padding: 5px;width: 70%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption">Diagnosis </span><span style="color:red;">* </span>
                            <br />
                             <table width="70%" cellpadding="0" cellspacing="0"> 
                                <tr>
                                    <td>
                            <asp:TextBox ID="txtDiagCode" runat="server" Width="100px" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true"  OnTextChanged="txtDiagCode_TextChanged"  ondblclick="return DiagnosisPopup('Diagnosis','Diagnosis');"></asp:TextBox>
                            <asp:TextBox ID="txtDiagName" runat="server" Width="70%" CssClass="label" BackColor="#e3f7ef" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true"  OnTextChanged="txtDiagName_TextChanged"   ondblclick="return DiagnosisPopup('Diagnosis','Diagnosis');"></asp:TextBox>
                            <asp:Button ID="btnDiagAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                OnClick="btnDiagAdd_Click" Text="Add"  OnClientClick="return PTDiagVal();"/>
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="gvDiagnosis" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" PageSize="200">
                                <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>

                                    <asp:TemplateField HeaderText="Code">
                                        <ItemTemplate>

                                            <asp:Label ID="lblDiagCode" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>

                                            <asp:Label ID="lblDiagName" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                OnClick="DeleteDiag_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                            </asp:GridView>
                        </div>

                         </td>
                     </tr>
                   
                      <tr>
                        <td  class="lblCaption1"  style="height:100px;">
                         Procedures
                        </td>
                           <td>
                             <asp:TextBox ID="txtProcedure" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#cccccc" Width="700px"  Height="50px"  TextMode="MultiLine"   ondblclick="return ServicessShow('Procedure','Procedure');" ></asp:TextBox>
                           </td>
                           
                     </tr>
                      <tr>
                        <td  class="lblCaption1"  style="height:100px;">
                         ADL 
                        </td>
                           <td>
                             <asp:TextBox ID="txtSurgery" runat="server" CssClass="label"  BorderWidth="1px" BorderColor="#cccccc" Width="700px"  Height="50px"   TextMode="MultiLine"  ></asp:TextBox>
                           </td>
                           
                     </tr>
                      </table>
              </contenttemplate>
        </asp:TabPanel>

    </asp:tabcontainer>
    </div>
</asp:content>

