﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomeCareRegPopup.aspx.cs" Inherits="HomeCare1.HomeCare.HomeCareRegPopup" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />
    <title></title>
    <script language="javascript" type="text/javascript">

        function passvalue(HHV_ID) {
            if (document.getElementById('hidPageName').value == 'HomeCareReg') {
                opener.location.href = "HomeCareRegistration.aspx?HHV_ID=" + HHV_ID;
            }
            opener.focus();
            window.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:hiddenfield id="hidPageName" runat="server" />
            <table width="930px;">
                <tr>
                    <td class="label" style="height: 30px;">Home Care Id 
                    </td>
                    <td>
                        <asp:textbox id="txtHomeCareId" runat="server" BorderWidth="1" bordercolor="#CCCCCC"   width="100px"></asp:textbox>
                    </td>
                    <td class="label" style="height: 30px;">Name 
                    </td>
                    <td>
                        <asp:textbox id="txtSrcName" runat="server" BorderWidth="1" bordercolor="#CCCCCC"  width="200px"></asp:textbox>
                    </td>
                    <td class="label" style="height: 30px;">File No

                    </td>
                    <td>
                        <asp:textbox id="txtSrcFileNo" runat="server" BorderWidth="1" bordercolor="#CCCCCC"  width="100px" maxlength="10"></asp:textbox>
                    </td>
                    <td>

                        <asp:button id="btnFind" runat="server" cssclass="button orange small"
                            onclick="btnFind_Click" text=" Find " />

                    </td>
                </tr>
            </table>
           
            <div style="padding-top: 0px; width: 930px; height: 450px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:gridview id="gvHCVisit" runat="server" autogeneratecolumns="False" allowpaging="true"
                    enablemodelvalidation="True" width="99%" pagesize="200" onpageindexchanging="gvHCVisit_PageIndexChanging">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                     <asp:TemplateField HeaderText="Home Care Id">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHV_ID")  %>')">
                                <asp:Label ID="Label3" CssClass="label" runat="server" Text='<%# Bind("HHV_ID") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="File No">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHV_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHV_PT_ID") %>' Width="100px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHV_ID")  %>')">
                                <asp:Label ID="Label2" CssClass="label" runat="server" Text='<%# Bind("FullName") %>' Width="300px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Visit Date">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHV_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHV_VISIT_DATEDesc") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Begin Date">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHV_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHV_BEGIN_DATEDesc") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("HHV_ID")  %>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server" Text='<%# Bind("HHV_END_DATEDesc") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />
            </asp:gridview>
            </div>
             <table width="930px;">
                <tr>
                    <td class="style2" align="left">
                        <asp:label id="Label9" runat="server" cssclass="label" Font-Bold="true"
                            text="Selected Records:"></asp:label>
                        &nbsp;
                                    <asp:label id="lblTotal" runat="server" cssclass="label" font-bold="true"></asp:label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
