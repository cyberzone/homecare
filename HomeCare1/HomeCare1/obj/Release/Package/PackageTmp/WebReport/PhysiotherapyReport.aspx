﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReport/Report.Master" AutoEventWireup="true" CodeBehind="PhysiotherapyReport.aspx.cs" Inherits="HomeCare1.WebReport.PhysiotherapyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" Runat="Server">
<style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        table, th, td
        {
            border: 1px solid #dcdcdc;
            height: 25px;
        }

        .BoldStyle
        {
            font-weight: bold;
        }
    </style>
    <div style="margin: 0px auto; width: 800px">
          <img style="padding:1px;" src="Content/images/HC_Report_Logo.PNG" />
        <br />
        <span class="PageHeader" style="font-size:18px;" >Physical Therapy Evaluation </span>

        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Visit Date:</td>
                <td>
                    <asp:Label ID="lblVisitDate" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">Begin Date:</td>
                <td>
                    <asp:Label ID="lblBeginDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
                <td class="lblCaption1">End Date:</td>
                <td>
                    <asp:Label ID="lblEndDt" runat="server" CssClass="lblCaption1"></asp:Label>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1 BoldStyle" colspan="6">Patient Name :&nbsp;
                    <asp:Label ID="lblPTName" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1 BoldStyle">File No : 
                 <asp:Label ID="lblPTID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1 BoldStyle">Nationality : 
                 <asp:Label ID="lblNationality" runat="server" CssClass="lblCaption1 BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Age :
                    <asp:Label ID="lblAge" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    Y
                     <asp:Label ID="lblAge1" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                    M

                </td>
            </tr>
            <tr>
                 <td class="lblCaption1  BoldStyle">Emirates ID : 
                     <asp:Label ID="lblEmiratesID" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Phone No :
                    <asp:Label ID="lblMobile" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Sex :
                    <asp:Label ID="lblSex" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Provider Name :
             <asp:Label ID="lblProviderName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Pollicy Type :
                    <asp:Label ID="lblPolicyType" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Policy No : 
                    <asp:Label ID="lblPolicyNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Reference Doctor :
                    <asp:Label ID="lblRefBy" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">License No :
                    <asp:Label ID="lblRefLicNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">Department :
                    <asp:Label ID="lblRefDept" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Facility Name :
                    <asp:Label ID="lblRefFacilityName" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                 <td class="lblCaption1  BoldStyle">Authorization No :
                    <asp:Label ID="lblAuthorizationNo" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
                <td class="lblCaption1  BoldStyle">

                    <asp:Label ID="Label4" runat="server" CssClass="lblCaption1  BoldStyle"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <div runat="server" visible="false" id="divPhyTherapy">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Physical Therapy Evaluation </span>
                    </td>
                </tr>
            </table>
            <div contenteditable="true" runat="server" visible="false" id="lblPhyTherapyInj_Surg_Date">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Date Of Injury : 
                            <asp:label id="lblPhyTherapyInjuryDate" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             Date  Of Surgery : 
                            <asp:label id="lblPhyTherapySurgeryDate" runat="server" cssclass="lblCaption1"></asp:label>


                        </td>
                    </tr>
                </table>
            </div>

            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyChiefCoplaint">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Chief Complaint / Ailment / Injury</td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPhyTherapyChiefCoplaint" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyInjuryRemarks">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">How you were Injured</td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPhyTherapyInjuryRemarks" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyReceived">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Received therapy for this condition :  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            Date&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;:
                            <asp:label id="lblPhyTherapyReceivedDate" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            How many Visits :  &nbsp;
                            <asp:label id="lblPhyTherapyHowmany" runat="server" cssclass="lblCaption1"></asp:label>

                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyCondition">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">condition been getting&nbsp; 
                            :
                            <asp:label id="lblPhyTherapyCondition" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  
                            Symptoms :  &nbsp;
                            <asp:label id="lblPhyTherapySymptoms" runat="server" cssclass="lblCaption1"></asp:label>

                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyBest_Worst">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">Mark the number that best corresponds to your pain : &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                            At Best&nbsp;
                            :
                            <asp:label id="lblPhyTherapyAtBest" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  
                            At Worst : &nbsp;
                            <asp:label id="lblPhyTherapyAtWorst" runat="server" cssclass="lblCaption1"></asp:label>

                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="true" id="div1">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle"> What decreases/makes your condition:   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                            At Best&nbsp;
                            :
                            <asp:label id="radAtBest" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  
                            At Worst : &nbsp;
                            <asp:label id="radAtWorst" runat="server" cssclass="lblCaption1"></asp:label>

                        </td>
                    </tr>
                </table>
            </div>
             <div contenteditable="true" runat="server" visible="true" id="div2">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle"> Previous medical intervention:    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                            <asp:label id="lblPrevMedIntervention" runat="server" cssclass="lblCaption1"></asp:label>
                               Other : &nbsp;
                            <asp:label id="lblPrevIntOther" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
             <div contenteditable="true" runat="server" visible="true" id="div3">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">Goals to be achieved by the end of therapy:    &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                           
                            <asp:label id="lblGoals" runat="server" cssclass="lblCaption1"></asp:label>
                             

                        </td>
                    </tr>
                </table>
            </div>
             <div contenteditable="true" runat="server" visible="true" id="div4">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">Previous Surgeries:     &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                           
                            <asp:label id="lblPrevSurgeries" runat="server" cssclass="lblCaption1"></asp:label>
                             

                        </td>
                    </tr>
                </table>
            </div>
             <div contenteditable="true" runat="server" visible="true" id="div5">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">Medications:     &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                           
                            <asp:label id="txtMedications" runat="server" cssclass="lblCaption1"></asp:label>
                             

                        </td>
                    </tr>
                </table>
            </div>
             <div contenteditable="true" runat="server" visible="true" id="div6">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">Allergies:      &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                           
                            <asp:label id="txtAllergies" runat="server" cssclass="lblCaption1"></asp:label>
                             

                        </td>
                    </tr>
                </table>
            </div>
              <div contenteditable="true" runat="server" visible="true" id="div7">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">Other:      &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                           
                            <asp:label id="txtOther" runat="server" cssclass="lblCaption1"></asp:label>
                             

                        </td>
                    </tr>
                </table>
            </div>
             <div contenteditable="true" runat="server" visible="true" id="div8">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle"> Medical information :      &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                           
                            <asp:label id="lblMedicalInfo" runat="server" cssclass="lblCaption1"></asp:label>
                             

                        </td>
                    </tr>
                </table>
            </div>

        </div>
        <br />
    </div>
</asp:Content>

