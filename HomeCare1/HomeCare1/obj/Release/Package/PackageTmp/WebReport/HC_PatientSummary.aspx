﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReport/Report.Master" AutoEventWireup="true" CodeBehind="HC_PatientSummary.aspx.cs" Inherits="HomeCare1.WebReport.HC_PatientSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BoxContent" Runat="Server">
<style type="text/css">
        #site-logo
        {
            display: none;
        }

        .box-header
        {
            display: none;
        }

        table
        {
            border-collapse: collapse;
        }

        table, th, td
        {
            border: 1px solid #dcdcdc;
            height: 25px;
        }

        .BoldStyle
        {
            font-weight: bold;
        }
    </style>
    <div style="margin: 0px auto; width: 800px">
        <img style="padding: 1px;" src="Content/images/HC_Report_Logo.PNG" />

        <br />
        <span class="PageHeader" style="font-size: 18px;">Home Care Summary </span>
        <br />
        <br />
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1">Visit Date:</td>
                <td>
                    <asp:label id="lblVisitDate" runat="server" cssclass="lblCaption1"></asp:label>
                </td>
                <td class="lblCaption1">Begin Date:</td>
                <td>
                    <asp:label id="lblBeginDt" runat="server" cssclass="lblCaption1"></asp:label>
                </td>
                <td class="lblCaption1">End Date:</td>
                <td>
                    <asp:label id="lblEndDt" runat="server" cssclass="lblCaption1"></asp:label>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td class="lblCaption1 BoldStyle" colspan="6">Patient Name :&nbsp;
                    <asp:label id="lblPTName" runat="server" cssclass="lblCaption1 BoldStyle"></asp:label>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1 BoldStyle">File No : 
                 <asp:label id="lblPTID" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1 BoldStyle">Nationality : 
                 <asp:label id="lblNationality" runat="server" cssclass="lblCaption1 BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">Age :
                    <asp:label id="lblAge" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                    Y
                     <asp:label id="lblAge1" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                    M

                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Emirates ID : 
                   <asp:label id="lblEmiratesID" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">Phone No :
                    <asp:label id="lblMobile" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">Sex :
                    <asp:label id="lblSex" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Provider Name :
             <asp:label id="lblProviderName" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">Pollicy Type :
                    <asp:label id="lblPolicyType" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">Policy No : 
                    <asp:label id="lblPolicyNo" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Reference Doctor :
                    <asp:label id="lblRefBy" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">License No :
                    <asp:label id="lblRefLicNo" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">Department :
                    <asp:label id="lblRefDept" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">Facility Name :
                    <asp:label id="lblRefFacilityName" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">Authorization No :
                    <asp:label id="lblAuthorizationNo" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
                <td class="lblCaption1  BoldStyle">

                    <asp:label id="Label4" runat="server" cssclass="lblCaption1  BoldStyle"></asp:label>
                </td>
            </tr>
        </table>


        <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Vital">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Vital Signs</span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvVital" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                 <HeaderStyle CssClass="GridRow"  Font-Bold="true"  />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Date" HeaderStyle-width="150px">
                                    <ItemTemplate>
                                            <asp:Label ID="lblVitalDate" CssClass="GridRow"   runat="server" Text='<%# Bind("VitalDateTime") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Weight">
                                    <ItemTemplate>
                                            <asp:Label ID="lblWeight" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_WEIGHT") %>'  ></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Height">
                                    <ItemTemplate>
                                            <asp:Label ID="lblHeight" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_HEIGHT") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Temprature">
                                    <ItemTemplate>
                                            <asp:Label ID="lblTemperatureF" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_TEMPERATURE_F") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BMI">
                                    <ItemTemplate>
                                            <asp:Label ID="lblBMI" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_BMI") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Pulse">
                                    <ItemTemplate>
                                            <asp:Label ID="lblPulse" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_PULSE") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Respiration">
                                    <ItemTemplate>
                                            <asp:Label ID="lblRespiration" CssClass="GridRow"   runat="server" Text='<%# Bind("HPV_RESPIRATION") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                              
                            </Columns>
                              

    </asp:gridview>
                    </td>
                </tr>
            </table>
            <br />
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHCReg_Diagnosis">

            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">Diagnosis </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:label id="lblDiagnosis" runat="server" cssclass="lblCaption1"></asp:label>
                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHCReg_Procedures">
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">Procedures </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:label id="lblProcedure" runat="server" cssclass="lblCaption1"></asp:label>
                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHCReg_Surgeries">
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">ADL </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:label id="lblSurgery" runat="server" cssclass="lblCaption1"></asp:label>
                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHCReg_NursingServices">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Nursing Services</span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">

                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvNursingService" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                <HeaderStyle CssClass="GridRow"  Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                       <asp:Label ID="lblServId" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_SERV_ID") %>' ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Services">
                                    <ItemTemplate>
                                         <asp:Label ID="lblServName" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_SERV_NAME") %>' ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Staff ID">
                                    <ItemTemplate>
                                       <asp:Label ID="lblStaffId" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_ID") %>' ></asp:Label>
                                 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                         <asp:Label ID="lblStaffName" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_STAFF_NAME") %>' ></asp:Label>

                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="License#">
                                    <ItemTemplate>
                                         <asp:Label ID="lblLicenseNo" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>'  ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                  <asp:TemplateField HeaderText="Days">
                                    <ItemTemplate>
                                         <asp:Label ID="lblNoOfDays" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_NOOFDAYS") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Effective Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartDate" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_START_DATEDesc") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                         <asp:Label ID="lblEndDate" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_END_DATEDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Authorization#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthNo" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                            </Columns>
                             

    </asp:gridview>
                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHCReg_PhysiotherapyServices">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Physiotherapy  Services </span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">

                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvPhyService" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                <HeaderStyle CssClass="GridRow"  Font-Bold="true"  />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Code"  >
                                    <ItemTemplate>
                                       <asp:Label ID="Label1" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_SERV_ID") %>' ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Services">
                                    <ItemTemplate>
                                         <asp:Label ID="Label5" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_SERV_NAME") %>' ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Staff ID">
                                    <ItemTemplate>
                                       <asp:Label ID="Label6" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_ID") %>' ></asp:Label>
                                 
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                         <asp:Label ID="Label7" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_STAFF_NAME") %>' ></asp:Label>

                                      
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="License#">
                                    <ItemTemplate>
                                         <asp:Label ID="Label8" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_STAFF_MOH_NO") %>'  ></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                  <asp:TemplateField HeaderText="Days">
                                    <ItemTemplate>
                                         <asp:Label ID="Label9" CssClass="GridRow" runat="server" Text='<%# Bind("HHSS_NOOFDAYS") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Effective Date">
                                    <ItemTemplate>
                                        <asp:Label ID="Label10" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_START_DATEDesc") %>'  ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry Date">
                                    <ItemTemplate>
                                         <asp:Label ID="Label11" CssClass="GridRow"   runat="server" Text='<%# Bind("HHSS_END_DATEDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Authorization#">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" CssClass="GridRow"  runat="server" Text='<%# Bind("HHSS_AUTHORIZATION_NO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                               
                            </Columns>
                             

    </asp:gridview>
                    </td>
                </tr>
            </table>
        </div>


        <div contenteditable="true" runat="server" visible="false" id="divHC_Monitoring_ProgressNotes">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Progress  Notes</span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">

                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvProgNotes" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                <HeaderStyle CssClass="GridRow"  Font-Bold="True" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Progress Notes">
                                        <ItemTemplate>
                                        
                                                 <asp:Label ID="lblProgressNotes" CssClass="GridRow" runat="server" Text='<%# Bind("HHN_NOTES") %>'  ></asp:Label>
                                        
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label13" CssClass="GridRow"  runat="server" Text='<%# Bind("CreatedStaffName") %>' ></asp:Label>
                                        </ItemTemplate>
                                    
                                    </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Date & Time">
                                        <ItemTemplate>
                                            <asp:Label ID="Label12" CssClass="GridRow"  runat="server" Text='<%# Bind("HHN_MONITORING_DATE_TimeDesc") %>' ></asp:Label>
                                        </ItemTemplate>
                                  
                                    </asp:TemplateField>
                                
                             </Columns>
                               

                        </asp:gridview>

                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHC_Monitoring_EmergencyNotes">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Emergency  Notes</span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">

                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvEmerNotes" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                <HeaderStyle CssClass="GridRow"  Font-Bold="True" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Progress Notes">
                                        <ItemTemplate>
                                        
                                                 <asp:Label ID="Label14" CssClass="GridRow" runat="server" Text='<%# Bind("HHN_NOTES") %>'  ></asp:Label>
                                        
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label15" CssClass="GridRow"   runat="server" Text='<%# Bind("CreatedStaffName") %>' ></asp:Label>
                                        </ItemTemplate>
                                    
                                    </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Date & Time">
                                        <ItemTemplate>
                                            <asp:Label ID="Label16" CssClass="GridRow"   runat="server" Text='<%# Bind("HHN_MONITORING_DATE_TimeDesc") %>' ></asp:Label>
                                        </ItemTemplate>
                                  
                                    </asp:TemplateField>
                                
                             </Columns>
                               

                        </asp:gridview>

                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHC_Monitoring_AdditionalNotes">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td class="lblCaption1  BoldStyle">Additional  Notes  </td>

                </tr>
                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvAddiNotes" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                <HeaderStyle CssClass="GridRow"  Font-Bold="True"  />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Progress Notes">
                                        <ItemTemplate>
                                        
                                                 <asp:Label ID="Label17" CssClass="GridRow" runat="server" Text='<%# Bind("HHN_NOTES") %>'  ></asp:Label>
                                        
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label18" CssClass="GridRow"   runat="server" Text='<%# Bind("CreatedStaffName") %>' ></asp:Label>
                                        </ItemTemplate>
                                    
                                    </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Date & Time">
                                        <ItemTemplate>
                                            <asp:Label ID="Label19" CssClass="GridRow"   runat="server" Text='<%# Bind("HHN_MONITORING_DATE_TimeDesc") %>' ></asp:Label>
                                        </ItemTemplate>
                                  
                                    </asp:TemplateField>
                                
                             </Columns>
                               

                        </asp:gridview>

                    </td>
                </tr>
            </table>
        </div>

        <div contenteditable="true" runat="server" visible="false" id="divHC_NurseCarePlan">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Nursing Care Plan </span>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" class="gridspacy">

                <tr>
                    <td class="lblCaption1  BoldStyle">
                        <asp:gridview id="gvNursingCarePlan" runat="server" autogeneratecolumns="False"
                            enablemodelvalidation="True" width="100%">
                                <HeaderStyle CssClass="GridRow"  Font-Bold="true"   />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                <asp:TemplateField HeaderText="Assessment">
                                    <ItemTemplate>
                                       <asp:Label ID="lblAssessment" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_ASSESSMENT") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nursing Diagnosis" >
                                    <ItemTemplate>
                                         <asp:Label ID="lblAnalysis" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_ANALYSIS") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Implementation Plan" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblPlanning" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_PLANNING") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Evaluation" >
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" CssClass="GridRow"  runat="server" Text='<%# Bind("HNCP_EVALUATION") %>'   ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                             

                        </asp:gridview>



                    </td>
                </tr>
            </table>
        </div>

        <div runat="server" visible="false" id="divHCEncounter">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Patient Encounter </span>
                    </td>
                </tr>
            </table>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounterAreaTop">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                             <div contenteditable="true" runat="server" visible="false" id="divHCEncounterArea">
                            Area of Encounter/Assessmen :  &nbsp; 
                      
                            <asp:label id="lblHCEncounterArea" runat="server" cssclass="lblCaption1"></asp:label>
                                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                            Arrival time to Area:  :  &nbsp; 
                      
                            <asp:label id="lblHCEncounterVisitDate" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                             <br />
                             <div contenteditable="true" runat="server" visible="false" id="divHCEncounterArrivalDate">
                            Date of visit :  &nbsp; 
                            <asp:label id="lblHCEncounterArrivalDate" runat="server" cssclass="lblCaption1"></asp:label>
                                 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                            Assessment done by :  &nbsp; 
                            <asp:label id="lblHCEncountertAsseDoneBy" runat="server" cssclass="lblCaption1"></asp:label>
                                 </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHCEncounterProvisionalDiag">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Provisional Diagnosis:  </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblHCEncounterProvisionalDiag" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounterCondition">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHCEncounterCondUpon">
                                Condition upon visit :  &nbsp;
                            <asp:label id="lblHCEncounterCondUponVisit" runat="server" cssclass="lblCaption1"></asp:label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:label id="lblHCEncounterCondOther" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <br />
                            <div contenteditable="true" runat="server" visible="false" id="divHCEncounterInfoObtFrom">
                                Information obtained from : &nbsp;
                                <asp:label id="lblHCEncounterInfoObtFrom" runat="server" cssclass="lblCaption1"></asp:label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:label id="lblHCEncounterInfoObtOther" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                        
                            <div contenteditable="true" runat="server" visible="false" id="divHCEncounterExpGiven">
                                Patient / Explanation given :&nbsp;
                                <asp:label id="lblHCEncounterExpGiven" runat="server" cssclass="lblCaption1"></asp:label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:label id="lblHCEncounterExpOther" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                        </td>
                    </tr>
                </table>
            </div>




            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Surgeries">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Present Complaint  </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPresentComplaient" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PastMedicHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">PastMedical History </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPastMedicalHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_SurgicalHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Surgical History </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblSurgicallHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PsychoSocEcoHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Psycho socio- economic History	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPsychoSocEcoHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_FamilyHist">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Family History	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblFamilyHistory" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PhysicalAssess">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Physical Assessment	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPhysicalAssessment" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_ReviewOfSys">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Review Of System	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblReviewOfSystem" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Investigations">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Investigations	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblInvestigations" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_PrincDiag">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Principal Diagnosis	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPrincipalDiagnosis" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_SecDiag">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Secondary  Diagnosis	 </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblSecondaryDiagnosis" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_TreatPlan">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Treatment Plan </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblTreatmentPlan" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Procedure">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Procedure </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblHCEncounter_Procedure" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_Treatment">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Treatment </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblTreatment" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divHCEncounter_FollowUpNotes">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">FollowUp Notes </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblFollowUpNotes" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div runat="server" visible="false" id="divHCAssessment">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Patient Assessment </span>
                    </td>
                </tr>
            </table>

            <div contenteditable="true" runat="server" visible="false" id="divHCAss_PresPastProb">

                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Present And / Or past Health Problems </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_PresPastHist">
                                Problems :<br />
                                <asp:label id="lblHC_ASS_Health_PresPastHist" runat="server" cssclass="lblCaption1"></asp:label>
                                <asp:label id="lblPresPastProblem" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_PresPastSurgeries">
                                <br />
                                Previous Hospitalizations :<br />
                                <asp:label id="lblPresPastSurgeries" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_Alergies">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Alergies  </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_AlergiesMedication">
                                Medication  :<br />
                                <asp:label id="lblAlergiesMedication" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_AlergiesFood">
                                <br />
                                Food  :<br />
                                <asp:label id="lblAlergiesFood" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_AlergiesOther">
                                <br />
                                Other :<br />
                                <asp:label id="lblAlergiesOther" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_MedicHist">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Medication History  </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_MedicHist_Medicine">
                                Medication  :<br />
                                <asp:label id="lblHC_ASS_Health_MedicHist_Medicine" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_MedicHist_Sleep">
                                <br />
                                Food  :<br />
                                <asp:label id="lblHC_ASS_Health_MedicHist_Sleep" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_MedicHist_PschoyAss">
                                <br />
                                Other :<br />
                                <asp:label id="lblHC_ASS_Health_MedicHist_PschoyAss" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHCAss_Health_MedicHist_PainAssYes">
                                <br />
                                Other :<br />
                                <asp:label id="lblPainAssYes" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Gast">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Gastrointestinal </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Gast_BowelSounds">
                                Bowel Sounds   :<br />
                                <asp:label id="lblHC_ASS_Phy_Gast_BowelSounds" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Gast_Elimination">
                                <br />
                                Elimination   :<br />
                                <asp:label id="lblHC_ASS_Phy_Gast_Elimination" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Gast_General">
                                <br />
                                General  :<br />
                                <asp:label id="lblHC_ASS_Phy_Gast_General" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Repro">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Reproductive </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Repro_Male">
                                Male   :<br />
                                <asp:label id="lblHC_ASS_Phy_Repro_Male" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Repro_Female">
                                <br />
                                FeMale   :<br />
                                <asp:label id="lblHC_ASS_Phy_Repro_Female" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Repro_Breasts">
                                <br />
                                Breasts   :<br />
                                <asp:label id="lblHC_ASS_Phy_Repro_Breasts" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Genit_General">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Genitourinary </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblHC_ASS_Phy_Genit_General" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Skin">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Skin/Integumentary </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Skin_Color">
                                Color   :<br />
                                <asp:label id="lblHC_ASS_Phy_Skin_Color" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Skin_Temperature">
                                <br />
                                Temperature    :<br />
                                <asp:label id="lblHC_ASS_Phy_Skin_Temperature" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Skin_Lesions">
                                <br />
                                Lesions   :<br />
                                <asp:label id="lblHC_ASS_Phy_Skin_Lesions" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Neuro">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Neurological </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Neuro_General">
                                General    :<br />
                                <asp:label id="lblHC_ASS_Phy_Neuro_General" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Neuro_Conscio">
                                <br />
                                Level of Consciousness     :<br />
                                <asp:label id="lblHC_ASS_Phy_Neuro_Conscio" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Neuro_Oriented">
                                <br />
                                Oriented to    :<br />
                                <asp:label id="lblHC_ASS_Phy_Neuro_Oriented" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Neuro_TimeResp">
                                <br />
                                Time Responsivenes    :<br />
                                <asp:label id="lblHC_ASS_Phy_Neuro_TimeResp" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Cardio">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Neurological </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Cardio_General">
                                General    :<br />
                                <asp:label id="lblHC_ASS_Phy_Cardio_General" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Cardio_Pulse">
                                <br />
                                Pulse  :<br />
                                <asp:label id="lblHC_ASS_Phy_Cardio_Pulse" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Cardio_PedalPulse">
                                <br />
                                Pedal Pulses     :<br />
                                <asp:label id="lblHC_ASS_Phy_Cardio_PedalPulse" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Cardio_Edema">
                                <br />
                                Edema    :<br />
                                <asp:label id="lblHC_ASS_Phy_Cardio_Edema" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Cardio_NailBeds">
                                <br />
                                Nail beds     :<br />
                                <asp:label id="lblHC_ASS_Phy_Cardio_NailBeds" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Cardio_Capillary">
                                <br />
                                Capillary refill     :<br />
                                <asp:label id="lblHC_ASS_Phy_Cardio_Capillary" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Ent">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">EENT & Mouth </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Ent_Eyes">
                                Eyes     :<br />
                                <asp:label id="lblHC_ASS_Phy_Ent_Eyes" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Ent_Ears">
                                <br />
                                Ears   :<br />
                                <asp:label id="lblHC_ASS_Phy_Ent_Ears" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Ent_Nose">
                                <br />
                                Nose     :<br />
                                <asp:label id="lblHC_ASS_Phy_Ent_Nose" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Ent_Throat">
                                <br />
                                Throat / Neck     :<br />
                                <asp:label id="lblHC_ASS_Phy_Ent_Throat" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Resp">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Respiratory </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Resp_Chest">
                                Chest Appearance      :<br />
                                <asp:label id="lblHC_ASS_Phy_Resp_Chest" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Resp_BreathPatt">
                                <br />
                                Breath Pattern    :<br />
                                <asp:label id="lblHC_ASS_Phy_Resp_BreathPatt" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Resp_BreathSound">
                                <br />
                                Breath Sound     :<br />
                                <asp:label id="lblHC_ASS_Phy_Resp_BreathSound" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Resp_BreathCough">
                                <br />
                                Breath Cough    :<br />
                                <asp:label id="lblHC_ASS_Phy_Resp_BreathCough" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Nutri">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Diet / Nutrition Assessment </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Nutri_Diet">
                                Diet    :<br />
                                <asp:label id="lblHC_ASS_Phy_Nutri_Diet" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Nutri_Appetite">
                                <br />
                                Appetite   :<br />
                                <asp:label id="lblHC_ASS_Phy_Nutri_Appetite" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Nutri_NutriSupport">
                                <br />
                                Nutritional Support    :<br />
                                <asp:label id="lblHC_ASS_Phy_Nutri_NutriSupport" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Nutri_FeedingDif">
                                <br />
                                Feeding difficulties:      :<br />
                                <asp:label id="lblHC_ASS_Phy_Nutri_FeedingDif" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Nutri_WeightStatus">
                                <br />
                                Weight status     :<br />
                                <asp:label id="lblHC_ASS_Phy_Nutri_WeightStatus" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Phy_Nutri_Diag">
                                <br />
                                By Diagnosis    :<br />
                                <asp:label id="lblHC_ASS_Phy_Nutri_Diag" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_SkinRisk">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Braden Skin Risk Assessment</td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_SkinRisk_Sensory">
                                Sensory perception     :<br />
                                <asp:label id="lblHC_ASS_Risk_SkinRisk_Sensory" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_SkinRisk_Moisture">
                                <br />
                                Moisture    :<br />
                                <asp:label id="lblHC_ASS_Risk_SkinRisk_Moisture" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_SkinRisk_Activity">
                                <br />
                                Activity    :<br />
                                <asp:label id="lblHC_ASS_Risk_SkinRisk_Activity" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_SkinRisk_Mobility">
                                <br />
                                Mobility :      :<br />
                                <asp:label id="lblHC_ASS_Risk_SkinRisk_Mobility" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_SkinRisk_Nutrition">
                                <br />
                                Nutrition      :<br />
                                <asp:label id="lblHC_ASS_Risk_SkinRisk_Nutrition" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_SkinRisk_Friction">
                                <br />
                                Friction / Shear     :<br />
                                <asp:label id="lblHC_ASS_Risk_SkinRisk_Friction" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_Fun">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Functional Assessment</td>
                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_Fun_SelfCaring">
                                Self Caring     :<br />
                                <asp:label id="lblHC_ASS_Risk_Fun_SelfCaring" runat="server" cssclass="lblCaption1"></asp:label>

                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_Fun_Musculos">
                                <br />
                                Musculoskeletal     :<br />
                                <asp:label id="lblHC_ASS_Risk_Fun_Musculos" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>
                            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_Fun_Equipment">
                                <br />
                                Use of Assisting Equipment     :<br />
                                <asp:label id="lblHC_ASS_Risk_Fun_Equipment" runat="server" cssclass="lblCaption1"></asp:label>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_Socio_Living">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Socioeconomic </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">Living Situation   :<br />
                            <asp:label id="lblHC_ASS_Risk_Socio_Living" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>

            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_Safety_General">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Fall/Safety Risk </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblHC_ASS_Risk_Safety_General" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>



            <div contenteditable="true" runat="server" visible="false" id="divHC_ASS_Risk_Edu_General">
                <br />
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Educational Assessment </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">Patient and / or family Need Eduction On   :<br />
                            <asp:label id="lblHC_ASS_Risk_Edu_General" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>

        </div>

        <div runat="server" visible="false" id="divPhyTherapy">
            <br />
            <table style="width: 100%" class="gridspacy">
                <tr>
                    <td style="background-image: url('<%= ResolveUrl("~/images/menubar-bg.jpg")%>');">
                        <span class="lblCaption1 BoldStyle" style="color: #fff;">Physical Therapy Evaluation </span>
                    </td>
                </tr>
            </table>
            <div contenteditable="true" runat="server" visible="false" id="lblPhyTherapyInj_Surg_Date">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Date Of Injury : 
                            <asp:label id="lblPhyTherapyInjuryDate" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             Date  Of Surgery : 
                            <asp:label id="lblPhyTherapySurgeryDate" runat="server" cssclass="lblCaption1"></asp:label>


                        </td>
                    </tr>
                </table>
            </div>

            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyChiefCoplaint">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Chief Complaint / Ailment / Injury</td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPhyTherapyChiefCoplaint" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyInjuryRemarks">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">How you were Injured</td>

                    </tr>
                    <tr>
                        <td class="lblCaption1  BoldStyle">
                            <asp:label id="lblPhyTherapyInjuryRemarks" runat="server" cssclass="lblCaption1"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyReceived">
                <table style="width: 100%" class="gridspacy">
                    <tr>
                        <td class="lblCaption1  BoldStyle">Received therapy for this condition :  &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            Date&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;:
                            <asp:label id="lblPhyTherapyReceivedDate" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            How many Visits :  &nbsp;
                            <asp:label id="lblPhyTherapyHowmany" runat="server" cssclass="lblCaption1"></asp:label>

                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyCondition">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">condition been getting&nbsp; 
                            :
                            <asp:label id="lblPhyTherapyCondition" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  
                            Symptoms :  &nbsp;
                            <asp:label id="lblPhyTherapySymptoms" runat="server" cssclass="lblCaption1"></asp:label>

                        </td>
                    </tr>
                </table>
            </div>
            <div contenteditable="true" runat="server" visible="false" id="divPhyTherapyBest_Worst">
                <table style="width: 100%" class="gridspacy">

                    <tr>
                        <td class="lblCaption1  BoldStyle">Number that best corresponds the pain : &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                            At Best&nbsp;
                            :
                            <asp:label id="lblPhyTherapyAtBest" runat="server" cssclass="lblCaption1"></asp:label>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;  
                            At Worst : &nbsp;
                            <asp:label id="lblPhyTherapyAtWorst" runat="server" cssclass="lblCaption1"></asp:label>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
