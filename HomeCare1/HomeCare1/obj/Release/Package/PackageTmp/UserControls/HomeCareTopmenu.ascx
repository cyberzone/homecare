﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeCareTopmenu.ascx.cs" Inherits="HomeCare1.UserControls.HomeCareTopmenu" %>
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />

<link href="Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">

    function LoadPage(strPage) {
        alert('test');
        window.top.frames[2].location.href = strPage;

        if (strPage == '../Default.aspx') {
            parent.location.href = strPage;
        }
    }
</script>

<table width="100%">
    <tr>
        <td >
            <div id='cssmenu'>

                <ul>
                     
                  
                    <li class='has-sub'><a href='#'><span>Home Care</span></a>
                        <ul>
                           
                             <li><a href='<%= ResolveUrl("~/HomeCare/HomeCareRegistration.aspx")%>'><span>Registration</span></a></li>
                             <li><a href='<%= ResolveUrl("~/HomeCare/WaitingList.aspx")%>'><span>Waiting List</span></a></li>
			                 <li><a href='<%= ResolveUrl("~/HomeCare/PatientEncounter.aspx")%>'><span>Patient Encounter</span></a></li>
                             <li><a href='<%= ResolveUrl("~/HomeCare/VitalSign.aspx")%>'><span>Vital Signs</span></a></li>
			                 <li><a href='<%= ResolveUrl("~/HomeCare/NurseCarePlan.aspx")%>'><span>Nursing Care Plan</span></a></li>
			                 <li><a href='<%= ResolveUrl("~/HomeCare/PhysicalTherapy.aspx")%>'><span>Physical Therapy Eval.</span></a></li>  
                             <li><a href='<%= ResolveUrl("~/HomeCare/PatientMonitoring.aspx")%>'><span>Patient Monitoring</span></a></li>
                             <li><a href='<%= ResolveUrl("~/HomeCare/TravelStatus.aspx")%>'><span>Travel Status</span></a></li> 
                             <li><a href='<%= ResolveUrl("~/HomeCare/CoordinatorStaffAssign.aspx")%>'><span>Coordinator Staff Assign</span></a></li> 


                            <li><a href='<%= ResolveUrl("~/Scans/Scans.aspx")%>'><span>Scans</span></a></li> 
                            <li><a href='<%= ResolveUrl("~/Scans/ScanList.aspx")%>'><span>Scaning </span></a></li> 
                            <li><a href='<%= ResolveUrl("~/Referral/Index.aspx")%>'><span>Referral </span></a></li> 
                              <li><a href='<%= ResolveUrl("~/HomeCare/FluidBalancechart.aspx")%>'><span>Fluid Balance chart </span></a></li> 
                        </ul>
                   
                </ul>
                <div style=" text-align: left;">
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:DropDownList ID="drpReport" style="font-size:11px" runat="server" CssClass="label" ></asp:DropDownList>
                        <asp:Button ID="btnPrint" runat="server"  style="width:50px;padding-left:5px;padding-right:5px;" CssClass="button orange small" Text="Print" OnClick="btnPrint_Click" />
                
                    <div style="float:right;padding-right:200px;">
                    <asp:LinkButton ID="lnkLogout" runat="server"  style="padding-left:5px;padding-right:5px;" PostBackUrl="~/Default.aspx" Height="22px" Width="50px" cssClass="button blue small" Text="Logout"></asp:LinkButton>
                    </div>
                 </div>
               
            </div>
             
        </td>
    </tr>
</table>
