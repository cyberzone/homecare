﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopMenu.ascx.cs" Inherits="HomeCare1.UserControls.TopMenu" %>

<link href="Styles/MenuStyle.css" rel="stylesheet" type="text/css" />


<table width="100%">
    <tr>
        <td >
            <div id='cssmenu'>

                <ul>
                     
                    <li class='has-sub'><a href='#'><span>Master</span></a>
                        <ul>
                            <li><a href='<%= ResolveUrl("~/Masters/ServiceMaster.aspx")%>'><span>Hospital Services</span></a></li>
                            <li><a href='<%= ResolveUrl("~/Masters/CompanyProfile.aspx")%>'><span>Company Profile</span></a></li>
                            <li><a href='<%= ResolveUrl("~/Masters/StaffProfile.aspx")%>'><span>Staff Profile</span></a></li>
                            <li><a href='<%= ResolveUrl("~/Masters/InitialNumber.aspx")%>'><span>Initial Number</span></a></li>
                             <li><a href='<%= ResolveUrl("~/Masters/CompanyPayerMaster.aspx")%>'><span>Company Payer ID</span></a></li>
                              <li><a href='<%= ResolveUrl("~/Masters/CommonMasters.aspx")%>'><span>Common Master</span></a></li>
                         
                        </ul>
                    </li>
                    <li class='has-sub'><a href='#'><span>Registration</span></a>
                        <ul>
                            <li><a href='<%= ResolveUrl("~/Registration/Patient_Registration.aspx")%>'><span>Patient Registration</span></a></li>
                            <li><a href='<%= ResolveUrl("~/Registration/CashPatientRegistration.aspx")%>'><span>Cash Registration</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/PatientFilingRequest.aspx")%>'><span>Filing Request</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/PatientWaitingList.aspx")%>'><span>Waiting List</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/PatientVisit.aspx")%>'><span>Patient Visit</span></a></li>
                             <li class='last'><a href='<%= ResolveUrl("~/Registration/AppointmentDoctor.aspx")%>'><span>Appointment Doctor</span></a></li>
                             <li class='last'><a href='<%= ResolveUrl("~/Registration/AppointmentService.aspx")%>'><span>Appointment Service</span></a></li>
                             <li class='last'><a href='<%= ResolveUrl("~/Registration/AppointmentDay.aspx")%>'><span>Appointment A Day</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/AppointmentWeek.aspx")%>'><span>Appointment A Week</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/PatientWaitingLIstDeleted.aspx")%>' style="visibility:visible"  ><span>Patient Visit Deleted List</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/PatientInformation.aspx")%>' style="visibility:visible"  ><span>Patient Information</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/InsuranceCardExpiry.aspx")%>' style="visibility:visible"  ><span>Insurance Card Expiry</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/PatientDrTransfer.aspx")%>' style="visibility:visible"  ><span>Patient Transferred List</span></a></li>

                        </ul>
                    </li>
                    <li class='has-sub'><a href='#'><span>Billing</span></a>
                        <ul>
                            

                            <li ><a href='<%= ResolveUrl("~/Billing/GenerateEClaim.aspx")%>'><span>Generate E-Claim</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/Billing/Remittance.aspx")%>'><span>Receipts Using XML</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/Billing/GenerateReSubmission.aspx")%>'><span>Generate Resubmission</span></a></li>

                            <li ><a href='<%= ResolveUrl("~/eAuthorization/eAuthApproval.aspx")%>'><span>eAuth Approval</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/eAuthorization/Authorization.aspx")%>'><span>Authorization</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/ePrescription/ePrescriptionApproval.aspx")%>'><span>ePrescription Approval</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/ePrescription/PBMePrescriptionApproval.aspx")%>'><span>PBM ePrescription</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/Billing/eClaimCoderVerification.aspx")%>'><span>Coder Verification</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/Billing/CoderEClaimGeneration.aspx")%>'><span>eClaim Submission(Verified)</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/Billing/CoderEClaimResubmission.aspx")%>'><span>eClaim ReSubmission(Verified)</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Billing/EMRInvoiceLandingPage.aspx?PageName=Invoice")%>'><span>Invoice</span></a></li> 
                             <li class='last'><a href='<%= ResolveUrl("~/Billing/Invoice.aspx")%>'><span>HMS Invoice</span></a></li> 

                        </ul>
                    </li>
                    <li class='has-sub last'><a href='#'><span>Report</span></a>
                          <ul>
                         
                            <li ><a href='<%= ResolveUrl("~/Billing/RemittanceReport.aspx")%>'><span>Receipts Report</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/Billing/ReSubmissionReport.aspx")%>'><span>Resubmission Report</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Registration/AppointmentReport.aspx")%>'><span>Appointment Report</span></a></li>

                            <li ><a href='<%= ResolveUrl("~/Billing/CoderReport.aspx?ReportName=Submission")%>'><span>Coder Submission Report</span></a></li>
                            <li ><a href='<%= ResolveUrl("~/Billing/CoderReport.aspx?ReportName=ReSubmission")%>'><span>Coder ReSubmission Report</span></a></li>
                        </ul>
                    </li>
                   <li class='has-sub last' style="display:none;"><a href='#'><span>Utility</span></a>
                          <ul>
                            <li class='last'><a href='<%= ResolveUrl("~/Utility/AccumedExport.aspx")%>'><span>Export Claim to View</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Utility/EMRIdUpdate.aspx")%>'><span>EMR Id Update</span></a></li> 
                            <li class='last'><a href='<%= ResolveUrl("~/Utility/AccumedReport.aspx")%>'><span>Accumed Report</span></a></li>
                            <li class='last'><a href='<%= ResolveUrl("~/Utility/HaadUtility.aspx")%>' style="display:none;"><span>Haad Data Upload</span></a></li>
                        </ul>
                    </li>
                       <li class='has-sub'><a href='<%= ResolveUrl("~/HomeCare/WaitingList.aspx")%>'><span>Home Care</span></a>
                     


                    </li>
                    <li>
                       
                    </li>
                    
                </ul>
            </div>
             
        </td>
    </tr>
</table>