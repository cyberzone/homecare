﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;


using HomeCare1.BAL;
using HomeCare1.DAL;


namespace HomeCare1.UserControls
{
    public partial class HomeCareTopmenu : System.Web.UI.UserControl
    {

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HomeCareLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindHCReports()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.GetHomeCareReports();

            drpReport.DataSource = DS;
            drpReport.DataTextField = "Name";
            drpReport.DataValueField = "Code";
            drpReport.DataBind();



        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    BindHCReports();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareTopmenu.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //if (Convert.ToString(Session["HomeCareId"]) == "" || Convert.ToString(Session["HomeCareId"]) == null)
            //{
            //    goto FunEnd;
            //}
            try
            {
                string strRptPath = drpReport.SelectedValue;
                string rptcall = @strRptPath + "?HomeCareId=" + Convert.ToString(Session["HomeCareId"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareTopmenu.btnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }
    }
}